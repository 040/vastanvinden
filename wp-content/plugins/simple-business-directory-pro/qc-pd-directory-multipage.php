<?php
defined('ABSPATH') or die("No direct script access!");

function sbd_show_category(){
	
	global $wp,$post;
	$current_url = home_url( $wp->request );
	$cterms = get_terms( 'pd_cat', array(
		'hide_empty' => true,
	) );
	
	if(sbd_get_option_page('sbd_directory_page')==get_option( 'page_on_front' )){
		$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));		
		/*
		if (strpos($current_url, $optionPage->post_name) === false) {
			$current_url = $current_url.'/'.$optionPage->post_name;
		}
		*/
		$current_url = home_url().'/'.$optionPage->post_name;
		
	}else{
		$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
		$current_url = home_url().'/'.$optionPage->post_name;
		
	}
	
	$temp_style = (pd_ot_get_option('pd_multipage_template')!=''?pd_ot_get_option('pd_multipage_template'):'style-10-multipage');
	
	if(!empty($cterms)){
		require QCSBD_DIR_CAT.'/'.$temp_style.'.php';
	}
	
		$customCss = pd_ot_get_option( 'pd_custom_style' );
		if( trim($customCss) != "" ) :
		?>
		<style type="text/css">
			<?php echo trim($customCss); ?>
		</style>
		<?php endif;
		
		$customjs = pd_ot_get_option( 'pd_custom_js' );
		if($customjs!=''):
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($)
		{
		<?php echo $customjs; ?>

		})
		</script>
		<?php
		endif;
	
	
}

add_shortcode('qcpd-directory-multipage', 'qcpd_directory_multipage_full_shortcode');
function qcpd_directory_multipage_full_shortcode( $atts = array() ){
	global $wp_query, $wp;
	ob_start();

	extract( shortcode_atts(
		array(
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'item_orderby' => "",
			'per_page' 	=> 10,
			'actual_pagination' => false
		), $atts
	));
	$actual_pagination = isset($atts['actual_pagination']) ? $atts['actual_pagination'] : 'false';
	$per_page = isset($atts['per_page']) ? $atts['per_page'] : 10;
	$sbd_fields_access = pd_ot_get_option('sbd_list_item_fields_show_only_loggedin_users');

	wp_enqueue_script( 'qcopd-google-map-script');
	wp_enqueue_script( 'qcopd-google-map-scriptasd');
	wp_enqueue_script( 'qcpd-custom-script');
?>

<?php
	$current_url = home_url( $wp->request );
	
	if(sbd_get_option_page('sbd_directory_page')==get_option( 'page_on_front' )){
		$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
		
		if (strpos($current_url, $optionPage->post_name) === false) {
			$current_url = $current_url.'/'.$optionPage->post_name;
		}
		
	}
	
	$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
	$current_url2 = home_url().'/'.$optionPage->post_name;
	
	$mapoption = (pd_ot_get_option('sbd_multipage_map')=='on'?'show':'hide');
	$temp_style = (pd_ot_get_option('pd_multipage_template')!=''?pd_ot_get_option('pd_multipage_template'):'style-10-multipage');
	
	$tag = (pd_ot_get_option('sbd_multi_tag')=='on'?' enable_tag_filter="true"':' enable_tag_filter="false"');
	
	$map_position = (pd_ot_get_option('sbd_multipage_map_position')!=''?pd_ot_get_option('sbd_multipage_map_position'):'top');
	
	$style_clm = (pd_ot_get_option('sbd_multipage_column')!=''?pd_ot_get_option('sbd_multipage_column'):3);
	
	$sbd_multi_cluster = (pd_ot_get_option('sbd_multi_cluster')=='on'?'true':'false');
	
	
    if((isset($wp_query->query_vars['sbditem']) && $wp_query->query_vars['sbditem']!='') or (isset($wp_query->query_vars['sbditemname']) && $wp_query->query_vars['sbditemname']!='')){
		
		
		
		$sbditem = $wp_query->query_vars['sbditem'];
		
		if($post = get_page_by_path( $wp_query->query_vars['sbdlist'], OBJECT, 'pd' )){
			
			$lists = get_post_meta( $post->ID, 'qcpd_list_item01' );
			$citem = '';
			foreach($lists as $k=>$list){
				
				if(!isset($wp_query->query_vars['sbditem'])):
					$sbditem = $wp_query->query_vars['sbditemname'];
					$list['qcpd_timelaps'] = str_replace(' ','-',strtolower($list['qcpd_item_title']));
				endif;
				$sbdtitleurl = $wp_query->query_vars['sbditemname'];
				$sbdtitle = str_replace(' ','-',strtolower($list['qcpd_item_title']));
				
				if(isset($list['qcpd_timelaps']) && $list['qcpd_timelaps']==$sbditem && urldecode($sbdtitleurl)== $sbdtitle){
					
					
					
					$citem = $k;
				?>
					<div class="pd_single_item_container">
						<div class="pd_single_breadcrumb">
							<ul class="pd_breadcrumb">
								<li><a href="<?php echo $current_url2; ?>"><?php echo get_the_title(); ?></a></li>
							  <?php if(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''): ?>
								<li><a href="<?php echo $current_url2.'/'.$wp_query->query_vars['sbdcat']; ?>"><?php echo str_replace('-',' ',ucfirst($wp_query->query_vars['sbdcat'])); ?></a></li>
							 <?php endif; ?>
							 
							 <?php if(isset($wp_query->query_vars['sbdlist']) && $wp_query->query_vars['sbdlist']!=''): ?>
								<li><a href="<?php echo $current_url2.'/'.$wp_query->query_vars['sbdcat'].'/'.$wp_query->query_vars['sbdlist']; ?>"><?php echo str_replace('-',' ',ucfirst($wp_query->query_vars['sbdlist'])); ?></a></li>
								<li class="pd_breadcrumb_last_child"><a href="#" disabled><?php echo $list['qcpd_item_title'] ?></a></li>
							<?php endif; ?>
							</ul>
							
							<div class="upvote-section upvote-section-style-single">
								<span data-post-id="<?php echo $post->ID; ?>" data-unique="<?php echo $post->ID.'_'.$list['qcpd_timelaps']; ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" class="pd-sbd-upvote-btn-single upvote-on">
									<i class="fa fa-thumbs-up"></i>
								</span>
								<span class="upvote-count count">
									<?php
									  if( isset($list['qcpd_upvote_count']) && (int)$list['qcpd_upvote_count'] > 0 ){
										echo (int)$list['qcpd_upvote_count'];
									  }
									?>
								</span>
							</div>
							
							
						</div>
						
						<div class="pd_single_content_multipage">
							
								<div class="feature-image">
								<?php
									$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

									$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

									$faviconImgUrl = "";
									$faviconFetchable = false;
									$filteredUrl = "";

									$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

									if( $showFavicon == 1 )
									{
										$filteredUrl = qcsbd_remove_http( $item_url );

										if( $item_url != '' )
										{

											$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
										}

										if( $directImgLink != '' )
										{

											$faviconImgUrl = trim($directImgLink);
										}

										$faviconFetchable = true;

										if( $item_url == '' && $directImgLink == '' ){
											$faviconFetchable = false;
										}
									}
									?>
								<!-- Image, If Present -->
									<?php if( isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>


										<?php 
											if (strpos($list['qcpd_item_img'], 'http') === FALSE){
										?>
										
											<?php
												$img = wp_get_attachment_image_src($list['qcpd_item_img'], 'medium');
												
												
											?>
											<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
										
										<?php
											}else{
										?>
										
											<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
										
										<?php
											}
										?>
										
										
									<?php elseif( $iconClass != "" ) : ?>

										<span class="icon fa-icon">
											<i class="fa <?php echo $iconClass; ?>"></i>
										</span>

									<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>

										<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">

									<?php else : ?>

										<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">

									<?php endif; ?>
								</div>
								
								
								<h2><?php echo $list['qcpd_item_title']; ?></h2>
								<p style="margin-bottom:15px;"><?php echo $list['qcpd_item_subtitle'] ?></p>
								<?php echo (isset($list['qcpd_description'])?apply_filters('the_content', $list['qcpd_description']):''); ?>
								<?php
									include(QCSBD_DIR_MOD.'/template-common/qc_custom_body_content.php');
									include(QCSBD_DIR_MOD.'/template-common/qc_custom_multipage_body_content.php');
								?>
								<div class="pd_bottom_desc">
								
									<?php
										if(isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''):
											if(pd_ot_get_option('sbd_lan_adress')!=''){
												$address_text = pd_ot_get_option('sbd_lan_adress');
											}else{
												$address_text = __('Address','qc-pd');
											}
									?>
										<div class="pd_business_hour">
											<p><span> <i class="fa fa-map-marker fa-map-marker-alt" aria-hidden="true"></i> <?php echo $address_text; ?>:</span> <?php echo $list['qcpd_item_full_address']; ?></p>
										</div>
									<?php endif; ?>
									
									<?php
									if(isset($list['qcpd_item_location']) && $list['qcpd_item_location']!=''):
										if(pd_ot_get_option('sbd_lan_locale_designation')!=''){
											$locale_text = pd_ot_get_option('sbd_lan_locale_designation');
										}else{
											$locale_text = __('Locale/Designation','qc-pd');
										}
									?>
										<div class="pd_business_hour">
											<p><span><i class="fa fa-location-arrow" aria-hidden="true"></i> <?php echo $locale_text; ?>:</span> <?php echo $list['qcpd_item_location']; ?></p>
										</div>
									<?php endif; ?>
									
									
									
									<?php
									if(isset($list['qcpd_item_business_hour']) and $list['qcpd_item_business_hour']!=''): 
										if(pd_ot_get_option('sbd_lan_business_hour')!=''){
											$businesstext = pd_ot_get_option('sbd_lan_business_hour');
										}else{
											$businesstext = __('Business Hours','qc-pd');
										}
									?>
										<div class="pd_business_hour">
											<p><span><i class="fa fa-clock-o fa-clock" aria-hidden="true"></i> <?php echo $businesstext; ?>:</span> <?php echo (isset($list['qcpd_item_business_hour']) && $list['qcpd_item_business_hour']!=''?trim($list['qcpd_item_business_hour']):''); ?></p>
										</div>
									<?php endif; ?>
									
									
									<?php
									if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('main_phone', $sbd_fields_access) ){
										if( is_user_logged_in() ){
											if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!=''):
												if(pd_ot_get_option('sbd_lan_phone')!=''){
													$phone_text = pd_ot_get_option('sbd_lan_phone');
												}else{
													$phone_text = __('Phone','qc-pd');
												}
									?>
												<div class="pd_business_hour">
													<p><span><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_text; ?>:</span> <a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>"><?php echo $list['qcpd_item_phone']; ?></a></p>
												</div>
								<?php
											endif;
										}
									}else{
										if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!=''):
											if(pd_ot_get_option('sbd_lan_phone')!=''){
												$phone_text = pd_ot_get_option('sbd_lan_phone');
											}else{
												$phone_text = __('Phone','qc-pd');
											}
									?>
											<div class="pd_business_hour">
												<p><span><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_text; ?>:</span> <a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>"><?php echo $list['qcpd_item_phone']; ?></a></p>
											</div>
								<?php
										endif;
									}
								?>
									
									<?php
									if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''):
										if(pd_ot_get_option('sbd_lan_email')!=''){
											$email_text = pd_ot_get_option('sbd_lan_email');
										}else{
											$email_text = __('Email','qc-pd');
										}
									?>
									<div class="pd_business_hour">
										<p><span><i class="fa fa-envelope-o fa-envelope" aria-hidden="true"></i> <?php echo $email_text; ?>:</span> <a href="#" data-email="<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>" class="sbd_email_form"><?php echo (pd_ot_get_option('sbd_lan_email_send_click')!=''?pd_ot_get_option('sbd_lan_email_send_click'):__('Click to Send Email', 'qc-pd')); ?></a></p>
									</div>
									<?php endif; ?>
									
									<div class="pd-bottom-area" style="text-align:left">
									<?php if($list['qcpd_item_facebook']!='' || $list['qcpd_item_yelp']!='' || $list['qcpd_item_linkedin']!='' || $list['qcpd_item_twitter']!=''): ?>
									<p style="font-weight:bold"><i class="fa fa-users" aria-hidden="true"></i> <?php echo (pd_ot_get_option('sbd_lan_social')!=''?pd_ot_get_option('sbd_lan_social'):__('Social', 'qc-pd')); ?>:</p>
									<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
									<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><i class="fa fa-facebook"></i></a></p>
									<?php endif; ?>
									<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
									<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><i class="fa fa-yelp"></i></a></p>
									<?php endif; ?>
									<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
									<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><i class="fa <?php echo (sbd_is_linkedin($list['qcpd_item_linkedin'])?'fa-linkedin-square fa-linkedin':'fa-instagram'); ?>"></i></a></p>
									<?php endif; ?>
									
									<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
									<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><i class="fa fa-twitter-square"></i></a></p>
									<?php endif; ?>
									<?php endif; ?>
									<?php
										include(QCSBD_DIR_MOD.'/template-common/qc_custom_icon_content.php');
										include(QCSBD_DIR_MOD.'/template-common/qc_custom_multipage_icon_content.php');
									?>
								</div>
									
								</div>
								
								
								<div class="pd_resource_action">
									<?php 
										if(pd_ot_get_option('sbd_lan_visit_link')!=''){
											$visitlink = pd_ot_get_option('sbd_lan_visit_link');
										}else{
											$visitlink = __('Visit This Link','qc-pd');
										}

									if($list['qcpd_item_link']!=''){
									?>
									<a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank" class="pd_single_button"><?php echo $visitlink; ?></a>
									<?php } ?>
									<nav class="pd-nav-socials">
										<h5 class="pd-social-title"><?php echo (pd_ot_get_option('sbd_lan_share')!=''?pd_ot_get_option('sbd_lan_share'):__('Share', 'qc-pd')); ?></h5>
										<ul>
											<li class="nav-socials__item">
												
												<a href="https://twitter.com/share?url=<?php echo $current_url; ?>/&amp;text=<?php echo urlencode($list['qcpd_item_title']); ?>" title="Twitter" target="_blank">
													<i class="fa fa-twitter"></i>
													
												</a>
											</li>
											<li class="nav-socials__item">
												
												<a href="https://facebook.com/sharer.php?u=<?php echo $current_url; ?>/&amp;t=<?php echo urlencode($list['qcpd_item_title']); ?>+<?php echo $current_url; ?>/" title="Facebook" target="_blank">
													<i class="fa fa-facebook"></i>
												</a>
											</li>
											<li class="nav-socials__item">
												
												<?php
													$param = '';
													if(isset($list['qcpd_item_img']) && $list['qcpd_item_img']!=''){
														$imgurlm = wp_get_attachment_image_src($list['qcpd_item_img'], 'medium');
														if(isset($imgurlm[0]) && $imgurlm[0]!=''){
															$param = '&amp;media='.$imgurlm[0];
														}
													}
												?>
												
												
												<a href="https://pinterest.com/pin/create/button/?url=<?php echo $current_url; ?>/<?php echo $param; ?>&amp;description=<?php echo urlencode($list['qcpd_item_title']); ?>" title="Pinterest" target="_blank">
													<i class="fa fa-pinterest-p"></i>
												</a>
											</li>
										</ul>
									</nav>									
									
								</div>
								
								<?php
									if(pd_ot_get_option('sbd_show_alexa_rank')=='on'):
									$parseurl = parse_url($list['qcpd_item_link']);
									$rankdata = sbd_alexaRank(@$parseurl['scheme'].'://'.@$parseurl['host']);
								?>
								<div class="pd_alexa_rank">
									<span><?php echo __('Alexa Global Rank','qc-pd'); ?> - <?php echo @$rankdata['globalRank'][0]; ?></span> <br> <span> <?php echo 'Alexa Country '.@$rankdata['CountryRank']['@attributes']['NAME']; ?> - <?php echo @$rankdata['CountryRank']['@attributes']['RANK']; ?></span>
								</div>
								<?php endif; ?>

						</div>
						<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!=''): ?>
						<div class="pd_map_container" id="pd_multipage_map"></div>
						<script>
							var address = '<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>';
							var custom_icon = '<?php echo (isset($list['qcpd_item_marker']) && $list['qcpd_item_marker']!=''?wp_get_attachment_image_src($list['qcpd_item_marker'])[0]:''); ?>';
							var geocoder;
							var map;
							var mapcontainer = 'pd_multipage_map';
							var toAddress = address;
							
							geocoder = new google.maps.Geocoder();
							var latlng = new google.maps.LatLng(40.84, 14.25);
							var myOptions = {
								zoom: <?php echo (pd_ot_get_option('sbd_map_zoom')!=''?pd_ot_get_option('sbd_map_zoom'):'13'); ?>,
								center: latlng,
								mapTypeControl: true,
								mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
								navigationControl: true,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							};
							
							
							map = new google.maps.Map(document.getElementById(mapcontainer), myOptions);
							
							
							  map.setCenter(new google.maps.LatLng(<?php echo (isset($list['qcpd_item_latitude']) && $list['qcpd_item_latitude']!=''?trim($list['qcpd_item_latitude']):''); ?>,<?php echo (isset($list['qcpd_item_longitude']) && $list['qcpd_item_longitude']!=''?trim($list['qcpd_item_longitude']):''); ?>));

								var infowindow = new google.maps.InfoWindow(
									{ content: '<b>'+toAddress+'</b>',
									  size: new google.maps.Size(150,50)
									});

								var marker = new google.maps.Marker({
									position: new google.maps.LatLng(<?php echo (isset($list['qcpd_item_latitude']) && $list['qcpd_item_latitude']!=''?trim($list['qcpd_item_latitude']):''); ?>,<?php echo (isset($list['qcpd_item_longitude']) && $list['qcpd_item_longitude']!=''?trim($list['qcpd_item_longitude']):''); ?>),
									map: map, 
									icon: custom_icon,
									title:toAddress
								}); 
								google.maps.event.addListener(marker, 'click', function() {
									infowindow.open(map,marker);
								});


						</script>
						<?php endif; ?>
						
					</div>
				<?php	
					do_action('qcpdr_single_item_review', $list, $post->ID);
				}
			}
			?>
			<?php if(count($lists)>3): ?>
				<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-multipage/style.css"; ?>" />
				<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-multipage/responsive.css"; ?>" />
				<div class="pd_single_related_content">
					<?php
						$it=0;
						$relatedArray = array();
						foreach($lists as $f=>$list){
							if($f>$citem && $it<3){
								$relatedArray[] = $list;
								$it++;
							}
						}
						if(count($relatedArray) < 3 and count($lists) > 3){
							for($rr=0;count($relatedArray)<3;$rr++){
								
								$relatedArray[] = $lists[$rr];
							}
						}
						if(pd_ot_get_option('sbd_lan_related_items')!=''){
							$relateditems = pd_ot_get_option('sbd_lan_related_items');
						}else{
							$relateditems = __('Related Items','qc-pd');
						}
						
					?>
					<h2><?php echo $relateditems; ?></h2>	
					<div class="qcld_pd_category_list" id="sbdopd-list-holder">
					
						<ul class="pd_single_related tooltip_tpl12-tpl sbd-list" id="jp-list-<?php echo $post->ID; ?>">
							<?php $count = 1; ?>
							<?php foreach( $relatedArray as $list ) : ?>
							<?php
								$canContentClass = "subtitle-present";

								if( !isset($list['qcpd_item_subtitle']) || $list['qcpd_item_subtitle'] == "" )
								{
									$canContentClass = "subtitle-absent";
								}
							?>
							
							<li id="item-<?php echo $post->ID ."-". $count; ?>" class="sbd-26">
								<?php
									$item_url = $list['qcpd_item_link'];
									$masked_url = $list['qcpd_item_link'];

									$mask_url = isset($mask_url) ? $mask_url : 'off';

									if( $mask_url == 'on' ){
										$masked_url = 'http://' . qcsbd_get_domain($list['qcpd_item_link']);
									}
								?>
								<div class="column-grid3">
									<div class="pd-main-content-area bg-color-0<?php echo (($count%5)+1); ?>">
										<div class="pd-main-panel">
											<div class="panel-title">
												<h3><?php
													echo trim($list['qcpd_item_title']);
												?></h3>
											</div>
											<div class="feature-image">
												<?php
													$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

													$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

													$faviconImgUrl = "";
													$faviconFetchable = false;
													$filteredUrl = "";

													$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

													if( $showFavicon == 1 )
													{
														$filteredUrl = qcsbd_remove_http( $item_url );

														if( $item_url != '' )
														{

															$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
														}

														if( $directImgLink != '' )
														{

															$faviconImgUrl = trim($directImgLink);
														}

														$faviconFetchable = true;

														if( $item_url == '' && $directImgLink == '' ){
															$faviconFetchable = false;
														}
													}
													?>
												<!-- Image, If Present -->
													<?php if(isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>

														<?php if (strpos($list['qcpd_item_img'], 'http') === FALSE){ ?>
															<?php
																$img = wp_get_attachment_image_src($list['qcpd_item_img']);
															?>
															<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
														<?php }else{ ?>
															<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
														<?php } ?>

													<?php elseif( $iconClass != "" ) : ?>

													<span class="icon fa-icon">
														<i class="fa <?php echo $iconClass; ?>"></i>
													</span>

													<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>


														<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">


													<?php else : ?>

														<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">

													<?php endif; ?>
											</div>
											
										</div>
										<div class="pd-hover-content">
											<div class="style-14-upvote-section" style="text-align: center;">
												

												<!-- upvote section -->
												<div class="upvote-section upvote-section-style-14 upvote upvote-icon">
													<span data-post-id="<?php echo $post->ID; ?>" data-unique="<?php echo $post->ID.'_'.$list['qcpd_timelaps']; ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" class="sbd-upvote-btn upvote-on">
														<i class="fa fa-thumbs-up"></i>
													</span>
													<span class="upvote-count count">
														<?php
														  if( isset($list['qcpd_upvote_count']) && (int)$list['qcpd_upvote_count'] > 0 ){
															echo (int)$list['qcpd_upvote_count'];
														  }
														?>
													</span>
												</div>
												<!-- /upvote section -->

											
											
											</div>
											<p><?php echo trim($list['qcpd_item_subtitle']); ?></p>
											<?php 
												if(pd_ot_get_option('sbd_lan_visit_page')!=''){
													$visit_page = pd_ot_get_option('sbd_lan_visit_page');
												}else{
													$visit_page = __('Visit Page','qc-pd');
												}
												
												$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
												$current_url = home_url().'/'.$optionPage->post_name;
											?>
											<div style="text-align: right;margin-right: 7px;">
											<a href="<?php echo $current_url.'/'.$wp_query->query_vars['sbdcat'].'/'.$wp_query->query_vars['sbdlist'].'/'.urlencode(str_replace(' ','-',strtolower(trim($list['qcpd_item_title'])))).'/'.$list['qcpd_timelaps']; ?>" ><?php echo $visit_page; ?></a>
											</div>
										<div class="pd-bottom-area">
									
											<?php if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!=''): ?>
												<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>"><i class="fa fa-phone"></i></a></p>
											<?php endif; ?>
											
											<?php if($list['qcpd_item_link']!=''): ?>
												<p><a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank"><i class="fa fa-link"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
											<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><i class="fa fa-facebook"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
											<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><i class="fa fa-yelp"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''): ?>
											<p><a href="mailto:<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>"><i class="fa fa-envelope"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
											<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><i class="fa <?php echo (sbd_is_linkedin($list['qcpd_item_linkedin'])?'fa-linkedin-square fa-linkedin':'fa-instagram'); ?>"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
											<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><i class="fa fa-twitter-square"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!=''): 
											$marker = '';
											if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
												$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
											}
											?>
											<p><a href="#" class="pd-map open-mpf-sld-link" full-address="<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>" data-mfp-src="#map-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $marker; ?>><i class="fa fa-map"></i></a></p>
											<?php endif; ?>
											
											<?php if(isset($list['qcpd_item_business_hour']) and $list['qcpd_item_business_hour']!=''): ?>
											<p><a href="#" class="open-mpf-sld-link" data-mfp-src="#busi-<?php echo get_the_ID() ."-". $count; ?>"><i class="fa fa-info-circle"></i></a></p>
											<?php endif; ?>
										
										</div>
										
										
										<div id="map-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
											<div class="pd_map_container" id="mapcontainer-<?php echo get_the_ID() ."-". $count; ?>"></div>
										</div>
							
										<div id="busi-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
											<div class="pd_business_container">
												<h2>Business Hours</h2>
												<p><?php echo (isset($list['qcpd_item_business_hour']) && $list['qcpd_item_business_hour']!=''?trim($list['qcpd_item_business_hour']):''); ?></p>
											</div>
										</div>
										</div>
									</div>
								</div>
							</li>

							<?php $count++; endforeach; ?>

						</ul>
					</div>					
				</div>
			<?php endif; ?>
			<?php
		}
		

		$customCss = pd_ot_get_option( 'pd_custom_style' );
		if( trim($customCss) != "" ) :
		?>
		<style type="text/css">
			<?php echo trim($customCss); ?>
		</style>
		<?php endif;
		
		$customjs = pd_ot_get_option( 'pd_custom_js' );
		if($customjs!=''):
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($)
		{
		<?php echo $customjs; ?>

		})
		</script>
		<?php
		endif;
		
	
	}
	elseif(isset($wp_query->query_vars['sbdlist']) && $wp_query->query_vars['sbdlist']!=''){
		
		
		if ( $post = get_page_by_path( $wp_query->query_vars['sbdlist'], OBJECT, 'pd' ) )
			$id = $post->ID;
		else
			$id = 0;
		
		if($id>0){
			?>
				<div class="pd_single_breadcrumb">
					<ul class="pd_breadcrumb">
						<li><a href="<?php echo $current_url2; ?>"><?php echo get_the_title(); ?></a></li>
					  <?php if(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''): ?>
						<li><a href="<?php echo $current_url2.'/'.$wp_query->query_vars['sbdcat']; ?>"><?php echo str_replace('-',' ',ucfirst($wp_query->query_vars['sbdcat'])); ?></a></li>
					 <?php endif; ?>
					 
					 <?php if(isset($wp_query->query_vars['sbdlist']) && $wp_query->query_vars['sbdlist']!=''): ?>
						<li class="pd_breadcrumb_last_child"><a href="#"><?php echo str_replace('-',' ',ucfirst($wp_query->query_vars['sbdlist'])); ?></a></li>
					<?php endif; ?>
					</ul>
				</div>
			<?php
			echo do_shortcode('[qcpd-directory mode="one" list_id="'.$id.'" actual_pagination="'.$actual_pagination.'" per_page="'.$per_page.'" style="'.$temp_style.'" column="'.$style_clm.'" upvote="on" search="true" item_count="on" orderby="'.$orderby.'" filterorderby="date" order="'.$order.'" filterorder="ASC" paginate_items="false" favorite="disable" tooltip="true" list_title_font_size="" item_orderby="'.$item_orderby.'" list_title_line_height="" title_font_size="" subtitle_font_size="" title_line_height="" subtitle_line_height="" filter_area="normal" topspacing="" marker_cluster="'.$sbd_multi_cluster.'" map_position="'.$map_position.'" map="'.$mapoption.'" multipage="true" '.$tag.']');
		}
		
	}elseif(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''){
		?>
		<div class="pd_single_breadcrumb">
			<ul class="pd_breadcrumb">
				<li><a href="<?php echo the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
			  <?php if(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''): ?>
				<li class="pd_breadcrumb_last_child"><a href="#"><?php echo str_replace('-',' ',ucfirst($wp_query->query_vars['sbdcat'])); ?></a></li>
			 <?php endif; ?>
			 
			 
			</ul>
		</div>
		<?php
		
		echo do_shortcode('[qcpd-directory category="'.$wp_query->query_vars['sbdcat'].'" style="'.$temp_style.'" column="'.$style_clm.'" upvote="on" search="true" item_count="on" orderby="'.$orderby.'" filterorderby="date" order="'.$order.'" filterorder="ASC" paginate_items="false" favorite="disable" tooltip="false" list_title_font_size="" item_orderby="'.$item_orderby.'" list_title_line_height="" title_font_size="" subtitle_font_size="" title_line_height="" subtitle_line_height="" filter_area="normal" topspacing="" marker_cluster="'.$sbd_multi_cluster.'" map_position="'.$map_position.'" map="'.$mapoption.'" multipage="true" '.$tag.' actual_pagination="'.$actual_pagination.'" per_page="'.$per_page.'"] ');

	}else{
		sbd_show_category();
	}
	

    $content = ob_get_clean();
    return $content;
}


add_action('init', 'sbd_title_override');

function sbd_title_override(){
	add_filter('pre_get_document_title', 'sbd_wp_title_for_multipage', 999);
}


function sbd_wp_title_for_multipage( $title )
{
	global $wp_query, $wp;
	
	if((isset($wp_query->query_vars['sbditem']) && $wp_query->query_vars['sbditem']!='') or (isset($wp_query->query_vars['sbditemname']) && $wp_query->query_vars['sbditemname']!='')){
		
		if($post = get_page_by_path( $wp_query->query_vars['sbdlist'], OBJECT, 'pd' )){
			$lists = get_post_meta( $post->ID, 'qcpd_list_item01' );
			$sbditem = $wp_query->query_vars['sbditem'];
			foreach($lists as $list){
				if(!isset($wp_query->query_vars['sbditem'])):
					$sbditem = $wp_query->query_vars['sbditemname'];
					$list['qcpd_timelaps'] = str_replace(' ','-',strtolower($list['qcpd_item_title']));
				endif;
				if(isset($list['qcpd_timelaps']) && $list['qcpd_timelaps']==$sbditem){
					$title = trim($list['qcpd_item_title']).' | '.get_bloginfo( 'name' ); 
				}
			}
		}
	}
	elseif(isset($wp_query->query_vars['sbdlist']) && $wp_query->query_vars['sbdlist']!=''){
		
		$title = str_replace('-',' ',ucfirst($wp_query->query_vars['sbdlist'])).' | '.get_bloginfo( 'name' );
		
	}elseif(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''){
		$title = str_replace('-',' ',ucfirst($wp_query->query_vars['sbdcat'])).' | '.get_bloginfo( 'name' ); 
	}

   /*  
    $title['page'] = '2'; // optional
    $title['tagline'] = 'Home Of Genesis Themes'; // optional
    $title['site'] = 'DevelopersQ'; //optional
    */
	
    return $title; 
}


add_action('wp_head', 'sbdmyCallbackToAddMeta', 100);
function sbdmyCallbackToAddMeta(){
	
	global $wp_query, $wp;
	$page_permalink = get_permalink();
	
	if((isset($wp_query->query_vars['sbditem']) && $wp_query->query_vars['sbditem']!='') or (isset($wp_query->query_vars['sbditemname']) && $wp_query->query_vars['sbditemname']!='')){
		
		if($post = get_page_by_path( $wp_query->query_vars['sbdlist'], OBJECT, 'pd' )){
			$lists = get_post_meta( $post->ID, 'qcpd_list_item01' );
			$sbditem = $wp_query->query_vars['sbditem'];
			foreach($lists as $list){
				$item_id = $list['qcpd_timelaps'];
				if(!isset($wp_query->query_vars['sbditem'])):
					$sbditem = $wp_query->query_vars['sbditemname'];
					$list['qcpd_timelaps'] = str_replace(' ','-',strtolower($list['qcpd_item_title']));
				endif;
				if(isset($list['qcpd_timelaps']) && $list['qcpd_timelaps']==$sbditem){
					
					$title = trim($list['qcpd_item_title']).' | '.get_bloginfo( 'name' ); 
					$description = trim($list['qcpd_item_subtitle']);
					echo "<meta name='description' content='".$description."'>\n";
					$itemImg = '';
					if(isset($list['qcpd_item_img']) && $list['qcpd_item_img']!=''){
						if (strpos($list['qcpd_item_img'], 'http') === FALSE){
							$img = wp_get_attachment_image_src($list['qcpd_item_img']);
							$itemImg = $img[0];
						}else{
							$itemImg = $list['qcpd_item_img'];
						}
					}elseif(isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != ""){
						$itemImg = $list['qcpd_item_img_link'];
					}else{
						$itemImg = QCSBD_IMG_URL.'/list-image-placeholder.png';
					}
					
					$canonical_permalink = $page_permalink.$wp_query->query_vars['sbdcat'].'/'.$wp_query->query_vars['sbdlist'].'/'.urlencode(str_replace(' ','-',strtolower($list['qcpd_item_title']))).'/'.$item_id;
					// echo '<link rel="canonical" href="'.home_url( $wp->request ).'" />'."\n";
					echo '<link rel="canonical" href="'.$canonical_permalink.'" />'."\n";
					echo '<meta property="og:title" content="'.$list['qcpd_item_title'].'" />'."\n";
					echo '<meta property="og:description" content="'.$description.'" />'."\n";
					echo '<meta property="og:image" content="'.$itemImg.'" />'."\n";

?>

 <script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "LocalBusiness",
  "name": "<?php echo $list['qcpd_item_title']; ?>",
  "url": "<?php echo $list['qcpd_item_link']; ?>",
  "logo": "<?php echo $img[0]; ?>",
  "image": "<?php echo $img[0]; ?>",
  "description": "<?php echo $list['qcpd_item_subtitle']; ?>",
  "address": {
	"@type": "PostalAddress",
	"streetAddress": "<?php echo $list['qcpd_item_full_address'] ?>",
	"addressLocality": "<?php echo $list['qcpd_item_location']; ?>"
  },
  "geo": {
	"@type": "GeoCoordinates",
	"latitude": "<?php echo $list['qcpd_item_latitude'] ?>",
	"longitude": "<?php echo $list['qcpd_item_longitude'] ?>"
  },
  "hasMap": "https://www.google.com/maps/search/?api=1&query=<?php echo $list['qcpd_item_latitude'] ?>,<?php echo $list['qcpd_item_longitude'] ?>",
  "openingHours": "<?php echo $list['qcpd_item_business_hour']; ?>",
  "contactPoint": {
	"@type": "ContactPoint",
	"telephone": "<?php echo $list['qcpd_item_phone']; ?>",
	"contactType": "customer support"
  }
}
 </script>

 
<?php					
					
				}
			}
		}
		
	}elseif(isset($wp_query->query_vars['sbdlist']) && $wp_query->query_vars['sbdlist']!=''){
		
		$post = get_term_by('slug', $wp_query->query_vars['sbdcat'], 'pd_cat');
		if(!empty($post)){
			echo "<meta name='description' content='".str_replace('-',' ',ucfirst($wp_query->query_vars['sbdlist']))." - ".$post->description."'>\n";
		}
		
	}elseif(isset($wp_query->query_vars['sbdcat']) && $wp_query->query_vars['sbdcat']!=''){
		
		$post = get_term_by('slug', $wp_query->query_vars['sbdcat'], 'pd_cat');
		if(!empty($post)){
			echo "<meta name='description' content='".$post->description."'>\n";
		}
		
	}
  
}





function sbd_custom_rewrite_tag() {
  add_rewrite_tag('%sbdcat%', '([^&]+)');
  add_rewrite_tag('%sbdlist%', '([^&]+)');
  add_rewrite_tag('%sbditemname%', '([^&]+)');
  add_rewrite_tag('%sbditem%', '([^&]+)');
}
add_action('init', 'sbd_custom_rewrite_tag', 10, 0);



function sbd_custom_rewrite_rule4() {
	if(pd_ot_get_option('sbd_enable_multipage')=='on'){
		$optionPageId = sbd_get_option_page('sbd_directory_page');
		
		if($optionPageId==''){
			$findid = qcsbd_get_id_by_shortcode('qcpd-directory-multipage');
			if($findid!=''){
				update_option( 'sbd_directory_page', $findid );
				$optionPageId = $findid;
			}
			
		}
		
		if($optionPageId!=''){
			$optionPage = get_page($optionPageId);

			$post_name = isset($optionPage->post_name) ? $optionPage->post_name : '';

			if(!empty($post_name))
				add_rewrite_rule('^'.$optionPage->post_name.'/([^/]*)/([^/]*)/([^/]*)/([^/]*)/?','index.php?pagename='.$optionPage->post_name.'&sbdcat=$matches[1]&sbdlist=$matches[2]&sbditemname=$matches[3]&sbditem=$matches[4]','top');

		}
	}

}
add_action('init', 'sbd_custom_rewrite_rule4', 10, 0);

function sbd_custom_rewrite_rule3() {
	if(pd_ot_get_option('sbd_enable_multipage')=='on'){
		$optionPageId = sbd_get_option_page('sbd_directory_page');
		
		if($optionPageId==''){
			$findid = qcsbd_get_id_by_shortcode('qcpd-directory-multipage');
			if($findid!=''){
				update_option( 'sbd_directory_page', $findid );
				$optionPageId = $findid;
			}
			
		}
		
		if($optionPageId!=''){
			$optionPage = get_page($optionPageId);

			$post_name = isset($optionPage->post_name) ? $optionPage->post_name : '';

			if(!empty($post_name))
				add_rewrite_rule('^'.$optionPage->post_name.'/([^/]*)/([^/]*)/([^/]*)/?','index.php?pagename='.$optionPage->post_name.'&sbdcat=$matches[1]&sbdlist=$matches[2]&sbditemname=$matches[3]','top');

		}
	}

}
add_action('init', 'sbd_custom_rewrite_rule3', 10, 0);

function sbd_custom_rewrite_rule() {
	if(pd_ot_get_option('sbd_enable_multipage')=='on'){
		
		$optionPageId = sbd_get_option_page('sbd_directory_page');
		
		if($optionPageId==''){
			$findid = qcsbd_get_id_by_shortcode('qcpd-directory-multipage');
			if($findid!=''){
				update_option( 'sbd_directory_page', $findid );
				$optionPageId = $findid;
			}
			
		}
		
		if($optionPageId!=''){
			$optionPage = get_page($optionPageId);

			$post_name = isset($optionPage->post_name) ? $optionPage->post_name : '';

			if(!empty($post_name))
				add_rewrite_rule('^'.$optionPage->post_name.'/([^/]*)/([^/]*)/?','index.php?pagename='.$optionPage->post_name.'&sbdcat=$matches[1]&sbdlist=$matches[2]','top');

		}
		
	}

}
add_action('init', 'sbd_custom_rewrite_rule', 10, 0);


function sbd_custom_rewrite_rule1() {
	
	if(pd_ot_get_option('sbd_enable_multipage')=='on'){
	
		$optionPageId = sbd_get_option_page('sbd_directory_page');
		
		if($optionPageId==''){
			$findid = qcsbd_get_id_by_shortcode('qcpd-directory-multipage');
			if($findid!=''){
				update_option( 'sbd_directory_page', $findid );
				$optionPageId = $findid;
			}
			
		}
		
		if($optionPageId!=''){
			$optionPage = get_page($optionPageId);

			$post_name = isset($optionPage->post_name) ? $optionPage->post_name : '';

			if(!empty($post_name))
				add_rewrite_rule('^'.$optionPage->post_name.'/([^/]*)/?','index.php?pagename='.$optionPage->post_name.'&sbdcat=$matches[1]','top');
			
			
		}
	}

}
add_action('init', 'sbd_custom_rewrite_rule1', 10, 0);

function sbd_184163_disable_canonical_front_page( $redirect ) {
    if ( is_page() && $front_page = get_option( 'page_on_front' ) ) {
        if ( is_page( $front_page ) )
            $redirect = false;
    }

    return $redirect;
}
add_filter( 'redirect_canonical', 'sbd_184163_disable_canonical_front_page' );
/**
 * Detect shortcodes and update the plugin options
 * @param post_id of an updated post
 */
function sbd_multipage_get_pages_with_shortcodes($post_ID){
	
	$post = get_post( $post_ID );
	if ( has_shortcode( $post->post_content, 'qcpd-directory-multipage' ) ) {
		update_option( 'sbd_directory_page', $post->ID );
	}
}
add_action( 'wp_insert_post', 'sbd_multipage_get_pages_with_shortcodes', 1);

function sbd_get_option_page($page, $param = false) {
	return get_option($page, $param);
}

add_filter('get_canonical_url', 'sbd_get_canonical_url', 10, 2);
function sbd_get_canonical_url( $canonical_url, $post ){
	if ( has_shortcode( $post->post_content, 'qcpd-directory-multipage' ) ) {
		$canonical_url = '';
	}
	return $canonical_url;
}