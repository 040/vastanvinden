<?php 
wp_head();
add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
?>
<link rel="stylesheet" type="text/css" href="<?php echo QCSBD_ASSETS_URL . "/css/directory-style.css"; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo QCSBD_URL . "/embed/css/embed-form.css"; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo QCSBD_ASSETS_URL . "/css/directory-style-rwd.css"; ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo QCSBD_ASSETS_URL . "/css/magnific-popup.css"; ?>"/>

<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php', is_ssl() ? 'https' : 'http' ); ?>';
    var qc_sbd_get_ajax_nonce = "<?php echo wp_create_nonce( 'qc-pd'); ?>";
</script>

<style>
    .pd-add .pd-add-btn {
        display: none;
    !important;
    }
	.slick-slide{height:unset !important;}
</style>
<?php 
$mapapi = 'AIzaSyBACyZ4vA8pQybj9ZdZP-J5zQHqfQkqOXY';
if(pd_ot_get_option('pd_map_api_key')!=''){
	$mapapi = pd_ot_get_option('pd_map_api_key');
}
?>
<script src="<?php echo QCSBD_URL . "/embed/js/jquery-1.11.3.js"; ?>"></script>
<script src="<?php echo QCSBD_ASSETS_URL . "/js/packery.pkgd.js"; ?>"></script>
<script src="<?php echo QCSBD_ASSETS_URL . "/js/tooltipster.bundle.min.js"; ?>"></script>
<script src="<?php echo QCSBD_URL . "/embed/js/embed-form.js"; ?>"></script>
<script src="<?php echo QCSBD_ASSETS_URL . "/js/jquery.magnific-popup.min.js"; ?>"></script>

<script src="<?php echo QCSBD_ASSETS_URL . "/js/directory-script.js"; ?>"></script>
<script src="<?php echo QCSBD_ASSETS_URL . "/js/slick.min.js"; ?>"></script>


<?php

$orderby = sanitize_text_field($_GET['orderby']);
$order = sanitize_text_field($_GET['order']);
$mode = sanitize_text_field($_GET['mode']);
$column = sanitize_text_field($_GET['column']);
$style = sanitize_text_field($_GET['style']);
$search = sanitize_text_field($_GET['search']);
$category = sanitize_text_field($_GET['category']);
$upvote = sanitize_text_field($_GET['upvote']);
$tooltip = sanitize_text_field($_GET['tooltip']);
$list_id = sanitize_text_field($_GET['list_id']);
$map = sanitize_text_field($_GET['map']);
$showmaponly = sanitize_text_field($_GET['showmaponly']);

$enable_tag_filter  = sanitize_text_field($_GET['enable_tag_filter']);
$distance_search = sanitize_text_field($_GET['distance_search']);
$marker_cluster = sanitize_text_field($_GET['marker_cluster']);
$image_infowindow = sanitize_text_field($_GET['image_infowindow']);
$item_count = sanitize_text_field($_GET['item_count']);
$item_details_page = sanitize_text_field($_GET['item_details_page']);
$filterorderby = sanitize_text_field($_GET['filterorderby']);
$main_click_action = sanitize_text_field($_GET['main_click_action']);
$phone_number = sanitize_text_field($_GET['phone_number']);
$paginate_items = sanitize_text_field($_GET['paginate_items']);
$per_page = sanitize_text_field($_GET['per_page']);
$enable_rtl = sanitize_text_field($_GET['enable_rtl']);
$enable_left_filter = sanitize_text_field($_GET['enable_left_filter']);
$enable_tag_filter_dropdown = sanitize_text_field($_GET['enable_tag_filter_dropdown']);
$enable_map_fullwidth = sanitize_text_field($_GET['enable_map_fullwidth']);
$enable_map_fullscreen = sanitize_text_field($_GET['enable_map_fullscreen']);
$enable_map_toggle_filter = sanitize_text_field($_GET['enable_map_toggle_filter']);
$map_position = sanitize_text_field($_GET['map_position']);
$list_title_font_size = sanitize_text_field($_GET['list_title_font_size']);
$item_orderby = sanitize_text_field($_GET['item_orderby']);
$list_title_line_height = sanitize_text_field($_GET['list_title_line_height']);
$title_font_size = sanitize_text_field($_GET['title_font_size']);
$review = sanitize_text_field($_GET['review']);
$subtitle_font_size = sanitize_text_field($_GET['subtitle_font_size']);
$title_line_height = sanitize_text_field($_GET['title_line_height']);
$subtitle_line_height = sanitize_text_field($_GET['subtitle_line_height']);
$pdmapofflightbox = sanitize_text_field($_GET['pdmapofflightbox']);
$filter_area = sanitize_text_field($_GET['filter_area']);
$topspacing = sanitize_text_field($_GET['topspacing']);

echo '<div class="clear">';


echo do_shortcode('[qcpd-directory mode="' . $mode . '" list_id="' . $list_id . '" style="' . $style . '" tooltip="' . $tooltip . '" column="' . $column . '" search="' . $search . '" map="' . $map . '" showmaponly="' . $showmaponly . '" category="' . $category . '" upvote="' . $upvote . '" item_count="on" orderby="' .$orderby. '" order="' . $order . '" enable_tag_filter="'.$enable_tag_filter.'" distance_search = "'.$distance_search.'" marker_cluster = "'.$marker_cluster.'" image_infowindow = "'.$image_infowindow.'" item_count = "'.$item_count.'" item_details_page = "'.$item_details_page.'" filterorderby = "'.$filterorderby.'" main_click_action = "'.$main_click_action.'" phone_number = "'.$phone_number.'" paginate_items = "'.$paginate_items.'" per_page = "'.$per_page.'" enable_rtl = "'.$enable_rtl.'" enable_left_filter = "'.$enable_left_filter.'" enable_tag_filter_dropdown = "'.$enable_tag_filter_dropdown.'" enable_map_fullwidth = "'.$enable_map_fullwidth.'" enable_map_fullscreen = "'.$enable_map_fullscreen.'" enable_map_toggle_filter = "'.$enable_map_toggle_filter.'" map_position = "'.$map_position.'" list_title_font_size = "'.$list_title_font_size.'" item_orderby = "'.$item_orderby.'" list_title_line_height = "'.$list_title_line_height.'" title_font_size = "'.$title_font_size.'" review = "'.$review.'" subtitle_font_size = "'.$subtitle_font_size.'" title_line_height = "'.$title_line_height.'" subtitle_line_height = "'.$subtitle_line_height.'" pdmapofflightbox = "'.$pdmapofflightbox.'" filter_area = "'.$filter_area.'" topspacing = "'.$topspacing.'" ]');

echo '</div>'; 

?>
<?php 
wp_footer();
?>





