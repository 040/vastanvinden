<?php
// Load template for embed link page url
function qcpd_load_embed_link_template($template)
{
    if (is_page('embed-link')) {
        return dirname(__FILE__) . '/qcpd-embed-link.php';
    }
    return $template;
}

add_filter('template_include', 'qcpd_load_embed_link_template', 99);


// Create embed page when plugin install or activate

//register_activation_hook(__FILE__, 'qcpd_create_embed_page');
add_action('init', 'qcpd_create_embed_page');
function qcpd_create_embed_page()
{

    if (get_page_by_title('Embed Link') == NULL) {
        //post status and options
        $post = array(
            'comment_status' => 'closed',
            'ping_status' => 'closed',
            'post_author' => get_current_user_id(),
            'post_date' => date('Y-m-d H:i:s'),
            'post_status' => 'publish',
            'post_title' => 'Embed Link',
            'post_type' => 'page',
        );
        //insert page and save the id
        $embedPost = wp_insert_post($post, false);
        //save the id in the database
        update_option('hclpage', $embedPost);
    }
}


    

add_action('qcpd_after_add_btn', 'qcpd_custom_embedder');
function qcpd_custom_embedder($shortcodeAtts)
{
	$embed_link_button = pd_ot_get_option('pd_enable_embed_list');
	if ($embed_link_button == 'on') {
    global $post;
	
	$site_title = get_bloginfo('title');
	$site_link = get_bloginfo('url');

	if( pd_ot_get_option( 'pd_embed_credit_title' ) != "" ){
		$site_title = pd_ot_get_option( 'pd_embed_credit_title' );
	}

	if( pd_ot_get_option( 'pd_embed_credit_link' ) != "" ){
		$site_link = pd_ot_get_option( 'pd_embed_credit_link' );
	}

    $pagename = $post->post_name;

    if ($pagename != 'embed-link') {
        ?>

		<!-- Generate Embed Code -->

        <a class="button-link js-open-modal cls-embed-btn" href="#" data-modal-id="popup"
           data-url="<?php bloginfo('url'); ?>/embed-link"
           data-orderby="<?php echo $shortcodeAtts['orderby']; ?>"
           data-order="<?php echo $shortcodeAtts['order']; ?>"
           data-mode="<?php echo $shortcodeAtts['mode']; ?>"
           data-list-id="<?php echo $shortcodeAtts['list_id']; ?>"
           data-column="<?php echo $shortcodeAtts['column']; ?>"
           data-style="<?php echo $shortcodeAtts['style']; ?>"
           data-search="<?php echo $shortcodeAtts['search']; ?>"
           data-category="<?php echo $shortcodeAtts['category']; ?>"
           
           data-enable_tag_filter="<?php echo $shortcodeAtts['enable_tag_filter']; //Start missing ?>" 
           data-distance_search="<?php echo $shortcodeAtts['distance_search']; ?>"
           data-marker_cluster="<?php echo $shortcodeAtts['marker_cluster']; ?>"
           data-image_infowindow="<?php echo $shortcodeAtts['image_infowindow']; ?>"
           data-item_count="<?php echo $shortcodeAtts['item_count']; ?>"
           data-item_details_page="<?php echo $shortcodeAtts['item_details_page']; ?>"
           data-filterorderby="<?php echo $shortcodeAtts['filterorderby']; ?>"
           data-main_click_action="<?php echo $shortcodeAtts['main_click_action']; ?>"
           data-phone_number="<?php echo $shortcodeAtts['phone_number']; ?>"
           data-paginate_items="<?php echo $shortcodeAtts['paginate_items']; ?>"
           data-per_page="<?php echo $shortcodeAtts['per_page']; ?>"
           data-enable_rtl="<?php echo $shortcodeAtts['enable_rtl']; ?>"
           data-enable_left_filter="<?php echo $shortcodeAtts['enable_left_filter']; ?>"
           data-enable_tag_filter_dropdown="<?php echo $shortcodeAtts['enable_tag_filter_dropdown']; ?>"
           data-enable_map_fullwidth="<?php echo $shortcodeAtts['enable_map_fullwidth']; ?>"
           data-enable_map_fullscreen="<?php echo $shortcodeAtts['enable_map_fullscreen']; ?>"
           data-enable_map_toggle_filter="<?php echo $shortcodeAtts['enable_map_toggle_filter']; ?>"
           data-map_position="<?php echo $shortcodeAtts['map_position']; ?>"
           data-list_title_font_size="<?php echo $shortcodeAtts['list_title_font_size']; ?>"
           data-item_orderby="<?php echo $shortcodeAtts['item_orderby']; ?>"
           data-list_title_line_height="<?php echo $shortcodeAtts['list_title_line_height']; ?>"
           data-title_font_size="<?php echo $shortcodeAtts['title_font_size']; ?>"
           data-review="<?php echo $shortcodeAtts['review']; ?>"
           data-subtitle_font_size="<?php echo $shortcodeAtts['subtitle_font_size']; ?>"
           data-title_line_height="<?php echo $shortcodeAtts['title_line_height']; ?>"
           data-subtitle_line_height="<?php echo $shortcodeAtts['subtitle_line_height']; ?>"
           data-pdmapofflightbox="<?php echo $shortcodeAtts['pdmapofflightbox']; ?>"
           data-filter_area="<?php echo $shortcodeAtts['filter_area']; ?>"
           data-topspacing="<?php echo $shortcodeAtts['topspacing']; ?>"
           
           data-upvote="<?php echo $shortcodeAtts['upvote']; ?>"
           data-map="<?php echo $shortcodeAtts['map']; ?>"
           data-maponly="<?php echo $shortcodeAtts['showmaponly']; ?>"
           data-credittitle="<?php echo $site_title; ?>"
           data-creditlink="<?php echo $site_link; ?>" title="Embed this List on your website!">
		     <?php 
				if(pd_ot_get_option('sbd_lan_share_list')!=''){
					echo pd_ot_get_option('sbd_lan_share_list');
				}else{
					echo __('Embed List', 'qc-pd') ;
				}
			 ?>
			<i class="fa fa-share-alt"></i>
		   </a>
            <?php
                add_action( 'wp_footer', 'pd_share_modal' );
            ?>
    <?php }}
}

function pd_share_modal() {
    ?>
    <div id="popup" class="modal-box">
            <header>
                <a href="#" class="js-modal-close close">×</a>
                <h3><?php echo __('Generate Embed Code For This List', 'qc-pd') ?></h3>
            </header>
            <div class="modal-body">
                <div class="iframe-css">
                    <div class="iframe-main">
                        <div class="ifram-row">
                            <div class="ifram-sm">
                                <span>Width: (in '%' or 'px')</span>
                                <input id="igwidth" name="igwidth" type="text" value="100">
                            </div>
                            <div class="ifram-sm" style="width: 70px;">
                                <span>&nbsp;</span>
                                <select name="igsizetype" class="iframe-main-select">
                                    <option value="%">%</option>
                                    <option value="px">px</option>
                                </select>
                            </div>
                            <div class="ifram-sm">
                                <span>Height: (in 'px')</span>
                                <input id="igheight" name="igheight" type="text" value="400">
                            </div>
                            <div class="ifram-sm">
                                <span>&nbsp;</span>
                                <a class="btn icon icon-code" id="generate-igcode"
                                   onclick=""><?php echo __('Generate & Copy', 'qc-pd') ?></a>
                                </select>
                            </div>
                        </div>

                        <div class="ifram-row">
                            <div class="ifram-lg">
                                <span class="qcld-span-label"><?php echo __('Generated Code', 'qc-pd') ?></span>
                                <br>
                                <textarea id="igcode_textarea" class="igcode_textarea" name="igcode" style="width:100%; height:120px;"
                                          readonly="readonly"></textarea>
                                <p class="guideline"><?php echo __('Hit "Generate & Copy" button to generate embed code. It will be copied
                                    to your Clipboard. You can now paste this embed code inside your website\'s HTML where
                                    you want to show the List.', 'qc-pd'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
}
