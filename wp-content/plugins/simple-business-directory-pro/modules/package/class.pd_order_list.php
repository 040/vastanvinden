<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class Sbd_order_list
{
	// class instance
	static $instance;

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', array( __CLASS__, 'set_screen' ), 10, 3 );
		add_action( 'admin_menu', array( $this, 'pd_custom_plugin_admin_menu' ) );

	}

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function pd_custom_plugin_admin_menu() {

		$hook = add_submenu_page(
			'edit.php?post_type=pd',
			'Your Orders',
			'Your Orders',
			'manage_options',
			'qcpd_order_list',
			array(
				$this,
				'qc_pd_plugin_order_list_page'
			)
		);

	}
	public function top_action(){
		global $wpdb;
		$table             = $wpdb->prefix.'pd_package_purchased';
		if(isset($_GET['act']) and $_GET['act']=='delete' ){
			$id = $_GET['id'];
			$wpdb->delete(
				$table,
				array( 'id' => $id ),
				array( '%d' )
			);
			echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Order Deleted </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		}
		
		if(isset($_GET['act']) and $_GET['act']=='cancel'){
			
			$id = $_GET['id'];
			$pdata = $wpdb->get_row("Select * from $table where 1 and `id`='$id'");
			$userid = $pdata->user_id;
			$packageid = $pdata->package_id;
			
			$results = $wpdb->get_results("select * from {$wpdb->prefix}pd_user_entry where 1 and `package_id`='$packageid' and `user_id`='$userid'");
			
			foreach($results as $result){
				$this->deny_subscriber_profile($result->id);
			}
			
			$wpdb->update(
                $table,
                array(
                    'status'  => 'cancel'
                ),
                array( 'id' => $id),
                array(
                    '%s',
                ),
                array( '%d')
            );
			echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Order has been canceled. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		}
		
		
	}
	public function qc_pd_plugin_order_list_page(){
		global $wpdb;
		if(!function_exists('wp_get_current_user')) {
			include(ABSPATH . "wp-includes/pluggable.php"); 
		}
		$table             = $wpdb->prefix.'pd_package_purchased';
		$current_user = wp_get_current_user();
		$this->top_action();

	?>
		<div class="qchero_sliders_list_wrapper">
			<div class="sliderhero_menu_title">
				<h2 style="font-size: 26px;text-align:center"><?php echo __('Order List', 'qc-pd') ?></h2>

			</div>
			<div class="qchero_slider_table_area">
				<div class="pd_payment_table">
					<div class="pd_payment_row header">
						<div class="pd_payment_cell">
							<?php _e( 'Date', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Transaction Id', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Buyer Name', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'User Name', 'qc-pd' ); ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Amount', 'qc-pd' ); ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Recurring', 'qc-pd' ); ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Status', 'qc-pd' ); ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Action', 'qc-pd' ); ?>
						</div>
					</div>

			<?php
			$rows = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table WHERE %d order by `date` DESC ", 1 ) );
			foreach($rows as $row){
			?>
				<div class="pd_payment_row">
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Date', 'qc-pd') ?></div>
						<?php echo date("F j, Y", strtotime($row->date)); ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Transaction Id', 'qc-pd') ?></div>
						<?php echo $row->transaction_id; ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Buyer Name', 'qc-pd') ?></div>
						<?php echo $row->payer_name; ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('User Name', 'qc-pd') ?></div>
						<?php
							$userinfo = get_user_by( 'ID', $row->user_id );
							if( $userinfo && !is_wp_error($userinfo) ){
								echo $userinfo->user_login;
							}
						?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Amount', 'qc-pd') ?></div>
						<?php echo $row->paid_amount ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Recurring', 'qc-pd') ?></div>
						<?php echo ($row->recurring==1?'Yes':'No'); ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Status', 'qc-pd') ?></div>
						<?php echo $row->status; ?>
					</div>
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Action', 'qc-pd') ?></div>
						<a href="<?php echo wp_nonce_url( admin_url( 'edit.php?post_type=pd&page=qcpd_order_list&act=delete&id=' . $row->id ), 'pd' ) ?>">
							<button class="button button-primary"><?php echo __('Delete', 'qc-pd') ?></button>
						</a>
					</div>
				</div>
			<?php
			}
			?>

			</div>

		</div>
		</div>
	<?php
	}
}
function Sbd_order_list(){
	return Sbd_order_list::get_instance();
}
Sbd_order_list();