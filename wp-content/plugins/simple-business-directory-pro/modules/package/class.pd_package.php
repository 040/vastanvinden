<?php
if ( ! defined( 'ABSPATH' ) ) exit;
class Sbd_package {
	// class instance
	static $instance;

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', array( __CLASS__, 'set_screen' ), 10, 3 );
		add_action( 'admin_menu', array( $this, 'pd_custom_plugin_admin_menu' ) );

	}

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function pd_custom_plugin_admin_menu() {

		$hook = add_submenu_page(
			'edit.php?post_type=pd',
			'Paid Packages',
			'Paid Packages',
			'manage_options',
			'qcpd_package',
			array(
				$this,
				'qc_pd_plugin_settings_page'
			)
		);




	}
	public function qc_pd_plugin_settings_page(){
		global $wpdb;
		if(!function_exists('wp_get_current_user')) {
			include(ABSPATH . "wp-includes/pluggable.php"); 
		}
		$table             = $wpdb->prefix.'pd_package';
		$current_user = wp_get_current_user();
		$msg = '';


		//get form data
		if(isset($_POST['qc_pd_item_duration']) and $_POST['qc_pd_item_duration']!='' and isset($_POST['qc_pd_save'])){

			$title = sanitize_text_field($_POST['qc_pd_package_title']);
			$description = wp_unslash($_POST['qc_pd_package_desc']);
			$duration = sanitize_text_field($_POST['qc_pd_item_duration']);
			$sandbox = isset($_POST['qc_pd_test_mode'])?$_POST['qc_pd_test_mode']:0;
			$enable = isset($_POST['qc_pd_package_enable'])?$_POST['qc_pd_package_enable']:0;
			$currency = sanitize_text_field($_POST['qc_pd_currency']);
			$item = sanitize_text_field($_POST['qc_pd_item']);
			$amount = ($_POST['qc_pd_amount']);
			$email = @$_POST['qc_pd_paypal'];
			$date = date('Y-m-d H:i:s');
			$recurring = @$_POST['qc_pd_recurring'];
			if($duration=='lifetime')
				$recurring = 0;

			if(isset($_POST['qc_pd_update']) and $_POST['qc_pd_update']!=''){
				$uid = $_POST['qc_pd_update'];
				$wpdb->update(
					$table,
					array(
						'date'  => $date,
						'title'   => $title,
						'description'   => $description,
						'duration'   => $duration,
						'currency'   => $currency,
						'item'   => $item,
						'Amount' => $amount,
						
						
						'enable'   => $enable,
						
					),

					array( 'id' => $uid),
					array(
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						'%s',
						
						
						'%d',
						

					),
					array( '%d')
				);
				$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Package has been Updated Successfully. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			}else{

				$wpdb->insert(
					$table,
					array(
						'date'  => $date,
						'title'   => $title,
						'description'   => $description,
						'duration'   => $duration,
						'currency'   => $currency,
						'item'   => $item,
						'Amount' => $amount,
						
						
						'enable'   => $enable,
						
					)
				);

				$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Package has been Created Successfully. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			}


		}
		if(isset($_GET['act']) && $_GET['act']=='delete'){
			$id = $_GET['id'];
			$wpdb->delete(
				$table,
				array( 'id' => $id ),
				array( '%d' )
			);
			
			$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Package has been Deleted Successfully. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		}
		
		
		
		
		if(isset($_GET['act']) && $_GET['act']=='addnew'){
?>
		<div class="wrap">

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">
					
					<?php
						if($msg!=''){
							echo $msg;
						}
						?>
						<h1><?php echo __('Add Your Package', 'qc-pd') ?></h1>
						<hr>

						<form method="post" action="">
							<table class="form-table">

                                <tr>
                                    <th><label for="qc_pd_package_title"><?php _e( 'Enable Package', 'qc-pd' ); ?></label>
                                    </th>

                                    <td>
                                        <input type="checkbox" id="qc_pd_package_enable" name="qc_pd_package_enable" value="1" checked="checked"/>

                                    </td>
                                </tr>
								<tr>
                                    <th><label for="qc_pd_package_title"><?php _e( 'Package Title', 'qc-pd' ); ?></label>
                                    </th>

                                    <td>
                                        <input type="text" id="qc_pd_package_title" name="qc_pd_package_title" value="" required/>

                                    </td>
                                </tr>
                                <tr>
                                    <th><label for="qc_pd_package_desc"><?php _e( 'Package Description', 'qc-pd' ); ?></label>
                                    </th>

                                    <td>
                                        
										<?php
										$meta_content = '';
										wp_editor($meta_content, 'pd_package_content_editor', array(
											'wpautop'               =>  true,
											'media_buttons' =>      false,
											'textarea_name' =>      'qc_pd_package_desc',
											'textarea_rows' =>      10,
											'teeny'                 =>  true
										));
										?>
                                    </td>
                                </tr>
								
								<tr>
									<th><label for="qc_pd_item_duration"><?php _e( 'Duration', 'qc-pd' ); ?></label>
									</th>

									<td>
										<select id="qc_pd_item_duration" name="qc_pd_item_duration" required>
											<option value="">None</option>
											<?php
											$max_duration = apply_filters('sbd_package_max_duration', 25);
											for($i=1;$i<$max_duration;$i++){

												echo '<option value="'.$i.'">'.$i.' Month</option>';
												

											}
											?>
											<option value="lifetime"><?php _e( 'Lifetime', 'qc-pd' ); ?></option>
										</select>
										<span class="description"><?php _e( 'Select duration for how long the listing will remain visible.', 'qc-pd' ); ?></span>
									</td>
								</tr>

                                <tr>
                                    <th><label for="qc_pd_currency"><?php _e( 'Currency', 'qc-pd' ); ?></label>
                                    </th>
                                    <td>
                                        <select name="qc_pd_currency" id="qc_pd_currency" required>

                                            <option value="USD">US Dollars ($)</option>
                                            <option value="EUR" >Euros (€)</option>
                                            <option value="GBP" >Pounds Sterling (£)</option>
                                            <option value="ARS" >Argentine Peso ($)</option>
                                            <option value="AUD" >Australian Dollars ($)</option>
                                            <option value="BRL" >Brazilian Real (R$)</option>
                                            <option value="CAD" >Canadian Dollars ($)</option>
                                            <option value="CNY" >Chinese Yuan</option>
                                            <option value="CZK" >Czech Koruna</option>
                                            <option value="DKK" >Danish Krone</option>
                                            <option value="HKD" >Hong Kong Dollar ($)</option>
                                            <option value="HUF" >Hungarian Forint</option>
                                            <option value="INR" >Indian Rupee</option>
                                            <option value="IDR" >Indonesia Rupiah</option>
                                            <option value="ILS" >Israeli Shekel</option>
                                            <option value="JPY" >Japanese Yen (¥)</option>
                                            <option value="MYR" >Malaysian Ringgits</option>
                                            <option value="MXN" >Mexican Peso ($)</option>
                                            <option value="NGN" >Nigerian Naira (₦)</option>
                                            <option value="NZD" >New Zealand Dollar ($)</option>
                                            <option value="NOK" >Norwegian Krone</option>
                                            <option value="PHP" >Philippine Pesos</option>
                                            <option value="PLN" >Polish Zloty</option>
                                            <option value="SGD" >Singapore Dollar ($)</option>
                                            <option value="ZAR" >South African Rand (R)</option>
                                            <option value="KRW" >South Korean Won</option>
                                            <option value="SEK" >Swedish Krona</option>
                                            <option value="CHF" >Swiss Franc</option>
                                            <option value="TWD" >Taiwan New Dollars</option>
                                            <option value="THB" >Thai Baht</option>
                                            <option value="TRY" >Turkish Lira</option>
                                            <option value="VND" >Vietnamese Dong</option>

                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <th><label for="qc_pd_item"><?php _e( 'Total Item', 'qc-pd' ); ?></label>
                                    </th>

                                    <td>
                                        <input type="text" id="qc_pd_item" name="qc_pd_item" value="" required/>
                                        <span class="description"><?php _e( 'How many listing user can add?', 'qc-pd' ); ?></span>
                                    </td>
                                </tr>

                                <tr>
									<th><label for="qc_pd_amount"><?php _e( 'Package Price', 'qc-pd' ); ?></label>
									</th>

									<td>
										<input type="text" id="qc_pd_amount" name="qc_pd_amount" value="" required/>
										<span class="description"><?php _e( 'Enter price for the package.', 'qc-pd' ); ?></span>
									</td>
								</tr>

								

								<tr>
									<th><label for="qc_pd_save"><?php _e( '', 'qc-pd' ); ?></label>
									</th>

									<td>
										
										<input type="submit" name="qc_pd_save" id="qc_pd_save" value="Save" />
										
									</td>
								</tr>

							</table>
						</form>
						<hr>
						<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=qcpd_package'); ?>">
							<button class="button button-normal"><?php echo __('Go Back', 'qc-pd') ?></button>
						</a>
					</div>
				</div>
			</div>
		</div>
			
<?php		
		}elseif( isset($_GET['act']) && $_GET['act']=='edit'){
		$id = $_GET['id'];
		$row     = $wpdb->get_row( "SELECT * FROM $table WHERE 1 and id='".$id."'" );
		
?>		
		<div class="wrap">

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">
			<?php
			if($msg!=''){
				echo $msg;
			}
			?>
			<h1><?php echo __('Manage Your Package', 'qc-pd') ?></h1>
			<hr>
			<?php
			if(empty($row)){
			?>
				<?php echo __('<p>You have no package created! Please submit the following information and press save to Create a Package and charge your client to submit there list item.</p>', 'qc-pd') ?>
				

			<?php } ?>

			<form method="post" action="">
				<table class="form-table">

					<tr>
						<th><label for="qc_pd_package_title"><?php _e( 'Enable Package', 'qc-pd' ); ?></label>
						</th>

						<td>
							<input type="checkbox" id="qc_pd_package_enable" name="qc_pd_package_enable" <?php echo (isset($row->enable) && $row->enable==1)?'checked="checked"':''; ?> value="1"/>

						</td>
					</tr>
					<tr>
						<th><label for="qc_pd_package_title"><?php _e( 'Package Title', 'qc-pd' ); ?></label>
						</th>

						<td>
							<input type="text" id="qc_pd_package_title" name="qc_pd_package_title" value="<?php echo (isset($row->title)&&$row->title!=''?$row->title:''); ?>" required/>

						</td>
					</tr>
					<tr>
						<th><label for="qc_pd_package_desc"><?php _e( 'Package Description', 'qc-pd' ); ?></label>
						</th>

						<td>
							

							<?php
							$meta_content = (isset($row->description)&&$row->description!=''?html_entity_decode($row->description):'');
							wp_editor($meta_content, 'pd_package_content_editor', array(
								'wpautop'               =>  true,
								'media_buttons' =>      false,
								'textarea_name' =>      'qc_pd_package_desc',
								'textarea_rows' =>      10,
								'teeny'                 =>  true
							));
							?>
							
						</td>
					</tr>

					<tr>
						<th><label for="qc_pd_item_duration"><?php _e( 'Duration', 'qc-pd' ); ?></label>
						</th>

						<td>
							<select id="qc_pd_item_duration" name="qc_pd_item_duration" required>
								<option value="">None</option>
								<?php
								$max_duration = apply_filters('sbd_package_max_duration', 25);
								for($i=1;$i<$max_duration;$i++){
									if(isset($row->duration) and $row->duration==$i){
										echo '<option value="'.$i.'" selected="selected">'.$i.' Month</option>';
									}else{
										echo '<option value="'.$i.'">'.$i.' Month</option>';
									}

								}
								?>
								<option value="lifetime" <?php echo ($row->duration=='lifetime'?'selected="selected"':''); ?>><?php _e( 'Lifetime', 'qc-pd' ); ?></option>
							</select>
							<span class="description"><?php _e( 'Select duration for how long the listing will remain visible.', 'qc-pd' ); ?></span>
						</td>
					</tr>

					<tr>
						<th><label for="qc_pd_currency"><?php _e( 'Currency', 'qc-pd' ); ?></label>
						</th>
						<td>
							<select name="qc_pd_currency" id="qc_pd_currency" required>

								<option value="USD" <?php echo (isset($row->currency)&&$row->currency=='USD'?'selected="selected"':''); ?>>US Dollars ($)</option>
								<option value="EUR" <?php echo (isset($row->currency)&&$row->currency=='EUR'?'selected="selected"':''); ?>>Euros (€)</option>
								<option value="GBP" <?php echo (isset($row->currency)&&$row->currency=='GBP'?'selected="selected"':''); ?>>Pounds Sterling (£)</option>
								<option value="ARS" <?php echo (isset($row->currency)&&$row->currency=='ARS'?'selected="selected"':''); ?>>Argentine Peso ($)</option>
								<option value="AUD" <?php echo (isset($row->currency)&&$row->currency=='AUD'?'selected="selected"':''); ?>>Australian Dollars ($)</option>
								<option value="BRL" <?php echo (isset($row->currency)&&$row->currency=='BRL'?'selected="selected"':''); ?>>Brazilian Real (R$)</option>
								<option value="CAD" <?php echo (isset($row->currency)&&$row->currency=='CAD'?'selected="selected"':''); ?>>Canadian Dollars ($)</option>
								<option value="CNY" <?php echo (isset($row->currency)&&$row->currency=='CNY'?'selected="selected"':''); ?>>Chinese Yuan</option>
								<option value="CZK" <?php echo (isset($row->currency)&&$row->currency=='CZK'?'selected="selected"':''); ?>>Czech Koruna</option>
								<option value="DKK" <?php echo (isset($row->currency)&&$row->currency=='DKK'?'selected="selected"':''); ?>>Danish Krone</option>
								<option value="HKD" <?php echo (isset($row->currency)&&$row->currency=='HKD'?'selected="selected"':''); ?>>Hong Kong Dollar ($)</option>
								<option value="HUF" <?php echo (isset($row->currency)&&$row->currency=='HUF'?'selected="selected"':''); ?>>Hungarian Forint</option>
								<option value="INR" <?php echo (isset($row->currency)&&$row->currency=='INR'?'selected="selected"':''); ?>>Indian Rupee</option>
								<option value="IDR" <?php echo (isset($row->currency)&&$row->currency=='IDR'?'selected="selected"':''); ?>>Indonesia Rupiah</option>
								<option value="ILS" <?php echo (isset($row->currency)&&$row->currency=='ILS'?'selected="selected"':''); ?>>Israeli Shekel</option>
								<option value="JPY" <?php echo (isset($row->currency)&&$row->currency=='JPY'?'selected="selected"':''); ?>>Japanese Yen (¥)</option>
								<option value="MYR" <?php echo (isset($row->currency)&&$row->currency=='MYR'?'selected="selected"':''); ?>>Malaysian Ringgits</option>
								<option value="MXN" <?php echo (isset($row->currency)&&$row->currency=='MXN'?'selected="selected"':''); ?>>Mexican Peso ($)</option>
								<option value="NGN" <?php echo (isset($row->currency)&&$row->currency=='NGN'?'selected="selected"':''); ?>>Nigerian Naira (₦)</option>
								<option value="NZD" <?php echo (isset($row->currency)&&$row->currency=='NZD'?'selected="selected"':''); ?>>New Zealand Dollar ($)</option>
								<option value="NOK" <?php echo (isset($row->currency)&&$row->currency=='NOK'?'selected="selected"':''); ?>>Norwegian Krone</option>
								<option value="PHP" <?php echo (isset($row->currency)&&$row->currency=='PHP'?'selected="selected"':''); ?>>Philippine Pesos</option>
								<option value="PLN" <?php echo (isset($row->currency)&&$row->currency=='PLN'?'selected="selected"':''); ?>>Polish Zloty</option>
								<option value="SGD" <?php echo (isset($row->currency)&&$row->currency=='SGD'?'selected="selected"':''); ?>>Singapore Dollar ($)</option>
								<option value="ZAR" <?php echo (isset($row->currency)&&$row->currency=='ZAR'?'selected="selected"':''); ?>>South African Rand (R)</option>
								<option value="KRW" <?php echo (isset($row->currency)&&$row->currency=='KRW'?'selected="selected"':''); ?>>South Korean Won</option>
								<option value="SEK" <?php echo (isset($row->currency)&&$row->currency=='SEK'?'selected="selected"':''); ?>>Swedish Krona</option>
								<option value="CHF" <?php echo (isset($row->currency)&&$row->currency=='CHF'?'selected="selected"':''); ?>>Swiss Franc</option>
								<option value="TWD" <?php echo (isset($row->currency)&&$row->currency=='TWD'?'selected="selected"':''); ?>>Taiwan New Dollars</option>
								<option value="THB" <?php echo (isset($row->currency)&&$row->currency=='THB'?'selected="selected"':''); ?>>Thai Baht</option>
								<option value="TRY" <?php echo (isset($row->currency)&&$row->currency=='TRY'?'selected="selected"':''); ?>>Turkish Lira</option>
								<option value="VND" <?php echo (isset($row->currency)&&$row->currency=='VND'?'selected="selected"':''); ?>>Vietnamese Dong</option>

							</select>
						</td>
					</tr>

					<tr>
						<th><label for="qc_pd_item"><?php _e( 'Total Item', 'qc-pd' ); ?></label>
						</th>

						<td>
							<input type="text" id="qc_pd_item" name="qc_pd_item" value="<?php echo (isset($row->item)&&$row->item!=''?$row->item:'10'); ?>" required/>
							<span class="description"><?php _e( 'How many listing user can add?', 'qc-pd' ); ?></span>
						</td>
					</tr>

					<tr>
						<th><label for="qc_pd_amount"><?php _e( 'Package Price', 'qc-pd' ); ?></label>
						</th>

						<td>
							<input type="text" id="qc_pd_amount" name="qc_pd_amount" value="<?php echo (isset($row->Amount)&&$row->Amount!=''?$row->Amount:''); ?>" required/>
							<span class="description"><?php _e( 'Enter price for the package.', 'qc-pd' ); ?></span>
						</td>
					</tr>

					

					<tr>
						<th><label for="qc_pd_save"><?php _e( '', 'qc-pd' ); ?></label>
						</th>

						<td>
							<?php
							if(isset($row->id) and $row->id!=''){?>
								<input type="hidden" name="qc_pd_update" id="qc_pd_update" value="<?php echo $row->id; ?>" />
							<?php } ?>
							<input type="submit" name="qc_pd_save" id="qc_pd_save" value="Save" />

						</td>
					</tr>

				</table>
			</form>
			<hr>
			<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=qcpd_package'); ?>">
				<button class="button button-normal"><?php echo __('Go Back', 'qc-pd') ?></button>
			</a>
			</div>
			</div>
			</div>
			</div>

<?php	
		}else{
		

		$rows     = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table WHERE %d", 1 ) ); ?>
		<div class="wrap">

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">

					<?php
						if($msg!=''){
							echo $msg;
						}
						?>
					
					<h1 style="float:left;margin-bottom: 12px;">Package List</h1>
					<a style="float:right" href="<?php echo admin_url( 'edit.php?post_type=pd&page=qcpd_package&act=addnew'); ?>">
						<button class="button button-primary"><?php echo __('Add New Package', 'qc-pd') ?></button>
					</a>
					<div class="qchero_slider_table_area">
						<div class="pd_payment_table">
							<div class="pd_payment_row header">
								<div class="pd_payment_cell">
									<?php _e( 'Title', 'qc-pd' ) ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Description', 'qc-pd' ) ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Duration', 'qc-pd' ) ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Amount', 'qc-pd' ); ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Currency', 'qc-pd' ); ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Total Items', 'qc-pd' ); ?>
								</div>
								<div class="pd_payment_cell">
									<?php _e( 'Status', 'qc-pd' ); ?>
								</div>
								
								<div class="pd_payment_cell">
									<?php _e( 'Action', 'qc-pd' ); ?>
								</div>
							</div>

					<?php

					foreach($rows as $row){
					?>
						<div class="pd_payment_row">
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Title', 'qc-pd') ?></div>
								<?php echo ($row->title); ?>
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Description', 'qc-pd') ?></div>
								<?php echo $row->description; ?>
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Duration', 'qc-pd') ?></div>
								<?php echo $row->duration; ?> Month
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Amount', 'qc-pd') ?></div>
								<?php
									
									echo $row->Amount;
								?>
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Currency', 'qc-pd') ?></div>
								<?php echo $row->currency ?>
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Total Items', 'qc-pd') ?></div>
								<?php echo ($row->item); ?>
							</div>
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Status', 'qc-pd') ?></div>
								<?php echo ($row->enable==1?'Active':'Inactive'); ?>
							</div>
							
							<div class="pd_payment_cell">
								<div class="pd_responsive_head"><?php echo __('Action', 'qc-pd') ?></div>
								<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=qcpd_package&act=edit&id='.$row->id); ?>">
									<button class="button button-primary"><?php echo __('Edit', 'qc-pd') ?></button>
								</a>
								<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=qcpd_package&act=delete&id='.$row->id); ?>">
									<button class="button button-danger"><?php echo __('Delete', 'qc-pd') ?></button>
								</a>
								
							</div>
						</div>
					<?php
					}
					?>

					</div>

				</div>
						
						
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="wrap">
			
		<?php 
		$table             = $wpdb->prefix.'sbd_claim_configuration';
		$current_user = wp_get_current_user();
		$msg = '';
		//echo $table;exit;

		//get form data
		if(isset($_POST['qc_sbd_claim_save'])){

			
			$enable = isset($_POST['qc_sld_claim_listing_enable'])?$_POST['qc_sld_claim_listing_enable']:0;
			$currency = sanitize_text_field($_POST['qc_sld_currency']);
			
			$amount = ($_POST['qc_sld_amount']);
			
			$date = date('Y-m-d H:i:s');
			
			
			if(isset($_POST['qc_sld_update']) and $_POST['qc_sld_update']!=''){
				$uid = $_POST['qc_sld_update'];
				$wpdb->update(
					$table,
					array(
						'date'  => $date,
						'currency'   => $currency,
						'Amount' => $amount,
						
						'enable'   => $enable,
						
					),

					array( 'id' => $uid),
					array(
						'%s',
						'%s',
						'%s',

						'%d',
						

					),
					array( '%d')
				);
				$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Claim Configuration Updated. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			}else{

				$wpdb->insert(
					$table,
					array(
						'date'  => $date,
						
						'currency'   => $currency,
						'Amount' => $amount,
						
						'enable'   => $enable,
						
					)
				);
				
				

				$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Claim Configuration Created. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			}


		}
		$row     = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE %d", 1 ) ); ?>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">

						<?php
						if($msg!=''){
							echo $msg;
						}
						?>
						<h1><?php echo __('Manage Claim Listing Payment', 'qc-pd') ?></h1>
						<hr>
						

						<form method="post" action="">
							<table class="form-table">

                                <tr>
                                    <th><label for="qc_pd_package_title"><?php _e( 'Enable Payment for Claim Listing', 'qc-pd' ); ?></label>
                                    </th>

                                    <td>
                                        <input type="checkbox" id="qc_pd_claim_listing_enable" name="qc_sld_claim_listing_enable" <?php echo (isset($row->enable) && $row->enable==1)?'checked="checked"':''; ?> value="1"/>
										

                                    </td>
                                </tr>

								<tr>
									<th><label for="qc_sld_amount"><?php _e( 'Claim Listing Price', 'qc-pd' ); ?></label>
									</th>

									<td>
										<input type="text" id="qc_sld_amount" name="qc_sld_amount" value="<?php echo (isset($row->Amount)&&$row->Amount!=''?$row->Amount:''); ?>" required/>
										<span class="description"><?php _e( 'Enter price for the Claim Listing.', 'qc-pd' ); ?></span>
									</td>
								</tr>

                                <tr>
                                    <th><label for="qc_sld_currency"><?php _e( 'Currency', 'qc-pd' ); ?></label>
                                    </th>
                                    <td>
                                        <select name="qc_sld_currency" id="qc_sld_currency" required>

                                            <option value="USD" <?php echo (isset($row->currency)&&$row->currency=='USD'?'selected="selected"':''); ?>>US Dollars ($)</option>
                                            <option value="EUR" <?php echo (isset($row->currency)&&$row->currency=='EUR'?'selected="selected"':''); ?>>Euros (€)</option>
                                            <option value="GBP" <?php echo (isset($row->currency)&&$row->currency=='GBP'?'selected="selected"':''); ?>>Pounds Sterling (£)</option>
                                            <option value="ARS" <?php echo (isset($row->currency)&&$row->currency=='ARS'?'selected="selected"':''); ?>>Argentine Peso ($)</option>
                                            <option value="AUD" <?php echo (isset($row->currency)&&$row->currency=='AUD'?'selected="selected"':''); ?>>Australian Dollars ($)</option>
                                            <option value="BRL" <?php echo (isset($row->currency)&&$row->currency=='BRL'?'selected="selected"':''); ?>>Brazilian Real (R$)</option>
                                            <option value="CAD" <?php echo (isset($row->currency)&&$row->currency=='CAD'?'selected="selected"':''); ?>>Canadian Dollars ($)</option>
                                            <option value="CNY" <?php echo (isset($row->currency)&&$row->currency=='CNY'?'selected="selected"':''); ?>>Chinese Yuan</option>
                                            <option value="CZK" <?php echo (isset($row->currency)&&$row->currency=='CZK'?'selected="selected"':''); ?>>Czech Koruna</option>
                                            <option value="DKK" <?php echo (isset($row->currency)&&$row->currency=='DKK'?'selected="selected"':''); ?>>Danish Krone</option>
                                            <option value="HKD" <?php echo (isset($row->currency)&&$row->currency=='HKD'?'selected="selected"':''); ?>>Hong Kong Dollar ($)</option>
                                            <option value="HUF" <?php echo (isset($row->currency)&&$row->currency=='HUF'?'selected="selected"':''); ?>>Hungarian Forint</option>
                                            <option value="INR" <?php echo (isset($row->currency)&&$row->currency=='INR'?'selected="selected"':''); ?>>Indian Rupee</option>
                                            <option value="IDR" <?php echo (isset($row->currency)&&$row->currency=='IDR'?'selected="selected"':''); ?>>Indonesia Rupiah</option>
                                            <option value="ILS" <?php echo (isset($row->currency)&&$row->currency=='ILS'?'selected="selected"':''); ?>>Israeli Shekel</option>
                                            <option value="JPY" <?php echo (isset($row->currency)&&$row->currency=='JPY'?'selected="selected"':''); ?>>Japanese Yen (¥)</option>
                                            <option value="MYR" <?php echo (isset($row->currency)&&$row->currency=='MYR'?'selected="selected"':''); ?>>Malaysian Ringgits</option>
                                            <option value="MXN" <?php echo (isset($row->currency)&&$row->currency=='MXN'?'selected="selected"':''); ?>>Mexican Peso ($)</option>
                                            <option value="NGN" <?php echo (isset($row->currency)&&$row->currency=='NGN'?'selected="selected"':''); ?>>Nigerian Naira (₦)</option>
                                            <option value="NZD" <?php echo (isset($row->currency)&&$row->currency=='NZD'?'selected="selected"':''); ?>>New Zealand Dollar ($)</option>
                                            <option value="NOK" <?php echo (isset($row->currency)&&$row->currency=='NOK'?'selected="selected"':''); ?>>Norwegian Krone</option>
                                            <option value="PHP" <?php echo (isset($row->currency)&&$row->currency=='PHP'?'selected="selected"':''); ?>>Philippine Pesos</option>
                                            <option value="PLN" <?php echo (isset($row->currency)&&$row->currency=='PLN'?'selected="selected"':''); ?>>Polish Zloty</option>
                                            <option value="SGD" <?php echo (isset($row->currency)&&$row->currency=='SGD'?'selected="selected"':''); ?>>Singapore Dollar ($)</option>
                                            <option value="ZAR" <?php echo (isset($row->currency)&&$row->currency=='ZAR'?'selected="selected"':''); ?>>South African Rand (R)</option>
                                            <option value="KRW" <?php echo (isset($row->currency)&&$row->currency=='KRW'?'selected="selected"':''); ?>>South Korean Won</option>
                                            <option value="SEK" <?php echo (isset($row->currency)&&$row->currency=='SEK'?'selected="selected"':''); ?>>Swedish Krona</option>
                                            <option value="CHF" <?php echo (isset($row->currency)&&$row->currency=='CHF'?'selected="selected"':''); ?>>Swiss Franc</option>
                                            <option value="TWD" <?php echo (isset($row->currency)&&$row->currency=='TWD'?'selected="selected"':''); ?>>Taiwan New Dollars</option>
                                            <option value="THB" <?php echo (isset($row->currency)&&$row->currency=='THB'?'selected="selected"':''); ?>>Thai Baht</option>
                                            <option value="TRY" <?php echo (isset($row->currency)&&$row->currency=='TRY'?'selected="selected"':''); ?>>Turkish Lira</option>
                                            <option value="VND" <?php echo (isset($row->currency)&&$row->currency=='VND'?'selected="selected"':''); ?>>Vietnamese Dong</option>

                                        </select>
                                    </td>
                                </tr>

								<tr>
									<th><label for="qc_sld_save"><?php _e( '', 'qc-pd' ); ?></label>
									</th>

									<td>
										<?php
										if(isset($row->id) and $row->id!=''){?>
											<input type="hidden" name="qc_sld_update" id="qc_sld_update" value="<?php echo $row->id; ?>" />
										<?php } ?>
										<input type="submit" name="qc_sbd_claim_save" id="qc_sld_save" value="Save" />

									</td>
								</tr>

							</table>
						</form>
						<hr>
						
					</div>
				</div>
			</div>
		</div>
		
		
<?php
		}
	}
}
function pd_package(){
	return Sbd_package::get_instance();
}
pd_package();