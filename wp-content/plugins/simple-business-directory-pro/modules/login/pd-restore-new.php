<?php
	if ( ! defined( 'ABSPATH' ) ) exit; 
	$new_password = sanitize_text_field( $_GET['pass'] );
	$login_url = $this->pdcustom_login_get_translated_option_page( 'sbd_login_url','');
	$restore_url = $this->pdcustom_login_get_translated_option_page( 'sbd_restore_url', '');
	$uid = absint($_REQUEST['uid']);
?>

<div class="cleanlogin-container">
	<form class="cleanlogin-form" action="<?php echo esc_url($restore_url); ?>" method="post">
		
		<fieldset>
			<div class="cleanlogin-field">
				<label><?php echo __( 'Your new password is', 'qc-pd' ); ?></label>
				<input type="text" name="pass" value="<?php echo $new_password; ?>">
				<!-- <input type="text" name="pass" value="<?php echo esc_attr($new_password); ?>"> -->
				<input type="hidden" name="qcpd-restore-pwd" value="restore">
				<input type="hidden" name="qcpd-restore-pwd-type" value="user">
				<input type="hidden" name="qcpd-uid" value="<?php echo esc_attr($uid); ?>">
			</div>
		
		</fieldset>
		
		<div class="cleanlogin-form-bottom" style="background: none;">
				
			<?php if ( $login_url != '' )
				// echo "<a href='$login_url' class='cleanlogin-form-login-link pd_logout_button'>". __( 'Log in', 'qc-pd') ."</a>";
			?>
			<button type="submit" class='cleanlogin-form-login-link pd_logout_button'><?php echo esc_html__( 'Log in', 'qc-opd'); ?></button>
						
		</div>
	</form>
</div>