<?php
	if ( ! defined( 'ABSPATH' ) ) exit;
	
	$login_url = $this->pdcustom_login_get_translated_option_page( 'sbd_login_url','');
	$register_url = $this->pdcustom_login_get_translated_option_page( 'pd_register_url', '');
	$restore_url = $this->pdcustom_login_get_translated_option_page( 'sbd_restore_url', '');
	$sbd_lan_only_password = pd_ot_get_option('sbd_lan_only_password') != '' ? pd_ot_get_option('sbd_lan_only_password') : __('Password', 'qc-pd');
?>

<div class="cleanlogin-container">		

	<form class="cleanlogin-form" method="post" action="<?php echo $login_url;?>">
		<h2> <?php echo (pd_ot_get_option('sbd_lan_login_title')!=''?pd_ot_get_option('sbd_lan_login_title'):__('Login', 'qc-pd')); ?> </h2>
		
		<fieldset>
			<div class="cleanlogin-field">
				<input class="cleanlogin-field-username" type="text" name="log" placeholder="<?php echo (pd_ot_get_option('sbd_lan_username')!=''?pd_ot_get_option('sbd_lan_username'):__('Username or Email', 'qc-pd')); ?>">
			</div>
			
			<div class="cleanlogin-field">
				<input class="cleanlogin-field-password" type="password" name="pwd" placeholder="<?php echo $sbd_lan_only_password; ?>">
			</div>
		</fieldset>
		
		<fieldset style="text-align:center; padding:0 !important;">
			<input class="cleanlogin-field submit_registration" type="submit" value="<?php echo (pd_ot_get_option('sbd_lan_login_with_space')!=''?pd_ot_get_option('sbd_lan_login_with_space'):__('Log in', 'qc-pd')); ?>" name="submit">
			<input type="hidden" name="action" value="login">
			
		</fieldset>
		
		<div class="cleanlogin-form-bottom">
			
			<div class="cleanlogin-field-remember">
				<?php 
					$create_account_text = pd_ot_get_option('sbd_lan_create_account') != '' ? pd_ot_get_option('sbd_lan_create_account') : __('Create Account', 'qc-pd');
					echo "<a style='float: right;color: #18191f ;font-weight: bold; padding-left:15px;' href='$register_url' class='cleanlogin-form-pwd-link'>".$create_account_text ."</a>";
				?>
			</div>

			<?php
				$forgot_pwd_text = pd_ot_get_option('sbd_lan_forgot_password') != '' ? pd_ot_get_option('sbd_lan_forgot_password') : __( 'Forgot your password?', 'qc-pd' ); 
				echo "<a style='float: right;color: #666;font-weight: bold; padding-right:15px;' href='$restore_url' class='cleanlogin-form-pwd-link'>". $forgot_pwd_text ."</a>";
			?>
						
		</div>
		

	</form>

</div>
