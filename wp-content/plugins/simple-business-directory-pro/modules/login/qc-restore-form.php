<?php
	if ( ! defined( 'ABSPATH' ) ) exit; 
?>

<div class="cleanlogin-container">
	<form class="cleanlogin-form" method="post" action="">
		<h2><?php echo pd_ot_get_option('sbd_lan_reset_password') != '' ? pd_ot_get_option('sbd_lan_reset_password') : __( 'Reset password', 'qc-pd' ); ?></h2>
		<fieldset>
		
			<div class="cleanlogin-field">
				<input class="cleanlogin-field-username" type="text" name="username" value="" placeholder="<?php echo pd_ot_get_option('sbd_lan_username') != '' ? pd_ot_get_option('sbd_lan_username') : __( 'Username or E-mail', 'qc-pd' ); ?>">
			</div>

			<div class="cleanlogin-field-website">
				<label for='website'><?php echo __('Website', 'qc-pd') ?></label>
	    		<input type='text' name='website' value=".">
	    	</div>
		
		</fieldset>
		
		<div style="text-align:center">	
			<input type="submit" value="<?php echo pd_ot_get_option('sbd_lan_restore_password') != '' ? pd_ot_get_option('sbd_lan_restore_password') : __( 'Restore password', 'qc-pd' ); ?>" name="submit">
			<input type="hidden" name="action" value="restore">		
		</div>

	</form>
</div>
<?php
	$customCss = pd_ot_get_option( 'pd_custom_style' );
	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>

<?php endif;
		