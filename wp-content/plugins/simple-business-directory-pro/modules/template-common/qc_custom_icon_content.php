<?php
if(pd_ot_get_option('sbd_field_1_enable')=='on' and isset($list['qcpd_field_1']) and $list['qcpd_field_1']!='' and pd_ot_get_option('sbd_field_1_type')!='text'){
	if(pd_ot_get_option('sbd_field_1_type')=='link'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_1']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_1_icon')!=''?pd_ot_get_option('sbd_field_1_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}elseif(pd_ot_get_option('sbd_field_1_type')=='phone'){
		?>
		<p><a target="_blank" href="tel:<?php echo $list['qcpd_field_1']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_1_icon')!=''?pd_ot_get_option('sbd_field_1_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}
}
if(pd_ot_get_option('sbd_field_2_enable')=='on' and isset($list['qcpd_field_2']) and $list['qcpd_field_2']!='' and pd_ot_get_option('sbd_field_2_type')!='text'){
	if(pd_ot_get_option('sbd_field_2_type')=='link'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_2']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_2_icon')!=''?pd_ot_get_option('sbd_field_2_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}elseif(pd_ot_get_option('sbd_field_2_type')=='phone'){
		?>
		<p><a target="_blank" href="tel:<?php echo $list['qcpd_field_2']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_2_icon')!=''?pd_ot_get_option('sbd_field_2_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}
}
if(pd_ot_get_option('sbd_field_3_enable')=='on' and isset($list['qcpd_field_3']) and $list['qcpd_field_3']!='' and pd_ot_get_option('sbd_field_3_type')!='text'){
	if(pd_ot_get_option('sbd_field_3_type')=='link'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_3']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_3_icon')!=''?pd_ot_get_option('sbd_field_3_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}elseif(pd_ot_get_option('sbd_field_3_type')=='phone'){
		?>
		<p><a target="_blank" href="tel:<?php echo $list['qcpd_field_3']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_3_icon')!=''?pd_ot_get_option('sbd_field_3_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}
}
if(pd_ot_get_option('sbd_field_4_enable')=='on' and isset($list['qcpd_field_4']) and $list['qcpd_field_4']!='' and pd_ot_get_option('sbd_field_4_type')!='text'){
	if(pd_ot_get_option('sbd_field_4_type')=='link'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_4']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_4_icon')!=''?pd_ot_get_option('sbd_field_4_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}elseif(pd_ot_get_option('sbd_field_4_type')=='phone'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_4']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_4_icon')!=''?pd_ot_get_option('sbd_field_4_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}
}
if(pd_ot_get_option('sbd_field_5_enable')=='on' and isset($list['qcpd_field_5']) and $list['qcpd_field_5']!='' and pd_ot_get_option('sbd_field_5_type')!='text'){
	if(pd_ot_get_option('sbd_field_5_type')=='link'){
		?>
		<p><a target="_blank" href="<?php echo $list['qcpd_field_5']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_5_icon')!=''?pd_ot_get_option('sbd_field_5_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}elseif(pd_ot_get_option('sbd_field_5_type')=='phone'){
		?>
		<p><a target="_blank" href="tel:<?php echo $list['qcpd_field_5']; ?>"><i class="fa <?php echo (pd_ot_get_option('sbd_field_5_icon')!=''?pd_ot_get_option('sbd_field_5_icon'):'fa-sun-o'); ?>"></i></a></p>
		<?php
	}
}