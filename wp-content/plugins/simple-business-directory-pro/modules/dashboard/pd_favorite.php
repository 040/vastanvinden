<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-7/template.css"; ?>" />
<?php
	$go_to_website_text = pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website';
	echo '<div id="sbdopd-list-holder" class="qc-grid qcpd-list-hoder">';
	global $wpdb;
	if(pd_ot_get_option('pd_enable_bookmark')=='on'){
		$b_title = pd_ot_get_option('pd_bookmark_title');
		
		$userid = get_current_user_id();
		$user_meta_data = get_user_meta($userid, 'pd_bookmark_user_meta');
		
		
		$conf['list_bg_color'] = pd_ot_get_option('pd_bookmark_item_background_color');
		
		$conf['list_bg_color_hov'] = pd_ot_get_option('pd_bookmark_item_background_color_hover');
		
		$conf['list_txt_color_hov'] = pd_ot_get_option('pd_bookmark_item_text_color_hover');
		$conf['list_txt_color'] = pd_ot_get_option('pd_bookmark_item_text_color');
		
		$conf['list_border_color'] = pd_ot_get_option('pd_bookmark_item_border_color');
		
		$conf['item_bdr_color'] = pd_ot_get_option('pd_bookmark_item_border_color');
		$conf['item_bdr_color_hov'] = pd_ot_get_option('pd_bookmark_item_border_color_hover');
		
		$conf['list_subtxt_color'] = pd_ot_get_option('pd_bookmark_item_sub_text_color');
		$conf['list_subtxt_color_hov'] = pd_ot_get_option('pd_bookmark_item_sub_text_color_hover');
	if(!empty($user_meta_data[0])){
?>
<style>

			#bookmark_list ul li .ilist-item-main {
					background-color: <?php echo $conf['list_bg_color']; ?>;
			}
			#bookmark_list ul li .ilist-item-main:hover {
					background-color: <?php echo $conf['list_bg_color_hov']; ?>;
			}
            #bookmark_list ul li .item-title-text {
                background: <?php echo $conf['list_border_color']; ?>;
            }

			#bookmark_list ul li .item-title-text h3{
			  color: <?php echo $conf['list_txt_color']; ?>;
				<?php if( isset($conf['item_title_font_size']) && $conf['item_title_font_size']!=''): ?>
				font-size:<?php echo $conf['item_title_font_size']; ?> !important;
				<?php endif; ?>

				<?php if( isset($conf['item_title_line_height']) && $conf['item_title_line_height']!=''): ?>
				line-height:<?php echo $conf['item_title_line_height']; ?> !important;
				<?php endif; ?>
			}

			#bookmark_list ul li .item-title-text h3:hover{
			  color: <?php echo $conf['list_txt_color_hov']; ?>;
			}

			#bookmark_list ul li .item-desc-text p{
			  color: <?php echo $conf['list_subtxt_color']; ?>;
				<?php if( isset($conf['item_subtitle_font_size']) &&  $conf['item_subtitle_font_size']!=''): ?>
				font-size:<?php echo $conf['item_subtitle_font_size']; ?> !important;
				<?php endif; ?>

				<?php if( isset($conf['item_subtitle_line_height']) &&  $conf['item_subtitle_line_height']!=''): ?>
				line-height:<?php echo $conf['item_subtitle_line_height']; ?>!important;
				<?php endif; ?>
			}

			#bookmark_list ul li .item-desc-text:hover p{
			  color: <?php echo $conf['list_subtxt_color_hov']; ?>;
			}




		</style>

		<div id="bookmark_list" class="qc-grid-item qcpd-list-column opd-column-<?php echo 1; echo " style-7";?>">

			<div class="opd-list-style-12 pd-container title_style_pd" >

				<h2><?php echo ($b_title!=''?$b_title:'Favorites'); ?></h2>
				<ul class="tooltip_tpl12-tpl pd-list" id="pd_bookmark_ul">
					
					<?php
					$lists = array();
			if(!empty($user_meta_data)){
				foreach($user_meta_data[0] as $postid=>$metaids){
					if(!empty($metaids)){
						foreach($metaids as $metaid){
							
							$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = $postid AND meta_key = 'qcpd_list_item01'");
							if(!empty($results)){
								foreach ($results as $key => $value) {
									$unserialized = unserialize($value->meta_value);
									if (trim($unserialized['qcpd_timelaps']) == trim($metaid)) {
										$customdata = $unserialized;
										$customdata['postid'] = $postid;
										$lists[] = $customdata;
									}
								}
							}
						}
					}
				}
			}
					usort($lists, "pd_custom_sort_by_tpl_title");
					$b = 1;		
					$count=0;	
					foreach($lists as $list){
						$count++;
					?>
					<?php
						$canContentClass = "subtitle-present";

						if( !isset($list['qcpd_item_subtitle']) || $list['qcpd_item_subtitle'] == "" )
						{
							$canContentClass = "subtitle-absent";
						}
					?>
					<li id="pd_bookmark_li_<?php echo $b; ?>" class="ilist-25">

						<?php
							$item_url = $list['qcpd_item_link'];
							$masked_url = $list['qcpd_item_link'];
							$mask_url ='off';
							if( $mask_url == 'on' ){
								$masked_url = 'http://' . qcpd_get_domain($list['qcpd_item_link']);
							}
							$hover_info = 'title="'.$go_to_website_text.'"';
							
						?>

						<div class="col-sbd-<?php echo '3'; ?>">
								<div class="ilist-item-main">
										<a <?php if( $mask_url == 'on') { echo 'onclick="document.location.href = \''.$item_url.'\'; return false;"'; } ?> <?php echo (isset($list['qcpd_item_nofollow']) && $list['qcpd_item_nofollow'] == 1) ? 'rel="nofollow"' : ''; ?> <?php echo (isset($main_click_action)&&$main_click_action==3?'':'href="'.$masked_url.'"'); ?>" <?php echo (isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1) ? 'target="_blank"' : ''; ?> <?php echo $hover_info; ?> >

												<div class="feature-img-box valign-center">
													<?php
													$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

													$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

													$faviconImgUrl = "";
													$faviconFetchable = false;
													$filteredUrl = "";

													$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

													if( $showFavicon == 1 )
													{
														$filteredUrl = qcpd_remove_http( $item_url );

														if( $item_url != '' )
														{

															$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
														}

														if( $directImgLink != '' )
														{

															$faviconImgUrl = trim($directImgLink);
														}

														$faviconFetchable = true;

														if( $item_url == '' && $directImgLink == '' ){
															$faviconFetchable = false;
														}
													}
													?>

															<!-- Image, If Present -->
													<?php  if( isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>
														<?php 
															if (strpos($list['qcpd_item_img'], 'http') === FALSE){
														?>
														
															<?php
																$img = wp_get_attachment_image_src($list['qcpd_item_img']);
															?>
															<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
														
														<?php
															}else{
														?>
														
															<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
														
														<?php
															}
														?>


													<?php elseif( $iconClass != "" ) : ?>

													<span class="icon fa-icon">
														<i class="fa <?php echo $iconClass; ?>"></i>
													</span>

													<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>


														<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">


													<?php else : ?>

														<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">

													<?php endif; ?>
												</div>
										</a>
										<?php if(isset($list['qcpd_item_subtitle'])): ?>
										<div class="item-desc-text">
												<p>
												<?php echo trim($list['qcpd_item_subtitle']); ?>
												<?php if(isset($list['qcpd_item_phone'])&&$list['qcpd_item_phone']!=''){
												
													echo '<span style="display:block;font-size:11px">';
										
													if(isset($list['qcpd_item_location']) and $list['qcpd_item_location']!=''){
														echo ' <span style="font-weight:bold">'.$list['qcpd_item_location'].'</span> &nbsp;&nbsp;';
													}
													echo ( isset($phone_number) && $phone_number=='1'?'<span class="sbd_phone"><i class="fa fa-phone"></i> '.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']).'</span>':'');
													
													echo '</span>';
													
												} ?>
												
												

												</p>
										</div>
									<?php endif; ?>


										<div class="item-title-text">
												<a <?php if( $mask_url == 'on') { echo 'onclick="document.location.href = \''.$item_url.'\'; return false;"'; } ?> <?php echo (isset($list['qcpd_item_nofollow']) && $list['qcpd_item_nofollow'] == 1) ? 'rel="nofollow"' : ''; ?> <?php echo (isset($main_click_action)&&$main_click_action==3?'':'href="'.$masked_url.'"'); ?>" <?php echo (isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1) ? 'target="_blank"' : ''; ?> <?php echo (isset($outbound_conf) && $outbound_conf == 1) ? 'onclick="trackOutboundLink(\''.$item_url.'\', this); return false;"' : ''; ?> ><h3 class="item-list-title"><?php
													echo trim($list['qcpd_item_title']);
												?></h3></a>

												<div class="bookmark-section-style-7">
							
								<?php 
								$bookmark = 1;
								if(isset($list['qcpd_is_bookmarked']) and $list['qcpd_is_bookmarked']!=''){
									$unv = explode(',',$list['qcpd_is_bookmarked']);
									if(in_array(get_current_user_id(),$unv)){
										$bookmark = 1;
									}
								}
								?>
							
							
								<span data-post-id="<?php echo $list['postid']; ?>" data-item-code="<?php echo trim($list['qcpd_timelaps']); ?>" data-is-bookmarked="<?php echo ($bookmark); ?>" data-li-id="pd_bookmark_li_<?php echo $b; ?>" class="bookmark-btn bookmark-on">
									
									<i class="fa fa-times-circle" aria-hidden="true"></i>
								</span>
								
							</div>
							<div class="pd-bottom-area">
						
								<?php if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!='' and $list['qcpd_item_phone']!=0): ?>
									<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>"><i class="fa fa-phone"></i></a></p>
								<?php endif; ?>
								
								<?php if($list['qcpd_item_link']!=''): ?>
									<p><a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank"><i class="fa fa-link"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><i class="fa fa-facebook"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><i class="fa fa-yelp"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''): ?>
								<p><a data-email="<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>" class="sbd_email_form" href="#"><?php sbd_icons_content('email'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><i class="fa <?php echo (sbd_is_linkedin($list['qcpd_item_linkedin'])?'fa-linkedin-square fa-linkedin':'fa-instagram'); ?>"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><i class="fa fa-twitter-square"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!='' and isset($pdmapofflightbox) && $pdmapofflightbox!='true'): 
								$marker = '';
								if(isset($list['qcpd_item_marker'])){
									$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
								}
								?>
								<p><a href="#" class="pd-map open-mpf-sld-link" full-address="<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>" data-mfp-src="#map-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $marker; ?>><i class="fa fa-map"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_description']) and $list['qcpd_description']!=''): ?>
								<p><a href="#" class="open-mpf-sld-link" data-post-id="<?php echo $list['postid']; ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" data-mfp-src="#busi-<?php echo $list['postid'] ."-". $b; ?>"><i class="fa fa-info-circle"></i></a></p>
								<?php endif; ?>
							
						</div>
							
							
							<div id="map-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
								<div class="pd_map_container" id="mapcontainer-<?php echo get_the_ID() ."-". $count; ?>"></div>
							</div>
				
							<div id="busi-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
								<div class="sbd_business_container">
									Loading...
								</div>
							</div>
										</div>
								</div>
						</div>

					</li>

				<?php $b++; }; ?>
					
				</ul>
<div style="clear:both;"></div>
				

			</div>
		</div>
<?php
	}
	
}
?>
<script type="text/javascript">

var login_url_pd = '<?php echo pd_ot_get_option('pd_bookmark_user_login_url'); ?>';
var template = '<?php echo 'style-7'; ?>';
var bookmark = {
	<?php 
	if ( is_user_logged_in() ) {
	?>
	is_user_logged_in:true,
	<?php
	} else {
	?>
	is_user_logged_in:false,
	<?php
	}
	?>
	userid: <?php echo get_current_user_id(); ?>

};
</script>