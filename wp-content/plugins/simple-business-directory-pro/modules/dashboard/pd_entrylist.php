<?php 

if(isset($_GET['did']) and $_GET['did']!=''){

    $this->delete_subscriber_profile($_GET['did']);
    echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 10px;">Your Item has been Deleted sucessfully. <br/></div>';

}
$s       = 1;
$rows     = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table WHERE %d and user_id=".$current_user->ID, $s ) );

?>
<h2><?php echo (pd_ot_get_option('sbd_lan_link_list_page')!=''?pd_ot_get_option('sbd_lan_link_list_page'):__('Listing Page', 'qc-pd')); ?></h2>
<div class="qc_pd_table_area">
  <div class="qc_pd_table">
	
	<div class="qc_pd_row pd_header">
	  
	  <div class="qc_pd_cell qc_pd_table_head">
		<?php _e('Image', 'qc-pd') ?>
	  </div>
	  <div class="qc_pd_cell qc_pd_table_head">
		<?php echo (pd_ot_get_option('sbd_lan_listing_title')!=''?pd_ot_get_option('sbd_lan_listing_title'):__('Listing Title', 'qc-pd')); ?>
	  </div>

	  <div class="qc_pd_cell qc_pd_table_head">
		<?php echo (pd_ot_get_option('sbd_lan_listing_subtitle')!=''?pd_ot_get_option('sbd_lan_listing_subtitle'):__('Listing Subtitle', 'qc-pd')); ?>
	  </div>
	  <div class="qc_pd_cell qc_pd_table_head">
		<?php echo (pd_ot_get_option('sbd_lan_category')!=''?pd_ot_get_option('sbd_lan_category'):__('Category', 'qc-pd')); ?>
	  </div>
	  <div class="qc_pd_cell qc_pd_table_head">
		<?php echo (pd_ot_get_option('sbd_lan_list')!=''?pd_ot_get_option('sbd_lan_list'):__('List', 'qc-pd')); ?>
	  </div>

        <div class="qc_pd_cell qc_pd_table_head">
			<?php _e('Package', 'qc-pd'); ?>
        </div>

	  <div class="qc_pd_cell qc_pd_table_head">
		<?php _e('Status', 'qc-pd'); ?>
	  </div>
	  
	  <div class="qc_pd_cell qc_pd_table_head">
		<?php _e('Action', 'qc-pd'); ?>
	  </div>
	</div>
<?php
$c=0;
foreach($rows as $row):

$c++;
?>

	<div class="qc_pd_row">
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo pd_ot_get_option('sbd_lan_image')!=''?pd_ot_get_option('sbd_lan_image'):__('Image', 'qc-pd'); ?></div>
		<a href="<?php echo $row->item_link; ?>" target="_blank" title="<?php echo $row->item_link; ?>"><?php 
			echo $this->getImage($row->image_url);
		?></a>
	  </div>
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo (pd_ot_get_option('sbd_lan_listing_title')!=''?pd_ot_get_option('sbd_lan_listing_title'):__('Listing Title', 'qc-pd')); ?></div>
		<?php echo wp_unslash($row->item_title); ?>
	  </div>
	 
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo (pd_ot_get_option('sbd_lan_listing_subtitle')!=''?pd_ot_get_option('sbd_lan_listing_subtitle'):__('Listing Subtitle', 'qc-pd')); ?></div>
		<?php echo ( $row->item_subtitle ); ?>
	  </div>
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo (pd_ot_get_option('sbd_lan_category')!=''?pd_ot_get_option('sbd_lan_category'):__('Category', 'qc-pd')); ?></div>
		<?php echo ($row->category) ?>
	  </div>
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo (pd_ot_get_option('sbd_lan_list')!=''?pd_ot_get_option('sbd_lan_list'):__('List', 'qc-pd')); ?></div>
		<?php echo get_the_title( $row->pd_list ); ?>
	  </div>

        <div class="qc_pd_cell">
            <div class="pd_responsive_head"><?php echo __('Package', 'qc-pd') ?></div>
			<?php
                if($row->package_id=='0'){
                    echo 'Free';
                }else{
					$package_purchase = $wpdb->get_row("SELECT * FROM $package_purchased_table WHERE 1 and id=".$row->package_id );
	                $package     = $wpdb->get_row("SELECT * FROM $package_table WHERE 1 and id=".$package_purchase->package_id );
	                echo $package->title;
                }
            ?>
        </div>

	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo __('Status', 'qc-pd') ?></div>
		<?php echo $this->getStatus($row->approval) ?>
	  </div>
	  
	  <div class="qc_pd_cell">
	  <div class="pd_responsive_head"><?php echo __('Action', 'qc-pd') ?></div>
		<a href="<?php echo esc_url( add_query_arg( array('action'=>'entryedit','id'=>$row->id), $url ) ); ?>"><button class="entry_list_edit"><?php echo __('Edit', 'qc-pd') ?></button></a>
		
		<?php $sbd_lan_package_dlt_confirm_text = pd_ot_get_option('sbd_lan_package_dlt_confirm_text') != '' ? pd_ot_get_option('sbd_lan_package_dlt_confirm_text') : __('Are you sure to delete this Record?', 'qc-pd') ?>
		<?php if( pd_ot_get_option('pd_prevent_user_delete_link') != 'on' ){ ?>
			<a title="delete" class="delete" onclick="return confirm('<?php echo $sbd_lan_package_dlt_confirm_text; ?>')" href="<?php echo esc_url( add_query_arg( array('action'=>'entrylist','did'=>$row->id), $url ) ); ?>"><button class="entry_list_delete"><?php echo __('Delete', 'qc-pd') ?></button></a>
		<?php } ?>
	  </div>
	  
	</div>
  <?php 
  endforeach;
  ?>

  </div>

</div>