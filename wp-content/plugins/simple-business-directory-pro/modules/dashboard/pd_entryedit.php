<?php 
 //code for form data entry
$flagg = false;
if(isset($_GET['id']) && $_GET['id']!=''){
$current_user = wp_get_current_user();
	if(isset($_POST['uid']) and $_POST['uid']!=''){
		
		$uid = $_POST['uid'];
		
		$sql = "SELECT * FROM {$wpdb->prefix}pd_user_entry where 1 and id = ".$uid;
		$pdata = $wpdb->get_row($sql);
		
		$item_title = sanitize_text_field($_POST['item_title']);
		$item_link = sanitize_text_field($_POST['item_link']);
		$item_subtitle = sanitize_text_field($_POST['item_subtitle']);
		

		$item_phone = sanitize_text_field($_POST['item_phone']);
		$item_location = sanitize_text_field($_POST['item_location']);
		$item_full_address = sanitize_text_field($_POST['item_full_address']);
		$item_latitude = sanitize_text_field($_POST['item_latitude']);
		$item_longitude = sanitize_text_field($_POST['item_longitude']);
		$item_linkedin = sanitize_text_field($_POST['item_linkedin']);
		$item_twitter = sanitize_text_field($_POST['item_twitter']);
		$item_facebook = sanitize_text_field($_POST['item_facebook']);
		$item_tags = sanitize_text_field($_POST['item_tags']);
		$item_yelp = sanitize_text_field($_POST['item_yelp']);
		$item_business_hour = sanitize_text_field($_POST['item_business_hour']);
		$item_email = sanitize_text_field($_POST['item_email']);
		$prev_image = esc_url_raw($_POST['prev_image']);
		$item_description = ($_POST['item_long_description']);
		
		if(isset($_POST['item_no_follow']) and $_POST['item_no_follow']==1){
			$item_no_follow = 1;
		}else{
			$item_no_follow = 0;
		}

	/* Image upload script */
	$file_name = '';
	$errors= array();
	$upload_dir = wp_upload_dir();
	if(isset($_FILES['sbd_link_image']) and $_FILES['sbd_link_image']['name']!=''){
	  $file_name = $_FILES['sbd_link_image']['name'];
	  $file_size =$_FILES['sbd_link_image']['size'];
	  $file_tmp =$_FILES['sbd_link_image']['tmp_name'];
	  $file_type=$_FILES['sbd_link_image']['type'];
	  
	  $file_ext=strtolower(end(explode('.',$_FILES['sbd_link_image']['name'])));
	  $custom_name = strtolower(explode('.',$_FILES['sbd_link_image']['name'])[0]);
	  $file_name = $custom_name.'_'.time().'.'.$file_ext;
	  
	  $expensions= array("jpeg","jpg","png","gif");
	  
	  if(in_array($file_ext,$expensions)=== false){
		 $errors[]="Extension not allowed, please choose a JPEG or PNG file.";
	  }
	  
	  if($file_size > 2097152){
		 $errors[]='File size must be excately 2 MB';
	  }
	  
	  if(empty($errors)==true){
		 move_uploaded_file($file_tmp,$upload_dir['path']."/".$file_name);
		 
		 	$img_path = $upload_dir['path'];
			$img_name = $file_name;
			$image = wp_get_image_editor( $img_path .'/'. $img_name );
			if ( ! is_wp_error( $image ) ) {
				$image->resize( 300, 300, true );
				$image->save( $img_path .'/'. $img_name );
			}
		 
	  }else{
		  $file_name='';
	  }
	}
		
		
		if($file_name!=''){
			$imageurl = $upload_dir['url'].'/'.$file_name;
		}else{
			if($prev_image==''){
				$imageurl = '';
			}else{
				$imageurl = $prev_image;
			}
			
		}


		$qc_pd_category = isset($_POST['qc_pd_category']) ? sanitize_text_field($_POST['qc_pd_category']) : '';
		$qc_pd_list = sanitize_text_field($_POST['qc_pd_list']);
		$datetime = date('Y-m-d H:i:s');
		$package_id = $_POST['package_id'];
		
		$qcpd_field_1 = '';
		if(isset($_POST['qcpd_field_1']) && $_POST['qcpd_field_1']!=''){
			$qcpd_field_1 = sanitize_text_field($_POST['qcpd_field_1']);
		}
		$qcpd_field_2 = '';
		if(isset($_POST['qcpd_field_2']) && $_POST['qcpd_field_2']!=''){
			$qcpd_field_2 = sanitize_text_field($_POST['qcpd_field_2']);
		}
		$qcpd_field_3 = '';
		if(isset($_POST['qcpd_field_3']) && $_POST['qcpd_field_3']!=''){
			$qcpd_field_3 = sanitize_text_field($_POST['qcpd_field_3']);
		}
		$qcpd_field_4 = '';
		if(isset($_POST['qcpd_field_4']) && $_POST['qcpd_field_4']!=''){
			$qcpd_field_4 = sanitize_text_field($_POST['qcpd_field_4']);
		}
		$qcpd_field_5 = '';
		if(isset($_POST['qcpd_field_5']) && $_POST['qcpd_field_5']!=''){
			$qcpd_field_5 = sanitize_text_field($_POST['qcpd_field_5']);
		}
		
		
		//Image delete code
		
		if($pdata->pd_list!=$qc_pd_list){
			$this->deny_subscriber_profile($uid);
		}
		
		
		$wpdb->update(
			$table,
			array(
				'item_title'  => $item_title,
				'item_link'   => $item_link,
				'item_subtitle' => $item_subtitle,
				'item_phone'	=> $item_phone,
				'location'		=> $item_location,
				'full_address'	=> $item_full_address,
				'latitude'		=> $item_latitude,
				'longitude'		=> $item_longitude,
				'linkedin'		=> $item_linkedin,
				'twitter'		=> $item_twitter,
				'facebook'		=> $item_facebook,
				'yelp'			=> $item_yelp,
				'business_hour'	=> $item_business_hour,
				'email'			=> $item_email,				
				'category'   => $qc_pd_category,
				'pd_list'  => $qc_pd_list,
				'user_id'=> $current_user->ID,
				'image_url'=> $imageurl,
				'time'=> $datetime,
				'nofollow'=> $item_no_follow,
				'approval'=> 3,
				'description'=>$item_description,
				'qcpd_field_1'=>$qcpd_field_1,
				'qcpd_field_2'=>$qcpd_field_2,
				'qcpd_field_3'=>$qcpd_field_3,
				'qcpd_field_4'=>$qcpd_field_4,
				'qcpd_field_5'=>$qcpd_field_5,
				'tags'=>$item_tags
			),
			array( 'id' => $uid),
			array(
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
			),
			array( '%d')
		);
		$listing_approve_autoapprove_notify_text = pd_ot_get_option('sbd_lan_package_updated_success_notification') != '' ? pd_ot_get_option('sbd_lan_package_updated_success_notification') : __('Your link has been updated sucessfully', 'qc-pd');

		$sbd_lan_package_approval_waiting_notification = pd_ot_get_option('sbd_lan_package_approval_waiting_notification') != '' ? pd_ot_get_option('sbd_lan_package_approval_waiting_notification') : __('Your link has been updated! Waiting for approval. ', 'qc-pd');

		if(in_array('administrator',$current_user->roles)){
			$this->approve_subscriber_profile($uid);
			echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 10px;">'. $listing_approve_autoapprove_notify_text .' <br/></div>';
        }else{
			$this->pd_edit_item_notification($current_user->ID, $item_title);
			if(pd_ot_get_option('pd_enable_auto_approval')=='on'){

				$this->approve_subscriber_profile($uid);
				echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 10px;">'. $listing_approve_autoapprove_notify_text .'<br/></div>';
			}else{
				echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 10px;">'.$sbd_lan_package_approval_waiting_notification.'<br/></div>';
			}
        }

	if(!empty($errors)){
		foreach($errors as $error){
			echo '<div style="color: red;border: 1px solid red;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">'.$error.'</div>';
		}
	}
		
}


$recid = sanitize_text_field($_GET['id']);
$s = 1;
$row     = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table WHERE %d and id=$recid", $s ) );

?>
<h2><?php echo (pd_ot_get_option('sbd_lan_link_edit_form')!=''?pd_ot_get_option('sbd_lan_link_edit_form'):__('Listings Edit Form', 'qc-pd')) ?> </h2>
<form class="sbd-frontend-submission-form" action="" method="POST" enctype="multipart/form-data">
	<ul class="pd_form-style-1 pd_width">
		
		<li><label><?php echo (pd_ot_get_option('sbd_lan_select_package')!=''?pd_ot_get_option('sbd_lan_select_package'):__('Select Package', 'qc-pd')) ?> <span class="pd_required">*</span></label>
            <select name="package_id">
            	<?php $package_none_text = pd_ot_get_option('sbd_lan_none') != '' ? pd_ot_get_option('sbd_lan_none') : __('None', 'qc-pd'); ?>
                <option value=""><?php echo $package_none_text; ?></option>
				<?php

				$submited_item = $wpdb->get_row("select count(*)as cnt from $table where 1 and package_id = 0 and user_id =".$current_user->ID);
				if(pd_ot_get_option('pd_enable_free_submission')=='on'){
					if(pd_ot_get_option('pd_free_item_limit')!='' and pd_ot_get_option('pd_free_item_limit') > $submited_item->cnt){
						if($row->package_id==0){
							echo '<option value="0" selected="selected">'.(pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')).'</option>';
						}else{
							echo '<option value="0">'.(pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')).'</option>';
						}

					}
				}
                


				?>

				<?php
				$pkglist = $wpdb->get_results("select ppt.id as id, ppt.expire_date as expiredate, pt.title, pt.item as total_item from $package_purchased_table as ppt, $package_table as pt where 1 and ppt.user_id = ".$current_user->ID." and ppt.package_id = pt.id order by ppt.date DESC");

				foreach($pkglist as $r){
					$submited_item = $wpdb->get_row("select count(*)as cnt from $table where 1 and package_id = ".$r->id." and user_id =".$current_user->ID);
					if(strtotime(date('Y-m-d')) < strtotime($r->expiredate) and $r->total_item > $submited_item->cnt){
					    if($row->package_id==$r->id){
					        echo '<option value="'.$r->id.'" selected="selected">'.$r->title.'</option>';
                        }else{
						    echo '<option value="'.$r->id.'">'.$r->title.'</option>';
                        }

					}
				}

				?>
            </select>

        </li>
		<?php if(pd_ot_get_option('pd_exclude_category')!='on'): ?>
		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_category')!=''?pd_ot_get_option('sbd_lan_category'):__('Category', 'qc-pd')); ?></label>
			
			<?php 
			$taxonomy = 'pd_cat';
			$terms = get_terms($taxonomy); //
			$excludel = pd_ot_get_option('pd_exclude_category_id');
			if ( $terms && !is_wp_error( $terms ) ) :
			?>
				<select id="qc_pd_category" class="pd_text_width" name="qc_pd_category" >
					<option value="" ><?php echo (pd_ot_get_option('sbd_lan_all_list')!=''?pd_ot_get_option('sbd_lan_all_list'):__('All List', 'qc-pd')); ?></option>
					<?php foreach ( $terms as $term ) { ?>
					
						<?php 
						if($excludel!=''){
							$exclude = explode(',',$excludel);
							if(!in_array($term->term_id,$exclude)){
								?>
								<?php if($term->name==$row->category): ?>
									<option value="<?php echo $term->name; ?>"selected="selected"><?php echo esc_attr($term->name); ?></option>
								<?php else: ?>
									<option value="<?php echo $term->name; ?>"><?php echo esc_attr($term->name); ?></option>
								<?php endif; ?>
								<?php
							}
						}else{
						?>
							<?php if($term->name==$row->category): ?>
								<option value="<?php echo $term->name; ?>"selected="selected"><?php echo esc_attr($term->name); ?></option>
							<?php else: ?>
								<option value="<?php echo $term->name; ?>"><?php echo esc_attr($term->name); ?></option>
							<?php endif; ?>
						<?php
						}
						?>
					
						
						
						
						
					<?php } ?>
				</select>
			<?php
			endif;
			?>
		</li>
		<?php endif; ?>
		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_select_list')!=''?pd_ot_get_option('sbd_lan_select_list'):__('Select List', 'qc-pd')); ?> <span class="pd_required">*</span></label>
			<select id="qc_pd_list" class="pd_text_width" name="qc_pd_list" required>
				<?php
					$excludel = pd_ot_get_option('pd_exclude_list');
					$exclude2 = pd_ot_get_option('pd_exclude_category_id');
					if($row->category!=''){
						$pd = new WP_Query( array( 
							'post_type' => 'pd',
							'tax_query' => array(
								array (
									'taxonomy' => 'pd_cat',
									'field' => 'name',
									'terms' => $row->category,
								)
							),
							'posts_per_page' => -1,
							'order' => 'ASC',
							'orderby' => 'menu_order'
							) 
						);
					}else{
						$query = array( 
							'post_type' => 'pd',
							'posts_per_page' => -1,
							'order' => 'ASC',
							'orderby' => 'menu_order'
							);
							
							if($exclude2!=''){
								$query['tax_query'] = array(
									array(
										'taxonomy' => 'pd_cat',
										'field'    => 'ID',
										'terms'    => explode(',', $exclude2),
										'operator' => 'NOT IN',
									),
								);
							}
						$pd = new WP_Query($query);
						
					}

					while( $pd->have_posts() ) : $pd->the_post();
					?>
						
						<?php 
						if($excludel!=''){
							$exclude = explode(',',$excludel);
							if(!in_array(get_the_ID(),$exclude)){
								?>
								<?php if(get_the_ID()==$row->pd_list): ?>
									<option value="<?php echo get_the_ID(); ?>" selected="selected"><?php the_title(); ?></option>
								<?php else: ?>
									<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php endif; ?>
								
								<?php
							}
						}else{
						?>
						
						<?php if(get_the_ID()==$row->pd_list): ?>
							<option value="<?php echo get_the_ID(); ?>" selected="selected"><?php the_title(); ?></option>
						<?php else: ?>
							<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
						<?php endif; ?>
						
						<?php
						}
						?>
						
						
						
					<?php
					endwhile;
				?>
			</select>
		</li>
		<li <?php echo ($flagg==false && get_option('sbd_website_link')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_link_include')!=''?pd_ot_get_option('sbd_lan_link_include'):__('Link (Include http:// or https://)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_link_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_link_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" id="sbd_item_link" name="item_link" class="field-long pd_text_width" value="<?php echo esc_url($row->item_link); ?>" />
		</li>
		<li <?php echo ($flagg==false && get_option('sbd_generate_info')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_generate_info_link')!=''?pd_ot_get_option('sbd_lan_generate_info_link'):__('Generator info from Link', 'qc-pd')); ?> </label>
            <input type="button" id="sbd_generate" class="" value="<?php echo pd_ot_get_option('sbd_lan_generate_button_text')!=''?pd_ot_get_option('sbd_lan_generate_button_text'):__('Generate','qc-pd'); ?>" />
        </li>
		
		<li><label><?php echo (pd_ot_get_option('sbd_lan_listing_title')!=''?pd_ot_get_option('sbd_lan_listing_title'):__('Listings Title', 'qc-pd')); ?> <span class="pd_required">*</span></label><input type="text" name="item_title" id="sbd_title" class="field-long pd_text_width" value="<?php echo esc_html(wp_unslash($row->item_title)); ?>" required/></li>
		
		<li <?php echo ($flagg==false && get_option('sbd_subtitle')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_subtitle')!=''?pd_ot_get_option('sbd_lan_listing_subtitle'):__('Listing Subtitle', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_subtitle_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_subtitle_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" id="sbd_subtitle" name="item_subtitle" class="field-long pd_text_width" value="<?php echo esc_html($row->item_subtitle); ?>"  />
		</li>
		<li <?php echo ($flagg==false && get_option('sbd_long_description')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_long')!=''?pd_ot_get_option('sbd_lan_listing_long'):__('Listing Long Description', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_long_description_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			
			
			<?php if(pd_ot_get_option('pd_enable_texteditor')=='on'):?>
				<?php wp_editor($row->description, 'pd_text_width', array('textarea_name' =>
				'item_long_description',
				'textarea_rows' => 20,
				'editor_height' => 100,
				'disabled' => 'enabled',
				'media_buttons' => false,
				'tinymce'       => array(
				'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink',)
				)); ?>
				
			<?php else: ?>
				<textarea <?php if( pd_ot_get_option('mark_listing_long_description_field_as_required') == 'on' ){ echo 'required'; } ?> class="field-long pd_text_width" name="item_long_description"><?php echo ($row->description); ?></textarea>
			<?php endif; ?>
		</li>
		<li <?php echo ($flagg==false && get_option('sbd_phone')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_main_phone')!=''?pd_ot_get_option('sbd_lan_main_phone'):__('Main Phone Number (For call)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_main_phone_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_main_phone_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_phone" class="field-long pd_text_width" value="<?php echo esc_html($row->item_phone); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_short_address')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_short_address')!=''?pd_ot_get_option('sbd_lan_short_address'):__('Short Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_short_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_short_address_field_as_required') == 'on' ){ echo 'required'; } ?>  type="text" name="item_location" class="field-long pd_text_width" value="<?php echo esc_html($row->location); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_full_address')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_full_address_map')!=''?pd_ot_get_option('sbd_full_address_map'):__('Full Address (For google maps)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_full_address_field_as_required') != 'off' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_full_address_field_as_required') != 'off' ){ echo 'required'; } ?> type="text" name="item_full_address" id="sbd_full_address" class="field-long pd_text_width" value="<?php echo esc_html($row->full_address); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_latitude')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_latitude')!=''?pd_ot_get_option('sbd_lan_latitude'):__('Latitude', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_latitude_field_as_required') != 'off' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_latitude_field_as_required') != 'off' ){ echo 'required'; } ?> type="text" name="item_latitude" id="sbd_lat" class="field-long pd_text_width" value="<?php echo esc_html($row->latitude); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_longitude')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_longitude')!=''?pd_ot_get_option('sbd_lan_longitude'):__('Longitude', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_longitude_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_longitude_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_longitude" id="sbd_long" class="field-long pd_text_width" value="<?php echo esc_html($row->longitude); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_linkedin')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_linkedin')!=''?pd_ot_get_option('sbd_lan_linkedin'):__('Linkedin/instagram Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_linkedin_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_linkedin_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_linkedin" class="field-long pd_text_width" value="<?php echo esc_html($row->linkedin); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_twitter')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_twitter')!=''?pd_ot_get_option('sbd_lan_twitter'):__('Twitter Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_twitter_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_twitter_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_twitter" class="field-long pd_text_width" value="<?php echo esc_html($row->twitter); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_email')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_email_address')!=''?pd_ot_get_option('sbd_lan_email_address'):__('Email Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_email_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_email_field_as_required') == 'on' ){ echo 'required'; } ?> type="email" name="item_email" class="field-long pd_text_width" value="<?php echo esc_html($row->email); ?>" placeholder="email@example.com"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_business_hour')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_business_hour')!=''?pd_ot_get_option('sbd_lan_business_hour'):__('Business Hours', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_business_hours_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_business_hours_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_business_hour" class="field-long pd_text_width" value="<?php echo esc_html($row->business_hour); ?>" placeholder="Mon - Fri 10:00am - 05:00pm"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_yelp')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_yelp')!=''?pd_ot_get_option('sbd_lan_yelp'):__('Yelp Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_yelp_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_yelp_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_yelp" class="field-long pd_text_width" value="<?php echo esc_html($row->yelp); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_facebook')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_facebook')!=''?pd_ot_get_option('sbd_lan_facebook'):__('Facebook Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_fb_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_fb_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_facebook" class="field-long pd_text_width" value="<?php echo esc_html($row->facebook); ?>"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_tags')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_tags')!=''?pd_ot_get_option('sbd_lan_tags'):__('Tags', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_tags" id="item_tags" class="field-long pd_text_width" value="<?php echo esc_html($row->tags); ?>"  />
		</li>
		
		<?php if(pd_ot_get_option('sbd_field_1_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_1_label'); ?> </label>
			<input type="text" name="qcpd_field_1" class="field-long pd_text_width" value="<?php echo esc_html($row->qcpd_field_1); ?>"  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_2_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_2_label'); ?> </label>
			<input type="text" name="qcpd_field_2" class="field-long pd_text_width" value="<?php echo esc_html($row->qcpd_field_2); ?>"  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_3_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_3_label'); ?> </label>
			<input type="text" name="qcpd_field_3" class="field-long pd_text_width" value="<?php echo esc_html($row->qcpd_field_3); ?>"  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_4_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_4_label'); ?> </label>
			<input type="text" name="qcpd_field_4" class="field-long pd_text_width" value="<?php echo esc_html($row->qcpd_field_4); ?>"  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_5_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_5_label'); ?> </label>
			<input type="text" name="qcpd_field_5" class="field-long pd_text_width" value="<?php echo esc_html($row->qcpd_field_5); ?>"  />
		</li>
		
		<?php } ?>
		
        
	<?php if(pd_ot_get_option('pd_image_upload')=='on'){ ?>
		<li <?php echo ($flagg==false && get_option('sbd_image')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_image')!=''?pd_ot_get_option('sbd_lan_listing_image'):__('Listings Image', 'qc-pd')); ?></label>
			
			<input type="file" name="sbd_link_image" id="sbd_link_image" >
			<br><span><?php echo (pd_ot_get_option('sbd_lan_max_resolu')!=''?pd_ot_get_option('sbd_lan_max_resolu'):__('Maximum image size 1MB. Resolution 300x300.','qc-pd')); ?></span>
			<div style="clear:both"></div>
			<div id="pd_preview_img">
				<?php if($row->image_url!=''): ?>
					<span class="pd_remove_bg_image">X</span>
					<?php echo $this->getImage($row->image_url); ?>
				<?php endif; ?>
			</div>
			<input type="hidden" name="prev_image" id="sbd_prev_image" value="<?php echo $row->image_url; ?>" />
		</li>
	<?php } ?>
		
		
		<?php if(pd_ot_get_option('pd_disable_no_follow')!='on'){ ?>
        <li <?php echo ($flagg==false && get_option('sbd_no_follow')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_no_follow')!=''?pd_ot_get_option('sbd_lan_no_follow'):__('No Follow', 'qc-pd')); ?> </label>
            <input type="checkbox" name="item_no_follow" <?php echo ($row->nofollow==1?'checked="checked"':''); ?> class="" value="1" />
        </li>
        <?php }else{
?>
		<li <?php echo ($flagg==false && get_option('sbd_no_follow')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_no_follow')!=''?pd_ot_get_option('sbd_lan_no_follow'):__('No Follow', 'qc-pd')); ?> </label>
            <input type="checkbox" name="item_no_follow" class="" value="1" <?php echo ($row->nofollow==1?'checked="checked"':''); ?> disabled="" />
        </li>
<?php

        	} ?>
		<li>
			<input type="hidden" name="uid" value="<?php echo $recid; ?>" />
			<input type="submit" class="pd_submit_style" value="<?php echo (pd_ot_get_option('sbd_lan_submit')!=''?pd_ot_get_option('sbd_lan_submit'):__('Submit','qc-pd')); ?>" />
		</li>
	</ul>
</form>
<?php 
}else{
	echo __('<p>Something Went Wrong.</p>','qc-pd');
}
?>