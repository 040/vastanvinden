<?php 
 //code for form data entry
global $wp;
$current_url =  home_url( $wp->request );
$current_user = wp_get_current_user();
//Is package purchased
$pp = $wpdb->get_row( "SELECT * FROM $package_purchased_table WHERE 1 and user_id = '".$current_user->ID."' and `expire_date` > '".date('Y-m-d')."'" );


$flagg = false;
if(empty($pp)){
	$flagg = false;
}else{
	$flagg = true;
}

if(isset($_GET['msg']) && $_GET['msg']=='success'){

	global $wp;
	$current_url =  home_url( $wp->request );
	$current_user = wp_get_current_user();

	//var_dump($current_user->user_email);

	$email   = $current_user->user_email ? $current_user->user_email : '';

	//Extract Domain
	$url = get_site_url();
	$url = parse_url($url);
	$domain = $url['host'];

	$admin_email = get_option('admin_email');
   	$toEmail = $email;
   	$fromEmail = "wordpress@" . $domain;

	$bodyContent = "";
	$bodyContent .= '<p class="qc_sbd_success_msg">'.(pd_ot_get_option('sbd_lan_payment_thanks')!=''?pd_ot_get_option('sbd_lan_payment_thanks'):__('Thanks for your payment. You can now add your listing below. I wish you success with your advertising.','qc-pd')).'</p>';

	$bodyContent .= '<p>'.$current_url.'</p>';

    $to = $toEmail;
    $body = $bodyContent;

    // sbd_lan_sbd_payments
    $sbd_lan_sbd_payments = pd_ot_get_option('sbd_lan_sbd_payments') != '' ? pd_ot_get_option('sbd_lan_sbd_payments') : 'SBD Payment';

    $name = sprintf(__('[%s] %s','qc-pd'), get_option('blogname'), $sbd_lan_sbd_payments);
    $subject = sprintf(__('[%s] %s','qc-pd'), get_option('blogname'), $sbd_lan_sbd_payments);

    $headers = array();
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $headers[] = 'From: ' . esc_html($name) . ' <' . esc_html($fromEmail) . '>';
    $headers[] = 'Reply-To: <' . esc_html($admin_email) . '>';

    wp_mail($to, $subject, $body, $headers);


	echo '<p class="qc_sbd_success_msg">'.(pd_ot_get_option('sbd_lan_payment_thanks')!=''?pd_ot_get_option('sbd_lan_payment_thanks'):__('Thanks for your payment. You can now add your listing below. I wish you success with your advertising.','qc-pd')).'</p>';


}

if( !$flagg && pd_ot_get_option('pd_enable_free_submission')=='off' && pd_ot_get_option('pd_package_add_tab')!='on'){
?>
	
	<?php
	if(pd_ot_get_option('sbd_enable_paypal_test_mode')=='on'){
		$mainurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	}else{
		$mainurl = 'https://www.paypal.com/cgi-bin/webscr';
	}				
	$packages = $wpdb->get_results("select * from $package_table where 1");
	?>
	<?php if(!empty($packages)): ?>
	<div class="sbd_price-table-wrapper">

		<h2><?php echo (pd_ot_get_option('sbd_lan_paid_pkg')!=''?pd_ot_get_option('sbd_lan_paid_pkg'):__('Available Packages','qc-pd')) ?></h2>
		
		<?php if(pd_ot_get_option('pd_enable_free_submission')=='on'): ?>
		  <div class="sbd_pricing-table">
			<h2 class="sbd_pricing-table__header"><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></h2>

			<ul class="sbd_pricing-table__list">
			  <li><?php echo (pd_ot_get_option('sbd_lan_free_listing')!=''?pd_ot_get_option('sbd_lan_free_listing'):__('Free Listing', 'qc-pd')) ?></li>
			  <li><?php echo pd_ot_get_option('pd_free_item_limit'); ?> <?php echo (pd_ot_get_option('sbd_lan_listing')!=''?pd_ot_get_option('sbd_lan_listing'):__('Listing', 'qc-pd')) ?></li>
			  <li><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></li>
			  <li>-</li>
			  
			  
			</ul>
		  </div>
		<?php endif; ?>
		
		<?php
		$pc = 0;
		foreach($packages as $package):
		$pc++;
		?>
		
	  <div class="sbd_pricing-table" <?php echo ($pc==1?'style="margin-left: 0px;"':''); ?>>
		<h2 class="sbd_pricing-table__header"><?php echo (isset($package->title)&&$package->title!=''?$package->title:''); ?></h2>

		<ul class="sbd_pricing-table__list">
		  <li><?php echo (isset($package->description)&&$package->description!=''?html_entity_decode($package->description):''); ?></li>
		  <li><?php echo (isset($package->item)&&$package->item!=''?$package->item:'0'); ?> <?php echo (pd_ot_get_option('sbd_lan_listing_for')!=''?pd_ot_get_option('sbd_lan_listing_for'):__('Listing for','qc-pd')) ?> <?php echo (isset($package->duration)&&$package->duration!='lifetime'?sprintf( _n( '%s Month', '%s Months', $package->duration, 'qc-pd' ), $package->duration ):ucwords($package->duration)); ?></li>
		  <li><?php echo (isset($package->Amount)&&$package->Amount!=''?$package->Amount:'0'); ?> <?php echo $package->currency; ?></li>
		  <li>
		  <?php if(pd_ot_get_option('sbd_enable_paypal_payment')!='off'): ?>
			  <form action="<?php echo $mainurl; ?>" method="post" id="paypalProcessor">
				<input type="hidden" name="cmd" value="_xclick" />

				<input type="hidden" name="business" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
				<input type="hidden" name="currency_code" value="<?php echo $package->currency; ?>" />
				<input type="hidden" name="no_note" value="1"/>
				<input type="hidden" name="no_shipping" value="1" />
				<input type="hidden" name="charset" value="utf-8" />

				<input type="hidden" name="notify_url" value="<?php echo esc_url( add_query_arg( array('user'=> $current_user->ID, 'packagesave'=>$package->id), $url ) ) ?>" />

				<input type="hidden" name="return" value="<?php echo esc_url( add_query_arg( 'payment', 'success', $url ) ) ?>" />

				<input type="hidden" name="cancel_return" value="<?php echo esc_url( add_query_arg( 'payment', 'cancel', $url ) ) ?>">
				<input type="hidden" name="item_name" value="<?php echo $package->title; ?>">
				<input type="hidden" name="amount" value="<?php echo (isset($package->Amount)&&$package->Amount!=''?$package->Amount:'0'); ?>">

				<input type="hidden" name="quantity" value="1">
				<input type="hidden" name="receiver_email" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
				<input type="image" name="submit" border="0"  src="<?php echo QCSBD_IMG_URL.'/btn_buynow_LG.gif'; ?>" alt="PayPal - The safer, easier way to pay online">
				<p style="margin: 0px 0px;padding: 0px;color: #000;font-size: 14px;margin-top: -6px;"><?php echo (pd_ot_get_option('sbd_lan_paypal')!=''?pd_ot_get_option('sbd_lan_paypal'):__('Paypal', 'qc-pd')); ?></p>
			  </form>
			 <?php endif; ?>
			 <?php if(pd_ot_get_option('sbd_enable_stripe_payment')=="on"): ?>
			  <form action="<?php echo esc_url( add_query_arg( array('payment'=> 'stripe-save', 'userid'=>$current_user->ID, 'package'=> $package->id), $url ) ) ?>" method="post">
					<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						  data-key="<?php echo pd_ot_get_option('sbd_stripe_public_key'); ?>"
						  data-description="<?php echo $package->title; ?>"
						  data-amount="<?php echo (isset($package->Amount)&&$package->Amount!=''?($package->Amount*100):'0'); ?>"
						  data-locale="auto"
						  data-currency="<?php echo $package->currency; ?>"	
						  ></script>
					<p style="margin: 0px 0px;padding: 0px;color: #000;font-size: 14px;"><?php echo (pd_ot_get_option('sbd_lan_stripe')!=''?pd_ot_get_option('sbd_lan_stripe'):__('Stripe', 'qc-pd')); ?></p>
				</form>
			<?php endif; ?>
		  </li>
		</ul>
	  </div>
	  <?php endforeach; ?>
	  
	</div>
	<?php endif; ?>
	
<?php	
}else{
	


if(isset($_POST['item_title']) and $_POST['item_title']!='' and $_POST['package_id']!=''){
	

	$item_title = sanitize_text_field($_POST['item_title']);
	$item_link = sanitize_text_field($_POST['item_link']);
	$item_subtitle = sanitize_text_field($_POST['item_subtitle']);
	
	
	$item_description = ($_POST['item_long_description']);
	if(isset($_POST['item_no_follow']) and $_POST['item_no_follow']==1){
	    $item_no_follow = 1;
    }else{
		$item_no_follow = 0;
    }

/* Image upload script */
$file_name = '';
$errors= array();
$upload_dir = wp_upload_dir();
if(isset($_FILES['sbd_link_image']) and $_FILES['sbd_link_image']['name']!=''){
  $file_name = $_FILES['sbd_link_image']['name'];
  $file_size =$_FILES['sbd_link_image']['size'];
  $file_tmp =$_FILES['sbd_link_image']['tmp_name'];
  $file_type=$_FILES['sbd_link_image']['type'];
  
  $file_ext=strtolower(end(explode('.',$_FILES['sbd_link_image']['name'])));
  $custom_name = strtolower(explode('.',$_FILES['sbd_link_image']['name'])[0]);
  $file_name = $custom_name.'_'.time().'.'.$file_ext;
  
  $expensions= array("jpeg","jpg","png","gif");
  
  if(in_array($file_ext,$expensions)=== false){
	 $errors[]="Extension not allowed, please choose a JPEG or PNG file.";
  }
  
  if($file_size > 2097152){
	 $errors[]='File size must be excately 2 MB';
  }
  
  if(empty($errors)==true){
	 move_uploaded_file($file_tmp, $upload_dir['path']."/".$file_name);
	 
	$img_path = $upload_dir['path'];
	$img_name = $file_name;
	$image = wp_get_image_editor( $img_path .'/'. $img_name );
	if ( ! is_wp_error( $image ) ) {
		$image->resize( 300, 300, true );
		$image->save( $img_path .'/'. $img_name );
	}

  }else{
	  $file_name='';
  }
}



	if($file_name!=''){
		$imageurl = $upload_dir['url'].'/'.$file_name;
    }else{
		$imageurl = '';
    }

    if( isset($_POST['qc_pd_category']) ){
		$qc_pd_category = sanitize_text_field($_POST['qc_pd_category']);
    }else{
    	$qc_pd_category = '';
    }

    $item_tags = sanitize_text_field($_POST['item_tags']);;
    $item_inputted_tags = sanitize_text_field($_POST['item_tags']);
    $pd_default_tags = $_POST['pd_default_tags'];
	
	$pd_default_tags_str = '';

	if(!empty($pd_default_tags)){
		$pd_default_tags_str = implode(",",$pd_default_tags);
	}

	if (!empty($item_tags)) {
		$item_tags = $item_tags;
		if (!empty($pd_default_tags_str)) {
			$item_tags = $item_tags.','.$pd_default_tags_str;
		}
	}else{
		$item_tags = $pd_default_tags_str;
	}
    // echo $item_tags;die();

	$qc_pd_list = sanitize_text_field($_POST['qc_pd_list']);
	$item_phone = sanitize_text_field($_POST['item_phone']);
	$item_location = sanitize_text_field($_POST['item_location']);
	$item_full_address = sanitize_text_field($_POST['item_full_address']);
	$item_latitude = sanitize_text_field($_POST['item_latitude']);
	$item_longitude = sanitize_text_field($_POST['item_longitude']);
	$item_linkedin = sanitize_text_field($_POST['item_linkedin']);
	$item_twitter = sanitize_text_field($_POST['item_twitter']);
	$item_facebook = sanitize_text_field($_POST['item_facebook']);
	// $item_tags = sanitize_text_field($_POST['item_tags']);
	$item_yelp = sanitize_text_field($_POST['item_yelp']);
	$item_business_hour = sanitize_text_field($_POST['item_business_hour']);
	$item_email = sanitize_text_field($_POST['item_email']);
	$datetime = date('Y-m-d H:i:s');
	$package_id = $_POST['package_id'];
	
	$qcpd_field_1 = '';
	if(isset($_POST['qcpd_field_1']) && $_POST['qcpd_field_1']!=''){
		$qcpd_field_1 = sanitize_text_field($_POST['qcpd_field_1']);
	}
	$qcpd_field_2 = '';
	if(isset($_POST['qcpd_field_2']) && $_POST['qcpd_field_2']!=''){
		$qcpd_field_2 = sanitize_text_field($_POST['qcpd_field_2']);
	}
	$qcpd_field_3 = '';
	if(isset($_POST['qcpd_field_3']) && $_POST['qcpd_field_3']!=''){
		$qcpd_field_3 = sanitize_text_field($_POST['qcpd_field_3']);
	}
	$qcpd_field_4 = '';
	if(isset($_POST['qcpd_field_4']) && $_POST['qcpd_field_4']!=''){
		$qcpd_field_4 = sanitize_text_field($_POST['qcpd_field_4']);
	}
	$qcpd_field_5 = '';
	if(isset($_POST['qcpd_field_5']) && $_POST['qcpd_field_5']!=''){
		$qcpd_field_5 = sanitize_text_field($_POST['qcpd_field_5']);
	}
	
	
		$wpdb->insert(
			$table,
			array(
				'item_title'  => $item_title,
				'item_link'   => $item_link,
				'item_subtitle' => $item_subtitle,
				'item_phone'	=> $item_phone,
				'location'		=> $item_location,
				'full_address'	=> $item_full_address,
				'latitude'		=> $item_latitude,
				'longitude'		=> $item_longitude,
				'linkedin'		=> $item_linkedin,
				'twitter'		=> $item_twitter,
				'facebook'		=> $item_facebook,
				'yelp'			=> $item_yelp,
				'business_hour'	=> $item_business_hour,
				'email'			=> $item_email,				
				'category'   => $qc_pd_category,
				'pd_list'  => $qc_pd_list,
				'user_id'=> $current_user->ID,
				'image_url'=> $imageurl,
				'time'=> $datetime,
				'nofollow'=> $item_no_follow,
                'package_id'=>$package_id,
				'description'=>$item_description,
				'qcpd_field_1'=>$qcpd_field_1,
				'qcpd_field_2'=>$qcpd_field_2,
				'qcpd_field_3'=>$qcpd_field_3,
				'qcpd_field_4'=>$qcpd_field_4,
				'qcpd_field_5'=>$qcpd_field_5,
				'tags'=>$item_tags
			)
		);

		

    if(in_array('administrator',$current_user->roles)){
        $lastid = $wpdb->insert_id;
        $this->approve_subscriber_profile($lastid);
        $this->redirect_after_item_submission();
        echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">Your list link has been successfully published. </div>';
    }else{

	    if(pd_ot_get_option('pd_email_notification')=='on'){
			$data = array(
				'title'=> $item_title,
				'phone'=> $item_phone,
				'email'=> $item_email
			);
	        $this->pd_new_item_notification($current_user->ID, $data);
        }

        if(pd_ot_get_option('pd_enable_auto_approval')=='on'){

            $lastid = $wpdb->insert_id;
            $this->approve_subscriber_profile($lastid);
            $this->redirect_after_item_submission();

            echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">Your list link has been successfully published. </div>';

        }else{
        	$this->redirect_after_item_submission();
            echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">Your link has been successfully submitted. We will review your item information before Publishing. Thank you for your patience. <br/></div>';
        }
    }
	
	if(!empty($errors)){
		foreach($errors as $error){
			echo '<div style="color: red;border: 1px solid red;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">'.$error.'</div>';
		}
	}


}
if($this->allow_item_submit==false){
	
	$free_link_txt = pd_ot_get_option('sbd_lan_free_link')!=''?pd_ot_get_option('sbd_lan_free_link'):'You have reached your free link submission limit.';
	
	echo '<div style="color: red;border: 1px solid red;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">'.$free_link_txt.' <br/></div>';
	return;
}

?>

<h2><?php echo (pd_ot_get_option('sbd_lan_add_your_link')!=''?pd_ot_get_option('sbd_lan_add_your_link'):__('Add Listing', 'qc-pd')); ?></h2>
<form class="sbd-frontend-submission-form" action="<?php echo $current_url.'/?action=entry'; ?>" method="POST" enctype="multipart/form-data">
	<ul class="pd_form-style-1 pd_width">
		
		<li><label><?php echo (pd_ot_get_option('sbd_lan_select_package')!=''?pd_ot_get_option('sbd_lan_select_package'):__('Select Package', 'qc-pd')) ?> <span class="pd_required">*</span></label>
            <select name="package_id">
                <?php $package_none_text = pd_ot_get_option('sbd_lan_none') != '' ? pd_ot_get_option('sbd_lan_none') : __('None', 'qc-pd'); ?>
                <option value=""><?php echo $package_none_text; ?></option>
				<?php
				$submited_item = $wpdb->get_row("select count(*)as cnt from $table where 1 and package_id = 0 and user_id =".$current_user->ID);

				if(pd_ot_get_option('pd_enable_free_submission')=='on'){
					if(pd_ot_get_option('pd_free_item_limit')!='' and pd_ot_get_option('pd_free_item_limit') > $submited_item->cnt){
						?>
						<option value="0"><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></option>
						<?php
					}else{
						?>
						<option value="0" disabled><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></option>
						<?php
					}
				}
                

				?>

                 <?php
                 $pkglist = $wpdb->get_results("select ppt.id as pid, ppt.package_id as id, ppt.expire_date as expiredate, pt.title, pt.item as total_item from $package_purchased_table as ppt, $package_table as pt where 1 and ppt.user_id = ".$current_user->ID." and ppt.package_id = pt.id order by ppt.date DESC");

                 foreach($pkglist as $row){
	                 $submited_item = $wpdb->get_row("select count(*)as cnt from $table where 1 and package_id = ".$row->pid." and user_id =".$current_user->ID);
                     if(strtotime(date('Y-m-d')) < strtotime($row->expiredate) and $row->total_item > $submited_item->cnt){
                     ?>
                         <option value="<?php echo $row->pid; ?>" selected="selected"><?php echo $row->title; ?></option>
                    <?php
                     }else{
                         ?>
                         <option value="<?php echo $row->pid; ?>" disabled><?php echo $row->title; ?></option>
                        <?php
                     }
                 }

                 ?>
            </select>
			<?php 
				if(pd_ot_get_option('pd_enable_free_submission')!='on' && empty($pkglist)){
					?>
					<p><?php echo sprintf(__( 'You have not purchased any package. Before submitting a listing please purchase a package from <a href="%s">Packages</a> tab.', 'qc-pd' ),esc_url( add_query_arg( 'action', 'package', $url ) )); ?></p>
					<?php
				}
			?>
        </li>
		<?php if(pd_ot_get_option('pd_exclude_category')!='on'): ?>
		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_category')!=''?pd_ot_get_option('sbd_lan_category'):__('Category', 'qc-pd')); ?></label>
			
				<?php 
					$taxonomy = 'pd_cat';
					$terms = get_terms($taxonomy); //
					$excludel = pd_ot_get_option('pd_exclude_category_id');
					if ( $terms && !is_wp_error( $terms ) ) :
					?>
						<select id="qc_pd_category" class="pd_text_width" name="qc_pd_category" >
							<option value="" ><?php echo (pd_ot_get_option('sbd_lan_all_list')!=''?pd_ot_get_option('sbd_lan_all_list'):__('All List', 'qc-pd')); ?></option>
							<?php foreach ( $terms as $term ) { ?>
							
								<?php 
								if($excludel!=''){
									$exclude = explode(',',$excludel);
									if(!in_array($term->term_id,$exclude)){
										?>
										<option value="<?php echo $term->name; ?>"><?php echo esc_attr($term->name); ?></option>
										<?php
									}
								}else{
								?>
								<option value="<?php echo $term->name; ?>"><?php echo esc_attr($term->name); ?></option>
								<?php
								}
								?>
							
								
							<?php } ?>
						</select>
					<?php
					endif;
				?>
		</li>
		<?php endif; ?>
		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_select_list')!=''?pd_ot_get_option('sbd_lan_select_list'):__('Select List', 'qc-pd')); ?> <span class="pd_required">*</span></label>
			<select id="qc_pd_list" class="pd_text_width" name="qc_pd_list" required>
				<?php
					$excludel = pd_ot_get_option('pd_exclude_list');
					$exclude2 = pd_ot_get_option('pd_exclude_category_id');
					$query = array( 
						'post_type' => 'pd',				
						'posts_per_page' => -1,
						'order' => 'ASC',
						'orderby' => 'menu_order',
						
						);
					if($exclude2!=''){
						$query['tax_query'] = array(
							array(
								'taxonomy' => 'pd_cat',
								'field'    => 'ID',
								'terms'    => explode(',', $exclude2),
								'operator' => 'NOT IN',
							),
						);
					}
					
					$sld = new WP_Query( $query );
					
					
					while( $sld->have_posts() ) : $sld->the_post();
					
					?>
						
						<?php 
						if($excludel!=''){
							$exclude = explode(',',$excludel);
							if(!in_array(get_the_ID(),$exclude)){
								?>
								<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
								<?php
							}
						}else{
						?>
						<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
						<?php
						}
						?>

					<?php
					endwhile;
				?>

			</select>
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_website_link')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_link_include')!=''?pd_ot_get_option('sbd_lan_link_include'):__('Link (Include http:// or https://)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_link_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_link_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" id="sbd_item_link" name="item_link" class="field-long pd_text_width" value="" />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_generate_info')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_generate_info_link')!=''?pd_ot_get_option('sbd_lan_generate_info_link'):__('Generator info from Link', 'qc-pd')); ?> </label>
            <input type="button" id="sbd_generate" class="" value="<?php echo pd_ot_get_option('sbd_lan_generate_button_text')!=''?pd_ot_get_option('sbd_lan_generate_button_text'):__('Generate','qc-pd'); ?>" />
        </li>
		
        <li><label><?php echo (pd_ot_get_option('sbd_lan_listing_title')!=''?pd_ot_get_option('sbd_lan_listing_title'):__('Listing Title', 'qc-pd')); ?> <span class="pd_required">*</span></label><input type="text" name="item_title" id="sbd_title" class="field-long pd_text_width" value="" required/></li>
		
		<li <?php echo ($flagg==false && get_option('sbd_subtitle')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_subtitle')!=''?pd_ot_get_option('sbd_lan_listing_subtitle'):__('Listing Subtitle', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_subtitle_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_subtitle_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" id="sbd_subtitle" name="item_subtitle" class="field-long pd_text_width" value=""  />
		</li>
		<li <?php echo ($flagg==false && get_option('sbd_long_description')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_long')!=''?pd_ot_get_option('sbd_lan_listing_long'):__('Listing Long Description', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_long_description_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<?php if(pd_ot_get_option('pd_enable_texteditor')=='on'):?>
				<?php wp_editor('', 'pd_text_width', array('textarea_name' =>
				'item_long_description',
				'textarea_rows' => 20,
				'editor_height' => 100,
				'disabled' => 'disabled',
				'media_buttons' => false,
				'tinymce'       => array(
				'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink',)
				)); ?>
			<?php else: ?>
				<textarea <?php if( pd_ot_get_option('mark_listing_long_description_field_as_required') == 'on' ){ echo 'required'; } ?> class="field-long pd_text_width" name="item_long_description"></textarea>
			<?php endif; ?>
			
			
			
			
			
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_phone')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_main_phone')!=''?pd_ot_get_option('sbd_lan_main_phone'):__('Main Phone Number (For call)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_main_phone_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_main_phone_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_phone" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_short_address')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_short_address')!=''?pd_ot_get_option('sbd_lan_short_address'):__('Short Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_short_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_short_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_location" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_full_address')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_full_address_map')!=''?pd_ot_get_option('sbd_full_address_map'):__('Full Address (For google maps)', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_full_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_full_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_full_address" id="sbd_full_address" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_latitude')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_latitude')!=''?pd_ot_get_option('sbd_lan_latitude'):__('Latitude', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_latitude_field_as_required') != 'off' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_latitude_field_as_required') != 'off' ){ echo 'required'; } ?> type="text" name="item_latitude" id="sbd_lat" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_longitude')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_longitude')!=''?pd_ot_get_option('sbd_lan_longitude'):__('Longitude', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_longitude_field_as_required') != 'off' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_longitude_field_as_required') != 'off' ){ echo 'required'; } ?> type="text" name="item_longitude" id="sbd_long" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_linkedin')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_linkedin')!=''?pd_ot_get_option('sbd_lan_linkedin'):__('Linkedin/instagram Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_linkedin_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_linkedin_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_linkedin" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_twitter')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_twitter')!=''?pd_ot_get_option('sbd_lan_twitter'):__('Twitter Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_twitter_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_twitter_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_twitter" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_email')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_email_address')!=''?pd_ot_get_option('sbd_lan_email_address'):__('Email Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_email_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_email_field_as_required') == 'on' ){ echo 'required'; } ?> type="email" name="item_email" class="field-long pd_text_width" value="" placeholder="email@example.com"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_business_hour')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_business_hour')!=''?pd_ot_get_option('sbd_lan_business_hour'):__('Business Hours', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_business_hours_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_business_hours_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_business_hour" class="field-long pd_text_width" value="" placeholder="Mon - Fri 10:00am - 05:00pm"  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_yelp')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_yelp')!=''?pd_ot_get_option('sbd_lan_yelp'):__('Yelp Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_yelp_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_yelp_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_yelp" class="field-long pd_text_width" value=""  />
		</li>
		
		<li <?php echo ($flagg==false && get_option('sbd_facebook')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_facebook')!=''?pd_ot_get_option('sbd_lan_facebook'):__('Facebook Address', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_fb_address_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
			<input <?php if( pd_ot_get_option('mark_listing_fb_address_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_facebook" class="field-long pd_text_width" value=""  />
		</li>

		<?php if( pd_ot_get_option('enable_predefined_tag_selection') == 'on' ){?>
			<li <?php echo ($flagg==false && get_option('sbd_tags')==0?'style="display:none"':''); ?>>
				<label><?php echo (pd_ot_get_option('sbd_lan_tags')!=''?pd_ot_get_option('sbd_lan_tags'):__('Tags', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>

				<?php
					$pd_default_tags = pd_ot_get_option('pd_default_tags');
					if (!empty($pd_default_tags)) {
						$pd_default_tags = explode(",",$pd_default_tags);
						if (!empty($pd_default_tags) && is_array($pd_default_tags) ) {
							foreach ($pd_default_tags as $key => $value) {
							?>
							<span style="margin-right: 10px;">
								<input style="margin-right: 5px;" type="checkbox" name="pd_default_tags[]" value="<?php echo $value; ?>"><?php echo ucwords($value); ?>
							</span>
							<?php
							}
						}
						?>
						<span style="margin-right: 10px;">
							<input style="margin-right: 5px;" type="checkbox" name="pd_default_tags_other" value="other">Other
						</span>
						<?php
					}
				?>
				<input style="display: none" <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_tags" id="item_tags" class="field-long pd_text_width pd_default_tags_input" value=""  />
			</li>
		<?php }else{ ?>
			<li <?php echo ($flagg==false && get_option('sbd_tags')==0?'style="display:none"':''); ?>>
				<label><?php echo (pd_ot_get_option('sbd_lan_tags')!=''?pd_ot_get_option('sbd_lan_tags'):__('Tags', 'qc-pd')); ?> <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ ?><span class="pd_required">*</span><?php } ?></label>
				<input <?php if( pd_ot_get_option('mark_listing_tags_field_as_required') == 'on' ){ echo 'required'; } ?> type="text" name="item_tags" id="item_tags" class="field-long pd_text_width" value=""  />
			</li>
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_1_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_1_label'); ?> </label>
			<input type="text" name="qcpd_field_1" class="field-long pd_text_width" value=""  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_2_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_2_label'); ?> </label>
			<input type="text" name="qcpd_field_2" class="field-long pd_text_width" value=""  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_3_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_3_label'); ?> </label>
			<input type="text" name="qcpd_field_3" class="field-long pd_text_width" value=""  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_4_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_4_label'); ?> </label>
			<input type="text" name="qcpd_field_4" class="field-long pd_text_width" value=""  />
		</li>
		
		<?php } ?>
		
		<?php if(pd_ot_get_option('sbd_field_5_enable')=='on'){ ?>
		
		<li <?php echo ($flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
			<label><?php echo pd_ot_get_option('sbd_field_5_label'); ?> </label>
			<input type="text" name="qcpd_field_5" class="field-long pd_text_width" value=""  />
		</li>
		
		<?php } ?>
		
        
        <?php if(pd_ot_get_option('pd_image_upload')=='on'){ ?>
		<li <?php echo ($flagg==false && get_option('sbd_image')==0?'style="display:none"':''); ?>>
			<label><?php echo (pd_ot_get_option('sbd_lan_listing_image')!=''?pd_ot_get_option('sbd_lan_listing_image'):__('Listings Image', 'qc-pd')); ?> </label>
			
			<input type="file" name="sbd_link_image" id="sbd_link_image" >
			<br><span><?php echo (pd_ot_get_option('sbd_lan_max_resolu')!=''?pd_ot_get_option('sbd_lan_max_resolu'):__('Maximum image size 1MB. Resolution 300x300.','qc-pd')); ?></span>
			<div style="clear:both"></div>
			<div id="pd_preview_img"></div>
			
		</li>
        <?php } ?>
		
		
<?php if(pd_ot_get_option('pd_disable_no_follow')!='on'){ ?>
        <li <?php echo ($flagg==false && get_option('sbd_no_follow')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_no_follow')!=''?pd_ot_get_option('sbd_lan_no_follow'):__('No Follow', 'qc-pd')); ?> </label>
            <input type="checkbox" name="item_no_follow" class="" value="1" <?php echo (pd_ot_get_option('pd_no_follow')!='off'?'checked':''); ?> />
        </li>
<?php }else{
	?>
	<li <?php echo ($flagg==false && get_option('sbd_no_follow')==0?'style="display:none"':''); ?>>
            <label><?php echo (pd_ot_get_option('sbd_lan_no_follow')!=''?pd_ot_get_option('sbd_lan_no_follow'):__('No Follow', 'qc-pd')); ?> </label>
            <input type="checkbox" name="item_no_follow" class="" value="1" <?php echo (pd_ot_get_option('pd_no_follow')!='off'?'checked':''); ?> disabled="" />
        </li>
	<?php 
	} ?>
        <li>
			<input type="submit" name="submititem" class="pd_submit_style" value="<?php echo (pd_ot_get_option('sbd_lan_submit')!=''?pd_ot_get_option('sbd_lan_submit'):__('Submit','qc-pd')); ?>" />
		</li>
	</ul>
</form>
<?php } ?>