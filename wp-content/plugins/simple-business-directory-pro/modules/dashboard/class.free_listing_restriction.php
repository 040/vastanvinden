<?php
if ( ! defined( 'ABSPATH' ) ) exit;
class Sbd_free_listing_restriction {
	// class instance
	static $instance;

	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', array( __CLASS__, 'set_screen' ), 10, 3 );
		add_action( 'admin_menu', array( $this, 'pd_custom_plugin_admin_menu' ) );

	}

	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function pd_custom_plugin_admin_menu() {

		$hook = add_submenu_page(
			'edit.php?post_type=pd',
			'Manage Free Listing Fields',
			'Manage Free Listing Fields',
			'manage_options',
			'qcpd_free_listing_fields',
			array(
				$this,
				'qc_pd_plugin_settings_page'
			)
		);




	}
	public function qc_pd_plugin_settings_page(){
		global $wpdb;
		if(!function_exists('wp_get_current_user')) {
			include(ABSPATH . "wp-includes/pluggable.php"); 
		}
		$table             = $wpdb->prefix.'pd_package';
		$current_user = wp_get_current_user();
		$msg = '';

		if(isset($_POST['free_field_restriction'])){
			
			if(isset($_POST['generate_info'])){
				update_option('sbd_generate_info', $_POST['generate_info']);				
			}else{
				update_option('sbd_generate_info', 0);
			}
			if(isset($_POST['website_link'])){
				update_option('sbd_website_link', $_POST['website_link']);				
			}else{
				update_option('sbd_website_link', 0);
			}
			if(isset($_POST['subtitle'])){
				update_option('sbd_subtitle', $_POST['subtitle']);				
			}else{
				update_option('sbd_subtitle', 0);
			}
			if(isset($_POST['long_description'])){
				update_option('sbd_long_description', $_POST['long_description']);				
			}else{
				update_option('sbd_long_description', 0);
			}
			if(isset($_POST['phone'])){
				update_option('sbd_phone', $_POST['phone']);				
			}else{
				update_option('sbd_phone', 0);
			}
			if(isset($_POST['short_address'])){
				update_option('sbd_short_address', $_POST['short_address']);				
			}else{
				update_option('sbd_short_address', 0);
			}
			if(isset($_POST['full_address'])){
				update_option('sbd_full_address', $_POST['full_address']);				
			}else{
				update_option('sbd_full_address', 0);
			}
			if(isset($_POST['latitude'])){
				update_option('sbd_latitude', $_POST['latitude']);				
			}else{
				update_option('sbd_latitude', 0);
			}
			if(isset($_POST['longitude'])){
				update_option('sbd_longitude', $_POST['longitude']);				
			}else{
				update_option('sbd_longitude', 0);
			}
			if(isset($_POST['linkedin'])){
				update_option('sbd_linkedin', $_POST['linkedin']);				
			}else{
				update_option('sbd_linkedin', 0);
			}
			if(isset($_POST['twitter'])){
				update_option('sbd_twitter', $_POST['twitter']);				
			}else{
				update_option('sbd_twitter', 0);
			}
			if(isset($_POST['email'])){
				update_option('sbd_email', $_POST['email']);				
			}else{
				update_option('sbd_email', 0);
			}
			if(isset($_POST['business_hour'])){
				update_option('sbd_business_hour', $_POST['business_hour']);				
			}else{
				update_option('sbd_business_hour', 0);
			}
			if(isset($_POST['yelp'])){
				update_option('sbd_yelp', $_POST['yelp']);				
			}else{
				update_option('sbd_yelp', 0);
			}
			if(isset($_POST['facebook'])){
				update_option('sbd_facebook', $_POST['facebook']);				
			}else{
				update_option('sbd_facebook', 0);
			}
			if(isset($_POST['tags'])){
				update_option('sbd_tags', $_POST['tags']);				
			}else{
				update_option('sbd_tags', 0);
			}
			if(isset($_POST['image'])){
				update_option('sbd_image', $_POST['image']);				
			}else{
				update_option('sbd_image', 0);
			}
			if(isset($_POST['no_follow'])){
				update_option('sbd_no_follow', $_POST['no_follow']);				
			}else{
				update_option('sbd_no_follow', 0);
			}
			if(isset($_POST['custom1'])){
				update_option('sbd_custom1', $_POST['custom1']);				
			}else{
				update_option('sbd_custom1', 0);
			}
			if(isset($_POST['custom2'])){
				update_option('sbd_custom2', $_POST['custom2']);				
			}else{
				update_option('sbd_custom2', 0);
			}
			if(isset($_POST['custom3'])){
				update_option('sbd_custom3', $_POST['custom3']);				
			}else{
				update_option('sbd_custom3', 0);
			}
			if(isset($_POST['custom4'])){
				update_option('sbd_custom4', $_POST['custom4']);				
			}else{
				update_option('sbd_custom4', 0);
			}
			if(isset($_POST['custom5'])){
				update_option('sbd_custom5', $_POST['custom5']);				
			}else{
				update_option('sbd_custom5', 0);
			}
			
			$msg = '<div id="message" class="updated notice notice-success is-dismissible"><p>Data has been Updated Successfully. </p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			
		}
		
		
		?>
		<style type="text/css">
		
		.sbd_custom_table td{padding:10px}
		.sbd_custom_table td label{width:300px !important;font-weight: bold;}
		
		</style>
		<div class="wrap">

			<div id="poststuff">
				<div id="post-body" class="metabox-holder">
					<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">
					<?php
						if($msg!=''){
							echo $msg;
						}
						?>
					<h1><?php echo __('Manage Free Lising Fields', 'qc-pd'); ?></h1>
					<br>
					<form action="" method="POST">
						<table class="wp-list-table sbd_custom_table">
							
							<tbody class="tbl-body">
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Website Link</label>
										<input type="checkbox" name="website_link" value="1" <?php echo (get_option('sbd_website_link')==1?'checked="checked"':'') ?> />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
									
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Generator info</label>
										<input type="checkbox" name="generate_info" value="1" <?php echo (get_option('sbd_generate_info')==1?'checked="checked"':'') ?> />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
									
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Listing Subtitle</label>
										<input type="checkbox" name="subtitle" <?php echo (get_option('sbd_subtitle')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Listing Long Description</label>
										<input type="checkbox" name="long_description" <?php echo (get_option('sbd_long_description')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Main Phone Number (For call)</label>
										<input type="checkbox" name="phone" <?php echo (get_option('sbd_phone')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Short Address</label>
										<input type="checkbox" name="short_address" <?php echo (get_option('sbd_short_address')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Full Address (For google maps)</label>
										<input type="checkbox" name="full_address" <?php echo (get_option('sbd_full_address')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Latitude</label>
										<input type="checkbox" name="latitude" <?php echo (get_option('sbd_latitude')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Longitude</label>
										<input type="checkbox" name="longitude" <?php echo (get_option('sbd_longitude')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Linkedin/instagram Address</label>
										<input type="checkbox" name="linkedin" <?php echo (get_option('sbd_linkedin')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Twitter Address</label>
										<input type="checkbox" name="twitter" <?php echo (get_option('sbd_twitter')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Email Address</label>
										<input type="checkbox" name="email" <?php echo (get_option('sbd_email')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Business Hours</label>
										<input type="checkbox" name="business_hour" <?php echo (get_option('sbd_business_hour')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Yelp Address</label>
										<input type="checkbox" name="yelp" <?php echo (get_option('sbd_yelp')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Facebook Address</label>
										<input type="checkbox" name="facebook" <?php echo (get_option('sbd_facebook')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Tags</label>
										<input type="checkbox" name="tags" <?php echo (get_option('sbd_tags')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Listings Image</label>
										<input type="checkbox" name="image" <?php echo (get_option('sbd_image')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">No Follow</label>
										<input type="checkbox" name="no_follow" <?php echo (get_option('sbd_no_follow')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Custom Field 1</label>
										<input type="checkbox" name="custom1" <?php echo (get_option('sbd_custom1')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Custom Field 2</label>
										<input type="checkbox" name="custom2" <?php echo (get_option('sbd_custom2')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Custom Field 3</label>
										<input type="checkbox" name="custom3" <?php echo (get_option('sbd_custom3')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Custom Field 4</label>
										<input type="checkbox" name="custom4" <?php echo (get_option('sbd_custom4')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<label style="width:100px;display: inline-block;">Custom Field 5</label>
										<input type="checkbox" name="custom5" <?php echo (get_option('sbd_custom5')==1?'checked="checked"':'') ?> value="1" />
										<span class="description">If checked, The field will be available for free listing.</span>
									</td>
								</tr>
								<tr>
									<td>
										<input type="hidden" name="free_field_restriction" value="free" />
										<input type="submit" value="Save" name="submit" class="button button-primary" />
									</td>
								</tr>
							</tbody>
					
						</table>
					</form>
					
					</div>
				</div>
			</div>
		</div>
		<?php

	}
}
function Sbd_free_listing_restriction(){
	return Sbd_free_listing_restriction::get_instance();
}
Sbd_free_listing_restriction();