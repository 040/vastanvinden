<?php
/*
* Author: QuantumCloud.
* Dashboard class.
*/

if ( ! defined( 'ABSPATH' ) ) exit;
class qc_sbd_dashboard
{
	private static $instance;

	protected $allow_item_submit; //Allow item submission
	protected $show_package; //Allow package
    protected $total_item;
    protected $submited_item;
    protected $remain_item;


	public static function instance(){
		if(!isset(self::$instance)){
			self::$instance = new qc_sbd_dashboard();
		}
		return self::$instance;
	}
	private function __construct(){
		
		if ( ! class_exists( 'WP_List_Table' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
		} 
		require(QCSBD_DIR_MOD.'/dashboard/qc-subscriber-entry-approve.php');
		require(QCSBD_DIR_MOD.'/dashboard/qc-subscriber_entry_list.php');

		//add_action('pre_get_posts',array($this,'pd_users_own_attachments'));
		
        add_action('template_redirect', array($this,'pdcustom_redirect_load_before_headers'));

		add_action('init', array($this, 'pd_plugin_init'));

		add_action( 'wp_enqueue_scripts', array($this,'pdcustom_dashboard_enqueue_style') );
		add_shortcode('sbd_dashboard', array($this,'sbd_dashboard_show'));
		//add_action('wp_loaded', array($this,'pdcustom_user_permission_add'));
		
		add_action( 'wp_ajax_qcld_pd_category_filter', array($this,'qcld_pd_category_filter_fnc') ); // ajax for logged in users
		add_action( 'wp_ajax_nopriv_qcld_pd_category_filter', array($this,'qcld_pd_category_filter_fnc') ); // ajax for not logged in users
		add_action('plugins_loaded',array($this,'qc_pd_admin_area'));

	}
    function pd_plugin_init(){
	    global $wpdb;
		
		if(!function_exists('wp_get_current_user')) {
			include(ABSPATH . "wp-includes/pluggable.php"); 
		}
		
		$url = qc_sbd_login_page()->pdcustom_login_get_translated_option_page('sbd_dashboard_url');
		
	    $current_user = wp_get_current_user();
	    $table             = $wpdb->prefix.'pd_package_purchased';
	    $table1             = $wpdb->prefix.'pd_package';
		



	    if(isset($_GET['packagesave']) and $_GET['packagesave']!=''){
			$pid = $_GET['packagesave'];
			
		    $package     = $wpdb->get_row( "SELECT * FROM $table1 WHERE 1 and id=".$pid );

		    if($package->duration=='lifetime'){
			    $package->duration = 120;
		    }

		    $userid = $_GET['user'];
		    if(!empty($_POST)){
		        $txn_id = $_POST['txn_id'];
		        $name = $_POST['first_name'].' '.$_POST['last_name'];
		        $payer_email = $_POST['payer_email'];
		        $amount = $_POST['mc_gross'];
		        $status = $_POST['payment_status'];
			    $date = date('Y-m-d H:i:s');
				
				if(isset($_REQUEST['custom']) && $_REQUEST['custom']=='recurring'){
					$custom = 1;
					$package->duration = 120;
				}else{
					$custom = 0;
				}
				
			    $expire_date = date("Y-m-d", strtotime("+$package->duration month", strtotime($date)));

			    $wpdb->insert(
				    $table,
				    array(
					    'date'  => $date,
					    'package_id'   => $package->id,
					    'user_id'   => $userid,
					    'paid_amount'   => $amount,
					    'transaction_id' => $txn_id,
					    'payer_name'   => $name,
					    'payer_email'   => $payer_email,
					    'status'   => $status,
                        'expire_date' => $expire_date,
						'recurring' => $custom
				    )
			    );
				$this->pd_new_order_notification($userid);
            }
			wp_redirect(add_query_arg( array('action'=>'entry', 'msg'=>'success'), $url ));exit;
        }



        if(isset($_GET['payment']) and $_GET['payment']=='renew'){

	        $pkg = $_GET['pkg'];
	        
	        $package1     = $wpdb->get_row( "SELECT * FROM $table WHERE 1 and id = ".$pkg);
			$package     = $wpdb->get_row( "SELECT * FROM $table1 WHERE 1 and id=".$package1->package_id );

	        if($package->duration=='lifetime'){
		        $package->duration = 120;
            }

	        $expire_date = date("Y-m-d H:i:s", strtotime("+$package->duration month", strtotime($package1->expire_date)));



	        if(!empty($_POST)){
		        $txn_id = $_POST['txn_id'];
		        $name = $_POST['first_name'].' '.$_POST['last_name'];
		        $payer_email = $_POST['payer_email'];
		        $amount = $_POST['mc_gross'];
		        $status = $_POST['payment_status'];
		        $date = date('Y-m-d H:i:s');


		        $wpdb->update(
			        $table,
			        array(
				        'renew'  => $date,
				        'expire_date'=>$expire_date,
				        'transaction_id' => $txn_id,
			        ),
                    array('id'=>$pkg),
                    array(
                        '%s',
                        '%s',
                        '%s',
                    ),
                    array('%d')
		        );

	        }
        }
		
		//Stripe payment
		if(isset($_GET['payment']) and $_GET['payment']=='stripe-save'){
			
			require_once(QCSBD_INC_DIR.'/stripe-php-master/init.php');
			\Stripe\Stripe::setApiKey(pd_ot_get_option('sbd_stripe_sectet_key'));
			
			$table             = $wpdb->prefix.'pd_package_purchased';
			$table1             = $wpdb->prefix.'pd_package';
			$package_id = $_GET['package'];
			$package     = $wpdb->get_row( "SELECT * FROM $table1 WHERE 1 and id=".$package_id );

		    if($package->duration=='lifetime'){
			    $package->duration = 120;
		    }
			
			$amount = ($package->Amount*100);
			$currency = $package->currency;
		    $userid = $_GET['userid'];
			$token  = $_POST['stripeToken'];
			$email  = $_POST['stripeEmail'];
			$customer = \Stripe\Customer::create(array(
				  'email' => $email,
				  'source'  => $token
			 ));
			
			$charge = \Stripe\Charge::create(array(
			  'customer' => $customer->id,
			  'amount'   => $amount,
			  'currency' => $currency
			));
			
			
		    if(!empty($_POST)){
		        $txn_id = $token;
		        $name = $email;
		        $payer_email = $email;
		        $amount = $package->Amount;
		        $status = 'success';
			    $date = date('Y-m-d H:i:s');
				
			    $expire_date = date("Y-m-d", strtotime("+$package->duration month", strtotime($date)));

			    $wpdb->insert(
				    $table,
				    array(
					    'date'  => $date,
					    'package_id'   => $package->id,
					    'user_id'   => $userid,
					    'paid_amount'   => $amount,
					    'transaction_id' => $txn_id,
					    'payer_name'   => $name,
					    'payer_email'   => $payer_email,
					    'status'   => $status,
                        'expire_date' => $expire_date,
						
				    )
			    );
				wp_reset_query();
				$this->pd_new_order_notification($userid);
            }
			wp_redirect(add_query_arg( array('action'=>'entry', 'msg'=>'success'), $url ));exit;
		}
		
		if(isset($_GET['payment']) and $_GET['payment']=='stripe-renew'){
			require_once(QCSBD_INC_DIR.'/stripe-php-master/init.php');
			\Stripe\Stripe::setApiKey(pd_ot_get_option('sbd_stripe_sectet_key'));
			
	        $pkg = $_GET['pkg'];
	        
	        $package1     = $wpdb->get_row( "SELECT * FROM $table WHERE 1 and id = ".$pkg);
			$package     = $wpdb->get_row( "SELECT * FROM $table1 WHERE 1 and id=".$package1->package_id );


	        if($package->duration=='lifetime'){
		        $package->duration = 120;
            }

	        $expire_date = date("Y-m-d H:i:s", strtotime("+$package->duration month", strtotime($package1->expire_date)));

			$amount = ($package->Amount*100);
			$currency = $package->currency;
		    $userid = $_GET['userid'];
			$token  = $_POST['stripeToken'];
			$email  = $_POST['stripeEmail'];
			$customer = \Stripe\Customer::create(array(
				  'email' => $email,
				  'source'  => $token
			 ));
			
			$charge = \Stripe\Charge::create(array(
			  'customer' => $customer->id,
			  'amount'   => $amount,
			  'currency' => $currency
			));

	        if(!empty($_POST)){
		        $txn_id = $token;
		        $name = $email;
		        $payer_email = $email;
		        $amount = $package->Amount;
		        $status = 'success';
		        $date = date('Y-m-d H:i:s');


		        $wpdb->update(
			        $table,
			        array(
				        'renew'  => $date,
				        'expire_date'=>$expire_date,
				        'transaction_id' => $txn_id,
			        ),
                    array('id'=>$pkg),
                    array(
                        '%s',
                        '%s',
                        '%s',
                    ),
                    array('%d')
		        );
				wp_reset_query();
	        }
        }
		//Paypal Claim Payment Handle
		
		if(isset($_GET['payment']) and $_GET['payment']=='claim-paypal'){
			$ctable = $wpdb->prefix.'sbd_claim_purchase';
			
	        $pkg = $_GET['pkg'];

	        if(!empty($_POST)){
		        $txn_id = $_POST['txn_id'];
		        $name = $_POST['first_name'].' '.$_POST['last_name'];
		        $payer_email = $_POST['payer_email'];
		        $amount = $_POST['mc_gross'];
		        $date = date('Y-m-d H:i:s');

		        $wpdb->update(
			        $ctable,
			        array(
				        'transaction_id'=>$txn_id,
						'paid_amount'	=>$amount,
						'payer_name'	=>$name,
						'payer_email'	=>$payer_email
			        ),
                    array('id'=>$pkg),
                    array(
                        '%s',
                        '%d',
                        '%s',
                        '%s',
                    ),
                    array('%d')
		        );
				wp_reset_query();
				if(pd_ot_get_option('sbd_auto_approve_claim')=='on'){
					$this->approve_claim_listing($pkg);
				}
				$dashboardurl = qc_sbd_login_page()->pdcustom_login_get_translated_option_page( 'sbd_dashboard_url','');
				wp_redirect($dashboardurl.'?action=claim&success='.urlencode('Your payment has beed successfull!'));
				exit;
	        }
        }
		
		// Stripe claim payment handle
		
		if(isset($_GET['payment']) and $_GET['payment']=='claim-stripe'){
			
			$ctable = $wpdb->prefix.'sbd_claim_purchase';
			$cptable = $wpdb->prefix.'sbd_claim_configuration';
			
			require_once(QCSBD_INC_DIR.'/stripe-php-master/init.php');
			\Stripe\Stripe::setApiKey(pd_ot_get_option('sbd_stripe_sectet_key'));
			
	        $pkg = $_GET['pkg'];
			
	        $claimpayment = $wpdb->get_row("select * from $cptable where 1");

			$amount = ($claimpayment->Amount*100);
			$currency = $claimpayment->currency;
			$token  = $_POST['stripeToken'];
			$email  = $_POST['stripeEmail'];
			$customer = \Stripe\Customer::create(array(
				  'email' => $email,
				  'source'  => $token
			 ));
			$charge = \Stripe\Charge::create(array(
			  'customer' => $customer->id,
			  'amount'   => $amount,
			  'currency' => $currency
			));
	        if(!empty($_POST)){
		        $txn_id = $token;
		        $name = $email;
		        $payer_email = $email;
		        $amount = $claimpayment->Amount;
		        $wpdb->update(
			        $ctable,
			        array(
				        'transaction_id'=>$txn_id,
						'paid_amount'	=>$amount,
						'payer_name'	=>$name,
						'payer_email'	=>$payer_email
			        ),
                    array('id'=>$pkg),
                    array(
                        '%s',
                        '%d',
                        '%s',
                        '%s',
                    ),
                    array('%d')
		        );
				wp_reset_query();
				if(pd_ot_get_option('sbd_auto_approve_claim')=='on'){
					$this->approve_claim_listing($pkg);
				}
				$dashboardurl = qc_sbd_login_page()->pdcustom_login_get_translated_option_page( 'sbd_dashboard_url','');
				wp_redirect($dashboardurl.'?action=claim&success='.urlencode('Your payment has beed successfull!'));
				exit;
	        }
        }

    }

	public function pd_new_item_notification($user_id, $item) {
        $user = new WP_User($user_id);

        $user_login = ($user->user_login);

        $sbd_lan_email_a_new_item_submitted = pd_ot_get_option('sbd_lan_email_a_new_item_submitted') != '' ? pd_ot_get_option('sbd_lan_email_a_new_item_submitted') : __('A New item has been submitted to your list', 'qc-pd');
        $sbd_lan_email_please_go_to_submitted = pd_ot_get_option('sbd_lan_email_please_go_to_submitted') != '' ? pd_ot_get_option('sbd_lan_email_please_go_to_submitted') : __('Please go to Simple Business Directory > Manage User Items to view this item.', 'qc-pd');

        $sbd_lan_email_item_name = pd_ot_get_option('sbd_lan_email_item_name') != '' ? pd_ot_get_option('sbd_lan_email_item_name') : __('Item Name ', 'qc-pd');
        $sbd_lan_email_item_phone_number = pd_ot_get_option('sbd_lan_email_item_phone_number') != '' ? pd_ot_get_option('sbd_lan_email_item_phone_number') : __('Main Phone Number ', 'qc-pd');
        $sbd_lan_email_item_email = pd_ot_get_option('sbd_lan_email_item_email') != '' ? pd_ot_get_option('sbd_lan_email_item_email') : __('Email ', 'qc-pd');
        $sbd_lan_email_item_submitted_by = pd_ot_get_option('sbd_lan_email_item_submitted_by') != '' ? pd_ot_get_option('sbd_lan_email_item_submitted_by') : __('Item Submitted By ', 'qc-pd');

        $message  = sprintf(__('%s %s. %s'), $sbd_lan_email_a_new_item_submitted, get_option('blogname'), $sbd_lan_email_please_go_to_submitted) . "\r\n\r\n";
		$message .= sprintf(__('%s : %s'), $sbd_lan_email_item_name, $item['title']) . "\r\n\r\n";
		$message .= sprintf(__('%s : %s'), $sbd_lan_email_item_phone_number, $item['phone']) . "\r\n\r\n";
		$message .= sprintf(__('%s : %s'), $sbd_lan_email_item_email, $item['email']) . "\r\n\r\n";
        $message .= sprintf(__('%s : %s'), $sbd_lan_email_item_submitted_by, $user_login) . "\r\n\r\n";

       // $sbd_lan_email_a_new_item_submitted = pd_ot_get_option('sbd_lan_email_a_new_item_submitted') != '' ? pd_ot_get_option('sbd_lan_email_a_new_item_submitted') : __('A New item has been submitted to your list', 'qc-pd');


        @wp_mail(pd_ot_get_option('pd_admin_email'), sprintf(__('[%s] %s!'), get_option('blogname'), $sbd_lan_email_a_new_item_submitted ), $message);

    }
	
	public function pd_new_order_notification($user_id) {
        $user = new WP_User($user_id);

		$sbd_lan_email_hi = pd_ot_get_option('sbd_lan_email_hi') != '' ? pd_ot_get_option('sbd_lan_email_hi') : __('Hi,', 'qc-pd');
		$new_package_placed = pd_ot_get_option('sbd_lan_email_a_new_package_placed') != '' ? pd_ot_get_option('sbd_lan_email_a_new_package_placed') : __('A New Package order has been placed to your site', 'qc-pd');
		
		$sbd_lan_email_order_placed_by = pd_ot_get_option('sbd_lan_email_order_placed_by') != '' ? pd_ot_get_option('sbd_lan_email_order_placed_by') : __('Order placed by', 'qc-pd');
		$sbd_lan_email_new_pak_placed_by = pd_ot_get_option('sbd_lan_email_new_pak_placed_by') != '' ? pd_ot_get_option('sbd_lan_email_new_pak_placed_by') : __('New Package order has been placed by', 'qc-pd');
		
		$message  = __($sbd_lan_email_hi) . "\r\n\r\n";
		 
        $message  .= sprintf(__('%s %s. %s'), $new_package_placed, get_option('blogname'), $sbd_lan_email_a_sbd_order_view) . "\r\n\r\n";
		
        $message  .= sprintf(__('%s: %s'), $sbd_lan_email_order_placed_by, $user->display_name) . "\r\n\r\n";

        @wp_mail(pd_ot_get_option('pd_admin_email'), sprintf(__('[%s] %s %s!'), get_option('blogname'), $user->display_name), $sbd_lan_email_new_pak_placed_by, $message);

    }
	
	public function sbd_claim_approval( $user_id, $item ) {
		
		$user = new WP_User($user_id);
		$user_login = stripslashes($user->user_login);
		$user_email = stripslashes($user->user_email);
		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		$sbd_lan_email_hi = pd_ot_get_option('sbd_lan_email_hi') != '' ? pd_ot_get_option('sbd_lan_email_hi') : __('Hi,', 'qc-pd');
		$sbd_lan_email_welcome_to = pd_ot_get_option('sbd_lan_email_welcome_to') != '' ? pd_ot_get_option('sbd_lan_email_welcome_to') : __('Welcome to', 'qc-pd');
		$sbd_lan_email_claim_listing_item = pd_ot_get_option('sbd_lan_email_claim_listing_item') != '' ? pd_ot_get_option('sbd_lan_email_claim_listing_item') : __('Your claim listing item ', 'qc-pd');
		$sbd_lan_email_has_been_approved = pd_ot_get_option('sbd_lan_email_has_been_approved') != '' ? pd_ot_get_option('sbd_lan_email_has_been_approved') : __('has been approved. ', 'qc-pd');

		$message  = __($sbd_lan_email_hi) . "\r\n\r\n";
		$message .= sprintf(__("%s %s! %s %s %s",'qc-pd'), $sbd_lan_email_welcome_to, get_option('blogname'), $sbd_lan_email_claim_listing_item, $item, $sbd_lan_email_has_been_approved) . "\r\n\r\n";

		$sbd_lan_email_find_the_item_in = pd_ot_get_option('sbd_lan_email_find_the_item_in') != '' ? pd_ot_get_option('sbd_lan_email_find_the_item_in') : __('You can find the item in', 'qc-pd');
		$sbd_lan_email_dashboard_your_links = pd_ot_get_option('sbd_lan_email_dashboard_your_links') != '' ? pd_ot_get_option('sbd_lan_email_dashboard_your_links') : __('dashboard > your links', 'qc-pd');
		$sbd_lan_email_dashboard_tab = pd_ot_get_option('sbd_lan_email_dashboard_tab') != '' ? pd_ot_get_option('sbd_lan_email_dashboard_tab') : __('tab.', 'qc-pd');
		
		$message .= sprintf(__("%s <a href='%s'> %s</a> %s",'qc-pd'), $sbd_lan_email_find_the_item_in,  qc_sbd_login_page()->pdcustom_login_get_translated_option_page( 'sbd_dashboard_url',''), $sbd_lan_email_dashboard_your_links, $sbd_lan_email_dashboard_tab ) . "\r\n";

		$sbd_lan_list_has_been_approved = pd_ot_get_option('sbd_lan_list_has_been_approved') != '' ? pd_ot_get_option('sbd_lan_list_has_been_approved') : __('Your claim listing item has been approved!', 'qc-pd');

		wp_mail($user_email, sprintf(__('[%s] %s','qc-pd'), get_option('blogname'), $sbd_lan_list_has_been_approved ), $message, $headers);

	}
	
	public function approve_claim_listing($id){
		global $wpdb;
		$table             = $wpdb->prefix.'sbd_claim_purchase';
		
		$pdata = $wpdb->get_row("Select * from $table where 1 and `id`='$id'");
		$userid = $pdata->user_id;
		$listId = $pdata->listid;
		$lists = get_post_meta( $listId, 'qcpd_list_item01' );
		$userentry = $wpdb->prefix.'pd_user_entry';
		
		$category = get_the_terms($listId,'pd_cat');
		if(!empty($category)){
			$catslug = $category[0]->slug;
		}else{
			$catslug = '';
		}

		$datetime = date('Y-m-d H:i:s');
		$lastid = '';
		foreach($lists as $item){
			if($item['qcpd_item_title']==$pdata->item){
				
				$wpdb->insert(
					$userentry,
					array(
						'item_title'  => $item['qcpd_item_title'],
						'item_link'   => $item['qcpd_item_link'],
						'item_subtitle' => $item['qcpd_item_subtitle'],
						'category'   => $catslug,
						'pd_list'  => $listId,
						'user_id'=>  $userid,
						'image_url'=> ($item['qcpd_item_img_link']==''?@$item['qcpd_item_img']:$item['qcpd_item_img_link']),
						'time'=> $datetime,
						'nofollow'=> $item['qcpd_item_nofollow'],
						'custom'=>$item['qcpd_timelaps'],
						'approval'=>3,
						'item_phone'	=> $item['qcpd_item_phone'],
						'location'		=> $item['qcpd_item_location'],
						'full_address'	=> $item['qcpd_item_full_address'],
						'latitude'		=> $item['qcpd_item_latitude'],
						'longitude'		=> $item['qcpd_item_longitude'],
						'linkedin'		=> $item['qcpd_item_linkedin'],
						'twitter'		=> $item['qcpd_item_twitter'],
						'facebook'		=> $item['qcpd_item_facebook'],
						'yelp'			=> $item['qcpd_item_yelp'],
						'business_hour'	=> $item['qcpd_item_business_hour'],
						'email'			=> $item['qcpd_item_email'],
						'description'			=> $item['qcpd_description'],
						'qcpd_field_1'=> (isset($item['qcpd_field_1'])?$item['qcpd_field_1']:''),
						'qcpd_field_2'=> (isset($item['qcpd_field_2'])?$item['qcpd_field_2']:''),
						'qcpd_field_3'=>(isset($item['qcpd_field_3'])?$item['qcpd_field_3']:''),
						'qcpd_field_4'=>(isset($item['qcpd_field_4'])?$item['qcpd_field_4']:''),
						'qcpd_field_5'=>(isset($item['qcpd_field_5'])?$item['qcpd_field_5']:''),
						'tags'=>$item['qcpd_tags'],
						'verified'=>'1',
						
					)
				);
				$lastid = $wpdb->insert_id;
				
				break;
				
				
			}
		}

		$wpdb->update(
			$table,
			array(
				'status'  => 'approved'
			),
			array( 'id' => $id),
			array(
				'%s',
			),
			array( '%d')
		);
		
		if($lastid!=''){
			$this->approve_subscriber_profile($lastid);
		}
		
		$this->sbd_claim_approval($userid, $pdata->item);

		
		
	}
	
	public function pd_claim_notification($user_id, $item) {
        $user = new WP_User($user_id);

        $user_login = ($user->user_login);

        $sbd_lan_list_has_been_claimed = pd_ot_get_option('sbd_lan_list_has_been_claimed') != '' ? pd_ot_get_option('sbd_lan_list_has_been_claimed') : __('A item has been claimed from your list', 'qc-pd');
        $sbd_lan_list_has_been_claimed_sbd = pd_ot_get_option('sbd_lan_list_has_been_claimed_sbd') != '' ? pd_ot_get_option('sbd_lan_list_has_been_claimed_sbd') : __('Please go to Simple Business Directory > Claimed Listing to view this claimed item.', 'qc-pd');
        $sbd_lan_item_claimed = pd_ot_get_option('sbd_lan_item_claimed') != '' ? pd_ot_get_option('sbd_lan_item_claimed') : __('Item Claimed', 'qc-pd');
        $sbd_lan_item_claimed_by = pd_ot_get_option('sbd_lan_item_claimed_by') != '' ? pd_ot_get_option('sbd_lan_item_claimed_by') : __('Item Claimed By', 'qc-pd');

        $message  = sprintf(__('%s %s. %s'), $sbd_lan_list_has_been_claimed, get_option('blogname'), $sbd_lan_list_has_been_claimed_sbd ) . "\r\n\r\n";
		$message .= sprintf(__('%s : %s'), $sbd_lan_item_claimed, $item) . "\r\n\r\n";
        $message .= sprintf(__('%s : %s'), $sbd_lan_item_claimed_by, $user_login) . "\r\n\r\n";

        $sbd_lan_a_link_has_been_claimed = pd_ot_get_option('sbd_lan_a_link_has_been_claimed') != '' ? pd_ot_get_option('sbd_lan_a_link_has_been_claimed') : __('A item has been claimed from your list!', 'qc-pd');


        @wp_mail(pd_ot_get_option('pd_admin_email'), sprintf(__('[%s] %s'), get_option('blogname'), $sbd_lan_a_link_has_been_claimed), $message);

    }
	
	
	public function pd_edit_item_notification($user_id, $item) {
        $user = new WP_User($user_id);

        $user_login = ($user->user_login);

        $link_edited_mail_test = pd_ot_get_option('sbd_lan_edit_user_list_email') != '' ? pd_ot_get_option('sbd_lan_edit_user_list_email') : sprintf(__('A link has been edited to your list %s. Please go to Simple Business Directory > Manage User Items to view this link.'), get_option('blogname'));

        $item_edited_mail_test = pd_ot_get_option('sbd_lan_item_edited') != '' ? pd_ot_get_option('sbd_lan_item_edited') : __('Item Edited', 'qc-pd');
        $item_edited_by_mail_test = pd_ot_get_option('sbd_lan_item_edited_by') != '' ? pd_ot_get_option('sbd_lan_item_edited_by') : __('Item Edited By', 'qc-pd');

        $link_edited_mail_test = str_replace('#blog_name', get_option('blogname'), $link_edited_mail_test);

        $message  = $link_edited_mail_test . "\r\n\r\n";
		$message .= sprintf('%1$s : %2$s', $item_edited_mail_test, $item) . "\r\n\r\n";
        $message .= sprintf('%1$s : %2$s', $item_edited_by_mail_test, $user_login) . "\r\n\r\n";

        $sbd_lan_a_link_has_been_edited = pd_ot_get_option('sbd_lan_a_link_has_been_edited') != '' ? pd_ot_get_option('sbd_lan_a_link_has_been_edited') : __('A link has been edited to your list!', 'qc-pd');

        @wp_mail(pd_ot_get_option('pd_admin_email'), sprintf(__('[%s] %s'), get_option('blogname'), $sbd_lan_a_link_has_been_edited), $message);

    }

	public function pd_users_own_attachments( $wp_query_obj ) {

		global $current_user, $pagenow;

		$is_attachment_request = ($wp_query_obj->get('post_type')=='attachment');

		if( !$is_attachment_request )
			return;

		if( !is_a( $current_user, 'WP_User') )
			return;

		if( !in_array( $pagenow, array( 'upload.php', 'admin-ajax.php' ) ) )
			return;

		if( !current_user_can('delete_pages') )
			$wp_query_obj->set('author', $current_user->ID );

		return;
	}
	
	
	/*
	*
	* Admin Area integration
	*/
	public function qc_pd_admin_area(){
		return sbd_user_entry::get_instance();
	}
	
	public function pdcustom_user_permission_add(){
		$current_user = wp_get_current_user();

		if(is_user_logged_in() && in_array('pduser',$current_user->roles)){
			$current_user->add_cap('upload_files');
		}
		
		if(is_user_logged_in() && in_array('subscriber',$current_user->roles)){
			
			if(pd_ot_get_option('pd_subscriber_image_upload')=='on'){
				$current_user->add_cap('upload_files');
			}else{
				$current_user->remove_cap('upload_files');
			}
			
		}
		
	}

    /**
     * Approve Subscriber profile.
     *
     * @return null
     */

    public function approve_subscriber_profile($id){
        global $wpdb;
		$package_purchase_table = $wpdb->prefix.'pd_package_purchased';
        $sql = "SELECT * FROM {$wpdb->prefix}pd_user_entry where 1 and id = ".$id;
        $identifier = time();
        $pdata = $wpdb->get_row($sql);
		
		$featured = 0;
		$exp_date = '';
		if($pdata->package_id > 0){
			$featured = 1;
			$package_purchase = $wpdb->get_row("SELECT DATE_FORMAT(`expire_date`,'%Y-%m-%d') as exp_date FROM $package_purchase_table where 1 and id = ".$pdata->package_id);
			$exp_date = $package_purchase->exp_date;
		}
		
		if(pd_ot_get_option('sbd_paid_item_featured')!='on'){
			$featured = 0;
		}
		
        if( $pdata->approval==0 || $pdata->approval==2){
			
            $prepare = array( //preparing Meta
                'qcpd_item_title' 			=> sanitize_text_field($pdata->item_title),
				'qcpd_item_link' 			=> trim($pdata->item_link),
				'qcpd_item_subtitle' 		=> sanitize_text_field($pdata->item_subtitle),
				'qcpd_description' 		=> ($pdata->description),
				'qcpd_item_img' 		=> trim($pdata->image_url),
				'qcpd_item_phone' 		=> trim($pdata->item_phone),
				'qcpd_item_location' 		=> trim($pdata->location),
				'qcpd_item_full_address' 		=> trim($pdata->full_address),
				'qcpd_item_latitude' 		=> trim($pdata->latitude),
				'qcpd_item_longitude' 		=> trim($pdata->longitude),
				'qcpd_item_email' 		=> trim($pdata->email),
				'qcpd_item_twitter' 		=> trim($pdata->twitter),
				'qcpd_item_linkedin' 		=> trim($pdata->linkedin),
				'qcpd_item_facebook' 		=> trim($pdata->facebook),
				'qcpd_item_yelp' 		=> trim($pdata->yelp),
				'qcpd_item_business_hour' 		=> trim($pdata->business_hour),
				'qcpd_fa_icon' 			=> '',
				'qcpd_item_nofollow' 		=> ($pdata->nofollow==1?1:0),
				'qcpd_item_newtab' 		=> 1,
				'qcpd_upvote_count' 		=> 0,
				'qcpd_entry_time' 			=> date('Y-m-d H:i:s'),
				'qcpd_ex_date'				=> $exp_date,
				'qcpd_timelaps' 			=> $identifier,
				'qcpd_is_bookmarked' 			=> 0,
				'qcpd_featured'			=> $featured,
				'qcpd_field_1'		=> trim($pdata->qcpd_field_1),
				'qcpd_field_2'		=> trim($pdata->qcpd_field_2),
				'qcpd_field_3'		=> trim($pdata->qcpd_field_3),
				'qcpd_field_4'		=> trim($pdata->qcpd_field_4),
				'qcpd_field_5'		=> trim($pdata->qcpd_field_5),
				'qcpd_tags'		=> trim($pdata->tags),
				'qcpd_paid'		=> ($pdata->package_id>0?'1':''),
				'qcpd_verified' => $pdata->verified,
				

            );

            add_post_meta( trim($pdata->pd_list), 'qcpd_list_item01', $prepare );

            $wpdb->update(
                $wpdb->prefix.'pd_user_entry',
                array(
                    'custom'  => $identifier,
                    'approval'=> 1
                ),
                array( 'id' => $id),
                array(
                    '%s',
                    '%d',
                ),
                array( '%d')
            );
        }elseif($pdata->approval==3){
			
            $sql = "SELECT * FROM {$wpdb->prefix}pd_user_entry where 1 and id = ".$id;
            $pdata = $wpdb->get_row($sql);
            $identifier = time();
            if($pdata->custom!=''){
                $this->deny_subscriber_profile($id);
            }

            $prepare = array( //preparing Meta
                'qcpd_item_title' 			=> sanitize_text_field($pdata->item_title),
				'qcpd_item_link' 			=> trim($pdata->item_link),
				'qcpd_item_subtitle' 		=> sanitize_text_field($pdata->item_subtitle),
				'qcpd_description' 		=> ($pdata->description),
				'qcpd_item_img' 		=> trim($pdata->image_url),
				'qcpd_item_phone' 		=> trim($pdata->item_phone),
				'qcpd_item_location' 		=> trim($pdata->location),
				'qcpd_item_full_address' 		=> trim($pdata->full_address),
				'qcpd_item_latitude' 		=> trim($pdata->latitude),
				'qcpd_item_longitude' 		=> trim($pdata->longitude),
				'qcpd_item_email' 		=> trim($pdata->email),
				'qcpd_item_twitter' 		=> trim($pdata->twitter),
				'qcpd_item_linkedin' 		=> trim($pdata->linkedin),
				'qcpd_item_facebook' 		=> trim($pdata->facebook),
				'qcpd_item_yelp' 		=> trim($pdata->yelp),
				'qcpd_item_business_hour' 		=> trim($pdata->business_hour),
				'qcpd_fa_icon' 			=> '',
				'qcpd_item_nofollow' 		=> ($pdata->nofollow==1?1:0),
				'qcpd_item_newtab' 		=> 1,
				'qcpd_upvote_count' 		=> 0,
				'qcpd_entry_time' 			=> date('Y-m-d H:i:s'),
				'qcpd_ex_date'				=> $exp_date,
				'qcpd_timelaps' 			=> $identifier,
				'qcpd_is_bookmarked' 			=> 0,
				'qcpd_featured'			=> $featured,
				'qcpd_field_1'		=> trim($pdata->qcpd_field_1),
				'qcpd_field_2'		=> trim($pdata->qcpd_field_2),
				'qcpd_field_3'		=> trim($pdata->qcpd_field_3),
				'qcpd_field_4'		=> trim($pdata->qcpd_field_4),
				'qcpd_field_5'		=> trim($pdata->qcpd_field_5),
				'qcpd_tags'		=> trim($pdata->tags),
				'qcpd_paid'		=> ($pdata->package_id>0?'1':''),
				'qcpd_verified' => $pdata->verified,

            );

            add_post_meta( trim($pdata->pd_list), 'qcpd_list_item01', $prepare );

            $wpdb->update(
                $wpdb->prefix.'pd_user_entry',
                array(
                    'custom'  => $identifier,
                    'approval'=> 1
                ),
                array( 'id' => $id),
                array(
                    '%s',
                    '%d',
                ),
                array( '%d')
            );
        }

    }

    /**
     * Redirect User After Item Submission.
     *
     * @return null
     */
    public function redirect_after_item_submission(){
    	if( pd_ot_get_option('pd_redirect_after_item_approval') != '' ){
    		$redirect_page = pd_ot_get_option('pd_redirect_after_item_approval');
    		$redirect_url = get_permalink( $redirect_page );
    		// wp_redirect( get_permalink($redirect_page) );
    		// exit;
    	?>
			<script>
				window.location.replace("<?php echo $redirect_url; ?>");
			</script>
    	<?php
    	}
    }

    /**
     * Delete User Entry.
     *
     * @return null
     */

    public function delete_subscriber_profile( $id ) {
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}pd_user_entry where 1 and id = ".$id;
        $pdata = $wpdb->get_row($sql);

        if(@$pdata->approval==1){
            $this->deny_subscriber_profile($id);
        }

        $wpdb->delete(
            "{$wpdb->prefix}pd_user_entry",
            array( 'id' => $id ),
            array( '%d' )
        );


    }

	public function clean($string) {
		$string = str_replace(' ', '-', trim($string)); // Replaces all spaces with hyphens.

		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
    /**
     * Deny User Entry.
     *
     * @return null
     */

    public function deny_subscriber_profile($id){
        global $wpdb;

        $sql = "SELECT * FROM {$wpdb->prefix}pd_user_entry where 1 and id = ".$id;
        $identifier = time();
        $pdata = $wpdb->get_row($sql);

        if( $pdata->approval==1 || $pdata->approval==3 ){

            $searchQuery = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE 1 and `post_id` = ".$pdata->pd_list." and `meta_key` = 'qcpd_list_item01'";
            $results = @$wpdb->get_results($searchQuery);
			
			foreach($results as $result){
				
				$unserialize = unserialize($result->meta_value);
				if($pdata->custom == $unserialize['qcpd_timelaps']){
					
					$meta_id = @$result->meta_id;

					@$wpdb->delete(
						"{$wpdb->prefix}postmeta",
						array( 'meta_id' => $meta_id ),
						array( '%d' )
					);

				}
				
				
			}
			
			$wpdb->update(
				$wpdb->prefix.'pd_user_entry',
				array(
					'custom'  => '',
					'approval'=> 2
				),
				array( 'id' => $id),
				array(
					'%s',
					'%d',
				),
				array( '%d')
			);

        }

    }

	/*
	*Wp enqueue Script
	* Load stylesheet.
	*/
	public function pdcustom_dashboard_enqueue_style(){

	}

	/*
	*Wp ajax function
	*Category filter
	*/
	public function qcld_pd_category_filter_fnc(){
		
		$category = sanitize_text_field($_POST['cat']);
		if($category!=''){
			$pd = new WP_Query( array( 
				'post_type' => 'pd',
				'tax_query' => array(
					array (
						'taxonomy' => 'pd_cat',
						'field' => 'name',
						'terms' => $category,
					)
				),
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'menu_order'
				) 
			);
		}else{
			$pd = new WP_Query( array( 
				'post_type' => 'pd',				
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'menu_order'
				) 
			);
		}
								
		
		$excludel = pd_ot_get_option('pd_exclude_list');
		
		while( $pd->have_posts() ) : $pd->the_post();
		?>
			<?php 
			if($excludel!=''){
				$exclude = explode(',',$excludel);
				if(!in_array(get_the_ID(),$exclude)){
					?>
					<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
					<?php
				}
			}else{
			?>
			<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
			<?php
			}
			?>
			
		<?php
		endwhile;
				
		die();
		
	}

    /*
    *
    * Load before header
    */
    function pdcustom_redirect_load_before_headers()
    {
        if (isset($_GET['action']) and $_GET['action'] == 'logout') {
            wp_logout();
			
			if(pd_ot_get_option('pd_logout_redirect')!='login'){
				$url = get_home_url();
			}else{
				$url = qc_sbd_login_page()->pdcustom_login_get_translated_option_page('sbd_login_url');
			}
            
            wp_safe_redirect($url);
        }
    }

	/*
	*
	* GET Lists Status
	*/
	public function getStatus($args){
		if($args==0){
			return '<span style="color:#f4b042">'.__('Pending', 'qc-pd').'</span>';
		}elseif($args==1){
			return '<span style="color:green">'.__('Approved', 'qc-pd').'</span>';
		}elseif($args==2){
			return '<span style="color:red">'.__('Deny', 'qc-pd').'</span>';
		}else{
			return '<span style="color:#f4b042">'.__('Edited', 'qc-pd').'</span>';
		}
	}

	/*
	*
	* GET Image
	*/
	public function getImage($args){
		if($args!=''){
			
			if (strpos($args, 'http') === FALSE){
				$img = wp_get_attachment_image_src($args);
				echo '<img src="'.$img[0].'" width="50"/>';
			}else{
				echo '<img src="'.$args.'" width="50"/>';
			}
			
			
			
		}else{
			echo '<img src="'.QCSBD_IMG_URL.'/no-image.png'.'" width="50"/>';
		}
	}	
	
	public function sbd_dashboard_show(){
		ob_start();
		global $wpdb;

		wp_enqueue_script( 'qcopd-google-map-script');
		wp_enqueue_script( 'qcopd-google-map-scriptasd');
		wp_enqueue_script( 'qcpd-custom-script');

		$current_user = wp_get_current_user();
		$table             = $wpdb->prefix.'pd_user_entry';
		$package_purchased_table = $wpdb->prefix.'pd_package_purchased';
        $package_table = $wpdb->prefix.'pd_package';

        $dashboard_url = qc_sbd_login_page()->pdcustom_login_get_translated_option_page('sbd_dashboard_url');
		
		if(is_user_logged_in() && (in_array('pduser',$current_user->roles) or in_array('administrator',$current_user->roles) or pd_ot_get_option('pd_enable_anyusers')=='on')){
			$url = qc_sbd_login_page()->pdcustom_login_get_translated_option_page('sbd_dashboard_url');

			//check whether package enable or not
			
			$get_package = $wpdb->get_row("select * from $package_table where 1 limit 1");
			
            $itempurchase = $wpdb->get_row("select sum(p.item)as cnt from $package_table as p, $package_purchased_table as pd where pd.package_id = p.id and pd.user_id = ".$current_user->ID);

            if(!empty($get_package) and $get_package->enable==1){
	            if($itempurchase->cnt!='' and $itempurchase->cnt > 0){
		            $this->show_package = true;
		            $this->total_item = $itempurchase->cnt;
	            }else{
		            $this->show_package = true;
	            }
            }else{
                $this->show_package = false;
            }

			if(pd_ot_get_option('pd_enable_free_submission')=='on'){
				if(pd_ot_get_option('pd_free_item_limit')!=''){
					$this->total_item += pd_ot_get_option('pd_free_item_limit');
                }
            }

            //find total submited item
            $submited_item = $wpdb->get_row("select count(*)as cnt from $table where 1 and user_id =".$current_user->ID);
			if($submited_item->cnt==''){
				$this->submited_item = 0;
            }else{
			    $this->submited_item = $submited_item->cnt;
            }

			if($this->total_item > 0){
			    if($this->total_item > $submited_item->cnt){
				    $this->remain_item = ($this->total_item - $submited_item->cnt);
				    $this->allow_item_submit = true;
                }else{
			        $this->remain_item = 0;
				    $this->allow_item_submit = false;
                }
            }else{
			    $this->remain_item = 0;
				$this->allow_item_submit = false;
            }
			
			if(is_user_logged_in() && in_array('administrator',$current_user->roles)){
				$this->allow_item_submit = true;
			}

?>
		<div class="sbd_dashboard_main_area">
			<nav class="pdnav pdnav--red">
				<ul class="pdnav__list">
				
					<li class="pdnav__list__item <?php echo (!isset($_GET['action'])?'pdactive':''); ?>"><a href="<?php echo $url; ?>"><?php echo pd_ot_get_option('sbd_lan_dashboard') != '' ? pd_ot_get_option('sbd_lan_dashboard') : __('Dashboard', 'qc-pd') ?></a></li>
                    

					<?php if(pd_ot_get_option('pd_package_tab_add')!='on'): ?>
					<li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='entry'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'entry', $url ) ) ?>"><?php echo pd_ot_get_option('sbd_lan_add') != '' ? pd_ot_get_option('sbd_lan_add') : __('Add', 'qc-pd') ?></a></li>
					<?php endif; ?>
					<li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='entrylist'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'entrylist', $url ) ) ?>"><?php echo pd_ot_get_option('sbd_lan_manage') != '' ? pd_ot_get_option('sbd_lan_manage') : __('Manage', 'qc-pd') ?></a></li>

                   <?php if(pd_ot_get_option('sbd_enable_claim_listing')=='on' && pd_ot_get_option('pd_package_tab_claim')!='on'): ?>
                   <li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='claim'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'claim', $url ) ) ?>"><?php echo pd_ot_get_option('sbd_lan_claim') != '' ? pd_ot_get_option('sbd_lan_claim') : __('Claim', 'qc-pd') ?></a></li>
				<?php endif; ?>

                <?php if($this->show_package==true && pd_ot_get_option('pd_package_tab')!='on'): ?>
                <li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='package'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'package', $url ) ) ?>"><?php echo pd_ot_get_option('sbd_lan_packages') != '' ? pd_ot_get_option('sbd_lan_packages') : __('Packages', 'qc-pd') ?></a></li>
                <?php endif; ?>
				
				<?php if(pd_ot_get_option('pd_enable_bookmark')=='on'): 
					$b_title = pd_ot_get_option('pd_bookmark_title');
				?>
                   <li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='favorite'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'favorite', $url ) ) ?>"><?php echo ($b_title!=''?$b_title:'Favorites'); ?></a></li>
				<?php endif; ?>

<!--					<li class="pdnav__list__item --><?php //echo (isset($_GET['action'])&&$_GET['action']=='payment'?'pdactive':''); ?><!--"><a href="--><?php //echo esc_url( add_query_arg( 'action', 'payment', $url ) ) ?><!--">Payment</a></li>-->
					
<!--					<li class="pdnav__list__item --><?php //echo (isset($_GET['action'])&&$_GET['action']=='help'?'pdactive':''); ?><!--"><a href="--><?php //echo esc_url( add_query_arg( 'action', 'help', $url ) ) ?><!--">Help</a></li>-->

                    <li class="pdnav__list__item <?php echo (isset($_GET['action'])&&$_GET['action']=='logout'?'pdactive':''); ?>"><a href="<?php echo esc_url( add_query_arg( 'action', 'logout', $url ) ) ?>"><?php echo pd_ot_get_option('sbd_lan_log_out') != '' ? pd_ot_get_option('sbd_lan_log_out') : __('Logout', 'qc-pd') ?></a></li>

				</ul>
			</nav>
			
			<?php if(!isset($_GET['action']) and !isset($_GET['payment']) )://Dashboard ?>
			
			
				<?php 
				$userpkgs = $wpdb->get_results("select * from $package_purchased_table where 1 and user_id = ".$current_user->ID." order by date DESC");
				if(!empty($userpkgs)):
					foreach($userpkgs as $userpkg):	
					$get_package = $wpdb->get_row("select * from $package_table where 1 and id = '".$userpkg->package_id."'");					
					//print_r($userpkg);exit;
						if(strtotime(date('Y-m-d')) < strtotime($userpkg->expire_date)):
							$package_title = '<b>'.$get_package->title.'</b>';
							$package_expire_date = '<b>'.date( "Y-m-d", strtotime( $userpkg->expire_date ) ).'</b>';
							$package_expire_text = pd_ot_get_option('sbd_lan_package_expiry_date_notification') != '' ? pd_ot_get_option('sbd_lan_package_expiry_date_notification') : __('Your Package #package_name will expire at #expire_date', 'qc-pd');
							$package_expire_text = str_replace(
														array('#package_name', '#expire_date'),
														array($package_title, $package_expire_date),
														$package_expire_text
													);
						?>
							<div class="sbd_package_notification">
								<?php echo $package_expire_text; ?>
							</div>
						<?php
						else:
							$package_title = '<b>'.$get_package->title.'</b>';
							$package_expire_date = '<b>'.date( "Y-m-d", strtotime( $userpkg->expire_date ) ).'</b>';
							$package_expire_text = pd_ot_get_option('sbd_lan_package_already_expire_notification') != '' ? pd_ot_get_option('sbd_lan_package_already_expire_notification') : __('Your Package #package_name is already expired at #expire_date', 'qc-pd');
							$package_expire_text = str_replace(
														array('#package_name', '#expire_date'),
														array($package_title, $package_expire_date),
														$package_expire_text
													);
						?>
							<div class="sbd_package_notification">
								<?php echo $package_expire_text; ?>
							</div>
						<?php
						endif;

					endforeach;
				endif;
				?>
			
                <?php

                if(isset($_POST['first_name']) && $_POST['first_name']!=''){
	                $user_id = $current_user->ID;
	                $user_data = wp_update_user( array( 'ID' => $user_id, 'first_name' => $_POST['first_name'], 'last_name' =>$_POST['last_name'], 'user_email'=> $_POST['user_email'], 'user_login'=> $_POST['user_login'] ) );
	                if ( is_wp_error( $user_data ) ) {
		                echo '<p style="color:red;font-size: 20px;">'.__('Something Went Wrong.','qc-pd').'</p>';
	                } else {
		                // Success!
		                echo '<p style="color:green;font-size: 20px;">'.__('User profile updated.','qc-pd').'</p>';
	                }
                }
                ?>
                <style type="text/css">
                    .pd_total_count {
                        font-family: arial, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }

                    .pd_total_count td {
                        text-align: left;
                        padding: 8px;
                    }
                </style>
				<h2>
					<?php 
					$fullName = $current_user->user_firstname.' '.$current_user->user_lastname;
					echo (pd_ot_get_option('sbd_lan_hi')!=''?pd_ot_get_option('sbd_lan_hi'):__('Hi','qc-pd')).sprintf(__( ' %s, ', 'qc-pd' ),$fullName).(pd_ot_get_option('sbd_lan_wc_dashboard')!=''?pd_ot_get_option('sbd_lan_wc_dashboard'):__('Welcome to Dashboard.','qc-pd'));
					?>
				</h2>
				
				
				
                <table class="pd_total_count">
                    <?php
                    if(in_array('administrator',$current_user->roles)){
                        ?>
                        <tr>
                            <td><?php echo (pd_ot_get_option('sbd_lan_total_listing')!=''?pd_ot_get_option('sbd_lan_total_listing'):__('Total Listings','qc-pd')) ?> : <?php echo (pd_ot_get_option('sbd_lan_unlimited')!=''?pd_ot_get_option('sbd_lan_unlimited'):__('Unlimited','qc-pd')); ?></td>
                            <td><?php echo (pd_ot_get_option('sbd_lan_submited_listing')!=''?pd_ot_get_option('sbd_lan_submited_listing'):__('Submited Listings','qc-pd')) ?> : <?php echo $this->submited_item; ?></td>
                            <td><?php echo (pd_ot_get_option('sbd_lan_remaining_listing')!=''?pd_ot_get_option('sbd_lan_remaining_listing'):__('Remaining Listings','qc-pd')) ?> : <?php echo (pd_ot_get_option('sbd_lan_unlimited')!=''?pd_ot_get_option('sbd_lan_unlimited'):__('Unlimited','qc-pd')); ?></td>
                        </tr>
                        <?php
                    }else{
                        ?>
                        <tr>
                            <td><?php echo (pd_ot_get_option('sbd_lan_total_listing')!=''?pd_ot_get_option('sbd_lan_total_listing'):__('Total Listings','qc-pd')) ?> : <?php echo $this->total_item; ?></td>
                            <td><?php echo (pd_ot_get_option('sbd_lan_submited_listing')!=''?pd_ot_get_option('sbd_lan_submited_listing'):__('Submited Listings','qc-pd')) ?> : <?php echo $this->submited_item; ?></td>
                            <td><?php echo (pd_ot_get_option('sbd_lan_remaining_listing')!=''?pd_ot_get_option('sbd_lan_remaining_listing'):__('Remaining Listings','qc-pd')) ?> : <?php echo $this->remain_item; ?></td>
                        </tr>
                        <?php
                    }
                    ?>

                </table>

				<?php
				if(pd_ot_get_option('pd_package_dashboard')!='on'):
				
				if(pd_ot_get_option('sbd_enable_paypal_test_mode')=='on'){
					$mainurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
				}else{
					$mainurl = 'https://www.paypal.com/cgi-bin/webscr';
				}				
				$packages = $wpdb->get_results("select * from $package_table where 1");
				?>
				<?php if(!empty($packages)): ?>
				<div class="sbd_price-table-wrapper">

					<h2><?php echo (pd_ot_get_option('sbd_lan_paid_pkg')!=''?pd_ot_get_option('sbd_lan_paid_pkg'):__('Available Packages','qc-pd')) ?></h2>
					
					<?php if(pd_ot_get_option('pd_enable_free_submission')=='on'): ?>
					  <div class="sbd_pricing-table">
						<h2 class="sbd_pricing-table__header"><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></h2>

						<ul class="sbd_pricing-table__list">
						  <li><?php echo (pd_ot_get_option('sbd_lan_free_listing')!=''?pd_ot_get_option('sbd_lan_free_listing'):__('Free Listing', 'qc-pd')) ?></li>
						  <li><?php echo pd_ot_get_option('pd_free_item_limit'); ?> <?php echo (pd_ot_get_option('sbd_lan_listing')!=''?pd_ot_get_option('sbd_lan_listing'):__('Listing', 'qc-pd')) ?></li>
						  <li><?php echo (pd_ot_get_option('sbd_lan_free')!=''?pd_ot_get_option('sbd_lan_free'):__('Free', 'qc-pd')) ?></li>
						  <li>-</li>
						  
						  
						</ul>
					  </div>
					<?php endif; ?>
					
					<?php
					$pc = 0;
					foreach($packages as $package):
					$pc++;
					?>
					
				  <div class="sbd_pricing-table" <?php echo ($pc==1?'style="margin-left: 0px;"':''); ?>>
					<h2 class="sbd_pricing-table__header"><?php echo (isset($package->title)&&$package->title!=''?$package->title:''); ?></h2>

					<ul class="sbd_pricing-table__list">
					  <li><?php echo (isset($package->description)&&$package->description!=''?html_entity_decode($package->description):''); ?></li>
					  <li><?php echo (isset($package->item)&&$package->item!=''?$package->item:'0'); ?> <?php echo (pd_ot_get_option('sbd_lan_listing_for')!=''?pd_ot_get_option('sbd_lan_listing_for'):__('Listing for','qc-pd')) ?> <?php echo (isset($package->duration)&&$package->duration!='lifetime'?sprintf( _n( '%s Month', '%s Months', $package->duration, 'qc-pd' ), $package->duration ):ucwords($package->duration)); ?></li>
					  <li><?php echo (isset($package->Amount)&&$package->Amount!=''?$package->Amount:'0'); ?> <?php echo $package->currency; ?></li>
					  <li>
						<?php if(pd_ot_get_option('sbd_enable_paypal_payment')!='off'): ?>
						  <form action="<?php echo $mainurl; ?>" method="post" id="paypalProcessor">
							<input type="hidden" name="cmd" value="_xclick" />

							<input type="hidden" name="business" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
							<input type="hidden" name="currency_code" value="<?php echo $package->currency; ?>" />
							<input type="hidden" name="no_note" value="1"/>
							<input type="hidden" name="no_shipping" value="1" />
							<input type="hidden" name="charset" value="utf-8" />

							<input type="hidden" name="notify_url" value="<?php echo esc_url( add_query_arg( array('user'=> $current_user->ID, 'packagesave'=>$package->id), $url ) ) ?>" />

							<input type="hidden" name="return" value="<?php echo esc_url( add_query_arg( 'payment', 'success', $url ) ) ?>" />

							<input type="hidden" name="cancel_return" value="<?php echo esc_url( add_query_arg( 'payment', 'cancel', $url ) ) ?>">
							<input type="hidden" name="item_name" value="<?php echo $package->title; ?>">
							<input type="hidden" name="amount" value="<?php echo (isset($package->Amount)&&$package->Amount!=''?$package->Amount:'0'); ?>">

							<input type="hidden" name="quantity" value="1">
							<input type="hidden" name="receiver_email" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
							<input type="image" name="submit" border="0"  src="<?php echo QCSBD_IMG_URL.'/btn_buynow_LG.gif'; ?>" alt="PayPal - The safer, easier way to pay online">
							<p style="margin: 0px 0px;padding: 0px;color: #000;font-size: 14px;margin-top: -6px;"><?php echo (pd_ot_get_option('sbd_lan_paypal')!=''?pd_ot_get_option('sbd_lan_paypal'):__('Paypal', 'qc-pd')); ?></p>
						  </form>
						  <?php endif; ?>
						  <?php if(pd_ot_get_option('sbd_enable_stripe_payment')=="on"): ?>
						  <form action="<?php echo esc_url( add_query_arg( array('payment'=> 'stripe-save', 'userid'=>$current_user->ID, 'package'=> $package->id), $url ) ) ?>" method="post">
								<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
									  data-key="<?php echo pd_ot_get_option('sbd_stripe_public_key'); ?>"
									  data-description="<?php echo $package->title; ?>"
									  data-amount="<?php echo (isset($package->Amount)&&$package->Amount!=''?($package->Amount*100):'0'); ?>"
									  data-locale="auto"
									  data-currency="<?php echo $package->currency; ?>"	
									  ></script>
								<p style="margin: 0px 0px;padding: 0px;color: #000;font-size: 14px;"><?php echo (pd_ot_get_option('sbd_lan_stripe')!=''?pd_ot_get_option('sbd_lan_stripe'):__('Stripe', 'qc-pd')); ?></p>
							</form>
							<?php endif; ?>
					  </li>
					</ul>
				  </div>
				  <?php endforeach; ?>
				  
				</div>
				<?php endif; ?>
				<?php endif; ?>
				
				
                <?php
				if(pd_ot_get_option('pd_profile_update')=='on'){
                ?>                    				
				<p class="updateprofile"><?php echo pd_ot_get_option('sbd_lan_update_profile_info') != '' ? pd_ot_get_option('sbd_lan_update_profile_info') : __('Update Profile Info', 'qc-pd') ?></p>
				<form method="post">
				<ul class="pd_form-style-1">
					<li>
						<label><?php echo pd_ot_get_option('sbd_lan_full_name') != '' ? pd_ot_get_option('sbd_lan_full_name') : __('Full Name', 'qc-pd') ?> <span class="pd_required">*</span></label>
						<input type="text" name="first_name" class="pd_field-divided" placeholder="<?php echo pd_ot_get_option('sbd_lan_first') != '' ? pd_ot_get_option('sbd_lan_first') : __('First', 'qc-pd') ?>" value="<?php echo $current_user->user_firstname; ?>" />&nbsp;
						<input type="text" name="last_name" class="pd_field-divided" placeholder="<?php echo pd_ot_get_option('sbd_lan_last') != '' ? pd_ot_get_option('sbd_lan_last') : __('Last', 'qc-pd') ?>" value="<?php echo $current_user->user_lastname; ?>" />
					</li>
					<li>
						<label><?php echo pd_ot_get_option('sbd_lan_email') != '' ? pd_ot_get_option('sbd_lan_email') : __('Email', 'qc-pd') ?> <span class="pd_required">*</span></label>
						<input type="email" name="user_email" class="field-long" value="<?php echo $current_user->user_email; ?>" />
					</li>
					<li>
						<label><?php echo pd_ot_get_option('sbd_lan_only_username') != '' ? pd_ot_get_option('sbd_lan_only_username') : __('Username', 'qc-pd') ?> <span class="pd_required">*</span></label>
						<input type="text" name="user_login" class="field-long" value="<?php echo $current_user->user_login; ?>" />
					</li>

					<li>
						<input class="pd_submit_style" type="submit" value="<?php echo pd_ot_get_option('sbd_lan_submit') != '' ? pd_ot_get_option('sbd_lan_submit') : __('Submit', 'qc-pd') ?>" />
					</li>
				</ul>
				</form>
                <?php } ?>

			<?php endif; ?>
			
			<?php if(isset($_GET['action']) && $_GET['action']=='entry' )://entry 
				require(QCSBD_DIR_MOD.'/dashboard/pd_entry.php');
			 endif; ?>
			
			<?php if(isset($_GET['action']) && $_GET['action']=='entrylist' )://entrylist ?>
				<?php require(QCSBD_DIR_MOD.'/dashboard/pd_entrylist.php'); ?>
			<?php endif; ?>
			
			<?php if(pd_ot_get_option('sbd_enable_claim_listing')=='on'): ?>
				<?php if(isset($_GET['action']) && $_GET['action']=='claim' )://entrylist ?>
					<?php require(QCSBD_DIR_MOD.'/dashboard/sld_claim.php'); ?>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if(isset($_GET['action']) && $_GET['action']=='package' )://Payment ?>
				<?php require(QCSBD_DIR_MOD.'/dashboard/pd_package.php'); ?>
			<?php endif; ?>
			
			<?php if(isset($_GET['action']) && $_GET['action']=='favorite' )://Favorite ?>
				<?php require(QCSBD_DIR_MOD.'/dashboard/pd_favorite.php'); ?>
			<?php endif; ?>

			<?php if(isset($_GET['action']) && $_GET['action']=='help' )://help ?>
				<h2>This is SBD Help page.</h2>
			<?php endif; ?>

			<?php if(isset($_GET['payment']) && $_GET['payment']=='success' )://help ?>
                <p style="color: #000;font-size: 16px;margin: 11px 0px;text-align:center;"><?php echo __('Payment has been done successfully. Thank you!
                    Now you can add links to our Lists.','qc-pd') ?></p>
			<?php endif; ?>

			<?php if(isset($_GET['payment']) && $_GET['payment']=='claim-stripe' )://help ?>
                <p style="color: #000;font-size: 16px;margin: 11px 0px;text-align:center;"><?php echo __('Payment has been done successfully for Claim Listing. Thank you! ','qc-pd') ?></p>
			<?php endif; ?>
			
			<?php if(isset($_GET['payment']) && $_GET['payment']=='claim-paypal' )://help ?>
                <p style="color: #000;font-size: 16px;margin: 11px 0px;text-align:center;"><?php echo __('Payment has been done successfully for Claim Listing. Thank you! ','qc-pd') ?></p>
			<?php endif; ?>

			<?php if(isset($_GET['action']) && $_GET['action']=='entryedit' )://help ?>
				<?php require(QCSBD_DIR_MOD.'/dashboard/pd_entryedit.php') ?>
			<?php endif; ?>
			
		</div>
<?php		
		}elseif(!is_user_logged_in()){
            $url = qc_sbd_login_page()->pdcustom_login_get_translated_option_page('sbd_login_url');

            if(pd_ot_get_option('sbd_lan_login_to_view_content')!=''){
				echo pd_ot_get_option('sbd_lan_login_to_view_content');
			}else{
				echo __('You have to login in to view this content. ', 'qc-pd') ;
			}

			if(pd_ot_get_option('sbd_lan_click_to_login')!=''){
				echo "<a href='".esc_url($url)."'>".pd_ot_get_option('sbd_lan_click_to_login')."</a>";
				//echo sprintf(__('<a href="%s">Click Here to log in</a>.','qc-pd'),$url);
			}else{
            	echo sprintf(__('<a href="%s">Click Here to log in</a>.','qc-pd'),$url);
			}
        }else{
			echo __('Sorry, You are not allowed to view the content of this page.','qc-pd');
		}
		
			$customCss = pd_ot_get_option( 'pd_custom_style' );

			if( trim($customCss) != "" ) :
		?>
			<style>
				<?php echo trim($customCss); ?>
			</style>

		<?php endif;
		$customjs = pd_ot_get_option( 'pd_custom_js' );
	
			if($customjs!=''):
		?>
				<script type="text/javascript">
				jQuery(document).ready(function($) {
					<?php echo $customjs; ?>
				});
				</script>
		<?php
			endif;
			
			$customCss = pd_ot_get_option( 'pd_custom_style' );

			if( trim($customCss) != "" ) :
		?>
				<style>
					<?php echo trim($customCss); ?>
				</style>
		<?php endif;
		
		
		return ob_get_clean();
	}
}

function qc_sbd_dashboard(){
	return qc_sbd_dashboard::instance();
}
qc_sbd_dashboard();