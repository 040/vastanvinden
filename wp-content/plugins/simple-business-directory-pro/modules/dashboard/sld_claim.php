<?php
if ( ! defined( 'ABSPATH' ) ) exit;
global $wp;
$current_url =  home_url( $wp->request );

$ctable = $wpdb->prefix.'sbd_claim_purchase';
$cptable = $wpdb->prefix.'sbd_claim_configuration';
$claimpayment = $wpdb->get_row("select * from $cptable where 1");

//Claim listing add handle goes here
if(isset($_POST['submitclaimitem'])){
	
	$date = date('Y-m-d H:i:s');
	
	$wpdb->insert(
		$ctable,
		array(
			'date'  => $date,
			'user_id'   => $current_user->ID,
			'listid'   => $_POST['claim_list'],
			'item'   => $_POST['claim_item'],
			'status'   => 'pending',
			
		)
	);
	$lastid = $wpdb->insert_id;
		
	$this->pd_claim_notification($current_user->ID, $_POST['claim_item']);
	
	
	if(!isset($claimpayment->enable) or $claimpayment->enable!=1){
		if(pd_ot_get_option('sbd_auto_approve_claim')=='on'){
			//$this->approve_claim_listing($lastid);
			//echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 15px;">Claim Listing Successfull! <br/></div>';
		}
	}
	
}

if(isset($_GET['act']) and $_GET['act']=='delete' ){
	$ctable = $wpdb->prefix.'sbd_claim_purchase';
	$id = $_GET['id'];
	$pdata = $wpdb->get_row("Select * from $ctable where 1 and `id`='$id'");
	//$userid = $pdata->user_id;
	
	$claim_delete = $wpdb->delete(
		$ctable,
		array( 'id' => $id ),
		array( '%d' )
	);
	
	echo '<div style="color: green;border: 1px solid green;margin: 2px;padding: 2px;text-align: center;margin-bottom: 8px;font-size: 15px;margin-top: 10px;">Your Item has been Deleted sucessfully.<br></div>';
}

$claims = $wpdb->get_results("select * from $ctable where 1 and `user_id`=$current_user->ID ");

?>

<?php if(isset($_GET['er']) && $_GET['er']!=''): ?>
<p style="color:#fff;background:red;padding:10px;text-align:center"><?php echo $_GET['er'] ?></p>
<?php endif; ?>
<?php if(isset($_GET['success']) && $_GET['success']!=''): ?>
<p style="color:#fff;background:green;padding:10px;text-align:center"><?php echo $_GET['success'] ?></p>
<?php endif; ?>

<h2><?php echo (pd_ot_get_option('sbd_lan_add_claim_listing')!=''?pd_ot_get_option('sbd_lan_add_claim_listing'):__('Add Claim Listing', 'qc-pd')); ?></h2>

<form action="<?php echo $current_url.'/?action=claim'; ?>" method="POST">
	<ul class="pd_form-style-1 pd_width">

		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_list_name')!=''?pd_ot_get_option('sbd_lan_list_name'):__('Select List Name','qc-pd')); ?></label>
			<select style="" id="sld_claim_list" name="claim_list" required>
				<option value=""><?php echo __('None', 'qc-pd') ?></option>
				<?php 
				$list_args_total = array(
					'post_type' => 'pd',
					'posts_per_page' => -1,
				);
				$list_query = new WP_Query( $list_args_total );
				while ( $list_query->have_posts() )
				{
					$list_query->the_post();
					echo '<option value="'.get_the_ID().'">'.get_the_title().'</option>';
				}
				?>
			</select>
		</li>
		<li>
			<label><?php echo (pd_ot_get_option('sbd_lan_item_name')!=''?pd_ot_get_option('sbd_lan_item_name'):__('Select Item Name','qc-pd')); ?></label>
			<select style="" id="sld_list_item" name="claim_item" required>
				<option value=""><?php echo __('None', 'qc-pd') ?></option>
			</select>
		</li>

        <li>
			<input type="submit" name="submitclaimitem" class="pd_submit_style" value="<?php echo (pd_ot_get_option('sbd_lan_submit')!=''?pd_ot_get_option('sbd_lan_submit'):__('Submit','qc-pd')); ?>" />
		</li>
	</ul>
</form>

<style type="text/css">

	strong {
		font-weight: bold;
	}

	em {
		font-style: italic;
	}

	table {
		background: #f5f5f5;
		border: 1px solid #fff !important;
		box-shadow: inset 0 1px 0 #fff;
		font-size: 12px;
		line-height: 24px;
		margin: 30px auto;
		text-align: left;
		width: 800px;
	}


	td {
		border-right: 1px solid #fff;
		border-left: 1px solid #e8e8e8;
		border-top: 1px solid #fff;
		border-bottom: 1px solid #e8e8e8;
		padding: 10px 15px;
		position: relative;
		transition: all 300ms;
	}



	td:last-child {
		border-right: 1px solid #e8e8e8;
		box-shadow: inset -1px 0 0 #fff;
	}

	tr:last-of-type td {
		box-shadow: inset 0 -1px 0 #fff;
	}

	tr:last-of-type td:first-child {
		box-shadow: inset 1px -1px 0 #fff;
	}

	tr:last-of-type td:last-child {
		box-shadow: inset -1px -1px 0 #fff;
	}
.pd_table_package_head{
	background: #474343;
	color: #fff;
	text-align: center;
}
	.pd_table_package_content{
		font-size:16px;
 }
.qc_pd_cell{
	text-align: center;
}

</style>

<h2><?php echo (pd_ot_get_option('sbd_lan_claim_listing')!=''?pd_ot_get_option('sbd_lan_claim_listing'):__('Claim Listing','qc-pd')); ?></h2>


<?php

if(pd_ot_get_option('sbd_enable_paypal_test_mode')=='on'){
    $mainurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
}else{
    $mainurl = 'https://www.paypal.com/cgi-bin/webscr';
}

if(!empty($claims)){
	?>
<?php 
if(isset($claimpayment->enable) and $claimpayment->enable==1){
	$paymentAmount = $claimpayment->Amount;
}else{
	$paymentAmount = 0;
}

/*
if($paymentAmount==$claim->paid_amount && $claim->status=='pending'):
echo __('<p>  Your claim list item is waiting for admin approval. After it is approved you will find your claim list item in "Your Links" Tab for editing.</p>','qc-pd').'<br>';
endif;
*/
?>
    <div class="qc_pd_table_area">
        <div class="qc_pd_table">

            <div class="qc_pd_row header">

                <div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'List Name', 'qc-pd' ) ?>
                </div>

                <div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Item Name', 'qc-pd' ); ?>
                </div>
                <div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Payable Amount', 'qc-pd' ); ?>
                </div>
				<div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Paid Amount', 'qc-pd' ); ?>
                </div>

                <div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Status', 'qc-pd' ); ?>
                </div>
				
				<?php if(!empty($claimpayment) and $claimpayment->enable==1): ?>
				<div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Payment', 'qc-pd' ); ?>
				</div>
				<?php endif; ?>

				<div class="qc_pd_cell qc_pd_table_head">
					<?php _e( 'Action', 'qc-opd' ); ?>
				</div>
                
            </div>

			<?php foreach($claims as $claim): ?>
                <div class="qc_pd_row">



                    <div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('List Name', 'qc-pd') ?></div>
						<?php echo get_the_title($claim->listid); ?>
                    </div>


                    <div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('Item Name', 'qc-pd') ?></div>
						<?php echo $claim->item; ?>
                    </div>

                    <div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('Payable Amount', 'qc-pd') ?></div>
						<?php 
							if(!empty($claimpayment) and $claimpayment->enable==1){
								echo $claimpayment->Amount.' '.$claimpayment->currency;
							}else{
								echo 0;
							}
						?>
                    </div>
					<div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('Paid Amount', 'qc-pd') ?></div>
						<?php echo $claim->paid_amount.' '.$claimpayment->currency; ?>
                    </div>

                    <div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('Status', 'qc-pd') ?></div>
						<?php echo ucfirst($claim->status); ?>
                    </div>
					<div class="qc_pd_cell">
                        <div class="pd_responsive_head"><?php echo __('Payment', 'qc-pd') ?></div>
					<?php if(!empty($claimpayment) and $claimpayment->enable==1 and $claim->paid_amount==0 and $claim->status=='pending'): ?>
                    
						<?php if(pd_ot_get_option('sbd_enable_paypal_payment')!='off'): ?>
						
                        <form action="<?php echo $mainurl; ?>" method="post" id="paypalProcessor">
                            <input type="hidden" name="cmd" value="_xclick" />

                            <input type="hidden" name="business" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
                            <input type="hidden" name="currency_code" value="<?php echo $claimpayment->currency; ?>" />
                            <input type="hidden" name="no_note" value="1"/>
                            <input type="hidden" name="no_shipping" value="1" />
                            <input type="hidden" name="charset" value="utf-8" />

                            <input type="hidden" name="notify_url" value="<?php echo esc_url( add_query_arg( array('payment'=> 'claim-paypal', 'pkg'=> $claim->id), $url ) ) ?>" />

                            <input type="hidden" name="return" value="<?php echo esc_url( add_query_arg( 'payment', 'success', $url ) ) ?>" />

                            <input type="hidden" name="cancel_return" value="<?php echo esc_url( add_query_arg( 'payment', 'cancel', $url ) ) ?>">
                            <input type="hidden" name="item_name" value="Claim Listing payment for <?php echo $claim->item; ?>">
                            <input type="hidden" name="amount" value="<?php echo (isset($claimpayment->Amount)&&$claimpayment->Amount!=''?$claimpayment->Amount:'0'); ?>">

                            <input type="hidden" name="quantity" value="1">
                            <input type="hidden" name="receiver_email" value="<?php echo pd_ot_get_option('sbd_paypal_email'); ?>">
                            <input type="image" name="submit" border="0"  src="<?php echo QCSBD_IMG_URL.'/btn_buynow_LG.gif'; ?>" alt="PayPal - The safer, easier way to pay online">
							<span style="display:block;margin:0;padding:0;">Paypal</span>
                        </form>
						<?php endif; ?>
						
						
						<?php if(pd_ot_get_option('sbd_enable_stripe_payment')=="on"): ?>
							
							<form action="<?php echo esc_url( add_query_arg( array('payment'=> 'claim-stripe', 'pkg'=> $claim->id), $url ) ) ?>" method="post">
								<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
									  data-key="<?php echo pd_ot_get_option('sbd_stripe_public_key'); ?>"
									  data-description="Claim Listing Payment for <?php echo $claim->item; ?>"
									  data-amount="<?php echo (isset($claimpayment->Amount)&&$claimpayment->Amount!=''?($claimpayment->Amount*100):'0'); ?>"
									  data-locale="auto"						  
									  ></script>
									 <span style="display:block;margin:0;padding:0;">Stripe</span>
							</form>
						
						<?php endif; ?>
						
                    

					<?php endif; ?>
					</div>

					<div class="qc_pd_cell">
						<?php if( $claim->status == 'approved' ): ?>
							<div class="pd_responsive_head"><?php echo __('Action', 'qc-opd') ?></div>
							
							<?php if($claim->status=='approved'):
							
								$userentry = $wpdb->prefix.'pd_user_entry';
								$get_user_item = $wpdb->get_row("Select * from $userentry where 1 and `pd_list` = '".$claim->listid."' and `user_id` = '".$claim->user_id."' order by id desc limit 1");

							?>
							<a href="<?php echo esc_url( add_query_arg( array('action'=>'entryedit','id'=>$get_user_item->id), $dashboard_url ) ); ?>">
								<button class="entry_list_edit"><?php echo __('Edit Item', 'qc-opd') ?></button>
							</a>
							<?php endif; ?>
							
							<a href="<?php echo esc_url( add_query_arg( array('action'=>'claim','act'=>'delete','id'=>$claim->id), $dashboard_url ) ); ?>">
								<button class="claim_list_delete entry_list_delete"><?php echo __('Delete', 'qc-opd') ?></button>
								
							</a>
						<?php endif; ?>
					</div>
                </div>
			<?php endforeach; ?>


        </div>

    </div>
	<?php
}else{
?>
    <p><?php echo __('No Claim Listing', 'qc-pd') ?></p>
<?php
}
?>

<script type="text/javascript">
	jQuery(window).on('load',function(){
		jQuery('.claim_list_delete').on('click', function(e){
			var claim_dlt_popup = confirm("Are you sure to delete this Record?");
			if (claim_dlt_popup == true) {
			  
			} else {
			  e.preventDefault();
			}
		});
	});
</script>