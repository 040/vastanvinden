<?php
define( 'sbd_addon_url', plugin_dir_url(__FILE__) );
define( 'sbd_SCRIPT_DEBUG', true );
add_action('admin_menu', 'sbd_addon_page', 999);
function sbd_addon_page(){
	add_submenu_page(
  		'edit.php?post_type=pd',
  		'AddOns',
  		'AddOns',
  		'manage_options',
  		'sbd-addons-page',
  		'sbd_addon_page_cb'
  	);
}

function sbd_addon_page_cb(){
	require_once('admin_ui2.php');
}