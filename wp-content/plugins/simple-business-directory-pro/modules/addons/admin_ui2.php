<?php 
global $wpdb;
$table    = $wpdb->prefix.'wpbot_subscription';
$sql1 = "SELECT count(*) FROM $table where 1";
$total_user_data = $wpdb->get_var( $sql1 );

$tableuser    = $wpdb->prefix.'wpbot_sessions';
$session_exists = $wpdb->get_row("select * from $tableuser where 1 and id = '1'");

if(!empty($session_exists)){
    $total_session = $session_exists->session;
}else{
    $total_session = 0;
}


global $woocommerce, $wp_scripts;
        $suffix = defined('sbd_SCRIPT_DEBUG') && sbd_SCRIPT_DEBUG ? '' : '.min';
            
            wp_register_style('qlcd-sld-admin-style', sbd_addon_url . 'css/admin-style.css', '', '', 'screen');
            wp_enqueue_style('qlcd-sld-admin-style');

            wp_enqueue_script('jquery');

            wp_register_style('qcld-sld-bootcampqc-css', sbd_addon_url . 'css/bootstrap.min.css', '', '', 'screen');
            wp_enqueue_style('qcld-sld-bootcampqc-css');



?>
<div class="wrap">
    <h1 class="wpbot_header_h1"><?php echo esc_html__('Simple Business Directory', 'wpchatbot'); ?> </h1>
</div>
<div class="wp-chatbot-wrap">
    <div class="wpbot_dashboard_header container"><h1><?php echo esc_html__('Simple Business Directory', 'kbx-qc'); ?></h1></div>

    <div class="wpbot_addons_section container">
        <div class="wpbot_single_addon_wrapper qc-display-flex qc-justify-center qc-flex-wrap kbx_pb_0">
            <h2 class="wpbot_single_addon_title"><?php echo esc_html__('AddOns', 'kbx-qc'); ?></h2>
            
            <div class="wpbot_single_addon kbx-center-addon">
                <div class="wpbot_single_content">
                    <div class="wpbot_addon_image">
                        <img src="<?php echo esc_url(sbd_addon_url.'images/rating-addon-logo.png'); ?>" title="" />
                    </div>
                    <div class="wpbot_addon_content">
                        <div class="wpbot_addon_title"><?php echo esc_html__('SBD Review Rating AddOn', 'kbx-qc'); ?></div>
                        <div class="wpbot_addon_details">
                            <span class="wp_addon_notinstalled">Not Installed</span>
                            <p>SBD Review Rating AddOn lets you and your users to add rating and review on your List Items.</p>
                            <a class="button button-secondary" href="https://www.quantumcloud.com/products/simple-business-directory-addons/" target="_blank" ><?php echo esc_html__('Get It Now', 'kbx-qc'); ?></a>
                        </div>            
                    </div>
                </div>
            </div>

            <div class="wpbot_single_addon kbx-center-addon">
                <div class="wpbot_single_content">
                    <div class="wpbot_addon_image">
                        <img src="<?php echo esc_url(sbd_addon_url.'images/link-checker.png'); ?>" title="" />
                    </div>
                    <div class="wpbot_addon_content">
                        <div class="wpbot_addon_title"><?php echo esc_html__('Directory Broken Business Checker', 'kbx-qc'); ?></div>
                        <div class="wpbot_addon_details">
                            <span class="wp_addon_notinstalled">Not Installed</span>
                            <p>Check Broken Links for SLD and SBD and other Post Types Links</p>
                            <a class="button button-secondary" href="https://www.quantumcloud.com/products/simple-business-directory-addons/" target="_blank" ><?php echo esc_html__('Get It Now', 'kbx-qc'); ?></a>
                        </div>            
                    </div>
                </div>
            </div>


        </div>


        <div class="wpbot_single_addon_wrapper">
            <h2 class="wpbot_single_addon_title"><?php echo esc_html__('Themes', 'kbx-qc'); ?></h2>
            <div class="wpbot_single_addon kbx-center-addon qc_addon_page_full_addon">
                <div class="wpbot_single_content">
                    <div class="wpbot_addon_image qc_addon_page_full_img">
                        <img src="<?php echo esc_url(sbd_addon_url.'images/theme.jpg'); ?>" title="" />
                    </div>
                    <div class="wpbot_addon_content">
                        <div class="wpbot_addon_title">Business Directory Theme</div>
                        <div class="wpbot_addon_details">
                            <span class="wp_addon_installed">Not Installed</span>
                            <p>Crafted carefully to make the best out of the popular Simple Business Directory plugin. One Click Install, Demo Data, Compatible with the Elementor and the Gutenberg Page Builder!</p>
                            <a class="button button-secondary" href="https://www.quantumcloud.com/products/themes/simple-link-directory/" target="_blank" >Get It Now</a>
                        </div>            
                    </div>

                </div>

            </div>

            
            <div style="clear:both"></div>
            
        </div>


    </div>
</div>