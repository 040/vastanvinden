<?php

//Registering Sub Menu for Ordering
add_action( 'admin_menu', 'qcpd_register_pd_manage_large_list' );

function qcpd_register_pd_manage_large_list() {
	add_submenu_page(
		'edit.php?post_type=pd',
		'Manage Large List',
		'Manage Large List',
		'edit_pages', 'pd-manage-large-list',
		'qcpd_pd_manage_large_list_page'
	);
}
function sbd_delete_elements($metaid, $listid){
	
	global $wpdb;
	$msg = '';
	$table = $wpdb->prefix."postmeta";
	$wpdb->delete(
	$table,
		array( 'meta_id' => $metaid ),
		array( '%d' )
	);
	//wp_redirect(admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&listid=' . $listid ));
	
}
function qcpd_pd_manage_large_list_page(){
	global $wpdb;
	
	
	if(isset($_GET['act']) && $_GET['act']=='edit' && isset($_GET['metaid']) && $_GET['metaid']!=''){
		
		sbd_get_element_edit_page($_GET['metaid']);
		
	}else if(isset($_GET['act']) && $_GET['act']=='create' && isset($_GET['listid']) && $_GET['listid']!=''){
		
		sbd_get_element_create_page($_GET['listid']);
		
	}
	else if(isset($_GET['listid']) && $_GET['listid']!=''){

		if(isset($_GET['act']) && $_GET['act']=='delete' && isset($_GET['listid']) && $_GET['listid']!='' && isset($_GET['id']) && $_GET['id']!=''){
			
			sbd_delete_elements($_GET['id'], $_GET['listid']);
			$msg = 'List element deleted successfully!';
		}
		$whereClass = '';
		if(isset($_POST['sbd_keyword']) && $_POST['sbd_keyword']!=''){
			$whereClass .=" and `meta_value` LIKE '%".$_POST['sbd_keyword']."%'";			
		}
	
	$listid = $_GET['listid'];
	
	$sql = "SELECT * FROM ".$wpdb->prefix."postmeta WHERE 1 and post_id = '$listid' and meta_key='qcpd_list_item01' $whereClass order by `meta_id` ASC";
	
	$sql2 = "SELECT count(*)as cnt FROM ".$wpdb->prefix."postmeta WHERE 1 and post_id = '$listid' and meta_key='qcpd_list_item01' $whereClass order by `meta_id` ASC";
	
	
	$total             = $wpdb->get_var( $sql2 );
	
	$items_per_page = 50;
	
	$page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
	$offset         = ( $page * $items_per_page ) - $items_per_page;
	
	$sql .=" LIMIT ${offset}, ${items_per_page}";
	
	$rows = $wpdb->get_results( $sql );
	$totalPage         = ceil($total / $items_per_page);
	
	if($totalPage > 1){
		$customPagHTML     =  '<div><span>Page '.$page.' of '.$totalPage.' </span>'.paginate_links( array(
		'base' => add_query_arg( 'cpage', '%#%' ),
		'format' => '',
		'prev_text' => __('&laquo;'),
		'next_text' => __('&raquo;'),
		'total' => $totalPage,
		'current' => $page
		)).'</div>';
	}
	
	
	
	
?>
		<style>
		.pd_payment_cell {

			max-width: 148px;
		}
		</style>
		<div class="qchero_sliders_list_wrapper">
			<div class="sld_menu_title">
				<h2 style="font-size: 26px;text-align:center"><?php echo __('All Elements for - ', 'qc-pd').get_the_title($listid); ?></h2>
			</div>
			<?php 
				if( isset($msg) && $msg!=''){
					echo '<div style="font-size: 20px;color: green;border-bottom: 1px solid;padding-bottom: 10px;margin-bottom: 10px;">'.$msg.'</div>';
				}
			?>
			<div class="sld_menu_title2">
				<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&act=create&listid=' . $listid ); ?>" class="button button-primary">Add New List Item</a>
				<div class="sbd_search_containter">
					<form action="" method="POST">
						<input type="text" name="sbd_keyword" value="<?php echo (isset($_POST['sbd_keyword']) && $_POST['sbd_keyword']!=''?$_POST['sbd_keyword']:''); ?>" />
						<input type="submit" value="Search" class="button button-secondary" />
					</form>
				</div>
				
			</div>
			<div class="sld_menu_title" style="text-align:left;"><?php if( isset($customPagHTML) && $customPagHTML != '' ){ echo $customPagHTML; } ?><span style="float:right;font-weight:bold;">Total <?php echo $total; ?></span></div>
			<div class="qchero_slider_table_area">
				<div class="pd_payment_table">
					<div class="pd_payment_row header">
						<div class="pd_payment_cell">
							<?php _e( 'ID', 'qc-pd' ) ?>
						</div>
						
						<div class="pd_payment_cell">
							<?php _e( 'Item Title', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Item Subtitle', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Item Phone', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Locale/Designation', 'qc-pd' ) ?>
						</div>
						<div class="pd_payment_cell">
							<?php _e( 'Action', 'qc-pd' ); ?>
						</div>
					</div>

			<?php
			
			
			foreach($rows as $row){
				$value = unserialize($row->meta_value);
				
			?>
				<div class="pd_payment_row">
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Date', 'qc-pd') ?></div>
						<?php echo $row->meta_id; ?>
					</div>
					
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Item Title', 'qc-pd') ?></div>
						<?php echo $value['qcpd_item_title']; ?>
					</div>
					
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Item Subtitle', 'qc-pd') ?></div>
						<?php echo $value['qcpd_item_subtitle']; ?>
					</div>
					
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Item Phone', 'qc-pd') ?></div>
						<?php echo $value['qcpd_item_phone']; ?>
					</div>
					
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Locale/Designation', 'qc-pd') ?></div>
						<?php echo $value['qcpd_item_location']; ?>
					</div>
					
					
					<div class="pd_payment_cell">
						<div class="pd_responsive_head"><?php echo __('Action', 'qc-pd') ?></div>

						<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&act=edit&metaid=' . $row->meta_id ); ?>">
							<button class="button button-danger"><?php echo __('Edit', 'qc-pd') ?></button>
						</a>
						<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&act=edit&metaid=' . $row->meta_id ); ?>" target="_blank">
							<button class="button button-danger"><?php echo __('Edit in new window', 'qc-pd') ?></button>
						</a>
						<a href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&listid='.$listid.'&act=delete&id=' . $row->meta_id ); ?>">
							<button class="button button-danger"><?php echo __('Delete', 'qc-pd') ?></button>
						</a>
					</div>
				</div>
			<?php
			}
			?>

			</div>

		</div>
		</div>
<?php
	}
	
}

function sbd_get_element_edit_page($metaid){
	
	global $wpdb;
	$table = $wpdb->prefix."postmeta";
	$row = $wpdb->get_row("SELECT * FROM $table WHERE 1 and meta_id = '$metaid'");
	$md = unserialize($row->meta_value);
	$msg = '';
	if(isset($_POST['meta_update'])){
		
		$item_title = stripcslashes(sanitize_text_field($_POST['item_title']));
		$item_link = sanitize_text_field($_POST['item_link']);
		$item_subtitle = stripcslashes(sanitize_text_field($_POST['item_subtitle']));
		$item_phone = sanitize_text_field($_POST['item_phone']);
		$item_location = sanitize_text_field($_POST['item_location']);
		$item_full_address = sanitize_text_field($_POST['item_full_address']);
		$item_latitude = sanitize_text_field($_POST['item_latitude']);
		$item_longitude = sanitize_text_field($_POST['item_longitude']);
		$item_linkedin = sanitize_text_field($_POST['item_linkedin']);
		$item_twitter = sanitize_text_field($_POST['item_twitter']);
		$item_facebook = sanitize_text_field($_POST['item_facebook']);
		$item_yelp = sanitize_text_field($_POST['item_yelp']);
		$item_business_hour = sanitize_text_field($_POST['item_business_hour']);
		$item_email = sanitize_text_field($_POST['item_email']);
		$item_fa = sanitize_text_field($_POST['item_fa']);
		$item_description = stripcslashes($_POST['item_long_description']);
		$item_no_follow = isset($_POST['item_no_follow']) ? sanitize_text_field($_POST['item_no_follow']) : '';
		$item_new_tab = isset($_POST['item_new_tab']) ? sanitize_text_field($_POST['item_new_tab']) : '';
		$item_mark_new = isset($_POST['item_mark_new']) ? sanitize_text_field($_POST['item_mark_new']) : '';
		$item_mark_featured = isset($_POST['item_mark_featured']) ? sanitize_text_field($_POST['item_mark_featured']) : '';
		$item_unpublish = isset($_POST['item_unpublish']) ? sanitize_text_field($_POST['item_unpublish']) : 0;
		$item_tags = stripcslashes(sanitize_text_field($_POST['item_tags']));
		$item_image = $_POST['item_image'];
		$item_marker_image = $_POST['item_marker_image'];
		$listid = sanitize_text_field($_POST['listid']);
		$datetime = date('Y-m-d H:i:s');

		
		$new_array = array(
			'qcpd_item_title'  => $item_title,
			'qcpd_item_link'   => $item_link,
			'qcpd_item_subtitle' => $item_subtitle,
			'qcpd_item_phone'	=> $item_phone,
			'qcpd_item_email'	=> $item_email,
			'qcpd_item_location'		=> $item_location,
			'qcpd_item_full_address'	=> $item_full_address,
			'qcpd_item_latitude'		=> $item_latitude,
			'qcpd_item_longitude'		=> $item_longitude,
			'qcpd_item_linkedin'		=> $item_linkedin,
			'qcpd_item_twitter'		=> $item_twitter,
			'qcpd_item_facebook'		=> $item_facebook,
			'qcpd_item_yelp'			=> $item_yelp,
			'qcpd_item_business_hour'	=> $item_business_hour,
			'qcpd_fa_icon'	=> $item_fa,
			'qcpd_item_nofollow'	=> $item_no_follow,
			'qcpd_item_newtab'	=> $item_new_tab,
			'qcpd_new'	=> $item_mark_new,
			'qcpd_featured'	=> $item_mark_featured,
			'qcpd_item_img'	=> $item_image,
			'qcpd_item_marker'	=> $item_marker_image,
			'qcpd_upvote_count'	=> (isset($md['qcpd_upvote_count'])?$md['qcpd_upvote_count']:''),
			'qcpd_entry_time'	=> (isset($md['qcpd_entry_time'])?$md['qcpd_entry_time']:$datetime),
			'qcpd_timelaps'	=> (isset($md['qcpd_timelaps'])?$md['qcpd_timelaps']:time()),
			'qcpd_is_bookmarked'	=> (isset($md['qcpd_is_bookmarked'])?$md['qcpd_is_bookmarked']:''),
			'qcpd_description'	=> $item_description,
			'qcpd_unpublished'	=> $item_unpublish,
			'qcpd_tags'	=> $item_tags,
			'qcpd_paid'		=> isset($md['qcpd_paid']) ? $md['qcpd_paid'] : ''

		);
		
		if(isset($_POST['qcpd_field_1']) && $_POST['qcpd_field_1']!=''){
			$new_array['qcpd_field_1']= $_POST['qcpd_field_1'];
		}
		if(isset($_POST['qcpd_field_2']) && $_POST['qcpd_field_2']!=''){
			$new_array['qcpd_field_2']= $_POST['qcpd_field_2'];
		}
		if(isset($_POST['qcpd_field_3']) && $_POST['qcpd_field_3']!=''){
			$new_array['qcpd_field_3']=$_POST['qcpd_field_3'];
		}
		if(isset($_POST['qcpd_field_4']) && $_POST['qcpd_field_4']!=''){
			$new_array['qcpd_field_4']=$_POST['qcpd_field_4'];
		}
		if(isset($_POST['qcpd_field_5']) && $_POST['qcpd_field_5']!=''){
			$new_array['qcpd_field_5']=$_POST['qcpd_field_5'];
		}



		if(isset($_POST['multipage-qcpd_field_1']) && $_POST['multipage-qcpd_field_1']!=''){
			$new_array['multipage-qcpd_field_1']= $_POST['multipage-qcpd_field_1'];
		}
		if(isset($_POST['multipage-qcpd_field_2']) && $_POST['multipage-qcpd_field_2']!=''){
			$new_array['multipage-qcpd_field_2']= $_POST['multipage-qcpd_field_2'];
		}
		if(isset($_POST['multipage-qcpd_field_3']) && $_POST['multipage-qcpd_field_3']!=''){
			$new_array['multipage-qcpd_field_3']=$_POST['multipage-qcpd_field_3'];
		}
		if(isset($_POST['multipage-qcpd_field_4']) && $_POST['multipage-qcpd_field_4']!=''){
			$new_array['multipage-qcpd_field_4']=$_POST['multipage-qcpd_field_4'];
		}
		if(isset($_POST['multipage-qcpd_field_5']) && $_POST['multipage-qcpd_field_5']!=''){
			$new_array['multipage-qcpd_field_5']=$_POST['multipage-qcpd_field_5'];
		}
		
		$wpdb->update(
			$table,
			array(
				'meta_value'  => serialize($new_array),
			),
			array( 'meta_id' => $metaid),
			array(
				'%s',
			),
			array( '%d')
		);
		$msg = 'List element updated successfully!';
		$row = $wpdb->get_row("SELECT * FROM $table WHERE 1 and meta_id = '$metaid'");
		$md = unserialize($row->meta_value);
	}
	
	
	
	?>
	
	<div class="wrap">
			
		
		<div id="poststuff">
			<div id="post-body" class="metabox-holder">
				<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">
				<?php 
					if($msg!=''){
						echo '<div style="font-size: 20px;color: green;border-bottom: 1px solid;padding-bottom: 10px;margin-bottom: 10px;">'.$msg.'</div>';
					}
				?>
				<h1><?php echo __('Edit List Item for', 'qc-pd') ?> - <?php echo get_the_title($row->post_id); ?></h1>
				
				<form method="post" action="">
					<table class="form-table">

						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Website Link (Ex: http://www.google.com)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_link" name="item_link" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_link'])?$md['qcpd_item_link']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label><?php echo __('Generator info', 'qc-pd') ?> </label>
							</th>
							<td>
								<input type="button" id="sbd_generate" class="" value="Generate" />
							</td>
						</tr>
					
						<tr>
							<th><label for="sbd_item_title"><?php _e( 'Item Title', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_title" name="item_title" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_title'])?esc_attr($md['qcpd_item_title']):''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_subtitle"><?php _e( 'Item Subtitle', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_subtitle" name="item_subtitle" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_subtitle'])?esc_attr($md['qcpd_item_subtitle']):''); ?>" />

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Main Phone Number (for tap to call) (Ex: 202-555-0178)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_phone" name="item_phone" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_phone'])?$md['qcpd_item_phone']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Email Address (Ex: email@example.com)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_email" name="item_email" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_email'])?$md['qcpd_item_email']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Full Address (for google map)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_full_address" name="item_full_address" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_full_address'])?$md['qcpd_item_full_address']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Latitude', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_latitude" name="item_latitude" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_latitude'])?$md['qcpd_item_latitude']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Longitude', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_longitude" name="item_longitude" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_longitude'])?$md['qcpd_item_longitude']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Locale/Designation', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_local" name="item_location" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_location'])?$md['qcpd_item_location']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Facebook Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_facebook" name="item_facebook" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_facebook'])?$md['qcpd_item_facebook']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Twitter Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_twitter" name="item_twitter" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_twitter'])?$md['qcpd_item_twitter']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Linkedin/instagram Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_linkedin" name="item_linkedin" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_linkedin'])?$md['qcpd_item_linkedin']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Yelp Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_yelp" name="item_yelp" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_yelp'])?$md['qcpd_item_yelp']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Business Hour', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_business_hour" name="item_business_hour" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_business_hour'])?$md['qcpd_item_business_hour']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Font Awesome Icon', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_fa" name="item_fa" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_fa_icon'])?$md['qcpd_fa_icon']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'No Follow', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_no_follow" name="item_no_follow" class=" pd_text_width" <?php echo (isset($md['qcpd_item_nofollow']) && $md['qcpd_item_nofollow']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Open Link in a New Tab', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_new_tab" name="item_new_tab" <?php echo (isset($md['qcpd_item_newtab']) && $md['qcpd_item_newtab']==1)?'checked="checked"':''; ?> class=" pd_text_width" value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Mark Item as New', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_mark_new" name="item_mark_new" class=" pd_text_width" <?php echo (isset($md['qcpd_new']) && $md['qcpd_new']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Mark Item as Featured', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_mark_featured" name="item_mark_featured" class=" pd_text_width" <?php echo (isset($md['qcpd_featured']) && $md['qcpd_featured']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Unpublish this Item', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_unpublish" name="item_unpublish" class=" pd_text_width" <?php echo (isset($md['qcpd_unpublished']) && $md['qcpd_unpublished']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Generate Image from Website Link', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_generate_image" name="sbd_generate_image" class=" pd_text_width"  value="" />

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Item Image', 'qc-pd' ); ?></label>
							</th>
							<td>
							
								<input type="button" class="sbd_item_image button-secondary" value="Upload Image" />
								<input type="hidden" id="item_image" name="item_image" value="<?php echo (isset($md['qcpd_item_img'])?$md['qcpd_item_img']:''); ?>" class="regular-text"
								value="">
								<div class="sbd_item_image_preview" style="margin-top:5px;">
								
								<?php 
									if (strpos($md['qcpd_item_img'], 'http') === FALSE){
								?>
									<?php
										$img = wp_get_attachment_image_src($md['qcpd_item_img']);
									?>
									<img src="<?php echo $img[0]; ?>" width="150" alt="" />

								<?php
									}else{
								?>
									<img src="<?php echo $md['qcpd_item_img']; ?>" width="150" alt="" />
								<?php
									}
								?>
								<br/>
								<input type="button" <?php if( !isset($md['qcpd_item_img']) || $md['qcpd_item_img'] == '' ){ echo 'style="display: none;"'; } ?> class="sbd_large_list_item_remove_image button-secondary" value="Remove Image" />
								
								</div>

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Marker Image', 'qc-pd' ); ?></label>
							</th>
							<td>
							
								<input type="button" class="sbd_item_marker_image button-secondary" value="Upload Image" />
								<input type="hidden" id="item_marker_image" name="item_marker_image" value="<?php echo (isset($md['qcpd_item_marker'])?$md['qcpd_item_marker']:''); ?>" class="regular-text"
								value="">
								<div class="sbd_item_marker_image_preview" style="margin-top:5px;">
								
								<?php 
									if (strpos($md['qcpd_item_marker'], 'http') === FALSE){
								?>
									<?php
										$img = wp_get_attachment_image_src($md['qcpd_item_marker']);
									?>
									<img src="<?php echo $img[0]; ?>" alt="" />

								<?php
									}else{
								?>
									<img src="<?php echo $md['qcpd_item_marker']; ?>" alt="" />
								<?php
									}
								?>
								<br/>
								<input type="button" <?php if( !isset($md['qcpd_item_marker']) || $md['qcpd_item_marker'] == '' ){ echo 'style="display: none;"'; } ?> class="sbd_large_list_item_remove_image button-secondary" value="Remove Image" />
								
								</div>

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Tags', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_tags" name="item_tags" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_tags'])?esc_attr($md['qcpd_tags']):''); ?>" />
							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Long Description (will show in lightbox and on multipage mode)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<?php wp_editor(html_entity_decode(stripcslashes((isset($md['qcpd_description'])?$md['qcpd_description']:''))),'item_long_description', array('textarea_name' =>
									'item_long_description',
									'textarea_rows' => 20,
									'editor_height' => 100,
									'disabled' => 'disabled',
									'media_buttons' => false,
									'tinymce'       => array(
										'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink',)
								)); ?>
							</td>
						</tr>
						
						
						<?php if(pd_ot_get_option('sbd_field_1_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_1_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_1" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_field_1'])?$md['qcpd_field_1']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_2_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_2_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_2" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_field_2'])?$md['qcpd_field_2']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_3_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_3_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_3" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_field_3'])?$md['qcpd_field_3']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_4_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_4_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_4" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_field_4'])?$md['qcpd_field_4']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_5_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_5_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_5" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_field_5'])?$md['qcpd_field_5']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>




						<!-- Multiple Custom Fields -->
						<?php if(pd_ot_get_option('multipage-sbd_field_1_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_1_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_1" class="field-long pd_text_width" value="<?php echo (isset($md['multipage-qcpd_field_1'])?$md['multipage-qcpd_field_1']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_2_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_2_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_2" class="field-long pd_text_width" value="<?php echo (isset($md['multipage-qcpd_field_2'])?$md['multipage-qcpd_field_2']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_3_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_3_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_3" class="field-long pd_text_width" value="<?php echo (isset($md['multipage-qcpd_field_3'])?$md['multipage-qcpd_field_3']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_4_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_4_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_4" class="field-long pd_text_width" value="<?php echo (isset($md['multipage-qcpd_field_4'])?$md['multipage-qcpd_field_4']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_5_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_5_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_5" class="field-long pd_text_width" value="<?php echo (isset($md['multipage-qcpd_field_5'])?$md['multipage-qcpd_field_5']:''); ?>"  /></td>
						</tr>
						
						<?php } ?>
						<!-- End Multipage Custom Field -->
						
						
						<tr>
							<th><label for="sbd_item_link"></label>
							</th>
							<td>
								<input type="hidden" name="listid" value="<?php echo $row->post_id; ?>" />
								<input type="hidden" name="metaid" value="<?php echo $row->meta_id; ?>" />
								<input type="submit" class="button-secondary" name="meta_update" value="Save" />
							</td>
							<td>
								
								<a class="button-primary" href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&listid='.$row->post_id); ?>">Go Back</a>
							</td>
						</tr>
						
					</table>
				</form>
				
				
				</div>
			</div>
		</div>
			
	</div>
	
	<?php
	
}

function sbd_get_element_create_page($listid){
	
	global $wpdb;
	$table = $wpdb->prefix."postmeta";
	$msg = '';
	if(isset($_POST['meta_update'])){
		
		$item_title = stripcslashes(sanitize_text_field($_POST['item_title']));
		$item_link = sanitize_text_field($_POST['item_link']);
		$item_subtitle = stripcslashes(sanitize_text_field($_POST['item_subtitle']));
		$item_phone = sanitize_text_field($_POST['item_phone']);
		$item_location = sanitize_text_field($_POST['item_location']);
		$item_full_address = sanitize_text_field($_POST['item_full_address']);
		$item_latitude = sanitize_text_field($_POST['item_latitude']);
		$item_longitude = sanitize_text_field($_POST['item_longitude']);
		$item_linkedin = sanitize_text_field($_POST['item_linkedin']);
		$item_twitter = sanitize_text_field($_POST['item_twitter']);
		$item_facebook = sanitize_text_field($_POST['item_facebook']);
		$item_yelp = sanitize_text_field($_POST['item_yelp']);
		$item_business_hour = sanitize_text_field($_POST['item_business_hour']);
		$item_email = sanitize_text_field($_POST['item_email']);
		$item_fa = sanitize_text_field($_POST['item_fa']);
		$item_description = stripcslashes($_POST['item_long_description']);
		$item_no_follow = isset($_POST['item_no_follow']) ? sanitize_text_field($_POST['item_no_follow']) : '';
		$item_new_tab = isset($_POST['item_new_tab']) ? sanitize_text_field($_POST['item_new_tab']) : '';
		$item_mark_new = isset($_POST['item_mark_new']) ? sanitize_text_field($_POST['item_mark_new']) : '';
		$item_mark_featured = isset($_POST['item_mark_featured']) ? sanitize_text_field($_POST['item_mark_featured']) : '';
		$item_unpublish = isset($_POST['item_unpublish']) ? sanitize_text_field($_POST['item_unpublish']) : 0;
		$item_tags = stripcslashes(sanitize_text_field($_POST['item_tags']));
		$item_image = $_POST['item_image'];
		$item_marker_image = $_POST['item_marker_image'];
		$listid = sanitize_text_field($_POST['listid']);
		$datetime = date('Y-m-d H:i:s');

		
		$new_array = array(
			'qcpd_item_title'  => $item_title,
			'qcpd_item_link'   => $item_link,
			'qcpd_item_subtitle' => $item_subtitle,
			'qcpd_item_phone'	=> $item_phone,
			'qcpd_item_email'	=> $item_email,
			'qcpd_item_location'		=> $item_location,
			'qcpd_item_full_address'	=> $item_full_address,
			'qcpd_item_latitude'		=> $item_latitude,
			'qcpd_item_longitude'		=> $item_longitude,
			'qcpd_item_linkedin'		=> $item_linkedin,
			'qcpd_item_twitter'		=> $item_twitter,
			'qcpd_item_facebook'		=> $item_facebook,
			'qcpd_item_yelp'			=> $item_yelp,
			'qcpd_item_business_hour'	=> $item_business_hour,
			'qcpd_fa_icon'	=> $item_fa,
			'qcpd_item_nofollow'	=> $item_no_follow,
			'qcpd_item_newtab'	=> $item_new_tab,
			'qcpd_new'	=> $item_mark_new,
			'qcpd_featured'	=> $item_mark_featured,
			'qcpd_item_img'	=> $item_image,
			'qcpd_item_marker'	=> $item_marker_image,
			'qcpd_upvote_count'	=> (isset($md['qcpd_upvote_count'])?$md['qcpd_upvote_count']:''),
			'qcpd_entry_time'	=> (isset($md['qcpd_entry_time'])?$md['qcpd_entry_time']:$datetime),
			'qcpd_timelaps'	=> (isset($md['qcpd_timelaps'])?$md['qcpd_timelaps']:time()),
			'qcpd_is_bookmarked'	=> (isset($md['qcpd_is_bookmarked'])?$md['qcpd_is_bookmarked']:''),
			'qcpd_description'	=> $item_description,
			'qcpd_unpublished'	=> $item_unpublish,
			'qcpd_tags'	=> $item_tags,

		);
		
		if(isset($_POST['qcpd_field_1']) && $_POST['qcpd_field_1']!=''){
			$new_array['qcpd_field_1']= $_POST['qcpd_field_1'];
		}
		if(isset($_POST['qcpd_field_2']) && $_POST['qcpd_field_2']!=''){
			$new_array['qcpd_field_2']= $_POST['qcpd_field_2'];
		}
		if(isset($_POST['qcpd_field_3']) && $_POST['qcpd_field_3']!=''){
			$new_array['qcpd_field_3']=$_POST['qcpd_field_3'];
		}
		if(isset($_POST['qcpd_field_4']) && $_POST['qcpd_field_4']!=''){
			$new_array['qcpd_field_4']=$_POST['qcpd_field_4'];
		}
		if(isset($_POST['qcpd_field_5']) && $_POST['qcpd_field_5']!=''){
			$new_array['qcpd_field_5']=$_POST['qcpd_field_5'];
		}



		if(isset($_POST['multipage-qcpd_field_1']) && $_POST['multipage-qcpd_field_1']!=''){
			$new_array['multipage-qcpd_field_1']= $_POST['multipage-qcpd_field_1'];
		}
		if(isset($_POST['multipage-qcpd_field_2']) && $_POST['multipage-qcpd_field_2']!=''){
			$new_array['multipage-qcpd_field_2']= $_POST['multipage-qcpd_field_2'];
		}
		if(isset($_POST['multipage-qcpd_field_3']) && $_POST['multipage-qcpd_field_3']!=''){
			$new_array['multipage-qcpd_field_3']=$_POST['multipage-qcpd_field_3'];
		}
		if(isset($_POST['multipage-qcpd_field_4']) && $_POST['multipage-qcpd_field_4']!=''){
			$new_array['multipage-qcpd_field_4']=$_POST['multipage-qcpd_field_4'];
		}
		if(isset($_POST['multipage-qcpd_field_5']) && $_POST['multipage-qcpd_field_5']!=''){
			$new_array['multipage-qcpd_field_5']=$_POST['multipage-qcpd_field_5'];
		}
		
		
		
		$wpdb->insert(
			$table,
			array(
				'post_id'  => $listid,
				'meta_key'   => 'qcpd_list_item01',
				'meta_value' => serialize($new_array)
			)
		);
		
		
		
		$msg = 'List element created successfully! <a class="button-primary" href="'.admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&listid='.$listid).'">Go Back</a>';
		

	}
	
	
	
	?>
	
	<div class="wrap">
			
		
		<div id="poststuff">
			<div id="post-body" class="metabox-holder">
				<div id="post-body-content" style="padding: 50px;box-sizing: border-box;box-shadow: 0 8px 25px 3px rgba(0,0,0,.2);background: #fff;">
				<?php 
					if($msg!=''){
						echo '<div style="font-size: 20px;color: green;border-bottom: 1px solid;padding-bottom: 10px;margin-bottom: 10px;">'.$msg.'</div>';
					}
				?>
				<h1><?php echo __('Create List Item for', 'qc-pd') ?> - <?php echo get_the_title($listid); ?></h1>
				
				<form method="post" action="">
					<table class="form-table">

						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Website Link (Ex: http://www.google.com)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_link" name="item_link" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_link'])?$md['qcpd_item_link']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label><?php echo __('Generator info', 'qc-pd') ?> </label>
							</th>
							<td>
								<input type="button" id="sbd_generate" class="" value="Generate" />
							</td>
						</tr>
					
						<tr>
							<th><label for="sbd_item_title"><?php _e( 'Item Title', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_title" name="item_title" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_title'])?esc_attr($md['qcpd_item_title']):''); ?>" required />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_subtitle"><?php _e( 'Item Subtitle', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_subtitle" name="item_subtitle" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_subtitle'])?esc_attr($md['qcpd_item_subtitle']):''); ?>" />

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Main Phone Number (for tap to call) (Ex: 202-555-0178)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_phone" name="item_phone" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_phone'])?$md['qcpd_item_phone']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Email Address (Ex: email@example.com)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_email" name="item_email" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_email'])?$md['qcpd_item_email']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Full Address (for google map)', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_full_address" name="item_full_address" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_full_address'])?$md['qcpd_item_full_address']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Latitude', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_latitude" name="item_latitude" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_latitude'])?$md['qcpd_item_latitude']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Longitude', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_longitude" name="item_longitude" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_longitude'])?$md['qcpd_item_longitude']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Locale/Designation', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_local" name="item_location" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_location'])?$md['qcpd_item_location']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Facebook Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_facebook" name="item_facebook" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_facebook'])?$md['qcpd_item_facebook']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Twitter Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_twitter" name="item_twitter" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_twitter'])?$md['qcpd_item_twitter']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Linkedin/instagram Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_linkedin" name="item_linkedin" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_linkedin'])?$md['qcpd_item_linkedin']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Yelp Page', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_yelp" name="item_yelp" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_yelp'])?$md['qcpd_item_yelp']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Business Hour', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_business_hour" name="item_business_hour" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_item_business_hour'])?$md['qcpd_item_business_hour']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Font Awesome Icon', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_fa" name="item_fa" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_fa_icon'])?$md['qcpd_fa_icon']:''); ?>" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'No Follow', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_no_follow" name="item_no_follow" class=" pd_text_width" <?php echo (isset($md['qcpd_item_nofollow']) && $md['qcpd_item_nofollow']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Open Link in a New Tab', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_new_tab" name="item_new_tab" <?php echo (isset($md['qcpd_item_newtab']) && $md['qcpd_item_newtab']==1)?'checked="checked"':''; ?> class=" pd_text_width" value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Mark Item as New', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_mark_new" name="item_mark_new" class=" pd_text_width" <?php echo (isset($md['qcpd_new']) && $md['qcpd_new']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Mark Item as Featured', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_mark_featured" name="item_mark_featured" class=" pd_text_width" <?php echo (isset($md['qcpd_featured']) && $md['qcpd_featured']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Unpublish this Item', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_item_unpublish" name="item_unpublish" class=" pd_text_width" <?php echo (isset($md['qcpd_unpublished']) && $md['qcpd_unpublished']==1)?'checked="checked"':''; ?> value="1" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Generate Image from Website Link', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="checkbox" id="sbd_generate_image" name="sbd_generate_image" class=" pd_text_width"  value="" />

							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Item Image', 'qc-pd' ); ?></label>
							</th>
							<td>
							
								<input type="button" class="sbd_item_image button-secondary" value="Upload Image" />
								<input type="hidden" id="item_image" name="item_image" value="<?php echo (isset($md['qcpd_item_img'])?$md['qcpd_item_img']:''); ?>" class="regular-text"
								value="">
								<div class="sbd_item_image_preview" style="margin-top:5px;">
									<img src="" width="150" alt="" />
									<br/>
									<input type="button" style="display: none;" class="sbd_large_list_item_remove_image button-secondary" value="Remove Image" />
								</div>

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Marker Image', 'qc-pd' ); ?></label>
							</th>
							<td>
							
								<input type="button" class="sbd_item_marker_image button-secondary" value="Upload Image" />
								<input type="hidden" id="item_marker_image" name="item_marker_image" value="<?php echo (isset($md['qcpd_item_marker'])?$md['qcpd_item_marker']:''); ?>" class="regular-text"
								value="">
								<div class="sbd_item_marker_image_preview" style="margin-top:5px;">
								
									<img src="" alt="" />
									<br/>
									<input type="button" style="display: none;" class="sbd_large_list_item_remove_image button-secondary" value="Remove Marker" />
								
								</div>

							</td>
						</tr>
						
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Tags', 'qc-pd' ); ?></label>
							</th>
							<td>
								<input type="text" id="sbd_item_tags" name="item_tags" class="field-long pd_text_width" value="<?php echo (isset($md['qcpd_tags'])?esc_attr($md['qcpd_tags']):''); ?>" />
							</td>
						</tr>
						<tr>
							<th><label for="sbd_item_link"><?php _e( 'Long Description (will show in lightbox and on multipage mode)', 'qc-pd' ); ?></label>
							</th>
							<td>
								
								
								<?php wp_editor(html_entity_decode(stripcslashes((isset($md['qcpd_description'])?$md['qcpd_description']:''))), 'item_long_description', array('textarea_name' =>
									'item_long_description',
									'textarea_rows' => 20,
									'editor_height' => 100,
									'disabled' => 'disabled',
									'media_buttons' => false,
									'tinymce'       => array(
										'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink',)
								)); ?>
								
							</td>
						</tr>
						
						<?php if(pd_ot_get_option('sbd_field_1_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_1_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_1" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_2_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_2_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_2" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_3_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_3_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_3" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_4_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_4_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_4" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('sbd_field_5_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('sbd_field_5_label'); ?> </label></th>
							<td><input type="text" name="qcpd_field_5" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>



						<!-- Multipage Custom Field -->
						<?php if(pd_ot_get_option('multipage-sbd_field_1_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom1')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_1_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_1" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_2_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom2')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_2_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_2" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_3_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom3')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_3_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_3" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_4_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom4')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_4_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_4" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						
						<?php if(pd_ot_get_option('multipage-sbd_field_5_enable')=='on'){ ?>
						
						<tr <?php echo ( isset($flagg) && $flagg==false && get_option('sbd_custom5')==0?'style="display:none"':''); ?>>
							<th><label><?php echo pd_ot_get_option('multipage-sbd_field_5_label'); ?> </label></th>
							<td><input type="text" name="multipage-qcpd_field_5" class="field-long pd_text_width" value=""  /></td>
						</tr>
						
						<?php } ?>
						<!-- End Multipage Custom Field -->
						
						<tr>
							<th><label for="sbd_item_link"></label>
							</th>
							<td>
								<input type="hidden" name="listid" value="<?php echo $listid; ?>" />
								
								<input type="submit" class="button-secondary" name="meta_update" value="Save" />
							</td>
							<td>
								
								<a class="button-primary" href="<?php echo admin_url( 'edit.php?post_type=pd&page=pd-manage-large-list&listid='.$listid); ?>">Go Back</a>
							</td>
						</tr>
						
					</table>
				</form>
				
				
				</div>
			</div>
		</div>
			
	</div>
	
	<?php
	
}