<?php 
function qcpd_modal_fa() 
{
	    $icons = get_option( 'fa_icons' );
        if ( ! $icons ) {
          $pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
          $subject = wp_remote_fopen( QCSBD_ASSETS_URL . '/css/font-awesome.css' );
		  
          preg_match_all( $pattern, $subject, $matches, PREG_SET_ORDER );
          $icons = array();
          foreach ( $matches as $match ) {
            $icons[] = array( 'css' => $match[2], 'class' => stripslashes( $match[1] ) );
          }
          update_option( 'fa_icons', $icons );
        }
        
?>

<div class="fa-field-modal" id="fa-field-modal" style="display:none">
  <div class="fa-field-modal-close">&times;</div>
  <h1 class="fa-field-modal-title"><?php _e( 'Select Font Awesome Icon', 'qc-pdb' ); ?></h1>

  <div class="fa-field-modal-icons">
	<?php if ( $icons ) : ?>

	  <?php foreach ( $icons as $icon ) : ?>

		<div class="fa-field-modal-icon-holder" data-icon="<?php echo $icon['class']; ?>">
		  <div class="icon">
			<i class="fa <?php echo $icon['class']; ?>"></i>
		  </div>
		  <div class="label">
			<?php echo $icon['class']; ?>
		  </div>
		</div>

	  <?php endforeach; ?>

	<?php endif; ?>
  </div>
</div>



<div class="fa-field-modal" id="fa-field-modal1" style="display:none;">
  <div class="fa-field-modal-close">&times;</div>
  <h1 class="fa-field-modal-title"><?php _e( 'Copy this item to other Lists', 'qc-pdb' ); ?></h1>

  <div class="fa-field-modal-icons">

		
		<?php $sldposts = get_posts( array( 'post_type' => 'pd', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) ); 
		?>

		<?php foreach($sldposts as $sld): ?>
			<div class="sld_list_item">
				<input class="sld_list_Checkbox" type="checkbox" value="<?php echo esc_attr($sld->ID); ?>" /><?php echo esc_html($sld->post_title); ?> (ID <?php echo esc_html($sld->ID); ?>)
			</div>
		<?php endforeach; ?>

		<div style="clear:both"></div>
		<input type="submit" id="sld_list_select" name="submit" value="Submit" />
  </div>
</div>

<?php
}

add_action( 'admin_footer', 'qcpd_modal_fa');