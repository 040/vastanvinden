<?php

/*TinyMCE Shortcode Generator Button - 25-01-2017*/

function qcpd_tinymce_shortcode_button_function() {
	add_filter ("mce_external_plugins", "qcpd_shortcode_generator_btn_js");
	add_filter ("mce_buttons", "qcpd_shortcode_generator_btn");
}

function qcpd_shortcode_generator_btn_js($plugin_array) {
	$plugin_array['qcpd_shortcode_btn'] = plugins_url('assets/js/qcpd-tinymce-button.js', __FILE__);
	return $plugin_array;
}

function qcpd_shortcode_generator_btn($buttons) {
	array_push ($buttons, 'qcpd_shortcode_btn');
	return $buttons;
}

add_action ('init', 'qcpd_tinymce_shortcode_button_function');

function qcpd_load_custom_wp_admin_style() {
        wp_register_style( 'pd_shortcode_gerator_css', QCSBD_ASSETS_URL . '/css/shortcode-modal.css', false, '1.0.0' );
        wp_enqueue_style( 'pd_shortcode_gerator_css' );
}
add_action( 'admin_enqueue_scripts', 'qcpd_load_custom_wp_admin_style' );

function qcpd_render_shortcode_modal() {

	?>

	<div id="sm-modal" class="sbd_modal">

		<!-- Modal content -->
		<div class="modal-content">
		
			<span class="close">
				<span class="dashicons dashicons-no"></span>
			</span>
			<h3> 
				<?php _e( 'SBD - Shortcode Generator' , 'qc-pd' ); ?></h3>
			<hr/>
			<div class="sbd_shortcode_generator_area">
<style type="text/css">
    .hero_tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .hero_tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }
    /* Change background color of buttons on hover */
    .hero_tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .hero_tab button.hero_active {
        background-color: #ccc;
    }
    /* Style the tab content */
    .hero_tabcontent {
        display: none;
        padding: 6px 12px;

        border-top: none;
        width: 704px;
    }
    #hero_general{display:block}
</style>

<div class="hero_tab">
    <button class="hero_tablinks hero_active" onclick="openCity(event, 'hero_general')">General</button>
    <button class="hero_tablinks" onclick="openCity(event, 'hero_settings')">Display Settings</button>

</div>
        <div id="hero_general" class="hero_tabcontent" style="padding: 6px 12px;">
			<div class="sm_shortcode_list">

				<div class="qcpd_single_field_shortcode">
					<label style="width: 200px;display: inline-block;">
						Mode
					</label>
					<select style="width: 275px;" id="pd_mode">
						<option value="all">All List</option>
						<option value="one">One List</option>
						<option value="category">List Category</option>
                        <option value="categorytab">Category Tab</option>
                        <option value="maponly">Map Only</option>
					</select>
				</div>

                <div class="qcpd_single_field_shortcode hidden-div" id="pd_cat_orderby">
                    <label style="width: 200px;display: inline-block;">
                        Category Order By
                    </label>
                    <select style="width: 275px;" id="pd_category_orderby">
                        <option value="date">Date</option>
                        <option value="ID">ID</option>
                        <option value="title">Title</option>
                        <option value="modified">Date Modified</option>
                        <option value="rand">Random</option>
                        
                    </select>
                </div>

                <div class="qcpd_single_field_shortcode hidden-div" id="pd_cat_order">
                    <label style="width: 200px;display: inline-block;">
                        Category Order
                    </label>
                    <select style="width: 275px;" id="pd_category_order">
                        <option value="ASC">Ascending</option>
                        <option value="DESC">Descending</option>
                    </select>
                </div>
				
				<div id="pd_list_div" class="qcpd_single_field_shortcode hidden-div">
					<label style="width: 200px;display: inline-block;">
						Select List 
					</label>
					<select style="width: 275px;" id="pd_list_id">
					
						<option value="">Please Select List</option>
						
						<?php
						
							$ilist = new WP_Query( array( 'post_type' => 'pd', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC') );
							if( $ilist->have_posts()){
								while( $ilist->have_posts() ){
									$ilist->the_post();
						?>
						
						<option value="<?php echo esc_attr(get_the_ID()); ?>"><?php echo esc_html(get_the_title()); ?></option>
						
						<?php } } ?>
						
					</select>
				</div>
				
				<div id="pd_list_cat" class="qcpd_single_field_shortcode hidden-div">
					<label style="width: 200px;display: inline-block;">
						List Category
					</label>
					<select style="width: 275px;" id="pd_list_cat_id">
					
						<option value="">Please Select Category</option>
						
						<?php
						
							$terms = get_terms( 'pd_cat', array(
								'hide_empty' => true,
							) );
							if( $terms ){
								foreach( $terms as $term ){
						?>
						
						<option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
						
						<?php } } ?>
						
					</select>
				</div>
				
				<div class="qcpd_single_field_shortcode" id="pd-templates">
					<label style="width: 200px;display: inline-block;">
						Template Style
					</label>
					<select style="width: 275px;" id="pd_style">
						<option value="simple">Default Style</option>
						<option value="style-1">Style 01</option>
						<option value="style-2">Style 02</option>
						<option value="style-3">Style 03</option>
						<option value="style-4">Style 04</option>
						<option value="style-5">Style 05</option>
						<option value="style-6">Style 06</option>
						<option value="style-7">Style 07</option>
						<option value="style-8">Style 08</option>
						<option value="style-9">Style 09</option>
						<option value="style-10">Style 10</option>
						
						
					</select>
					
					<!--<div id="demo-preview-link">
						<div id="demo-url">
						</div>
					</div>-->
					
				</div>
				
				<div id="pd_hide_list_title" class="qcpd_single_field_shortcode" >
					<label style="width: 200px;display: inline-block;">
						Hide List Title
					</label>
					<input id="pd_hide_list_title" name="pd_hide_list_title" value="true" type="checkbox">
				</div>

				<div id="pd_show_map" class="qcpd_single_field_shortcode" >
					<label style="width: 200px;display: inline-block;">
						Show Map
					</label>
					<input id="pdmap" name="pdmap" value="show" type="checkbox">
				</div>
				
				<div class="qcpd_single_field_shortcode pd_map_position" style="display:none" id="pd_map_position_div">
					<label style="width: 200px;display: inline-block;">
						Map Position
					</label>
					<select style="width: 275px;" id="pd_map_position">
						<option value="top">Top</option>
						<option value="right">Right</option>					
					</select>

				</div>
				
				
				
				<div class="qcpd_single_field_shortcode map_full_width" style="display:none">
					<label style="width: 200px;display: inline-block;">
						Show Map Full Width
					</label>
					<input class="pd_map_full_width" name="ckbox" value="true" type="checkbox">
				</div>
				
				<div class="qcpd_single_field_shortcode map_full_screen" style="display:none">
					<label style="width: 200px;display: inline-block;">
						Show Map Full Screen
					</label>
					<input class="pd_map_full_screen" name="ckbox" value="true" type="checkbox">
				</div>
				<div class="qcpd_single_field_shortcode map_toggle_filter" style="display:none">
					<label style="width: 200px;display: inline-block;">
						Enable Toggle Filtering
					</label>
					<input class="pd_map_toggle_filter" name="ckbox" value="true" type="checkbox">
				</div>
				
				
				
				<div id="pd_map_off" class="qcpd_single_field_shortcode" style="display:block">
					<label style="width: 200px;display: inline-block;">
						Turn off map in lightbox
					</label>
					<input class="pdmapofflightbox" name="pdmapofflightbox" value="true" type="checkbox">
				</div>
				
				<div id="pd_column_div" class="qcpd_single_field_shortcode">
					<label style="width: 200px;display: inline-block;">
						Column
					</label>
					<select style="width: 275px;" id="pd_column">
						<option value="1">Column 1</option>
						<option value="2">Column 2</option>
						<option value="3">Column 3</option>
						<option value="4">Column 4</option>
					</select>
				</div>


                <div class="qcpd_single_field_shortcode" id="qcpd_item_order_by">
                    <label style="width: 200px;display: inline-block;">
                        List Item Order By
                    </label>
                    <select style="width: 275px;" id="pd_item_orderby">

                        <option value="">None</option>
                        <option value="upvotes">Upvotes</option>
                        <option value="title">Title</option>
                        <option value="timestamp">Date Modified</option>
                        <option value="random">Random</option>
                        <option value="distance">Distance from User Location</option>

                    </select>
                </div>
				
				<div class="qcpd_single_field_shortcode" id="pd_con_orderby">
					<label style="width: 200px;display: inline-block;">
						List Order By
					</label>
					<select style="width: 275px;" id="pd_orderby">
						<option value="date">Date</option>
						<option value="ID">ID</option>
						<option value="title">Title</option>
						<option value="modified">Date Modified</option>
						<option value="rand">Random</option>
						<option value="menu_order">Menu Order</option>
					</select>
				</div>
				
				<div class="qcpd_single_field_shortcode" id="pd_con_order">
					<label style="width: 200px;display: inline-block;">
						List Order
					</label>
					<select style="width: 275px;" id="pd_order">
						<option value="ASC">Ascending</option>
						<option value="DESC">Descending</option>
					</select>
				</div>
				
				<div class="qcpd_single_field_shortcode" id="pd_filter_orderby">
					<label style="width: 200px;display: inline-block;">
						Filter Button Order By
					</label>
					<select style="width: 275px;" id="pd_filter_orderby">
						<option value="date">Date</option>
						<option value="ID">ID</option>
						<option value="title">Title</option>
						<option value="modified">Date Modified</option>
						<option value="rand">Random</option>
						<option value="menu_order">Menu Order</option>
					</select>
				</div>
				
				<div class="qcpd_single_field_shortcode" id="pd_filter_order">
					<label style="width: 200px;display: inline-block;">
						Filter Button Order
					</label>
					<select style="width: 275px;" id="pd_filter_order">
						<option value="ASC">Ascending</option>
						<option value="DESC">Descending</option>
					</select>
				</div>
				<div class="qcpd_single_field_shortcode" id="pd_mainclick">
					<label style="width: 200px;display: inline-block;">
						Main Click Action
					</label>
					<select style="width: 275px;" id="main_click_action">
						<option value="1">Go to Website</option>
						<option value="0">Call</option>
						<option value="2">Popup</option>
						<option value="3">Do nothing</option>
						<option value="4">Item Details Page</option>
						
					</select>
				</div>
				
				<div class="qcpd_single_field_shortcode" id="pd_phone">
					<label style="width: 200px;display: inline-block;">
						Phone Number
					</label>
					<select style="width: 275px;" id="phone_number">
						<option value="1">Show</option>
						<option value="0">Hide</option>
					</select>
				</div>
				
				
				<div class="qcpd_single_field_shortcode" id="pd_embed">
					<label style="width: 200px;display: inline-block;">
						Embed Option
					</label>
					<select style="width: 275px;" id="embed_option">
						<option value="true">Show</option>
						<option value="false">Hide</option>
					</select>
				</div>

				<!-- <div class="qcpd_single_field_shortcode checkbox-pd pd-off-field pg-template" id="qcpd_pagination">
					<label>
						<input class="pd_enable_pagination" name="ckbox" value="on" type="checkbox">
						Enable Pagination
					</label>
				</div> -->

				<div class="qcpd_single_field_shortcode" id="qcpd_enable_pagination">
					<label style="width: 200px;display: inline-block;">
						Pagination
					</label>
					<select style="width: 275px;" id="qcpd_enable_pagination_option">
						<option value="">No Pagination</option>
						<option value="js-pagination">JS Pagination (for small directory)</option>
						<option value="page-pagination">Page Pagination (for large directory)</option>
					</select>
				</div>

				<div id="pd_itemperpage_div" class="qcpd_single_field_shortcode pd-off-field pg-enabled" id="qcpd_item_perpage">
					<label style="width: 200px;display: inline-block;">
						Items Per Page
					</label>
					<input style="width: 275px;" id="pd_items_per_page" type="text" name="pd_items_per_page" class="pd_items_per_page" value="10">
				</div>
				
				
				<div class="qcpd_single_field_shortcode checkbox-pd sbd-leftfilter" id="pd-leftfilter">
					<label>
						<input class="pd_left_filter" name="ckbox" value="true" type="checkbox">
						Enable Left Filter
					</label>
				</div>
				<div class="qcpd_single_field_shortcode checkbox-pd ">
					<label>
						<input class="pd_tag_filter" name="ckbox" value="true" type="checkbox">
						Enable Tag Filter
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd " id="pd_tag_filter_dropdown">
					<label>
						<input class="pd_tag_filter_dropdown" name="ckbox" value="true" type="checkbox">
						Show Tag Filter as Dropdown
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd" id="pd-rtl">
					<label>
						<input class="pd_rtl" name="ckbox" value="true" type="checkbox">
						Enable RTL
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd">
					<label>
						<input class="pd_search" name="ckbox" value="true" type="checkbox">
						Search
					</label>
				</div>
				<div class="qcpd_single_field_shortcode checkbox-pd">
					<label>
						<input class="pd_radius" name="ckbox" value="true" type="checkbox">
						Distant Search
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd">
					<label>
						<input class="pd_cluster" name="ckbox" value="true" type="checkbox">
						Enable Map Marker Cluster
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd">
					<label>
						<input class="pd_image_off_infowindow" name="ckbox" value="true" type="checkbox">
						Turn off Image for Infowindow
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd" id="qcpd_upvote">
					<label>
						<input class="pd_upvote" name="ckbox" value="on" type="checkbox">
						Upvote
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd" id="qcpd_item_count">
					<label>
						<input class="pd_item_count" name="ckbox" value="on" type="checkbox">
						Item Count
					</label>
				</div>
				
				<div class="qcpd_single_field_shortcode checkbox-pd">
					<label>
						<input class="item_details_page" name="ckbox" value="on" type="checkbox">
						Show a Link to Details Page (if Multi Page<br> Mode is Enabled)
					</label>
				</div>

				

				<div class="qcpd_single_field_shortcode checkbox-pd tt-template" id="qcpd_tooltip">
					<label>
						<input class="pd_enable_tooltip" name="ckbox" value="on" type="checkbox">
						Enable Tooltip / Popup Texts
					</label>
				</div>
				
				<!-- Display Review option if review addon is enable -->
			<?php if( class_exists('QCPD\QCPD_Review_Rating') && function_exists('qcpdr_ot_get_option')  && qcpdr_ot_get_option('qcpdr_enable_reviews') != 'off' ) { ?>
				<div class="qcpd_single_field_shortcode checkbox-pd " id="qcpd_sbd_enable_review">
					<label>
						<input class="qcpd_sbd_enable_review" name="ckbox" value="on" type="checkbox">
						Enable Review
					</label>
				</div>
			<?php } ?>
				

				
			</div>
		</div>

        <div id="hero_settings" class="hero_tabcontent" style="padding: 6px 12px;">
            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Filter Area
                </label>
                <select style="width: 275px;" id="pd_filter_area">
                    <option value="normal">Normal</option>
                    <option value="fixed">Fixed</option>

                </select>
            </div>
            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Filter Area Top Spacing
                </label>
                <input type="text" style="width: 275px;" id="pd_topspacing" placeholder="Ex: 50" />
            </div>
			<div id="qcpd_general_settings">
            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    List Title Font Size
                </label>
                <select style="width: 275px;" id="pd_list_title_font_size">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>

            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    List Title Line Height
                </label>
                <select style="width: 275px;" id="pd_list_title_line_height">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>

            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Item Title Font Size
                </label>
                <select style="width: 275px;" id="pd_title_font_size">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>

            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Item Subtitle Font Size
                </label>
                <select style="width: 275px;" id="pd_subtitle_font_size">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>

            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Item Title Line Height
                </label>
                <select style="width: 275px;" id="pd_title_line_height">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>

            <div class="qcpd_single_field_shortcode">
                <label style="width: 200px;display: inline-block;">
                    Item Subtitle Line Height
                </label>
                <select style="width: 275px;" id="pd_subtitle_line_height">
                    <option value="">Default</option>
			        <?php
			        for($i=10;$i<50;$i++){
				        echo '<option value="'.$i.'px">'.$i.'px</option>';
			        }
			        ?>
                </select>
            </div>
			</div>
        </div>

        <div class="qcpd_single_field_shortcode">
            <label style="width: 200px;display: inline-block;">
            </label>
            <input class="pd-sc-btn" type="button" id="qcpd_add_shortcode" value="Generate Shortcode" />
        </div>

        <script type="text/javascript">
            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("hero_tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("hero_tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" hero_active", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " hero_active";
            }

        </script>

		</div>
		<div class="sbd_shortcode_container" style="display:none;">
			<div class="qcpd_single_field_shortcode">
                <textarea style="width:100%;height:200px" id="sbd_shortcode_container"></textarea>
				<p><b>Copy</b> the shortcode & use it any text block. <button class="sbd_copy_close button button-primary button-small" style="float:right">Copy & Close</button></p>
            </div>
		</div>
		</div>

	</div>
	<?php
	exit;
}

add_action( 'wp_ajax_show_qcpd_shortcodes', 'qcpd_render_shortcode_modal');