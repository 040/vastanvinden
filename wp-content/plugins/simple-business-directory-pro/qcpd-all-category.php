<?php
/**
 * Created by QuantunCloud.
 * Date: 9/14/2017
 * Time: 3:16 PM
 */

defined('ABSPATH') or die("No direct script access!");

add_shortcode('pd-tab', 'qcpd_directory_all_category');
function qcpd_directory_all_category($atts = array()){


	
	//Defaults & Set Parameters
	extract( shortcode_atts(
		array(
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'mode' => 'all',
			'list_id' => '',
			'column' => '1',
			'style' => 'simple',
			'list_img' => 'true',
			'search' => 'true',
			'radius' => '',
			'category' => "",
			'upvote' => "on",
			'item_count' => "on",
			'top_area' => "on",
			'item_orderby' => "",
			'item_order' => "",
			'mask_url' => "off",
			'tooltip' => 'false',
			'paginate_items' => 'false',
			'per_page' => 5,
			'category_orderby'=>'date',
			'category_order'=>'ASC',
			'category_remove'=>'',
			'list_title_font_size' => '' ,
			'list_title_line_height' => '' ,
			'map'=>'',
			'title_font_size' => '',
			'subtitle_font_size' => '',
			'title_line_height' => '',
			'subtitle_line_height' => '',
			'filter_area' => 'normal',
			'topspacing' => 0,
			'enable_map_fullwidth'=>'false',
			'enable_embedding' => 'true',
			'main_click_action'=>1,
			'pdmapofflightbox'=>'false',
			'enable_tag_filter'=>'true',
			'enable_tag_filter_dropdown'=>'false',
			'distance_search' => '',
			'item_details_page'	=> 'off',
			'map_position'=> 'top',
			'marker_cluster' => '',
			'actual_pagination'	=>	'false',
			'hide_list_title' => 'false',
			'review' => 'false',
			'all_tab' => 'hide',

		), $atts
	));

	//var_dump($show_all_tab);
	//wp_die();


	if($radius=='' and pd_ot_get_option('sbd_enable_distant_search')=='on'){
		$radius = "true";
	}

	
	//var_dump($category);

    //Category remove array
    if($category_remove != ''){
	    $category_remove = explode(',',$category_remove);
	    $categoryremove = $category_remove;
    }else{
	    $categoryremove = array();
    }

    if($category != ''){
	    $ids_to_exclude = explode(',',$category);
	    $ids_to_exclude = $ids_to_exclude;
    }else{
	    $ids_to_exclude = array();
    }


	$cterms = get_terms( 'pd_cat', array(
		'hide_empty' => true,
		'orderby' => $category_orderby,
		'order' => $category_order,
		'slug'  => $ids_to_exclude,
	) );


	
	ob_start();

	if( $map != 'hide' ){
		wp_enqueue_script( 'qcopd-google-map-script');
		wp_enqueue_script( 'qcopd-google-map-scriptasd');
	}
	wp_enqueue_script( 'qcpd-custom-script');

    if(!empty($cterms)){
?>
<?php
	$customCss = pd_ot_get_option( 'pd_custom_style' );

	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>

<?php endif; ?>
        <div class="qcld_pd_tab_main"><!--start qcld_pd_tab_main-->
            <div class="qcld_pd_tab">
            	 <?php if($all_tab != 'hide'){ ?>
            	<button  class="qcld_pd_tablinks qcld_pd_active" data-contentid="all"> <?php echo pd_ot_get_option('sbd_lan_all_category_tab_text') ? pd_ot_get_option('sbd_lan_all_category_tab_text') : 'All'; ?> 
				</button>
            	 <?php } ?>
                <?php
                $ci = 0;
                foreach ($cterms as $cterm){
                    if(!in_array($cterm->term_id,$categoryremove)){
						$image_id = get_term_meta ( $cterm -> term_id, 'category-image-id', true );
						
                        ?>
                            <button style="<?php echo (!$image_id?'padding-left:22px!important':''); ?>" class="qcld_pd_tablinks <?php echo ( ($ci==0 && $all_tab == 'hide' ) ?'qcld_pd_active':''); ?>" data-contentid="<?php echo $cterm->slug; ?>" ><?php echo $cterm->name; ?> <span class="cat_img_top">
							<?php if($image_id) echo wp_get_attachment_image ( $image_id, 'thumbnail' ); ?>
							</span> </button>
                        <?php
                        $ci++;
                    }
                }
                ?>
            </div>
            <?php if($show_all_tab != 'false'){ ?>
            <div id="all" class="qcld_pd_tabcontent" style="display:block">
		        <?php
                    $shortcodeText = '[qcpd-directory mode="all" search="'.$search.'" radius="'.$radius.'" upvote="'.$upvote.'" item_count="'.$item_count.'" top_area="'.$top_area.'" mask_url="'.$mask_url.'" tooltip="'.$tooltip.'" paginate_items="'.$paginate_items.'" hide_list_title="'.$hide_list_title.'" actual_pagination="'.$actual_pagination.'" per_page="'.$per_page.'" style="'.$style.'" column="'.$column.'" orderby="'.$orderby.'" enable_embedding="'.$enable_embedding.'" order="'.$order.'" list_title_font_size="'.$list_title_font_size.'" item_orderby="'.$item_orderby.'" list_title_line_height="'.$list_title_line_height.'" title_font_size="'.$title_font_size.'" subtitle_font_size="'.$subtitle_font_size.'" title_line_height="'.$title_line_height.'" subtitle_line_height="'.$subtitle_line_height.'" map="'.$map.'" filter_area="'.$filter_area.'" topspacing="'.$topspacing.'" cattabid="'.$ci.'" main_click_action="'.$main_click_action.'" pdmapofflightbox="'.$pdmapofflightbox.'" item_details_page="'.$item_details_page.'" enable_tag_filter="'.$enable_tag_filter.'" marker_cluster="'.$marker_cluster.'" enable_tag_filter_dropdown="'.$enable_tag_filter_dropdown.'" distance_search="'.$distance_search.'" map_position="'.$map_position.'" review="'.$review.'"]';
		        echo do_shortcode($shortcodeText);
		        ?>

		        <script type="text/javascript">
		        	jQuery(document).ready(function($){
		        		setTimeout(function(){
							
							jQuery('.qcld_pd_tablinks.qcld_pd_active').trigger('click');
							
						},1000)
							
					});
		        </script>
            </div>
        <?php } ?>

	        <?php
	        $ci = 0;
	        foreach ($cterms as $cterm){
		        if(!in_array($cterm->term_id,$categoryremove)){
			        ?>


                    <div id="<?php echo $cterm->slug; ?>" class="qcld_pd_tabcontent"  <?php echo ( ($ci==0 && $all_tab == 'hide' )?'style="display:block"':''); ?>>
				        <?php
                            $shortcodeText = '[qcpd-directory category="'.$cterm->slug.'" search="'.$search.'" radius="'.$radius.'" upvote="'.$upvote.'" item_count="'.$item_count.'" top_area="'.$top_area.'" mask_url="'.$mask_url.'" tooltip="'.$tooltip.'" paginate_items="'.$paginate_items.'" hide_list_title="'.$hide_list_title.'" actual_pagination="'.$actual_pagination.'" per_page="'.$per_page.'" style="'.$style.'" column="'.$column.'" orderby="'.$orderby.'" enable_embedding="'.$enable_embedding.'" order="'.$order.'" list_title_font_size="'.$list_title_font_size.'" item_orderby="'.$item_orderby.'" list_title_line_height="'.$list_title_line_height.'" title_font_size="'.$title_font_size.'" subtitle_font_size="'.$subtitle_font_size.'" title_line_height="'.$title_line_height.'" subtitle_line_height="'.$subtitle_line_height.'" map="'.$map.'" filter_area="'.$filter_area.'" topspacing="'.$topspacing.'" cattabid="'.$ci.'" main_click_action="'.$main_click_action.'" pdmapofflightbox="'.$pdmapofflightbox.'" item_details_page="'.$item_details_page.'" enable_tag_filter="'.$enable_tag_filter.'" marker_cluster="'.$marker_cluster.'" enable_tag_filter_dropdown="'.$enable_tag_filter_dropdown.'" distance_search="'.$distance_search.'" map_position="'.$map_position.'" review="'.$review.'"]';
				        echo do_shortcode($shortcodeText);
				        ?>
                    </div>

			        <?php
			        $ci++;
		        }
	        }
	        ?>



        </div><!--end qcld_pd_tab_main-->
		
		<?php if(pd_ot_get_option('pd_enable_filtering_left')=='on'): ?>
			<script>
				jQuery(document).ready(function ($) {

					var fullwidth = window.innerWidth;
					if (fullwidth < 479) {
						$('.filter-carousel').not('.slick-initialized').slick({


							infinite: false,
							speed: 500,
							slidesToShow: 1,


						});
					} else {
						$('.filter-carousel').not('.slick-initialized').slick({

							dots: false,
							infinite: false,
							speed: 500,
							slidesToShow: 1,
							centerMode: false,
							variableWidth: true,
							slidesToScroll: 3,

						});
					}

				});
				
			</script>
		<?php endif; ?>
		<script>
			var per_page = <?php echo $per_page; ?>;
		</script>

<?php if($enable_map_fullwidth=='true'): ?>
<script type="text/javascript">
function getOffset1( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}
jQuery(document).ready(function($){
	
	var fullwidth = jQuery("body").prop("clientWidth"); 
	var maindivcon = jQuery('.sbd_map').parent()[0];
	var getleft = getOffset1(maindivcon);
	var fullwidth = parseFloat(fullwidth) - 2;
	var leftval = parseFloat(getleft.left)+11;
	console.log(fullwidth);
	jQuery('.sbd_map').css({
		'width':fullwidth+'px',		
		'left':'-'+leftval+'px',
	});
	
	$(window).resize(function() {
		var fullwidth = jQuery("body").prop("clientWidth"); 
		var maindivcon = jQuery('.sbd_map').parent()[0];
		var getleft = getOffset1(maindivcon);
		var fullwidth = parseFloat(fullwidth) - 2;
		var leftval = parseFloat(getleft.left)+11;
		//console.log(fullwidth);
		jQuery('.sbd_map').css({
			'width':fullwidth+'px',		
			'left':'-'+leftval+'px',
		});
	});
	
})
</script>
<?php endif; ?>		
       
<?php
    }

	$content = ob_get_clean();
	return $content;

}

add_shortcode('sbd-multipage-category', 'qcpd_multipage_all_category');
function qcpd_multipage_all_category($atts = array()){
	ob_start();
	wp_enqueue_script( 'qcopd-google-map-script');
	wp_enqueue_script( 'qcopd-google-map-scriptasd');
	wp_enqueue_script( 'qcpd-custom-script');
?>
<?php
	$customCss = pd_ot_get_option( 'pd_custom_style' );

	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>

<?php endif; ?>
<?php
	sbd_show_category();
	$content = ob_get_clean();
	return $content;
}

?>