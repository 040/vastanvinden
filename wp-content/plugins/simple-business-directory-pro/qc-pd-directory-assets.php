<?php

/*Load Scripts only in the shortcode page*/
function qcpd_check_for_shortcode($posts) {
	if ( empty($posts) )
		return $posts;

	// false because we have to search through the posts first
	$found = false;

	// search through each post
	foreach ($posts as $post) {
		// check the post content for the short code
		if ( stripos($post->post_content, 'qcpd-directory') )
			// we have found a post with the short code
			$found = true;
		// stop the search
		break;
	}

	if ($found){
		//Load Script and Stylesheets
		//add_action('wp_enqueue_scripts', 'qcpd_load_all_scripts');
	}

	return $posts;
}

//perform the check when the_posts() function is called
add_action('the_posts', 'qcpd_check_for_shortcode');

add_action('template_redirect', 'qcpd_check_for_shorcode');
function qcpd_check_for_shorcode(){
	global $wp_query;

	$post = $wp_query->get_queried_object();
		
		if(isset($post->post_content) && $post->post_content!=''){
			if ( $post && strpos($post->post_content, 'qcpd-directory' ) !== false ) {
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
			elseif( $post && strpos($post->post_content, 'sbd_single_item' ) !== false ) {
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');			
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
			elseif($post && strpos($post->post_content, 'sbd_dashboard' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');			
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
			elseif($post && strpos($post->post_content, 'sbd_login' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'sbd_registration' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'sbd_restore' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'pd-tab' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'qcpd-directory-multipage' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'sbd-multipage-category' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && strpos($post->post_content, 'sbd_claim_listing' ) !== false ){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);

			}elseif($post && $post->post_title=='Embed Link'){
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
				add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
			}
		}else{
			add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
			add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			add_action('wp_enqueue_scripts', 'qcsbd_category_tab', 30, 1);
			add_action('wp_enqueue_scripts', 'qcsbd_dashboard_css');
		}

}




/*Load Global Scripts*/


function qcsbd_dashboard_css(){
	wp_register_style( 'pdcustom_dashboard-css', QCSBD_ASSETS_URL.'/css/dashboardstyle.css', __FILE__ );
	wp_enqueue_style( 'pdcustom_dashboard-css' );
}


function qcpd_load_global_scripts()
{
	global $pd_plugin_version;
	 if(pd_ot_get_option('pd_image_upload')=='on'){
	    wp_enqueue_media();
	 }
	//FontAwesome
	wp_enqueue_style( 'qcawesomefont.css', QCSBD_ASSETS_URL . '/css/qcawesomefont.min.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'qcpd-custom-css', QCSBD_ASSETS_URL . '/css/directory-style.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'qcpd-custom-rwd-css', QCSBD_ASSETS_URL . '/css/directory-style-rwd.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'qcpd-custom-registration-css', QCSBD_ASSETS_URL . '/css/sbd_registration.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'qcpd-pdcustom-common-css', QCSBD_ASSETS_URL . '/css/pdcustomize-common.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'qcopd-magpopup-css', QCSBD_ASSETS_URL . '/css/magnific-popup.css', array(), $pd_plugin_version);
	wp_enqueue_style('qcpd-embed-form-css', QCSBD_URL . '/embed/css/embed-form.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'pd-tab-css', QCSBD_ASSETS_URL . '/css/tab_style.css', array(), $pd_plugin_version);
	wp_enqueue_style( 'pd-autocomplete-css', QCSBD_ASSETS_URL . '/css/jquery-autocomplete-ui.css', array(), $pd_plugin_version);

	wp_register_style( 'pdcustom_login-css', QCSBD_ASSETS_URL.'/css/style.css', __FILE__ );
	wp_enqueue_style( 'pdcustom_login-css' );
	
	//FontAwesome
    wp_enqueue_style('qcpd-embed-form-css', QCSBD_URL . '/embed/css/embed-form.css', array(), $pd_plugin_version);

    //Scripts
    wp_enqueue_script('qcpd-embed-form-script', QCSBD_URL . '/embed/js/embed-form.js', array('jquery'), $pd_plugin_version);

	//Scripts
	//wp_enqueue_script( 'jquery', 'jquery');
	wp_enqueue_script("jquery");
	//wp_deregister_script('jquery');
	//wp_enqueue_script( 'qcpd-grid-packery', QCSBD_ASSETS_URL . '/js/packery.pkgd.js', array('jquery'),'',true);
	//wp_enqueue_script( 'qcopd-magpopup-js', QCSBD_ASSETS_URL . '/js/jquery.magnific-popup.min.js', array('jquery'),'',true);
	
	wp_enqueue_script( 'qcpd-tooltipster', QCSBD_ASSETS_URL . '/js/tooltipster.bundle.min.js', array('jquery'), $pd_plugin_version,true);
	//wp_enqueue_script( 'qcpd-admin-tagInput-js', QCSBD_ASSETS_URL . '/js/tagInput.js', array('jquery') );
	wp_enqueue_style( 'qcpd-fa-modal-css', QCSBD_ASSETS_URL . '/css/admin-fa-css.css' );
	//wp_enqueue_script( 'qcpd-custom-script', QCSBD_ASSETS_URL . '/js/directory-script.js', array('jquery', 'qcpd-grid-packery'),'',true);
	
	
	wp_enqueue_script('qcpd-embed-form-script', QCSBD_URL . '/embed/js/embed-form.js', array('jquery'), $pd_plugin_version,true);

	wp_enqueue_script( 'qcpd-pdcustom-common-script', QCSBD_ASSETS_URL . '/js/pdcustomization-common.js', array('jquery'), $pd_plugin_version,true);

	/*$mapapi = 'AIzaSyBACyZ4vA8pQybj9ZdZP-J5zQHqfQkqOXY';
	if(pd_ot_get_option('pd_map_api_key')!=''){
		$mapapi = pd_ot_get_option('pd_map_api_key');
	}*/

	$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

	if($pd_map_open_street_map == 'on'){


		wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-openstreet-tab.js', array('jquery'), $pd_plugin_version,true);

	}else{

		$mapapi = 'AIzaSyBACyZ4vA8pQybj9ZdZP-J5zQHqfQkqOXY';
		if(pd_ot_get_option('pd_map_api_key')!=''){
			$mapapi = pd_ot_get_option('pd_map_api_key');
		}
		
		//google map api...
		wp_register_script( 'qcopd-google-map-script','https://maps.googleapis.com/maps/api/js?key='.$mapapi.'&libraries=geometry,places', null, null, false );
		wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-tab.js', array('jquery'), $pd_plugin_version, true );

	}
	
	//google map api...
	// wp_register_script( 'qcopd-google-map-script','https://maps.googleapis.com/maps/api/js?key='.$mapapi.'&libraries=geometry,places', null, null, false );
	
	wp_register_script( 'qcopd-google-map-scriptasd',QCSBD_ASSETS_URL.'/js/oms.min.js', null, null, false );
	
	$filterType = pd_ot_get_option( 'pd_filter_ptype' );

	
	wp_enqueue_style( 'jq-slick.css-css', QCSBD_ASSETS_URL . '/css/slick.css');
	wp_enqueue_style( 'jq-slick-theme-css', QCSBD_ASSETS_URL . '/css/slick-theme.css');
	wp_enqueue_script( 'jq-slick.min-js', QCSBD_ASSETS_URL . '/js/slick.min.js', array('jquery'));
	
	// wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-tab.js', array('jquery'), $pd_plugin_version,true);
	
	$sbd_combine_distant_tag_live_search = pd_ot_get_option('sbd_combine_distant_tag_live_search') ? pd_ot_get_option('sbd_combine_distant_tag_live_search') : 'off';
	$inclusive_tag_filter = pd_ot_get_option('inclusive_tag_filter') ? pd_ot_get_option('inclusive_tag_filter') : 'off';
	$pd_enable_map_addr_phone_clickable = pd_ot_get_option('pd_enable_map_addr_phone_clickable') ? pd_ot_get_option('pd_enable_map_addr_phone_clickable') : 'off';
	$sbd_custom_marker_image_width = pd_ot_get_option('sbd_custom_marker_image_width') ? intval(pd_ot_get_option('sbd_custom_marker_image_width')) : 30;
	$sbd_custom_marker_image_height = pd_ot_get_option('sbd_custom_marker_image_height') ? intval(pd_ot_get_option('sbd_custom_marker_image_height')) : 30;
	$sbd_map_fitbounds = pd_ot_get_option('sbd_map_fitbounds') ? pd_ot_get_option('sbd_map_fitbounds') : 'on';
	$sld_variables = array(
		'distance_location_text' =>	(pd_ot_get_option('sbd_lan_distance_location')!=''?pd_ot_get_option('sbd_lan_distance_location'):'Please provide location'),
		'distance_value_text'=>	(pd_ot_get_option('sbd_lan_distance_value')!=''?pd_ot_get_option('sbd_lan_distance_value'):'Please provide Distance value'),
		'distance_no_result_text' => (pd_ot_get_option('sbd_lan_no_result')!=''?pd_ot_get_option('sbd_lan_no_result'):'No result found!'),
		'radius_circle_color'=> (pd_ot_get_option('sbd_radius_circle_color')!=''?pd_ot_get_option('sbd_radius_circle_color'):'#FF0000'),
		'zoom'=> (pd_ot_get_option('sbd_map_zoom')!=''?(int)pd_ot_get_option('sbd_map_zoom'):13),
		'popup_zoom'=> (pd_ot_get_option('sbd_popup_map_zoom')!=''?pd_ot_get_option('sbd_popup_map_zoom'):'13'),
		'latitute'=> (pd_ot_get_option('sbd_map_lat')!=''?pd_ot_get_option('sbd_map_lat'):'37.090240'),
		'longitute'=> (pd_ot_get_option('sbd_map_long')!=''?pd_ot_get_option('sbd_map_long'):'-95.712891'),
		'global_marker'=>(pd_ot_get_option('sbd_global_marker_image')!=''?pd_ot_get_option('sbd_global_marker_image'):''),
		'paid_marker'=>(pd_ot_get_option('sbd_paid_marker_image')!=''?pd_ot_get_option('sbd_paid_marker_image'):''),
		'paid_marker_default'=> QCSBD_IMG_URL.'/Pink-icon.png',
		'sbd_combine_distant_tag_live_search' => $sbd_combine_distant_tag_live_search,
		'inclusive_tag_filter' => $inclusive_tag_filter,
		'pd_enable_map_addr_phone_clickable' => $pd_enable_map_addr_phone_clickable,
		'sbd_custom_marker_image_width'		=>	$sbd_custom_marker_image_width,
		'sbd_custom_marker_image_height'	=>	$sbd_custom_marker_image_height,
		'sbd_map_fitbounds'	=>	$sbd_map_fitbounds,
		'mailto_send_email'=>(pd_ot_get_option('pd_enable_mailto_send_email')!=''?pd_ot_get_option('pd_enable_mailto_send_email'):''),
	);
	
	wp_localize_script('qcpd-custom1-script', 'sld_variables', $sld_variables);
}

function qcsbd_category_tab(){
	global $pd_plugin_version;

	$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

	if($pd_map_open_street_map == 'on'){
		
		wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-openstreet-tab.js', array('jquery'), $pd_plugin_version,true);

	}else{
		
		wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-tab.js', array('jquery'), $pd_plugin_version,true);

	}
	
	// wp_enqueue_script( 'qcpd-custom1-script', QCSBD_ASSETS_URL . '/js/category-tab.js', array('jquery'), $pd_plugin_version,true);

	$sbd_combine_distant_tag_live_search = pd_ot_get_option('sbd_combine_distant_tag_live_search') ? pd_ot_get_option('sbd_combine_distant_tag_live_search') : 'off';
	$inclusive_tag_filter = pd_ot_get_option('inclusive_tag_filter') ? pd_ot_get_option('inclusive_tag_filter') : 'off';
	$pd_enable_map_addr_phone_clickable = pd_ot_get_option('pd_enable_map_addr_phone_clickable') ? pd_ot_get_option('pd_enable_map_addr_phone_clickable') : 'off';
	$sbd_custom_marker_image_width = pd_ot_get_option('sbd_custom_marker_image_width') ? intval(pd_ot_get_option('sbd_custom_marker_image_width')) : 30;
	$sbd_custom_marker_image_height = pd_ot_get_option('sbd_custom_marker_image_height') ? intval(pd_ot_get_option('sbd_custom_marker_image_height')) : 30;
	$sbd_map_fitbounds = pd_ot_get_option('sbd_map_fitbounds') ? pd_ot_get_option('sbd_map_fitbounds') : 'on';
	$pd_enable_map_autopan = pd_ot_get_option('pd_enable_map_autopan') ? pd_ot_get_option('pd_enable_map_autopan') : 'off';
	$sld_variables = array(
		'distance_location_text' =>	(pd_ot_get_option('sbd_lan_distance_location')!=''?pd_ot_get_option('sbd_lan_distance_location'):'Please provide location'),
		'distance_value_text'=>	(pd_ot_get_option('sbd_lan_distance_value')!=''?pd_ot_get_option('sbd_lan_distance_value'):'Please provide Distance value'),
		'distance_no_result_text' => (pd_ot_get_option('sbd_lan_no_result')!=''?pd_ot_get_option('sbd_lan_no_result'):'No result found!'),
		'radius_circle_color'=> (pd_ot_get_option('sbd_radius_circle_color')!=''?pd_ot_get_option('sbd_radius_circle_color'):'#FF0000'),
		'zoom'=> (pd_ot_get_option('sbd_map_zoom')!=''?(int)pd_ot_get_option('sbd_map_zoom'):13),
		'popup_zoom'=> (pd_ot_get_option('sbd_popup_map_zoom')!=''?pd_ot_get_option('sbd_popup_map_zoom'):'13'),
		'latitute'=> (pd_ot_get_option('sbd_map_lat')!=''?pd_ot_get_option('sbd_map_lat'):'37.090240'),
		'longitute'=> (pd_ot_get_option('sbd_map_long')!=''?pd_ot_get_option('sbd_map_long'):'-95.712891'),
		'global_marker'=>(pd_ot_get_option('sbd_global_marker_image')!=''?pd_ot_get_option('sbd_global_marker_image'):''),
		'paid_marker'=>(pd_ot_get_option('sbd_paid_marker_image')!=''?pd_ot_get_option('sbd_paid_marker_image'):''),
		'paid_marker_default'=> QCSBD_IMG_URL.'/Pink-icon.png',
		'sbd_combine_distant_tag_live_search' => $sbd_combine_distant_tag_live_search,
		'inclusive_tag_filter' => $inclusive_tag_filter,
		'pd_enable_map_addr_phone_clickable' => $pd_enable_map_addr_phone_clickable,
		'sbd_custom_marker_image_width'		=>	$sbd_custom_marker_image_width,
		'sbd_custom_marker_image_height'	=>	$sbd_custom_marker_image_height,
		'sbd_map_fitbounds'	=>	$sbd_map_fitbounds,
		'pd_enable_map_autopan'	=>	$pd_enable_map_autopan,
		'mailto_send_email'=>(pd_ot_get_option('pd_enable_mailto_send_email')!=''?pd_ot_get_option('pd_enable_mailto_send_email'):''),
	);
	
	wp_localize_script('qcpd-custom1-script', 'sld_variables', $sld_variables);
	
}

/*******************************
 * Admin Script
 *******************************/
function qcpd_admin_enqueue($hook) {
	global $pd_plugin_version;
	global $typenow;
	$post_types = get_post_types();

	wp_enqueue_media();
	wp_enqueue_script( 'qcpd-fa-script', QCSBD_ASSETS_URL . '/js/admin-fa-script.js', $pd_plugin_version );
	wp_enqueue_script( 'qcpd-admin-cmn-js', QCSBD_ASSETS_URL . '/js/admin-common.js', $pd_plugin_version );

	$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

	wp_add_inline_script( 'qcpd-admin-cmn-js', 
            'var pd_map_open_street_map = "'.esc_html($pd_map_open_street_map).'";
		    ');

	if( $typenow === 'pd' ){
		wp_enqueue_script( 'qcpd-admin-tagInput-js', QCSBD_ASSETS_URL . '/js/tagInput.js', array('jquery'), $pd_plugin_version );
	}
	wp_enqueue_script( 'qcpd-pdcustom-common-script-admin', QCSBD_ASSETS_URL . '/js/pdcustomization-common.js', array('jquery'), $pd_plugin_version);
	wp_enqueue_style( 'qcpd-fa-modal-css', QCSBD_ASSETS_URL . '/css/admin-fa-css.css', array(), $pd_plugin_version );
	wp_enqueue_style( 'qcpd-fa-css', QCSBD_ASSETS_URL . '/css/qcawesomefont.min.css', array(), $pd_plugin_version );

	if( $typenow === 'pd' ){
		wp_enqueue_style( 'qcpd-common-css', QCSBD_ASSETS_URL . '/css/admin-common.css', array(), $pd_plugin_version );
	}else{
		foreach ($post_types as $key => $value) {
			if( $key == $typenow ){
				wp_enqueue_style( 'qcpd-common-css', QCSBD_ASSETS_URL . '/css/admin-common.css', array(), $pd_plugin_version );
			}
		}
	}

	wp_enqueue_style( 'qcpd-pdcustom-common-css-admin', QCSBD_ASSETS_URL . '/css/pdcustomize-common.css', array(), $pd_plugin_version);
	wp_enqueue_script('jquery-ui-datepicker');
	/*$mapapi = 'AIzaSyBACyZ4vA8pQybj9ZdZP-J5zQHqfQkqOXY';
	if(pd_ot_get_option('pd_map_api_key')!=''){
		$mapapi = pd_ot_get_option('pd_map_api_key');
	}
	wp_enqueue_script( 'qcopd-google-map-script-bc','https://maps.googleapis.com/maps/api/js?key='.$mapapi.'&libraries=geometry,places', null, null, false );*/

	$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

	if($pd_map_open_street_map == 'on'){

		
		wp_enqueue_style( 'qcpd-leaflet-css', 'https://unpkg.com/leaflet@1.3.1/dist/leaflet.css' );
		wp_enqueue_script( 'qcopd-google-map-leaflet-src', 'https://unpkg.com/leaflet@1.3.1/dist/leaflet.js' );

	}else{

		$mapapi = 'AIzaSyBACyZ4vA8pQybj9ZdZP-J5zQHqfQkqOXY';
		if(pd_ot_get_option('pd_map_api_key')!=''){
			$mapapi = pd_ot_get_option('pd_map_api_key');
		}
		wp_enqueue_script( 'qcopd-google-map-script-bc','https://maps.googleapis.com/maps/api/js?key='.$mapapi.'&libraries=geometry,places', null, null, false );
	}



}

add_action( 'admin_enqueue_scripts', 'qcpd_admin_enqueue' );

/*Global Font Configs*/


add_action('wp_head', 'pd_global_font_configurations_func');

function pd_global_font_configurations_func()
{

	$pd_use_global_font = pd_ot_get_option('pd_use_global_font');
	if(isset($pd_use_global_font) and $pd_use_global_font=='yes'){
		$pdFontConfig = pd_ot_get_option( 'pd_global_font' );
		if( isset($pdFontConfig) && count($pdFontConfig) > 0 ){
			$fontFamily = ucwords(trim($pdFontConfig[0]['family']));
			?>
            <!-- Global Font Linking -->
            
			<?php
			if( $fontFamily != '' ){
				?>
				<link href="https://fonts.googleapis.com/css?family=<?php echo str_replace(" ", "+", $fontFamily); ?>" rel="stylesheet" />
                <!-- Global Font Settings -->
                <style>
                    .qc-grid-item h3, .qc-grid-item h3 span, .qc-grid-item .upvote-count, .qc-grid-item ul li, .qc-grid-item ul li a, .pdp-holder a, .html5tooltip-top .html5tooltip-text, .html5tooltip-top a, .tooltipster-base{
                        font-family: <?php echo $fontFamily; ?> !important;
                    }
                </style>
				<?php
			}

		}
	}

}

function sbd_my_scripts() {
	global $pd_plugin_version;
	wp_register_script('sbd-app-js', QCSBD_ASSETS_URL . '/js/jquery.magnific-popup.min.js', array('jquery'), $pd_plugin_version, true );
	wp_enqueue_script('sbd-app-js');
	wp_enqueue_script( 'qcpd-grid-packery', QCSBD_ASSETS_URL . '/js/packery.pkgd.js', array('jquery'), $pd_plugin_version,true);
	wp_enqueue_script( 'qcpd-markercluster-sbd', 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js', array(),'',false);

	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script( 'jquery-ui-autocomplete' );

	$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

	if($pd_map_open_street_map == 'on'){

		wp_enqueue_style( 'jq-openstreet-theme-css', 'https://unpkg.com/leaflet@1.3.1/dist/leaflet.css');
		wp_enqueue_style( 'jq-openstreet-marker-css', 'https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css');
		wp_enqueue_style( 'jq-openstreet-MarkerCluster-css', 'https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css');

		wp_enqueue_script( 'qcpd-openstreet-sbd', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js');
		wp_enqueue_script( 'qcpd-openstreet-marker-sbd', 'https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js');
		
		wp_enqueue_script( 'qcpd-custom-script', QCSBD_ASSETS_URL . '/js/directory-openstreet-script.js', array( 'jquery', 'qcpd-openstreet-sbd', 'qcpd-openstreet-marker-sbd'), $pd_plugin_version,true);


	}else{

		

		/*if($pd_enable_ajax_search == 'on'){
			wp_register_script( 'qcpd-custom-script', QCSBD_ASSETS_URL . '/js/directory-script-ajax.js', array('jquery', 'qcpd-grid-packery'), $pd_plugin_version,true);



		}else{*/
			wp_register_script( 'qcpd-custom-script', QCSBD_ASSETS_URL . '/js/directory-script.js', array('jquery', 'qcpd-grid-packery'), $pd_plugin_version,true);

		//}


	}



	$sld_cluster = array(
		'cluster'			=> false,
		'image_infowindow'	=> false,
		'pagination'		=> 'false',
		'per_page'			=> 1,
		'main_click' 		=> 1,
		'map' 				=> 'true',
		'map_marker_scroll' => 'false',
		'distance_search' 	=> 'false'
	);	

	wp_localize_script('qcpd-custom-script', 'cluster', $sld_cluster);

	//wp_register_script( 'qcpd-custom-script', QCSBD_ASSETS_URL . '/js/directory-script.js', array('jquery', 'qcpd-grid-packery'), $pd_plugin_version,true);

	$sbd_combine_distant_tag_live_search = pd_ot_get_option('sbd_combine_distant_tag_live_search') ? pd_ot_get_option('sbd_combine_distant_tag_live_search') : 'off';
	$inclusive_tag_filter = pd_ot_get_option('inclusive_tag_filter') ? pd_ot_get_option('inclusive_tag_filter') : 'off';
	$pd_enable_map_addr_phone_clickable = pd_ot_get_option('pd_enable_map_addr_phone_clickable') ? pd_ot_get_option('pd_enable_map_addr_phone_clickable') : 'off';
	$sbd_custom_marker_image_width = pd_ot_get_option('sbd_custom_marker_image_width') ? intval(pd_ot_get_option('sbd_custom_marker_image_width')) : 30;
	$sbd_custom_marker_image_height = pd_ot_get_option('sbd_custom_marker_image_height') ? intval(pd_ot_get_option('sbd_custom_marker_image_height')) : 30;
	$sbd_map_fitbounds = pd_ot_get_option('sbd_map_fitbounds') ? pd_ot_get_option('sbd_map_fitbounds') : 'on';
	$pd_enable_map_autopan = pd_ot_get_option('pd_enable_map_autopan') ? pd_ot_get_option('pd_enable_map_autopan') : 'off';
	$pd_enable_autocomplete_search = pd_ot_get_option('pd_enable_autocomplete_search') ? pd_ot_get_option('pd_enable_autocomplete_search') : '';
	$sld_variables = array(
		'distance_location_text' =>	(pd_ot_get_option('sbd_lan_distance_location')!=''?pd_ot_get_option('sbd_lan_distance_location'):'Please provide location'),
		'distance_value_text'=>	(pd_ot_get_option('sbd_lan_distance_value')!=''?pd_ot_get_option('sbd_lan_distance_value'):'Please provide Distance value'),
		'distance_no_result_text' => (pd_ot_get_option('sbd_lan_no_result')!=''?pd_ot_get_option('sbd_lan_no_result'):'No result found!'),
		'radius_circle_color'=> (pd_ot_get_option('sbd_radius_circle_color')!=''?pd_ot_get_option('sbd_radius_circle_color'):'#FF0000'),
		'zoom'=> (pd_ot_get_option('sbd_map_zoom')!=''?pd_ot_get_option('sbd_map_zoom'):'13'),
		'popup_zoom'=> (pd_ot_get_option('sbd_popup_map_zoom')!=''?pd_ot_get_option('sbd_popup_map_zoom'):'13'),
		'latitute'=> (pd_ot_get_option('sbd_map_lat')!=''?pd_ot_get_option('sbd_map_lat'):'37.090240'),
		'longitute'=> (pd_ot_get_option('sbd_map_long')!=''?pd_ot_get_option('sbd_map_long'):'-95.712891'),
		'global_marker'=>(pd_ot_get_option('sbd_global_marker_image')!=''?pd_ot_get_option('sbd_global_marker_image'):''),
		'paid_marker'=>(pd_ot_get_option('sbd_paid_marker_image')!=''?pd_ot_get_option('sbd_paid_marker_image'):''),
		'paid_marker_default'=> QCSBD_IMG_URL.'/Pink-icon.png',
		'view_details'=> (pd_ot_get_option('sbd_lan_view_details')!=''?pd_ot_get_option('sbd_lan_view_details'):'View Details'),
		'email_sent'=> (pd_ot_get_option('sbd_lan_email_sent')!=''?pd_ot_get_option('sbd_lan_email_sent'):'Email has been sent. Thank you!'),
		'qcajax_nonce'=> wp_create_nonce( "2q3c5sbd-email1-ajax2-noncea" ),
		'enable_landmarks'=> (pd_ot_get_option('pd_enable_landmarks')=='off'?'off':'on'),
		'sbd_combine_distant_tag_live_search' => $sbd_combine_distant_tag_live_search,
		'inclusive_tag_filter' => $inclusive_tag_filter,
		'pd_enable_map_addr_phone_clickable' => $pd_enable_map_addr_phone_clickable,
		'sbd_custom_marker_image_width'		=>	$sbd_custom_marker_image_width,
		'sbd_custom_marker_image_height'	=>	$sbd_custom_marker_image_height,
		'sbd_map_fitbounds'	=>	$sbd_map_fitbounds,
		'pd_enable_map_autopan'	=>	$pd_enable_map_autopan,
		'pd_enable_autocomplete_search'	=>	$pd_enable_autocomplete_search,
		'mailto_send_email'=>(pd_ot_get_option('pd_enable_mailto_send_email')!=''?pd_ot_get_option('pd_enable_mailto_send_email'):''),
	);
	
	wp_localize_script('qcpd-custom-script', 'sld_variables', $sld_variables);

	$sld_nonce = "var qc_sbd_get_ajax_nonce ='". wp_create_nonce( 'qc-pd')."';";

	wp_add_inline_script('qcpd-custom-script',  $sld_nonce, 'before');



	/*
	$pd_snazzymap_js = '';
	if( trim(pd_ot_get_option('pd_snazzymap_js')) != '' ){
		$pd_snazzymap_js = array(
			'pd_snazzymap_js' => trim((pd_ot_get_option('pd_snazzymap_js')))
		);
	}
	// print_r($pd_snazzymap_js);
	// die();
	// wp_localize_script('qcpd-custom-script', 'pd_snazzymap_js', $pd_snazzymap_js);
	*/

}

add_action('wp_head', 'pd_snazzymap_js');
function pd_snazzymap_js(){
	$pd_snazzymap_js = '';
	if( trim(pd_ot_get_option('pd_snazzymap_js')) ){
		$pd_snazzymap_js = trim(pd_ot_get_option('pd_snazzymap_js'));
	}
?>
	<script>
		<?php if( trim(pd_ot_get_option('pd_snazzymap_js')) ){ ?>
			var pd_snazzymap_js = <?php echo $pd_snazzymap_js; ?>;
		<?php }else{ ?>
			var pd_snazzymap_js = '';
		<?php } ?>
	</script>
<?php
}

add_action('wp_print_scripts', 'sbd_deregister_script', 100);
function sbd_deregister_script(){
	if(is_admin()){
		wp_dequeue_script( 'default' );
		wp_deregister_script( 'default' );
	}
}

