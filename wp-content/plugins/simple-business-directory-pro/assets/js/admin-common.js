﻿jQuery(document).ready(function($){
	$('#tab_frontend').on('click',function(e){
		e.preventDefault();
		$('#pd_page_check').html('<p class="pd_page_loading">Loading...</p>');
		var datarr = ['sbd_login', 'sbd_registration', 'sbd_dashboard', 'sbd_restore'];
		for(var i=0;i<4;i++){
			
			$.post(ajaxurl, {
				action: 'qcpd_search_pd_page', 
				shortcode: datarr[i],

				},
				
				function(data) {
					$('#pd_page_check .pd_page_loading').hide();
					
					if(data!=='' && !data.match(/not/g)){
						$('#pd_page_check').append('<p style="color:green">'+data+'</p>');
					}else{
						$('#pd_page_check').append('<p style="color:red">'+data+'</p>');
					}
					
					
					
				});
			
			
		}
		//
	})
	
	$(document).on('click', '.sld_collapse', function(e){
		e.preventDefault();
		
		var elem = $(this);
		elem.closest('.field-item').addClass('sld_section_collapse');
		elem.removeClass('sld_collapse').addClass('sld_expend');
		elem.text('Expand');
		
	})
	$(document).on('click', '.sld_expend', function(e){
		e.preventDefault();
		
		var elem = $(this);
		elem.closest('.field-item').removeClass('sld_section_collapse');
		elem.removeClass('sld_expend').addClass('sld_collapse');
		elem.text('Collapse');
		
	})
	
	$(document).on('click','.sld_ctm_btn1', function(e){
		e.preventDefault();
		var elem = $(this);
		$( ".sld_collapse" ).each(function( index ) {
		  $( this ).click();
		});
		
		elem.removeClass('sld_ctm_btn1').addClass('sld_ctm_btn1_e');
		elem.text('Expand All');
		
		
	})
	$(document).on('click','.sld_ctm_btn1_e', function(e){
		e.preventDefault();
		var elem = $(this);
		$( ".sld_expend" ).each(function( index ) {
		  $( this ).click();
		});
		
		elem.removeClass('sld_ctm_btn1_e').addClass('sld_ctm_btn1');
		elem.text('Collapse All');
		
		
	})
	$(document).on('click','.sld_ctm_btn2', function(e){
		e.preventDefault();
		 $('html, body').animate({
			scrollTop: $("#text-ad-block").offset().top - 100
		}, 2000);
	})
	
	
	$(document).on('click', '#qcpd_tags input', function(e){
		var elem = $(this);
		var elemid = this.id;
		$.post(ajaxurl, {
			action: 'qcpd_tag_pd_page', 
			
			},
			
		function(data) {
			
			$('#wpwrap').append(data);
			$('#pdtagvalue').val(elem.val());
			$('#pd-tags').attr('data', elemid);
			
			//console.log($(data).find('.fa-field-modal-title').text());

			$('#pd-tags').tagInput();
			$('.labelinput').focus();
			$.post(ajaxurl, {
			action: 'qcpd_search_pd_tags', 
			},
			
			function(data) {
				console.log(data);
				
				$('.labelinput').autocomplete({
					  source: data.split(','),
					  
				});					
				
			});
				

			
		});
		
	})
	
	$(document).on('click', '#sbd_item_tags', function(e){
		var elem = $(this);
		var elemid = this.id;
		$.post(ajaxurl, {
			action: 'qcpd_tag_pd_page', 
			
			},
			
		function(data) {
			
			$('#wpwrap').append(data);
			$('#pdtagvalue').val(elem.val());
			$('#pd-tags').attr('data', elemid);
			
			//console.log($(data).find('.fa-field-modal-title').text());

			$('#pd-tags').tagInput();
			$('.labelinput').focus();
			$.post(ajaxurl, {
			action: 'qcpd_search_pd_tags', 
			},
			
			function(data) {
				
				$('.labelinput').autocomplete({
					  source: data.split(','),
					  
				});					
				
			});
				

			
		});
		
	})
	
	$(document).on( "autocompleteselect",'.labelinput', function( event, ui ) {
		//event.preventDefault();
		if( event.keyCode == 13 ){
			event.preventDefault();
		}
	});
	
	$(document).on('click','.closelabel',function(e){
		e.preventDefault();
	})
	
	$( document ).on( 'click','.fa-field-modal-close', function() {
		
		$('.fa-field-modal-wrap').remove();
		$('#fa-field-modal-tag').remove();

	});
	$(document).on('click','#pd_tag_select', function(){
		$('#'+$('#pd-tags').attr('data')).val($('#pdtagvalue').val());
		$('.fa-field-modal-wrap').remove();
		$('#fa-field-modal-tag').remove();
	})
	
	//$("body").on('focusout', "input[name*='[qcpd_item_full_address]']", function(e){
	/*$(document).on('click', "input[name*='[qcpd_image_from_link]']", function(e){
		var objc = $(this);
		
		
		if($(this).is(':checked')) {
			if(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val()!='' && objc.closest('.cmb_metabox').find('#qcpd_item_img input').val()==''){
				
				$.post(ajaxurl, {
				action: 'qcpd_img_download', 
				url: objc.closest('.cmb_metabox').find('#qcpd_item_link input').val(),

				},
				function(data) {
					if(data!=''){
						objc.closest('.cmb_metabox').find('#qcpd_item_img input').val(data);
					}
					
				});
				
			}
			
			//alert(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val() + objc.closest('.cmb_metabox').find('#qcpd_item_img input').val());
		}
	})*/
	
	$(document).on('click', "#sbd_generate_image", function(e){
		var objc = $(this);
		
		
		if($(this).is(':checked')) {
			if($('#sbd_item_link').val()!='' && $('#item_image').val()==''){
				
				$.post(ajaxurl, {
				action: 'qcpd_img_download', 
				url: $('#sbd_item_link').val(),

				},
				function(data) {
					
					var data = $.parseJSON(data);
					console.log(data);
					if(data!=''){
						$('#item_image').val(data.attachmentid);
						$('.sbd_item_image_preview img').attr('src', data.imgurl);
					}
					
				});
				
			}
			
			//alert(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val() + objc.closest('.cmb_metabox').find('#qcpd_item_img input').val());
		}
	})
	
	$(document).on('click', "input[name*='[qcpd_image_from_link]']", function(e){
		var objc = $(this);
		
		
		if($(this).is(':checked')) {
			if(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val()!='' && objc.closest('.cmb_metabox').find('#qcpd_item_img input').val()==''){
				html = "<div id='sbd_ajax_preloader'><div class='sbd_ajax_loader'></div></div>";
				$('#wpwrap').append(html);
				$.post(ajaxurl, {
				action: 'qcpd_img_download', 
				url: objc.closest('.cmb_metabox').find('#qcpd_item_link input').val(),

				},
				function(data) {
					data = JSON.parse(data);
					console.log(data);
					$('#sbd_ajax_preloader').remove();

					
					if(data.imgurl.match(/.jpg/g) || data.imgurl!==null){
						objc.closest('.cmb_metabox').find('#qcpd_item_img input').val(data.attachmentid);
						objc.closest('.cmb_metabox').find('#qcpd_item_img .cmb-file-holder').show();
						objc.closest('.cmb_metabox').find('#qcpd_item_img .cmb-remove-file').removeClass('hidden').show();
						objc.closest('.cmb_metabox').find('#qcpd_item_img .cmb-file-upload').addClass('hidden').hide();
						objc.closest('.cmb_metabox').find('#qcpd_item_img .cmb-file-holder').append('<img src="'+data.imgurl+'" width="150" height="150" />');
					}else{
						alert('Could not generate image. Please check URL and try again.');
					}
					
					
				});
				
			}
			
			//alert(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val() + objc.closest('.cmb_metabox').find('#qcpd_item_img input').val());
		}
	})
	
	$(document).on('click', "input[name*='[qcpd_generate_title]']", function(e){
		var objc = $(this);
		if($(this).is(':checked')) {
			
			if(objc.closest('.cmb_metabox').find('#qcpd_item_link input').val()!=''){
				html = "<div id='sbd_ajax_preloader'><div class='sbd_ajax_loader'></div></div>";
				$('#wpwrap').append(html);
				$.post(ajaxurl, {
				action: 'qcpd_generate_text', 
				url: objc.closest('.cmb_metabox').find('#qcpd_item_link input').val(),
				},
				function(data) {
					
					$('#sbd_ajax_preloader').remove();
					data = JSON.parse(data);
					objc.closest('.cmb_metabox').find('#qcpd_item_title input').val(data.title)
					objc.closest('.cmb_metabox').find('#qcpd_item_subtitle input').val(data.description)
					objc.prop('checked', false);
				});
				
			}else{
				alert('Item link field cannot left empty!');
			}
			
		}
		
	})
	
	$('#sbd_flash_button').on('click',function(e){
		e.preventDefault();
		$('#sbd_flash_msg').html('<p class="sbd_page_loading">Loading...</p>');

			$.post(ajaxurl, {
				action: 'qcpd_flash_rewrite_rules', 
				},
				
				function(data) {

					$('#sbd_flash_msg').html('<p class="sbd_page_loading" style="color:green;">Rewrite has been Flushed Successfully!</p>');

				}
			);
	})


	$('.cmb_metabox').addClass('sbd_openstreet_map');

	function qcpd_openstreet_view_map_Function(arr, currentDom){

		var out = "";
		var i;
		$('.sbd_street_map_results').remove();
		currentDom.closest('#qcpd_item_full_address').append('<div class="sbd_street_map_results"></div>');

		if(arr.length > 0){
		 
			for(i = 0; i < arr.length; i++){
		  
				var display_name = '"' + arr[i].display_name + '"';
				out += "<div class='sbd_address' title='"+ arr[i].display_name +"' data-lat='"+ arr[i].lat +"' data-lon='"+ arr[i].lon +"' data-name='"+ arr[i].display_name +"' >" + arr[i].display_name + "</div>";
			}
			$('.sbd_street_map_results').html(out);

		}else{
			$('.sbd_street_map_results').html("Sorry, no results...");
			$('.sbd_street_map_results').show();
			$(".cmb_metabox.sbd_openstreet_map .cmb-row").css({"overflow": "visible"});

			setTimeout(function(){
				$('.sbd_street_map_results').fadeOut();
			},3000)
			return;

		}
		
		$('.sbd_street_map_results').show();
		$(".cmb_metabox.sbd_openstreet_map .cmb-row").css({"overflow": "visible"});

	}


	$(document).on('keyup', "input[name*='qcpd_item_full_address']", function(e){

		if(pd_map_open_street_map == 'on'){

			var currentDom = $(this);
			var inp = currentDom.val();
			var xmlhttp = new XMLHttpRequest();
			var url = "https://nominatim.openstreetmap.org/search?format=json&limit=10&q=" + inp;
			 xmlhttp.onreadystatechange = function()
			 {
			   if (this.readyState == 4 && this.status == 200)
			   {
				var myArr = JSON.parse(this.responseText);
				qcpd_openstreet_view_map_Function(myArr, currentDom);
			   }
			 };
			 xmlhttp.open("GET", url, true);
			 xmlhttp.send();

		}
		
	});


	$(document).on('click', '.sbd_address', function(e){
		var currentDom = $(this);
		var latVal = currentDom.attr('data-lat');
		var lonVal = currentDom.attr('data-lon');
		var nameVal = currentDom.attr('data-name');
		
		currentDom.closest('.cmb_metabox').find('#qcpd_item_latitude input').val(latVal);
		currentDom.closest('.cmb_metabox').find('#qcpd_item_longitude input').val(lonVal);
		currentDom.closest('.cmb_metabox').find('#qcpd_item_full_address input').val(nameVal);
		
		$('.sbd_street_map_results').hide();
		$(".cmb_metabox.sbd_openstreet_map .cmb-row").css({"overflow": "hidden"});
		
		
	});
		
	$("body").on('focusout', "input[name*='[qcpd_item_full_address]']", function(e){

		if(pd_map_open_street_map != 'on'){

			var objc = $(this);

			var geocoder = new google.maps.Geocoder();
			var address = objc.val();

			geocoder.geocode( { 'address': address}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
					
					objc.closest('.cmb_metabox').find('#qcpd_item_latitude input').val(latitude);
					objc.closest('.cmb_metabox').find('#qcpd_item_longitude input').val(longitude);
					
					
				}else{
					objc.closest('.cmb_metabox').find('#qcpd_item_latitude input').val(0);
					objc.closest('.cmb_metabox').find('#qcpd_item_longitude input').val(0);
					console.log('Unable to retrive lat, lng from this address');
				}
			
			});


		}


		
	})

	
	
	$("body").on('focus', "input[name*='[qcpd_item_full_address]']", function(e){

		if(pd_map_open_street_map != 'on'){
		
			var objc = $(this);
			var input = document.getElementById(objc.attr('id'));
			var autocomplete = new google.maps.places.Autocomplete(input);
			var geocoder = new google.maps.Geocoder;
			autocomplete.addListener('place_changed', function() {
          
				var place = autocomplete.getPlace();

				if (!place.place_id) {
					return;
				}

				/*
				geocoder.geocode({'placeId': place.place_id}, function(results, status) {

					if (status !== 'OK') {
					  alert('Geocoder failed due to: ' + status);
					  return;
					}
					//console.log(results[0].geometry.location.lat());
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
					
					objc.closest('.cmb_metabox').find('#qcpd_item_latitude input').val(latitude);
					objc.closest('.cmb_metabox').find('#qcpd_item_longitude input').val(longitude);

				})
				*/


			})

		}
		
	})
	$("body").on('focus', "input[name*='[qcpd_ex_date]']", function(e){
		$(this).datepicker({
			dateFormat : 'yy-mm-dd'
		});
	})
	/*
	$("input[name*='[qcpd_ex_date]']").datepicker({
		dateFormat : 'yy-mm-dd'
	});*/
	
	
	
	$('#sbd_shortcode_generator_meta').on('click', function(e){
		 $('#sbd_shortcode_generator_meta').prop('disabled', true);
		$.post(
			ajaxurl,
			{
				action : 'show_qcpd_shortcodes'
				
			},
			function(data){
				 $('#sbd_shortcode_generator_meta').prop('disabled', false);
				$('#wpwrap').append(data);
			}
		)
	})

	$(document).on( 'click', '.sbd_copy_close', function(){
        $(this).parent().parent().parent().parent().parent().remove();
    })
	
    $(document).on( 'click', '.modal-content .close', function(){
        $(this).parent().parent().remove();
    }).on( 'click', '#qcpd_add_shortcode',function(){
	
      var mode = $('#pd_mode').val();
      var column = $('#pd_column').val();
      var style = $('#pd_style').val();
      var upvote = $('.pd_upvote:checked').val();
      var search = $('.pd_search:checked').val();
      var radius = $('.pd_radius:checked').val();
      var cluster = $('.pd_cluster:checked').val();
      var infowindowimage = $('.pd_image_off_infowindow:checked').val();
      var count = $('.pd_item_count:checked').val();
      var item_details_page = $('.item_details_page:checked').val();
      var orderby = $('#pd_orderby').val();
      var filterorderby = $('#pd_filter_orderby').val();
	  
      var item_orderby = $('#pd_item_orderby').val();
      var order = $('#pd_order').val();
      var filterorder = $('#pd_filter_order').val();
	  
	  var listId = $('#pd_list_id').val();
	  var catSlug = $('#pd_list_cat_id').val();

	  var list_title_font_size = $('#pd_list_title_font_size').val();
	  var list_title_line_height = $('#pd_list_title_line_height').val();

	  var title_font_size = $('#pd_title_font_size').val();
	  var subtitle_font_size = $('#pd_subtitle_font_size').val();
	  var title_line_height = $('#pd_title_line_height').val();
	  var subtitle_line_height = $('#pd_subtitle_line_height').val();

	  var paginate = $('.pd_enable_pagination:checked').val();
	  var pagination_type = $('#qcpd_enable_pagination_option').val();
	  var tooltip = $('.pd_enable_tooltip:checked').val();

	  var per_page = $('#pd_items_per_page').val();

	  var filter_area = $('#pd_filter_area').val();
	  var topspacing = $('#pd_topspacing').val();

	  var pd_category_orderby = $('#pd_category_orderby').val();
	  var pd_category_order = $('#pd_category_order').val();
	  
	  var itemperpage = $('#itemperpage').val();
	  var infinityscroll = $('#infinityscroll:checked').val();
	  var pdmap = $('#pdmap:checked').val();
	  var pdmapposition = $('#pd_map_position').val();
	  var pdmaponly = $('#pdmaponly:checked').val();
	  
	  var embed_option = $('#embed_option').val();
	  var main_click_action = $('#main_click_action').val();
	  var phone_number = $('#phone_number').val();
	  var pd_rtl = $('.pd_rtl:checked').val();
	  var pd_left_filter = $('.pd_left_filter:checked').val();
	  var pd_tag_filter = $('.pd_tag_filter:checked').val();
	  var pd_tag_filter_dropdown = $('.pd_tag_filter_dropdown:checked').val();
	  var pd_map_full_width = $('.pd_map_full_width:checked').val();
	  var pd_map_full_screen = $('.pd_map_full_screen:checked').val();
	  var pd_map_toggle_filter = $('.pd_map_toggle_filter:checked').val();
	  var pdmapofflightbox = $('.pdmapofflightbox:checked').val();

	  var pd_hide_list_title = $('#pd_hide_list_title:checked').val();
	  
		
	  if( style == '' )
	  {
		alert("Please select a valid template style.");
		return;
	  }

	  if(mode=='categorytab'){
	  	var shortcode = 'pd-tab';
	  }else{
          var shortcode = 'qcpd-directory';
	  }
	  
	  var shortcodedata = '['+shortcode;
		  		  
		  if( mode !== 'category' ){
			  if(mode=='maponly'){
				  if(typeof(catSlug) !='undefined' && catSlug!==''){
					  shortcodedata +=' category="'+catSlug+'"';
				  }else{
					  shortcodedata +=' mode="all"';
				  }
				  
			  }else{
				shortcodedata +=' mode="'+mode+'"';
			  }
			  
		  }
		  
		  if( mode == 'one' && listId != "" ){
			  shortcodedata +=' list_id="'+listId+'"';
		  }
		  
		  if( mode == 'category' && catSlug != "" ){
			  shortcodedata +=' category="'+catSlug+'"';
		  }
		  
		  if( style !== '' ){

			  shortcodedata +=' style="'+style+'"';

		  }
		  
		  var style = $('#pd_style').val();
		

		  
		  if( typeof(column) != 'undefined' && column !== '' ){
			  shortcodedata +=' column="'+column+'"';
		  }

		  if( typeof(pd_hide_list_title) != 'undefined' && pd_hide_list_title !== '' ){
			  shortcodedata +=' hide_list_title="'+pd_hide_list_title+'"';
		  }
		  
		  if( typeof(upvote) != 'undefined' && upvote!='' ){
			  shortcodedata +=' upvote="'+upvote+'"';
		  }else{
			  shortcodedata +=' upvote="off"';
		  }
		  
		  if( typeof(search)!= 'undefined' && search!='' ){
			  shortcodedata +=' search="'+search+'"';
		  }else{
			  shortcodedata +=' search="false"';
		  }
		  if( typeof(radius)!= 'undefined' && radius!='' ){
			  shortcodedata +=' distance_search="'+radius+'"';
		  }else{
			  shortcodedata +=' distance_search="false"';
		  }
		  
		  if( typeof(cluster)!= 'undefined' && cluster!='' ){
			  shortcodedata +=' marker_cluster="'+cluster+'"';
		  }else{
			  shortcodedata +=' marker_cluster="false"';
		  }
		  
		  if( typeof(infowindowimage)!= 'undefined' && infowindowimage!='' ){
			  shortcodedata +=' image_infowindow="'+infowindowimage+'"';
		  }else{
			  shortcodedata +=' image_infowindow="false"';
		  }
		  
		  if( typeof(count)!= 'undefined' && count!='' ){
			  shortcodedata +=' item_count="'+count+'"';
		  }else{
			  shortcodedata +=' item_count="off"';
		  }
		  
		  if( typeof(item_details_page)!= 'undefined' && item_details_page!='' ){
			  shortcodedata +=' item_details_page="'+item_details_page+'"';
		  }else{
			  shortcodedata +=' item_details_page="off"';
		  }
		  
		  if( typeof(orderby)!= 'undefined' && orderby !== '' ){
			  shortcodedata +=' orderby="'+orderby+'"';
		  }else{
			  shortcodedata +=' orderby="date"';
		  }
		  if( typeof(filterorderby) != 'undefined' && filterorderby !== '' ){
			  shortcodedata +=' filterorderby="'+filterorderby+'"';
		  }else{
			  shortcodedata +=' filterorderby="date"';
		  }
		  
		  if( embed_option !== '' ){
			  shortcodedata +=' enable_embedding="'+embed_option+'"';
		  }
		  
		  if(typeof(main_click_action)!=="undefined" && main_click_action!==""){
			  shortcodedata +=' main_click_action="'+main_click_action+'"';
		  }
		  
		  if(typeof(phone_number)!=="undefined" && phone_number!==""){
			  shortcodedata +=' phone_number="'+phone_number+'"';
		  }
		  
		  if( order !== '' ){
			  shortcodedata +=' order="'+order+'"';
		  }
		  if( typeof(filterorder) != 'undefined' && filterorder !== '' ){
			  shortcodedata +=' filterorder="'+filterorder+'"';
		  }
		  
		/*  if( typeof(paginate) != 'undefined' && paginate!='' ){
			  shortcodedata +=' paginate_items="true"';
		  }else{
			  shortcodedata +=' paginate_items="false"';
		  }
		  
		  if( typeof(paginate) != 'undefined' && per_page !== '' ){
			  shortcodedata +=' per_page="'+per_page+'"';
		  }*/

		  if( typeof(pagination_type) != 'undefined' && pagination_type!='' ){
		  	if( pagination_type == 'js-pagination' ){
			  shortcodedata +=' paginate_items="true" actual_pagination="false"';
		  	}else if( pagination_type == 'page-pagination' ){
		  		shortcodedata +=' paginate_items="false" actual_pagination="true"';
		  	}else{
		  		shortcodedata +=' paginate_items="false" actual_pagination="false"';	
		  	}
		  }else{
			  shortcodedata +=' paginate_items="false" actual_pagination="false"';
		  }
		  
		  if( typeof(pagination_type) != 'undefined' && per_page !== '' ){
			  shortcodedata +=' per_page="'+per_page+'"';
		  }
		  
		  if( typeof(pd_rtl) != 'undefined' && pd_rtl !== '' ){
			  shortcodedata +=' enable_rtl="'+pd_rtl+'"';
		  }else{
			  shortcodedata +=' enable_rtl="false"';
		  }
		  
		  if( typeof(pd_left_filter) != 'undefined' && pd_left_filter !== '' ){
			  shortcodedata +=' enable_left_filter="'+pd_left_filter+'"';
		  }else{
			  shortcodedata +=' enable_left_filter="false"';
		  }
		  
		  if( typeof(pd_tag_filter) != 'undefined' && pd_tag_filter !== '' ){
			  shortcodedata +=' enable_tag_filter="'+pd_tag_filter+'"';
		  }else{
			  shortcodedata +=' enable_tag_filter="false"';
		  }
		  
		  if( typeof(pd_tag_filter_dropdown) != 'undefined' && pd_tag_filter_dropdown !== '' ){
			  shortcodedata +=' enable_tag_filter_dropdown="'+pd_tag_filter_dropdown+'"';
		  }else{
			  shortcodedata +=' enable_tag_filter_dropdown="false"';
		  }
		  if( typeof(pd_map_full_width) != 'undefined' && pd_map_full_width !== '' ){
			  shortcodedata +=' enable_map_fullwidth="'+pd_map_full_width+'"';
		  }else{
			  shortcodedata +=' enable_map_fullwidth="false"';
		  }
		  if( typeof(pd_map_full_screen) != 'undefined' && pd_map_full_screen !== '' ){
			  shortcodedata +=' enable_map_fullscreen="'+pd_map_full_screen+'"';
		  }else{
			  shortcodedata +=' enable_map_fullscreen="false"';
		  }
		  if( typeof(pd_map_toggle_filter) != 'undefined' && pd_map_toggle_filter !== '' ){
			  shortcodedata +=' enable_map_toggle_filter="'+pd_map_toggle_filter+'"';
		  }else{
			  shortcodedata +=' enable_map_toggle_filter="false"';
		  }
		  
		  if( typeof(infinityscroll) != 'undefined' && infinityscroll !== '' ){
			  shortcodedata +=' infinityscroll="'+infinityscroll+'"';
		  }
		  
		  if( typeof(pdmap) != 'undefined' && pdmap !== '' ){
			  shortcodedata +=' map="'+pdmap+'"';
		  }else{
			  if(mode=='maponly'){
				  shortcodedata +=' map="show"';
			  }else{
				  shortcodedata +=' map="hide"';
			  }
			  
		  }
		  
		  if( typeof(pdmapposition) != 'undefined' && pdmapposition !== '' ){
			  shortcodedata +=' map_position="'+pdmapposition+'"';
		  }
		  

		  
		  if( typeof(itemperpage) != 'undefined' && itemperpage !== '' ){
			  shortcodedata +=' itemperpage="'+itemperpage+'"';
		  }
		  
		  if( typeof(tooltip) != 'undefined' && tooltip!='' ){
			  shortcodedata +=' tooltip="true"';
		  }else{
			  shortcodedata +=' tooltip="false"';
		  }

			if(mode=='categorytab'){
				if(typeof(pd_category_orderby)!='undefined' || pd_category_orderby!=''){
					shortcodedata +=' category_orderby="'+pd_category_orderby+'"';
				}

				if(typeof(pd_category_order)!='undefined' || pd_category_order!=''){
					shortcodedata +=' category_order="'+pd_category_order+'"';
				}
			}
			
			if(mode=='maponly'){
				shortcodedata +=' showmaponly="yes"';
			}


		  if(typeof(list_title_font_size)!='undefined' || list_title_font_size!=''){
              shortcodedata +=' list_title_font_size="'+list_title_font_size+'"';
		  }else{
			  shortcodedata +=' list_title_font_size=""';
		  }

		  if(typeof(item_orderby)!='undefined' || item_orderby!=''){
              shortcodedata +=' item_orderby="'+item_orderby+'"';
		  }else{
			  shortcodedata +=' item_orderby=""';
		  }

		  if(typeof(list_title_line_height)!='undefined' || list_title_line_height!=''){
              shortcodedata +=' list_title_line_height="'+list_title_line_height+'"';
		  }else{
			  shortcodedata +=' list_title_line_height=""';
		  }

		  if(typeof(title_font_size)!='undefined' || title_font_size!=''){
              shortcodedata +=' title_font_size="'+title_font_size+'"';
		  }else{
			  shortcodedata +=' title_font_size=""';
		  }

        if(typeof(subtitle_font_size)!='undefined' || subtitle_font_size!=''){
            shortcodedata +=' subtitle_font_size="'+subtitle_font_size+'"';
        }else{
			shortcodedata +=' subtitle_font_size=""';
		}
        if(typeof(title_line_height)!='undefined' || title_line_height!=''){
            shortcodedata +=' title_line_height="'+title_line_height+'"';
        }else{
			shortcodedata +=' title_line_height=""';
		}
        if(typeof(subtitle_line_height)!='undefined' || subtitle_line_height!=''){
            shortcodedata +=' subtitle_line_height="'+subtitle_line_height+'"';
        }else{
			shortcodedata +=' subtitle_line_height=""';
		}
		if(typeof(pdmapofflightbox)!='undefined' && pdmapofflightbox!==''){
            shortcodedata +=' pdmapofflightbox="'+pdmapofflightbox+'"';
        }else{
			shortcodedata +=' pdmapofflightbox="false"';
		}

        if(typeof(filter_area)!='undefined' || filter_area!=''){
            shortcodedata +=' filter_area="'+filter_area+'"';
        }

        if(typeof(topspacing)!='undefined' || topspacing!=''){
            shortcodedata +=' topspacing="'+topspacing+'"';
        }

        //review parameter if review addon is installed
        if( jQuery('#qcpd_sbd_enable_review').length > 0 ){
            if( jQuery('.qcpd_sbd_enable_review').is(':checked') ){
            	shortcodedata += ' review="true" ';
            }else{
            	shortcodedata += ' review="false" ';
            }
        }
		  
		  shortcodedata += ']';
		/*
		  tinyMCE.activeEditor.selection.setContent(shortcodedata);
		  
		  $('#sm-modal').remove();
		 */
		$('.sbd_shortcode_generator_area').hide();
		$('.sbd_shortcode_container').show();
		$('#sbd_shortcode_container').val(shortcodedata);
		$('.sbd_copy_close').attr('short-data', shortcodedata);
		$('#sbd_shortcode_container').select();
		document.execCommand('copy');


    }).on( 'change', '#pd_mode',function(){
	
		var mode = $('#pd_mode').val();
		
		if( mode == 'one' ){
			$('#pd_list_div').css('display', 'block');
			$('#pd_list_cat').css('display', 'none');
			$('#pd_con_orderby').css('display', 'none');
			$('#pd_con_order').css('display', 'none');

            $('#pd_cat_orderby').css('display', 'none');
            $('#pd_cat_order').css('display', 'none');
			$('#pd_show_map').css('display', 'block');
            
			$('#pd_column_div').hide();
			$('#pd_itemperpage_div').show();
			$('#qcpd_item_order_by').show();
			$('#pd_filter_orderby').show();
			$('#pd_filter_order').show();
			$('#pd_mainclick').show();
			$('#pd_phone').show();
			$('#pd_embed').show();
			$('#qcpd_upvote').show();
			$('#qcpd_item_count').show();
			$('#qcpd_pagination').show();
			$('#qcpd_enable_pagination').show();
			$('#qcpd_item_perpage').show();
			$('#qcpd_tooltip').show();
			$('#qcpd_general_settings').show();
			$('#pd-leftfilter').show();
			$('#pd-rtl').show();
			$('#pd_map_off').show();
			
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			$('#pd-templates').show();
			$('.map_toggle_filter').hide();
		}
		else if( mode == 'category' ){
			$('#pd_list_cat').css('display', 'block');
			$('#pd_list_div').css('display', 'none');
            $('#pd_con_orderby').css('display', 'block');
            $('#pd_con_order').css('display', 'block');

            $('#pd_cat_orderby').css('display', 'none');
            $('#pd_cat_order').css('display', 'none');
			$('#pd_show_map').css('display', 'block');
            
			$('#pd_column_div').show();
			$('#pd_itemperpage_div').show();
			$('#qcpd_item_order_by').show();
			$('#pd_filter_orderby').show();
			$('#pd_filter_order').show();
			$('#pd_mainclick').show();
			$('#pd_phone').show();
			$('#pd_embed').show();
			$('#qcpd_upvote').show();
			$('#qcpd_item_count').show();
			$('#qcpd_pagination').show();
			$('#qcpd_enable_pagination').show();
			$('#qcpd_item_perpage').show();
			$('#qcpd_tooltip').show();
			$('#qcpd_general_settings').show();
			$('#pd-leftfilter').show();
			$('#pd-rtl').show();
			$('#pd_map_off').show();
			$('.map_toggle_filter').hide();
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			$('#pd-templates').show();

		}else if(mode=='categorytab'){
            $('#pd_cat_orderby').css('display', 'block');
            $('#pd_cat_order').css('display', 'block');
            $('#pd_list_div').css('display', 'none');
            $('#pd_list_cat').css('display', 'none');
            $('#pd_con_orderby').css('display', 'block');
            $('#pd_con_order').css('display', 'block');
            $('#pd_show_map').css('display', 'block');
			$('#pd_itemperpage_div').show();
			$('#qcpd_item_order_by').show();
			$('#pd_filter_orderby').show();
			$('#pd_filter_order').show();
			$('#pd_mainclick').show();
			$('#pd_phone').show();
			$('#pd_embed').show();
			$('#qcpd_upvote').show();
			$('#qcpd_item_count').show();
			$('#qcpd_pagination').show();
			$('#qcpd_enable_pagination').show();
			$('#qcpd_item_perpage').show();
			$('#qcpd_tooltip').show();
			$('#qcpd_general_settings').show();
			$('#pd-leftfilter').show();
			$('#pd-rtl').show();
			$('#pd_map_off').show();
			
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			$('#pd-templates').show();
            $('.map_toggle_filter').hide();
			$('#pd_column_div').show();
		}else if(mode=='maponly'){
			$('#pd_list_cat').show();
			$('#pd_column_div').hide();
			$('#pd_itemperpage_div').hide();
			$('#qcpd_item_order_by').hide();
			$('#pd_con_orderby').hide();
			$('#pd_con_order').hide();
			$('#pd_filter_orderby').hide();
			$('#pd_filter_order').hide();
			$('#pd_mainclick').hide();
			$('#pd_phone').hide();
			$('#pd_embed').hide();
			
			$('#qcpd_upvote').hide();
			$('#qcpd_item_count').hide();
			$('#qcpd_pagination').hide();
			$('#qcpd_enable_pagination').hide();
			$('#qcpd_item_perpage').hide();
			$('#qcpd_tooltip').hide();
			$('#qcpd_general_settings').hide();
			$('#pd-leftfilter').hide();
			$('#pd-rtl').hide();
			$('#pd_map_off').hide();
			$('#pd_map_position_div').hide();
			$('#pd_show_map').hide();
			$('.map_full_width').show();
			$('.map_full_screen').show();
			$('#pd-templates').hide();
			$('.map_toggle_filter').show();
		}
		else{
			$('.map_toggle_filter').hide();
			$('#pd_list_div').css('display', 'none');
			$('#pd_list_cat').css('display', 'none');
            $('#pd_con_orderby').css('display', 'block');
            $('#pd_con_order').css('display', 'block');
            $('#pd_cat_orderby').css('display', 'none');
            $('#pd_cat_order').css('display', 'none');
			$('#pd_show_map').css('display', 'block');
			$('#pd_column_div').show();
			$('#pd_itemperpage_div').show();
			$('#qcpd_item_order_by').show();
			$('#pd_filter_orderby').show();
			$('#pd_filter_order').show();
			$('#pd_mainclick').show();
			$('#pd_phone').show();
			$('#pd_embed').show();
			$('#qcpd_upvote').show();
			$('#qcpd_item_count').show();
			$('#qcpd_pagination').show();
			$('#qcpd_enable_pagination').show();
			$('#qcpd_item_perpage').show();
			$('#qcpd_tooltip').show();
			$('#qcpd_general_settings').show();
			$('#pd-leftfilter').show();
			$('#pd-rtl').show();
			$('#pd_map_off').show();
			
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			$('#pd-templates').show();
		}
		
	}).on( 'change', '#pd_style',function(){
	
		var style = $('#pd_style').val();

		if( style == '' ){
			alert("Please select a valid template style.");
			return;
		}

		
		if( style != 'style-10' ){
			$('.pd-off-field').css('display', 'block');
			$('.sbd-leftfilter').css('display', 'block');
		}
		else
		{
			$('.pd-off-field').css('display', 'none');
			$('.sbd-leftfilter').css('display', 'none');
			
		}

		if( style == 'simple' ){
			$('#pd_infinity_scroll').show();
			$('#pd_item_per_page').show();
			$('.tt-template').css('display', 'block');
			
			
			
		}
		else
		{
			$('#pd_infinity_scroll').hide();
			$('#pd_item_per_page').hide();
			$('.tt-template').css('display', 'none');
			
			
		}
		
if( style == 'simple' || style == 'style-1' || style == 'style-2' || style == 'style-4' || style == 'style-5' || style == 'style-6' || style == 'style-7' || style == 'style-8' || style == 'style-9' ){
			
			$('#pd_column_div').css('display', 'block');
		}
		else{
			
    		$('#pd_column_div').css('display', 'none');
		}
		
		/*if( style == 'simple' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/" target="_blank">View Demo for Default Style</a>');
		}
		else if( style == 'style-1' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-1/" target="_blank">View Demo for Style-1</a>');
		}
		else if( style == 'style-2' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-2/" target="_blank">View Demo for Style-2</a>');
		}
		else if( style == 'style-3' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-3/" target="_blank">View Demo for Style-3</a>');
		}
		else if( style == 'style-4' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-4/" target="_blank">View Demo for Style-4</a>');
		}
		else if( style == 'style-5' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-5/" target="_blank">View Demo for Style-5</a>');
		}
		else if( style == 'style-6' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-6/" target="_blank">View Demo for Style-6</a>');
		}
		else if( style == 'style-7' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-7/" target="_blank">View Demo for Style-7</a>');
		}
		else if( style == 'style-8' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-8/" target="_blank">View Demo for Style-8</a>');
		}
		else if( style == 'style-9' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-9/" target="_blank">View Demo for Style-9</a>');
		}
		else if( style == 'style-10' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-10/" target="_blank">View Demo for Style-10</a>');
		}
		else if( style == 'style-11' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-11/" target="_blank">View Demo for Style-11</a>');
		}
		else if( style == 'style-12' ){
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/style-12/" target="_blank">View Demo for Style-12</a>');
		}
		else{
		   $('#demo-preview-link #demo-url').html('<a href="http://dev.quantumcloud.com/pd/" target="_blank">View Demo for Default Style</a>');
		}*/	
		
		if($('#pd_mode').val()=='one'){
			
			if(style == 'style-3' || style == 'style-4' || style == 'style-7' || style == 'style-8' || style == 'style-10'){
				
				$('#pd_column_div').show();
				
			}else{
				
				$('#pd_column_div').hide();
				
			}
			
		}
		
		
	}).on('click', '#pdmaponly' , function(){
		onlymappd = $('#pdmaponly:checked').val();
		
		if(typeof(onlymappd)!='undefined'){
			$('#pd_column_div').hide();
			$('#pd_itemperpage_div').hide();
			$('#qcpd_item_order_by').hide();
			$('#pd_con_orderby').hide();
			$('#pd_con_order').hide();
			$('#pd_filter_orderby').hide();
			$('#pd_filter_order').hide();
			$('#pd_mainclick').hide();
			$('#pd_phone').hide();
			$('#pd_embed').hide();
			
			$('#qcpd_upvote').hide();
			$('#qcpd_item_count').hide();
			$('#qcpd_pagination').hide();
			$('#qcpd_enable_pagination').hide();
			$('#qcpd_item_perpage').hide();
			$('#qcpd_tooltip').hide();
			$('#qcpd_general_settings').hide();
			$('#pd-leftfilter').hide();
			$('#pd-rtl').hide();
			$('#pd_map_off').hide();
			$('#pd_map_position_div').hide();
		}else{
			
			$('#pd_column_div').show();
			$('#pd_itemperpage_div').show();
			$('#qcpd_item_order_by').show();
			$('#pd_con_orderby').show();
			$('#pd_con_order').show();
			$('#pd_filter_orderby').show();
			$('#pd_filter_order').show();
			$('#pd_mainclick').show();
			$('#pd_phone').show();
			$('#pd_embed').show();
			$('#qcpd_upvote').show();
			$('#qcpd_item_count').show();
			$('#qcpd_pagination').show();
			$('#qcpd_enable_pagination').show();
			$('#qcpd_item_perpage').show();
			$('#qcpd_tooltip').show();
			$('#qcpd_general_settings').show();
			$('#pd-leftfilter').show();
			$('#pd-rtl').show();
			$('#pd_map_off').show();
			$('#pd_map_position_div').show();
		}
	});
	
	$(document).on('click','#pdmap', function(){
		if($('#pdmap').is(":checked")){
			$('.pd_map_position').show();
			if($('#pd_map_position').val()=='top'){
				$('.map_full_width').show();
				$('.map_full_screen').show();
				
			}
		}else{
			$('.pd_map_position').hide();
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			
		}
	})
	
	$(document).on('change', '#pd_map_position', function(){
		if($(this).val()=='right'){
			$('.map_full_width').hide();
			$('.map_full_screen').hide();
			
			$('.sbd-leftfilter').hide();
		}else{
			$('.map_full_width').show();
			$('.map_full_screen').show();
			
			if($('#pd_style').val()!='style-10'){
				$('.sbd-leftfilter').show();
			}
			
		}
	})
	

	// service image 
	$(document).on('click','.sbd_item_image', function(e){
		
        e.preventDefault();
        $container = $(this);

            var image = wp.media({
                title: 'Upload Item Image',
                // mutiple: true if you want to upload multiple files at once
                multiple: false,
                button: {text: 'Insert'}
            }).open()
            .on('select', function (e) {
				
                var uploaded_image = image.state().get('selection').toJSON();
				
				$('#item_image').val(uploaded_image[0].id);
				$('.sbd_item_image_preview img').attr('src', uploaded_image[0].url);
				$container.siblings('.sbd_item_image_preview').find('.sbd_large_list_item_remove_image').show();

            });

    });
	
	$(document).on('click','.sbd_item_marker_image', function(e){
		
        e.preventDefault();
        $container = $(this);

            var image = wp.media({
                title: 'Upload Marker Image',
                // mutiple: true if you want to upload multiple files at once
                multiple: false,
                button: {text: 'Insert'}
            }).open()
            .on('select', function (e) {
				
                var uploaded_image = image.state().get('selection').toJSON();
				
				$('#item_marker_image').val(uploaded_image[0].id);
				$('.sbd_item_marker_image_preview img').attr('src', uploaded_image[0].url);
				$container.siblings('.sbd_item_marker_image_preview').find('.sbd_large_list_item_remove_image').show();

            });

    });	

    jQuery(document).on( 'click', '.sbd_large_list_item_remove_image', function(){
		if( jQuery(this).parent('.sbd_item_image_preview').length > 0 ){
			jQuery(this).parent('.sbd_item_image_preview').find('img').attr('src','');
			jQuery(this).parent('.sbd_item_image_preview').prev('#item_image').val('');
		}
		if( jQuery(this).parent('.sbd_item_marker_image_preview').length > 0 ){
			jQuery(this).parent('.sbd_item_marker_image_preview').find('img').attr('src','');
			jQuery(this).parent('.sbd_item_marker_image_preview').prev('#item_marker_image').val('');
		}
		jQuery(this).hide();
	});
	
	$(document).on('click', "#sbd_generate", function(e){
		var objc = $(this);
		
			
			if($('#sbd_item_link').val()!=''){
				html = "<div id='sbd_ajax_preloader'><div class='sbd_ajax_loader'></div></div>";
				$('body').append(html);
				$.post(ajaxurl, {
				action: 'qcpd_generate_text', 
				url: $('#sbd_item_link').val(),
				},
				function(data) {
					
					$('#sbd_ajax_preloader').remove();
					data = JSON.parse(data);
					$('#sbd_item_title').val(data.title)
					$('#sbd_item_subtitle').val(data.description)
					
				});
				
			}else{
				alert('Item link field cannot left empty!');
			}

		
	})
	
	$('#sbd_item_full_address').on('focusout', function(e){
		var objc = $(this);
		var geocoder = new google.maps.Geocoder();
		var address = objc.val();

		geocoder.geocode( { 'address': address}, function(results, status) {

		if (status == google.maps.GeocoderStatus.OK) {
			var latitude = results[0].geometry.location.lat();
			var longitude = results[0].geometry.location.lng();
			
			$('#sbd_item_latitude').val(latitude);
			$('#sbd_item_longitude').val(longitude);
			
			
		}else{
			console.log('Unable to retrive lat, lng from this address');
		}
		
		}); 
		
	})
	
	$('#sbd_item_full_address').on('focus', function(e){
		
		var objc = $(this);
		var input = document.getElementById(objc.attr('id'));
		var autocomplete = new google.maps.places.Autocomplete(input);
		var geocoder = new google.maps.Geocoder;
		autocomplete.addListener('place_changed', function(e) {
			e.preventDefault();
			var place = autocomplete.getPlace();

			if (!place.place_id) {
			return;
			}

			geocoder.geocode({'placeId': place.place_id}, function(results, status) {

				if (status !== 'OK') {
				  alert('Geocoder failed due to: ' + status);
				  return;
				}
				//console.log(results[0].geometry.location.lat());
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				
				$('#sbd_item_latitude').val(latitude);
				$('#sbd_item_longitude').val(longitude);

			})
			
		})
		
	})
	
});


//Validation for Latitude and Longitude on admin edit post
jQuery(document).ready(function(){
	jQuery("body").on('keyup focusout keypress', "input[name*='[qcpd_item_latitude]']", function(e){
		var val = jQuery(this).val();
		var error = 0;
		var error_code = '';

		if( val.trim() ){
			if( !jQuery(this).parent('.field-item').next().hasClass('sbd_error_field') ){
				jQuery(this).parent('.field-item').after('<p class="sbd_error_field"></p>');
			}
			if( isNaN(val) ){
				error = 1;
				error_code = 'NaN';
			}
			if( error == 1 && error_code == 'NaN' ){

			}else{
				if( val > -90 && val < 90 ){
					//console.log('latitude is valid');
				}else{
					error = 1;
					error_code = 'invalid_lat';
				}
			}
			if( error == 1 ){
				console.log(error_code);
				jQuery(this).parent('.field-item').next('.sbd_error_field').html('Please enter a valid Latitude');
				jQuery(this).parents('form#post').attr('onsubmit', 'return false');
				jQuery(this).parents('form#post').find('#publish').attr('disabled', true);
			}
		}
		if( error == 0 ){
			jQuery(this).parent('.field-item').next('.sbd_error_field').remove();
			if( !jQuery("input[name*='[qcpd_item_longitude]']").parent('.field-item').next().hasClass('sbd_error_field') ){			
				jQuery(this).parents('form#post').removeAttr('onsubmit');
				jQuery(this).parents('form#post').find('#publish').removeAttr('disabled').removeClass('disabled');
			}
		}
	});

	jQuery("body").on('keyup focusout keypress', "input[name*='[qcpd_item_longitude]']", function(e){
		var val = jQuery(this).val();
		var error = 0;
		var error_code = '';

		if( val.trim() ){
			if( !jQuery(this).parent('.field-item').next().hasClass('sbd_error_field') ){
				jQuery(this).parent('.field-item').after('<p class="sbd_error_field"></p>');
			}
			if( isNaN(val) ){
				error = 1;
				error_code = 'NaN';
			}
			if( error == 1 && error_code == 'NaN' ){

			}else{
				if( val > -180 && val < 180 ){
					//console.log('longitude is valid');
				}else{
					error = 1;
					error_code = 'invalid_long';
				}
			}
			if( error == 1 ){
				console.log(error_code);
				jQuery(this).parent('.field-item').next('.sbd_error_field').html('Please enter a valid Longitude');
				jQuery(this).parents('form#post').attr('onsubmit', 'return false');
				jQuery(this).parents('form#post').find('#publish').attr('disabled', true);
			}
		}
		if( error == 0){
			jQuery(this).parent('.field-item').next('.sbd_error_field').remove();
			if( !jQuery("input[name*='[qcpd_item_latitude]']").parent('.field-item').next().hasClass('sbd_error_field') ){
				jQuery(this).parents('form#post').removeAttr('onsubmit');
				jQuery(this).parents('form#post').find('#publish').removeAttr('disabled').removeClass('disabled');
			}
		}
	});


});