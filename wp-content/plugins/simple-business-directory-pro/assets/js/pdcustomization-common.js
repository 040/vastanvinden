jQuery(document).ready(function($){
	
	    $('#pd-upload-btn').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            multiple: false
        }).open()
        .on('select', function(e){
            
            var uploaded_image = image.state().get('selection').first();
            var image_url = uploaded_image.toJSON().url;
            $('#pd_pf_image_url').val(image_url);
			var html = ['<span class="pd_remove_bg_image">X</span>',
				'<img src="'+image_url+'" alt="" />'
			].join("");
			$('#pd_preview_img').html(html);
        });
    });
	
	$(document).on( 'click', '.pd_remove_bg_image', function(){
		
		$('#pd_preview_img').html('');
		$('#pd_pf_image_url').val('');
		if($('#sbd_prev_image').length>0){
			$('#sbd_prev_image').val('');
		}
	})
	
	
	
	$('#qc_pd_category').on('change', function(){
		var city = $('#qc_pd_category').val();
		
			$.post(
				ajaxurl,
				{
					action : 'qcld_pd_category_filter',
					cat : city,
				},
				function(data){
					
					$('#qc_pd_list').html(data);
					
				}
			);
		
	})
	
})

function initMap() {

}
