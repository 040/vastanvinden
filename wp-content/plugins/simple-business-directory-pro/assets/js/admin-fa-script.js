jQuery(document).ready(function($) {
	
	$(document).on('click','#qcpd_fa_icon > .field-item > input', function(e){
		
	    e.preventDefault();

		$('#fa-field-modal').show();
		$("#fa-field-modal").attr("data", this.id);

	});
	$(document).on('click','#sbd_item_fa', function(e){
		
	    e.preventDefault();

		$('#fa-field-modal').show();
		$("#fa-field-modal").attr("data", this.id);

	});
	$(document).on('click','#qcpd_other_list > .field-item > input', function(e){
		
	    e.preventDefault();

		$('#fa-field-modal1').show();
		$("#fa-field-modal1").attr("data", this.id);

	});
	
	$(document).on('click','#sld_list_select', function(e){
		e.preventDefault();
		var arr = [];
		$('.sld_list_Checkbox:checked').each(function () {
		   arr.push($(this).val());
		   $(this).prop('checked', false);
		});
		
		$getid = $("#fa-field-modal1").attr('data');
		$('#'+$getid).val(arr.join(","));
		$('#fa-field-modal1').removeAttr("data");
		$('#fa-field-modal1').hide();
	})
	
	
	$(document).on('click','.sbd_show_fa', function(e){
		
	    e.preventDefault();

		$('#fa-field-modal').show();
		$("#fa-field-modal").attr("data", this.id);

	});

	$( '.fa-field-modal-close' ).on( 'click', function() {
		$('#fa-field-modal').removeAttr("data");
		$('#fa-field-modal1').removeAttr("data");
		$('#fa-field-modal').hide();
		$('#fa-field-modal1').hide();
		$('#fa-field-modal-tag').remove();

	});

	$( '.fa-field-modal-icon-holder' ).on( 'click', function() {

		var icon = $(this).data('icon');
		$getid = $("#fa-field-modal").attr('data');
		$('#'+$getid).val(icon);
		$('#fa-field-modal').removeAttr("data");
		$('#fa-field-modal').hide();
	});



});

function showfamodal(data){
	
	document.getElementById('fa-field-modal').style.display = 'block';
	document.getElementById('fa-field-modal').setAttribute("data", data.id);
	//jQuery.('#fa-field-modal').show();
}