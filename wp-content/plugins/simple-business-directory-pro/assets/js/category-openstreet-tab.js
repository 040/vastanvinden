	jQuery(document).ready(function($){
	var sbd_enable_map_addr_phone_clickable = sld_variables.pd_enable_map_addr_phone_clickable;
	var sbd_map_fitbounds = sld_variables.sbd_map_fitbounds;

		var geocoder;
		var map;
		var circle = null;
		var markers = [];
		var iw;

		//var mapCreate = null;
		//var markerClusters;
	
	jQuery('.qcld_pd_tablinks').on('click', function(evt){
		var qcld_pd_event = jQuery(this).data('contentid');
		var i, qcld_pd_tabcontent, qcld_pd_tablinks;
		qcld_pd_tabcontent = document.getElementsByClassName("qcld_pd_tabcontent");
		for (i = 0; i < qcld_pd_tabcontent.length; i++) {
			qcld_pd_tabcontent[i].style.display = "none";
		}
		qcld_pd_tablinks = document.getElementsByClassName("qcld_pd_tablinks");
		for (i = 0; i < qcld_pd_tablinks.length; i++) {
			qcld_pd_tablinks[i].className = qcld_pd_tablinks[i].className.replace(" qcld_pd_active", "");
		}
		document.getElementById(qcld_pd_event).style.display = "block";
		evt.currentTarget.className += " qcld_pd_active";
		
		jQuery('#'+qcld_pd_event +' .qcpd-single-list-pd').each(function(e){
			
			if(jQuery(this).find('.pdp-holder').length > 0 && jQuery(this).find('.pdp-holder > .jp-current').length==0){

				var containerId = jQuery(this).find('.pdp-holder').attr('id');
				var containerList = jQuery(this).find('ul').attr('id');
				console.log(containerList);
				jQuery("#"+jQuery(this).find('.pdp-holder').attr('id')).jPages({
					containerID : containerList,
					perPage : per_page,
				});
				
			}
			
		})
		
		
		
		jQuery('.sbd_main_wrapper:visible .qc-grid').packery({
			itemSelector: '.qc-grid-item',
			gutter: 10
		});
		jQuery( '.sbd_main_wrapper:visible').siblings('.sbd-filter-area').find('.filter-btn[data-filter="all"]' ).trigger( "click" );
		
		var newmapid = jQuery('#'+qcld_pd_event+' .sbd_map').attr('id');
		
		if(typeof(newmapid)!=="undefined" || newmapid!==''){
			initializetab(newmapid);
		}
		
		
	});

	var mapid = "sbd_all_location0";
	if(jQuery('#'+mapid).length>0){
		initializetab(mapid);
	}
	
	function initializetab(mapid) {

		var geocoder;
		var map;
		var circle;
		var circle = null;
		var markers = [];
		var iw;

		var mapCreate = null;
		var markerClusters;

		var lat = parseInt(sld_variables.latitute);
		var lon = parseInt(sld_variables.longitute);

		var container = L.DomUtil.get(mapid); 
		if(container != null){ container._leaflet_id = null; }

	  	mapCreate = L.map(mapid).setView([sld_variables.latitute, sld_variables.longitute], parseInt(sld_variables.zoom));
	  	markerClusters = L.markerClusterGroup(); 

		var minZoomVal = 2; 

		if(cluster.map_fullwidth == 'true'){
			var minZoomVal = 3; 
		}

		if(cluster.map_position == 'right'){
			var minZoomVal = 2; 
		}

	  	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			minZoom: minZoomVal,
			maxZoom: 50,
			attribution: '',
			key: 'BC9A493B41014CAABB98F0471D759707'
		}).addTo(mapCreate);

		myLooptab(mapCreate,map, markerClusters);
		
		setTimeout(function () {
		   mapCreate.invalidateSize(true);
		}, 1000);

	}

	var i = 0;                     

	function myLooptab(mapCreate,map, markerClusters) {
		//if( typeof google !== 'undefined' ){
			//markers = [];
			jQuery(".qcld_pd_tabcontent:visible .qc-grid-item ul li:visible").each(function(){
				var obj = jQuery(this);
				
				var icons = obj.find('.pd-bottom-area').clone();
				
				icons.find('.pd-map').removeClass('open-mpf-sld-link');
			
				if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
					icons.find('.pd-map').attr("href", "https://www.google.com/maps/dir/?api=1&origin=none&destination="+encodeURIComponent(obj.attr('data-address'))+"&travelmode=driving");
					icons.find('.pd-map').attr("target", "_blank");
				}
			icons.find('.open-mpf-sld-link').remove();
			icons.find('.pd-map').remove();
			icons = icons.html();
			
			var custom_content = obj.find('.sbd_custom_content').clone();
			custom_content = custom_content.html();
			//others information
			var others_info = '';
			if(obj.attr('data-local')!=''){
				others_info +="<p><b>Local: </b>"+obj.attr('data-local')+"</p>";
			}
			if(obj.attr('data-phone')!=''){
				others_info +="<p><b>Phone: </b>"+obj.attr('data-phone')+"</p>";
			}
			if(obj.attr('data-businesshour')!=''){
				others_info +="<p><b>Business Hours: </b>"+obj.attr('data-businesshour')+"</p>";
			}
				
				var imgurl = '';
				if(obj.find('img').length>0){
					imgurl = obj.find('img').attr('src');
				}
				var target = '';
			
				if(typeof(obj.find('a').attr('target'))!=='undefined'){
					target = 'target="_blank"';
				}
				if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
					i++;
					
					var locations = obj.attr('data-latlon');
					var latlng = locations.split(',');
					var title = obj.attr('data-title');
					var address = obj.attr('data-address');
					var url = obj.attr('data-url');
					var subtitle = obj.attr('data-subtitle');
					
					var markericon = '';
					if(sld_variables.global_marker!=''){
						markericon = sld_variables.global_marker;
					}
					if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
						markericon = sld_variables.paid_marker_default; //default paid marker
						if(sld_variables.paid_marker!=''){
							markericon = sld_variables.paid_marker; // If paid marker is set then override the default
						}
					}

					if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
						markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
					}
					var map_marker_id = jQuery(this).attr('id');
				var icon = markericon;

				maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

				mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

				if( icon != '' ){

					var myIcon = L.icon({
					  iconUrl: icon,
					  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
					  iconAnchor: [25, 50],
					  popupAnchor: [-10, -50],
					});

					var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

				}else{

					var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
				}

				if( sbd_map_fitbounds == 'on' ){
 
					marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
					
				}else{

					marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
				}
				
				markerClusters.clearLayers();
				markers.push(marker);

				marker.on({
				    mouseover: function(e){
				        for(var i in markers){
				            var markerID = markers[i].options.title;
				            if (markerID == e.target.options.title ){
				                markers[i].openPopup();
				        		
				            }else{
				                markers[i].closePopup();
				            };
				        }
				    },
				});
					
					
				}

			});

			// var group = new L.featureGroup(markers);
			// mapCreate.fitBounds(group.getBounds().pad(1));

			if( sbd_map_fitbounds == 'on' ){

				var group = new L.featureGroup(markers);
				mapCreate.fitBounds(group.getBounds());

			}else{

				var southWest = new L.LatLng(markers[0]._latlng.lat, markers[0]._latlng.lng),
		    	northEast = new L.LatLng(markers[1]._latlng.lat, markers[1]._latlng.lng),
		    	bounds = new L.LatLngBounds(southWest, northEast);
				mapCreate.fitBounds(bounds, {padding: [50, 50]});

				mapCreate.setZoom(parseInt(sld_variables.zoom));
			}

			// localStorage.setItem("mapCreate",  mapCreate );
			// localStorage.setItem("markers",  markers );

	}

	function SBDselectOpenStreetMarker(id, status){

        for( var i in markers ){
            var markerID = markers[i].options.title;
            if (markerID == id && status == "start"){
                markers[i].openPopup();
        		
            }else{
                markers[i].closePopup();
            };
        }
    }
    

    $(document).on('mouseover mousemove','.qc-grid-item ul li a:first-of-type',function(){
		if( sld_variables.pd_enable_map_autopan == 'on' ){
			var selectorID = jQuery(this).closest('li').attr('id');
			SBDselectOpenStreetMarker(selectorID, 'start');
		}
	});

    function SBDOpenStreetRemoveAllMarkers(){

	    for( var i = 0; i < markers.length; i++){
		    mapCreate.removeLayer(markers[i]);
		}

	}

//jQuery('.sbd_radius_find').on('click',function(){
$(document).on('click','.qcld_pd_tab_main .sbd_radius_find',function(){
	//if( typeof google !== 'undefined' ){
		if( !jQuery('.qcld_pd_tab_main').find('.qcld_pd_tabcontent') ){
			return false;
		}	
		var address = jQuery('.qcld_pd_tabcontent:visible .sbd_location_name').val();
		var data_lat = jQuery('.qcld_pd_tabcontent:visible .sbd_location_name').attr('data-lat');
		var data_lang = jQuery('.qcld_pd_tabcontent:visible .sbd_location_name').attr('data-lon');
		var radiusi = jQuery('.qcld_pd_tabcontent:visible .sbd_distance').val();
		
		if(address==''){
			alert(sld_variables.distance_location_text);
			return;
		}
		
		if(radiusi==''){
			alert(sld_variables.distance_value_text);
			return;
		}
		
		if(jQuery('.qcld_pd_tabcontent:visible .sdb_distance_cal').val()=='miles'){
			radiusi = radiusi*1.60934;
		}
		

		var radius = parseInt(radiusi, 10)*1000;
		var counter = 0;

		var qcld_pd_event = jQuery('.qcld_pd_tablinks.qcld_pd_active').data('contentid');
		var mapid = jQuery('#'+qcld_pd_event+' .sbd_map').attr('id');
				
		if(typeof(mapid)!=="undefined" || mapid!==''){
			//initializetab(mapid);
			var container = L.DomUtil.get(mapid); 
			if(container != null){ container._leaflet_id = null; }

		  	mapCreate = L.map(mapid).setView([sld_variables.latitute, sld_variables.longitute], parseInt(sld_variables.zoom));
		  	markerClusters = L.markerClusterGroup(); 

			var minZoomVal = 2; 

			if(cluster.map_fullwidth == 'true'){
				var minZoomVal = 3; 
			}

			if(cluster.map_position == 'right'){
				var minZoomVal = 2; 
			}

		  	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				minZoom: minZoomVal,
				maxZoom: 50,
				attribution: '',
				key: 'BC9A493B41014CAABB98F0471D759707'
			}).addTo(mapCreate);


		}		

		SBDOpenStreetRemoveAllMarkers();
	
		if (data_lat !==null && data_lang !==null  ){
			  
			if (mapCreate.hasLayer(circle)) mapCreate.removeLayer(circle);

			if (circle != undefined) {
		      	mapCreate.removeLayer(circle);
		    }

			circle = L.circle([ data_lat, data_lang ], radius, {
	            color: sld_variables.radius_circle_color,
	            fillColor: sld_variables.radius_circle_color,
	            fillOpacity: 0.35
	        }).addTo(mapCreate);

			
			var list_selector = ".qc-grid-item ul li";
			if(sld_variables.sbd_combine_distant_tag_live_search == "on"){
				if( jQuery('.pdp-holder').is(':visible') ){
					list_selector = ".qc-grid-item ul li";
				}else{
					list_selector = ".qc-grid-item ul li:visible";
				}
			}
			jQuery(list_selector).each(function(){
				
				var obj = jQuery(this);
				
				var icons = obj.find('.pd-bottom-area').clone();
				icons.find('.pd-map').removeClass('open-mpf-sld-link');
			
				if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
					icons.find('.pd-map').attr("href", "https://nominatim.openstreetmap.org/search?format=json&limit=1&q="+encodeURIComponent(obj.attr('data-address'))+"");
					icons.find('.pd-map').attr("target", "_blank");
				}
				icons.find('.open-mpf-sld-link').remove();
				icons.find('.pd-map').remove();
				icons = icons.html();
				
				var custom_content = obj.find('.sbd_custom_content').clone();
				custom_content = custom_content.html();
				//others information
				var others_info = '';
				if(obj.attr('data-local')!=''){
					others_info +="<p><b><i class='fa fa-location-arrow' aria-hidden='true'></i> </b>"+obj.attr('data-local')+"</p>";
				}
				if(obj.attr('data-phone')!=''){
					if( sbd_enable_map_addr_phone_clickable == 'on' ){
						others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b><a href='tel:"+obj.attr('data-phone')+"'>"+obj.attr('data-phone')+"</a></p>";
					}else{
						others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b>"+obj.attr('data-phone')+"</p>";
					}
				}
				if(obj.attr('data-businesshour')!=''){
					others_info +="<p><b><i class='fa fa-clock-o fa-clock' aria-hidden='true'></i> </b>"+obj.attr('data-businesshour')+"</p>";
				}
				
				
				var imgurl = '';
				if(obj.find('img').length>0){
					imgurl = obj.find('img').attr('src');
				}
				var target = '';
			
				if(typeof(obj.find('a').attr('target'))!=='undefined'){
					target = 'target="_blank"';
				}
				if(typeof(obj.find('a').attr('data-newtab'))!=='undefined'){
							target = 'target="_blank"';
						}
				if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
					i++;
					
					var locations = obj.attr('data-latlon');
					var latlng = locations.split(',');
					var title = obj.attr('data-title');
					var address = obj.attr('data-address');
					var url;
					if(typeof(obj.find('a').attr('href'))!=='undefined' && obj.find('a').is('a:not([href^="#"])') && obj.find('a').is('a:not([href^="tel"])')){
						url = obj.find('a').attr('href');
					}else{
						url = obj.attr('data-url');
					}
					var subtitle = obj.attr('data-subtitle');
					var markericon = '';
					if(sld_variables.global_marker!=''){
						markericon = sld_variables.global_marker;
					}
					if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
						markericon = sld_variables.paid_marker_default; //default paid marker
						if(sld_variables.paid_marker!=''){
							markericon = sld_variables.paid_marker; // If paid marker is set then override the default
						}
					}

					if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
						markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
					}

    				let circleCenter = circle.getLatLng();

					var map_marker_id = jQuery(this).attr('id');
					var icon = markericon;

					maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

					//mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

					mapCreate.setView([ data_lat, data_lang], mapCreate.getZoom());

					if( icon != '' ){

						var myIcon = L.icon({
						  iconUrl: icon,
						  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
						  iconAnchor: [25, 50],
						  popupAnchor: [-10, -50],
						});

						var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

					}else{

						var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
					}
					
					let markerPos = marker.getLatLng();

					if ( circleCenter.distanceTo(markerPos) < circle.getRadius() ) {

						if( sbd_map_fitbounds == 'on' ){
		 
							marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
							
						}else{

							marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();

						}

						markerClusters.clearLayers();
						markers.push(marker);

						var isClicked = false;

						marker.on({
						    mouseover: function(e) {

						        for (var i in markers){
						            var markerID = markers[i].options.title;
						            if (markerID == e.target.options.title ){
						                markers[i].openPopup();
						        		
						            }else{
						                markers[i].closePopup();
						            };
						        }


						    },
						    
						});

						//var group = new L.featureGroup(markers);
						//mapCreate.fitBounds(group.getBounds().pad(0.5));

						if( sbd_map_fitbounds == 'on' ){

							var group = new L.featureGroup(markers);
							mapCreate.fitBounds(group.getBounds());

						}else{

							var southWest = new L.LatLng(markers[0]._latlng.lat, markers[0]._latlng.lng),
					    	northEast = new L.LatLng(markers[1]._latlng.lat, markers[1]._latlng.lng),
					    	bounds = new L.LatLngBounds(southWest, northEast);
							mapCreate.fitBounds(bounds, {padding: [50, 50]});

							mapCreate.setZoom(parseInt(sld_variables.zoom));
						}



						mapCreate.setView(circleCenter);
						
						obj.removeClass("showMe");
						obj.show();
						obj.addClass("showMe");
						
					}else{
						obj.fadeOut();
						obj.hide();
						obj.removeClass("showMe");	
					}
					

					
				}else{
					obj.fadeOut();
					obj.hide();
					obj.removeClass("showMe");	
				}
				
			});
			
			if(cluster.pagination=='true'){
				jQuery('.pdp-holder').hide();
			}
			
			setTimeout(function(){
				var visibleItems = 0;
				var totalvisibleitem = 0;
				jQuery(".qcpd-single-list-pd, .qcpd-single-list-pd-1, .opd-list-pd-style-8, .opd-list-pd-style-9, .pd-container, .qc-sbd-single-item-11, .qc-sld-masonary-15").each(function(){
					
					visibleItems = jQuery(this).find("li.showMe").length;
					totalvisibleitem += jQuery(this).find("li.showMe").length;
					
					//console.log(visibleItems);
					
					if(visibleItems==0){
						jQuery(this).hide();
						
					}else{
						jQuery(this).show();
					}
				});
				

				setTimeout(function(){
					jQuery('.qc-grid').packery({
					  itemSelector: '.qc-grid-item',
					  gutter: 10
					});
					setTimeout(function(){
						if(totalvisibleitem==0){
							// alert(sld_variables.distance_no_result_text);
							jQuery('#sbdopd-list-holder').append('<h2 id="distance_no_result_found">'+sld_variables.distance_no_result_text+'</h2>');
						}
					},500)
					
				},1000)
			},1000)
			
			
		  } else {
			console.log('Geocode was not successful for the following reason: ' + status);
			counter++;
			
		  }

		// });
		

	//}
	return;
})

jQuery('.qcld_pd_tab_main .sbd_radius_clear').on('click', function(){
	
		if( !jQuery('.qcld_pd_tab_main').find('.qcld_pd_tabcontent') ){
			return false;
		}	
		var obj = jQuery(this);
		jQuery('.qcld_pd_tabcontent:visible .sbd_location_name').val('');
		jQuery('.qcld_pd_tabcontent:visible .sbd_distance').val('');

		var qcld_pd_event = jQuery('.qcld_pd_tablinks.qcld_pd_active').data('contentid');
		var mapid = jQuery('#'+qcld_pd_event+' .sbd_map').attr('id');
				
		if(typeof(mapid)!=="undefined" || mapid!==''){

			var container = L.DomUtil.get(mapid); 
			if(container != null){ container._leaflet_id = null; }

		  	mapCreate = L.map(mapid).setView([sld_variables.latitute, sld_variables.longitute], parseInt(sld_variables.zoom));
		  	markerClusters = L.markerClusterGroup(); 

			var minZoomVal = 2; 

			if(cluster.map_fullwidth == 'true'){
				var minZoomVal = 3; 
			}

			if(cluster.map_position == 'right'){
				var minZoomVal = 2; 
			}

		  	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				minZoom: minZoomVal,
				maxZoom: 50,
				attribution: '',
				key: 'BC9A493B41014CAABB98F0471D759707'
			}).addTo(mapCreate);


		}		

		if (circle != undefined) {
	      mapCreate.removeLayer(circle);
	    }

		//if (circle) circle.setMap(null);
		jQuery(".qcpd-single-list-pd, .qcpd-single-list-pd-1, .opd-list-pd-style-8, .opd-list-pd-style-9, .pd-container").each(function(){
			
			jQuery(this).show();
			
		});
		
		jQuery(".qcld_pd_tabcontent:visible .qc-grid-item ul li").each(function(){
			
			jQuery(this).show();
			
		})
		
		clearmarker();
		myLooptab(mapCreate,map, markerClusters);
		setTimeout(function(){
			jQuery('.qc-grid').packery({
			  itemSelector: '.qc-grid-item',
			  gutter: 10
			});
		},1000)


});

	function clearmarker(){

	    for(var i = 0; i < markers.length; i++){
		    mapCreate.removeLayer(markers[i]);
		}

	}

	
	
	function infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info) {
	
			
		var html = "<div>";
		
		if(imgurl!='' && !cluster.image_infowindow){
			html += "<div class='sbd_pop_img'><img src='"+imgurl+"' /></div>";
		}
		
      	html += "<div class='sbd_pop_text'><h3>" + title + "</h3><p>" + subtitle + "</p>";
		
		if(address!=''){
			if( sbd_enable_map_addr_phone_clickable == 'on' ){
				 html+="<p><b><i class='fa fa-map-marker fa-map-marker-alt' aria-hidden='true'></i> </b><a href='http://maps.google.com/maps?q=" + encodeURIComponent( address ) + "' target='_blank'>" + address + "</a></p>";
			}else{
				html+="<p><b><i class='fa fa-map-marker fa-map-marker-alt' aria-hidden='true'></i> </b>" + address + "</p>";				
			}
		}
		if(others_info!=''){
			html+=others_info;
		}
		if(custom_content!='' && typeof(custom_content)!='undefined'){
			html+=custom_content;
		}
		
		html +="</div></div>";
		if(url!='' && url.length > 2){
			html +="<a style='text-align: center;display:block;font-weight: bold;' href='" + url + "' "+ target +">"+sld_variables.view_details+"</a>";
		}
		html +="<div class='sbd_bottom_area_marker'>"+icons+"</div>";	
				
		html+="</div></div>";
				
		return html;


	}


	//code for filter area
	jQuery(document).on("click",".sbd-filter-area a", function(event){
		event.preventDefault();
		
		var sbd_mode='categoryTab';
		if( !jQuery(this).parent('.sbd-filter-area').parent().hasClass('qcld_pd_tabcontent') ){
			sbd_mode='';
			return false;
		}	

		var findmapid = jQuery('.qcld_pd_tabcontent:visible .sbd_map:visible').length>0?jQuery('.qcld_pd_tabcontent:visible .sbd_map:visible').attr('id'):'';

		if(findmapid!=''){
			SBDOpenStreetRemoveAllMarkers();
		}
		
		
		var filterName = jQuery(this).attr("data-tag");

		jQuery(".sbd-filter-area a").removeClass("filter-active");
		jQuery(".sbd-tag-filter-area a").removeClass("filter-active");
		jQuery(this).addClass("filter-active");

		if( filterName == "all" ){

			jQuery(".qcld_pd_tabcontent:visible #sbdopd-list-holder .qc-grid-item").css("display", "block");
			if(findmapid!=''){
				myLooptab(mapCreate,map, markerClusters);
			}

		}else{

			var categoryGridSelector = ".qcld_pd_tabcontent:visible .qc-grid";
			if( jQuery(categoryGridSelector).length == 0 ){
				categoryGridSelector = ".qcld_pd_tabcontent:visible .qcpd-list-hoder";
			}
			jQuery(categoryGridSelector+" .qc-grid-item").css("display", "none");
			jQuery(categoryGridSelector+" .qc-grid-item."+filterName+"").css("display", "block");
			if(findmapid!=''){
				jQuery(categoryGridSelector+" ."+filterName+" ul li").each(function(){
					var obj = jQuery(this);
					
					var icons = obj.find('.pd-bottom-area').clone();
					icons.find('.pd-map').removeClass('open-mpf-sld-link');
					//if( typeof google !== 'undefined' ){
						if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
							icons.find('.pd-map').attr("href", "https://www.google.com/maps/dir/?api=1&origin=none&destination="+encodeURIComponent(obj.attr('data-address'))+"&travelmode=driving");
							icons.find('.pd-map').attr("target", "_blank");
						}
					
						icons.find('.open-mpf-sld-link').remove();
						icons.find('.pd-map').remove();
						icons = icons.html();
						
						var custom_content = obj.find('.sbd_custom_content').clone();
						custom_content = custom_content.html();
						//others information
						var others_info = '';
						if(obj.attr('data-local')!=''){
							others_info +="<p><b>Local: </b>"+obj.attr('data-local')+"</p>";
						}
						if(obj.attr('data-phone')!=''){
							others_info +="<p><b>Phone: </b>"+obj.attr('data-phone')+"</p>";
						}
						if(obj.attr('data-businesshour')!=''){
							others_info +="<p><b>Business Hours: </b>"+obj.attr('data-businesshour')+"</p>";
						}
						
						var imgurl = '';
						if(obj.find('img').length>0){
							imgurl = obj.find('img').attr('src');
						}
						var target = '';
					
						if(typeof(obj.find('a').attr('target'))!=='undefined'){
							target = 'target="_blank"';
						}
						var obj = jQuery(this);
						if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
							var locations = obj.attr('data-latlon');
							var latlng = locations.split(',');
							var title = obj.attr('data-title');
							var address = obj.attr('data-address');
							var url = obj.attr('data-url');
							var subtitle = obj.attr('data-subtitle');
							var markericon = '';
							if(sld_variables.global_marker!=''){
								markericon = sld_variables.global_marker;
							}
							if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
								markericon = sld_variables.paid_marker_default; //default paid marker
								if(sld_variables.paid_marker!=''){
									markericon = sld_variables.paid_marker; // If paid marker is set then override the default
								}
							}

							if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
								markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
							}
						var map_marker_id = jQuery(this).attr('id');
						var icon = markericon;

						maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

						mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

						if( icon != '' ){

							var myIcon = L.icon({
							  iconUrl: icon,
							  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
							  iconAnchor: [25, 50],
							  popupAnchor: [-10, -50],
							});

							var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

						}else{

							var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
						}
						
						let markerPos = marker.getLatLng();

							if( sbd_map_fitbounds == 'on' ){
			 
								marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
								
							}else{

								marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();

							}

						markerClusters.clearLayers();
						markers.push(marker);

						var isClicked = false;

						marker.on({
						    mouseover: function(e) {

						        for (var i in markers){
						            var markerID = markers[i].options.title;
						            if (markerID == e.target.options.title ){
						                markers[i].openPopup();
						        		
						            }else{
						                markers[i].closePopup();
						            };
						        }


						    },
						    
						});

						//var group = new L.featureGroup(markers);
						//mapCreate.fitBounds(group.getBounds().pad(0.5));

						if( sbd_map_fitbounds == 'on' ){

							var group = new L.featureGroup(markers);
							mapCreate.fitBounds(group.getBounds());

						}else{

							var southWest = new L.LatLng(markers[0]._latlng.lat, markers[0]._latlng.lng),
					    	northEast = new L.LatLng(markers[1]._latlng.lat, markers[1]._latlng.lng),
					    	bounds = new L.LatLngBounds(southWest, northEast);
							mapCreate.fitBounds(bounds, {padding: [50, 50]});

							mapCreate.setZoom(parseInt(sld_variables.zoom));
						}
							


					}
					//}
					
				})
			}
		}

		jQuery('.qc-grid').packery({
		  itemSelector: '.qc-grid-item',
		  gutter: 10
		});

	});

	var filter = [];

	function qcsbd_CheckArrays(array1, array2){
		if( Array.isArray(array1) && Array.isArray(array2) ){
			//return array1.length === array2.length && array1.sort().every(function(value, index) { return value === array2.sort()[index]});
			return array2.every(function (v) {
		        return array1.includes(v);
		    });
		}else{
			return false;
		}
	}

	$(document).on("click",".sbd-tag-filter-area a", function(event){
		
		event.preventDefault();

		var sbd_mode='categoryTab';
		if( !jQuery(this).parent('.sbd-tag-filter-area').parent().hasClass('qcld_pd_tabcontent') ){
			sbd_mode='';
			return false;
		}	


		var combined_search = 0;
		if(sld_variables.sbd_combine_distant_tag_live_search == "on"){
			var location_name = jQuery('#sbd_location_name').val();
			var distance = jQuery('.sbd_distance').val();
			if( location_name != '' && distance != '' && event.type == 'click' ){
				combined_search = 0;
			}
		}
		
		//console.log(combined_search);
		var mapIDselector = 'sbd_all_location';
        var sbd_mode='';
		if( jQuery(this).parent('.sbd-tag-filter-area').parent().hasClass('qcld_pd_tabcontent') ){
			var mapIDselector = jQuery('.sbd_map:visible').attr('id');
			sbd_mode='categoryTab';

			var qcld_pd_event = jQuery('.qcld_pd_tablinks.qcld_pd_active').data('contentid');
			var mapid = jQuery('#'+qcld_pd_event+' .sbd_map').attr('id');
					
			if(typeof(mapid)!=="undefined" || mapid!==''){

				var container = L.DomUtil.get(mapid); 
				if(container != null){ container._leaflet_id = null; }

			  	mapCreate = L.map(mapid).setView([sld_variables.latitute, sld_variables.longitute], parseInt(sld_variables.zoom));
			  	markerClusters = L.markerClusterGroup(); 

				var minZoomVal = 2; 

				if(cluster.map_fullwidth == 'true'){
					var minZoomVal = 3; 
				}

				if(cluster.map_position == 'right'){
					var minZoomVal = 2; 
				}

			  	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					minZoom: minZoomVal,
					maxZoom: 50,
					attribution: '',
					key: 'BC9A493B41014CAABB98F0471D759707'
				}).addTo(mapCreate);


			}		
			

		}
		

        $(this).toggleClass("pd_tag_filter-active");
		
		// Retrieve the input field text and reset the count to zero
        //var filter = $(this).attr('data-tag'), count = 0;

        var currently_tag = $(this).attr('data-tag');
		if( !currently_tag && $(this).hasClass('pd_tag_filter-active') ){
			currently_tag = 'all';
			filter = [];
		}
		// console.log(currently_tag);

		var inclusive_tag_filter = sld_variables.inclusive_tag_filter;
		// var inclusive_tag_filter = 'off';

		if( inclusive_tag_filter == 'on' && currently_tag == 'all' ){
			jQuery(this).siblings('.pd_tag_filter').removeClass('pd_tag_filter-active');
		}

        if( sbd_mode == 'categoryTab' ){
	        filter = [];
	        jQuery('.sbd-tag-filter-area:visible .pd_tag_filter-active').each(function(){
        		var current_tag = $(this).attr('data-tag');
        		filter.push(current_tag);
	        });

	        var count = 0;
	        var search_term = new RegExp(filter.join('|'), "i");
        }else{    	
	        var current_tag = $(this).attr('data-tag');
	        if($(this).hasClass('pd_tag_filter-active')){
	        	filter.push(current_tag);
			}else{
				var index = filter.indexOf(current_tag);
			    if (index > -1) {
			       filter.splice(index, 1);
			    }
			}
	        var count = 0;

	        //Deselct "All" when clicked on any other tag
	        if( current_tag ){
	        	filter = filter.filter(function(item, pos) {
				    return item;
				});
				jQuery(this).siblings().eq(0).removeClass('pd_tag_filter-active');
	        }

	        //Remove Duplicate Element from the Array
	        if( filter.length > 0 ){
		        filter = filter.filter(function(item, pos) {
				    return filter.indexOf(item) == pos;
				});
	        }

	        if( inclusive_tag_filter == 'on' ){
	        	var search_term = new RegExp(filter.join('|'), "ig");
	        }else{
	        	var search_term = new RegExp(filter.join('|'), "i");
	        }
	  		//   search_term = search_term.filter(function (el) {
			//   return el != null;
			// });
        }

        if( inclusive_tag_filter == 'on' && currently_tag != 'all' && filter.length > 0 ){
			
			for (var i = 0; i <= filter.length; i++) {
				if( filter[i] && filter[i] != '' ){
					jQuery('.pd_tag_filter[data-tag="'+filter[i]+'"]').addClass('pd_tag_filter-active');
				}
				// console.log('tag ');
				// console.log(jQuery('.pd_tag_filter[data-tag='+filter[i]+']').text());
			}
		}

        if( cluster.map=='true' ){
			if(jQuery('#'+mapIDselector).length>0 && combined_search == 0){
				SBDOpenStreetRemoveAllMarkers();
			}
		}
        // Loop through the comment list
        var loopedItemSeclector = "#sbdopd-list-holder ul li";
        if( sbd_mode == 'categoryTab' ){
        	loopedItemSeclector = ".qcld_pd_tabcontent:visible .qc-grid ul li";
        	if( $(loopedItemSeclector).length == 0 ){
        		loopedItemSeclector = ".qcld_pd_tabcontent:visible .qcpd-list-hoder ul li";
        	}
        }
        $(loopedItemSeclector).each(function(){

            var dataTitleTxt = $(this).find('a').attr('data-tag');

            if( typeof(dataTitleTxt) == 'undefined' ){
                dataTitleTxt = "-----";
            }

            var inclusive_tag_filter = sld_variables.inclusive_tag_filter;

            // Check inclusive Tag Filter
 			if( inclusive_tag_filter == 'on' && currently_tag != 'all' ){
        	//console.log("working");

 				// var checked_array = dataTitleTxt.match(search_term);
 				var main_array = dataTitleTxt.split(',');
 				var match_array = qcsbd_CheckArrays(main_array, filter);
 				// If the list item does not contain the text phrase fade it out
	            if ( match_array ) {
	
	 				$(this).show();
					$(this).addClass("showMe");

					var filterButton = $('.sbd-tag-filter-area').find('.filter-active:not([data-filter="all"])');
					if( sbd_mode == 'categoryTab' ){
						filterButton = $('qcld_pd_tabcontent:visible').find('.sbd-tag-filter-area').find('.filter-active:not([data-filter="all"])');
					}
					if( filterButton.length ){
						if($(this).parents('.qc-grid-item').is(':hidden')){
							return;
						}
					}
	                count++;

	               // if( typeof google !== 'undefined' ){
						if(jQuery('#'+mapIDselector).length>0 && combined_search == 0){
							var obj = $(this);
							
							var icons = obj.find('.pd-bottom-area').clone();
							icons.find('.pd-map').removeClass('open-mpf-sld-link');
				
							if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
								icons.find('.pd-map').attr("href", "https://nominatim.openstreetmap.org/search?format=json&limit=1&q="+encodeURIComponent(obj.attr('data-address'))+"");
								icons.find('.pd-map').attr("target", "_blank");
							}
							icons.find('.open-mpf-sld-link').remove();
							icons.find('.pd-map').remove();
							icons = icons.html();
							
							var custom_content = obj.find('.sbd_custom_content').clone();
							custom_content = custom_content.html();
							//others information
							var others_info = '';
							if(obj.attr('data-local')!=''){
								others_info +="<p><b><i class='fa fa-location-arrow' aria-hidden='true'></i> </b>"+obj.attr('data-local')+"</p>";
							}
							if(obj.attr('data-phone')!=''){
								if( sbd_enable_map_addr_phone_clickable == 'on' ){
									others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b><a href='tel:"+obj.attr('data-phone')+"'>"+obj.attr('data-phone')+"</a></p>";
								}else{
									others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b>"+obj.attr('data-phone')+"</p>";
								}
							}
							if(obj.attr('data-businesshour')!=''){
								others_info +="<p><b><i class='fa fa-clock-o fa-clock' aria-hidden='true'></i> </b>"+obj.attr('data-businesshour')+"</p>";
							}
							
							var imgurl = '';
							if(obj.find('img').length>0){
								imgurl = obj.find('img').attr('src');
							}
							var target = '';
				
							if(typeof(obj.find('a').attr('target'))!=='undefined'){
								target = 'target="_blank"';
							}
							if(typeof(obj.find('a').attr('data-newtab'))!=='undefined'){
								target = 'target="_blank"';
							}


							
							if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
								
								var locations = obj.attr('data-latlon');
								var latlng = locations.split(',');
								var title = obj.attr('data-title');
								var address = obj.attr('data-address');
								var url;
								if(typeof(obj.find('a').attr('href'))!=='undefined' && obj.find('a').is('a:not([href^="#"])') && obj.find('a').is('a:not([href^="tel"])')){
									url = obj.find('a').attr('href');
								}else{
									url = obj.attr('data-url');
								}
								var subtitle = obj.attr('data-subtitle');
								var markericon = '';
								if(sld_variables.global_marker!=''){
									markericon = sld_variables.global_marker;
								}
								if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
									markericon = sld_variables.paid_marker_default; //default paid marker
									if(sld_variables.paid_marker!=''){
										markericon = sld_variables.paid_marker; // If paid marker is set then override the default
									}
								}

								if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
									markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
								}
								var map_marker_id = jQuery(this).attr('id');
								var icon = markericon;

								maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

								mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

								if( icon != '' ){

									var myIcon = L.icon({
									  iconUrl: icon,
									  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
									  iconAnchor: [25, 50],
									  popupAnchor: [-10, -50],
									});

									var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

								}else{

									var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
								}
								
								let markerPos = marker.getLatLng();

									if( sbd_map_fitbounds == 'on' ){
					 
										marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
										
									}else{

										marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();

									}

								markerClusters.clearLayers();
								markers.push(marker);



								var isClicked = false;

								marker.on({
								    mouseover: function(e) {

								        for (var i in markers){
								            var markerID = markers[i].options.title;
								            if (markerID == e.target.options.title ){
								                markers[i].openPopup();
								        		
								            }else{
								                markers[i].closePopup();
								            };
								        }

								    },
								    
								});

								//var group = new L.featureGroup(markers);
								//mapCreate.fitBounds(group.getBounds().pad(0.5));

								if( sbd_map_fitbounds == 'on' ){

									var group = new L.featureGroup(markers);
									mapCreate.fitBounds(group.getBounds());

								}else{

									var southWest = new L.LatLng(markers[0]._latlng.lat, markers[0]._latlng.lng),
							    	northEast = new L.LatLng(markers[1]._latlng.lat, markers[1]._latlng.lng),
							    	bounds = new L.LatLngBounds(southWest, northEast);
									mapCreate.fitBounds(bounds, {padding: [50, 50]});

									mapCreate.setZoom(parseInt(sld_variables.zoom));
								}


							}
						}
					//}
	            // Show the list item if the phrase matches and increase the count by 1
	            }
	            else {
					$(this).fadeOut();
					$(this).removeClass("showMe");		
					
	            }
 			}

 			if( inclusive_tag_filter != 'on' || currently_tag == 'all' ){
 				// If the list item does not contain the text phrase fade it out
	            if ( dataTitleTxt.search( search_term ) < 0 ) {
	                $(this).fadeOut();
					$(this).removeClass("showMe");		
	 
	            // Show the list item if the phrase matches and increase the count by 1
	            }
	            else {
	                $(this).show();
					$(this).addClass("showMe");

					var filterButton = $('.sbd-filter-area').find('.filter-active:not([data-filter="all"])');
					if( sbd_mode == 'categoryTab' ){
						filterButton = $('qcld_pd_tabcontent:visible').find('.sbd-filter-area').find('.filter-active:not([data-filter="all"])');
					}
					if( filterButton.length ){
						if($(this).parents('.qc-grid-item').is(':hidden')){
							return;
						}
					}
	                count++;
					if(jQuery('#'+mapIDselector).length>0 && combined_search == 0){
						var obj = $(this);
						
						var icons = obj.find('.pd-bottom-area').clone();
						icons.find('.pd-map').removeClass('open-mpf-sld-link');
						
						if( typeof google !== 'undefined' ){	
							if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
								icons.find('.pd-map').attr("href", "https://nominatim.openstreetmap.org/search?format=json&limit=1&q="+encodeURIComponent(obj.attr('data-address'))+"");
								icons.find('.pd-map').attr("target", "_blank");
							}
							icons.find('.open-mpf-sld-link').remove();
							icons.find('.pd-map').remove();
							icons = icons.html();
							
							var custom_content = obj.find('.sbd_custom_content').clone();
							custom_content = custom_content.html();
							//others information
							var others_info = '';
							if(obj.attr('data-local')!=''){
								others_info +="<p><b><i class='fa fa-location-arrow' aria-hidden='true'></i> </b>"+obj.attr('data-local')+"</p>";
							}
							if(obj.attr('data-phone')!=''){
								if( sbd_enable_map_addr_phone_clickable == 'on' ){
									others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b><a href='tel:"+obj.attr('data-phone')+"'>"+obj.attr('data-phone')+"</a></p>";
								}else{
									others_info +="<p><b><i class='fa fa-phone' aria-hidden='true'></i> </b>"+obj.attr('data-phone')+"</p>";
								}
							}
							if(obj.attr('data-businesshour')!=''){
								others_info +="<p><b><i class='fa fa-clock-o fa-clock' aria-hidden='true'></i> </b>"+obj.attr('data-businesshour')+"</p>";
							}
							
							var imgurl = '';
							if(obj.find('img').length>0){
								imgurl = obj.find('img').attr('src');
							}
							var target = '';
				
							if(typeof(obj.find('a').attr('target'))!=='undefined'){
								target = 'target="_blank"';
							}
							if(typeof(obj.find('a').attr('data-newtab'))!=='undefined'){
								target = 'target="_blank"';
							}
							
							if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
								
								var locations = obj.attr('data-latlon');
								var latlng = locations.split(',');
								var title = obj.attr('data-title');
								var address = obj.attr('data-address');
								var url;
								if(typeof(obj.find('a').attr('href'))!=='undefined' && obj.find('a').is('a:not([href^="#"])') && obj.find('a').is('a:not([href^="tel"])')){
									url = obj.find('a').attr('href');
								}else{
									url = obj.attr('data-url');
								}
								var subtitle = obj.attr('data-subtitle');
								var markericon = '';
								if(sld_variables.global_marker!=''){
									markericon = sld_variables.global_marker;
								}
								if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
									markericon = sld_variables.paid_marker_default; //default paid marker
									if(sld_variables.paid_marker!=''){
										markericon = sld_variables.paid_marker; // If paid marker is set then override the default
									}
								}

								if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
									markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
								}
								var map_marker_id = jQuery(this).attr('id');
								var icon = markericon;

								maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

								mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

								if( icon != '' ){

									var myIcon = L.icon({
									  iconUrl: icon,
									  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
									  iconAnchor: [25, 50],
									  popupAnchor: [-10, -50],
									});

									var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

								}else{

									var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
								}
								
								let markerPos = marker.getLatLng();

									if( sbd_map_fitbounds == 'on' ){
					 
										marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
										
									}else{

										marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();

									}

								markerClusters.clearLayers();
								markers.push(marker);


								var isClicked = false;

								marker.on({
								    mouseover: function(e) {

								        for (var i in markers){
								            var markerID = markers[i].options.title;
								            if (markerID == e.target.options.title ){
								                markers[i].openPopup();
								        		
								            }else{
								                markers[i].closePopup();
								            };
								        }


								    },
								    
								});

								var group = new L.featureGroup(markers);
								mapCreate.fitBounds(group.getBounds().pad(0.5));

							}
						}
					}
					
	            }
 			}
        });

		var singleItemSelector = $(".qcpd-single-list-pd, .qc-sbd-single-item-11, .qcpd-single-list-pd-1, .opd-list-pd-style-8, .opd-list-pd-style-9, .pd-container");
		if( sbd_mode == 'categoryTab' ){
			singleItemSelector = $('.qcld_pd_tabcontent:visible').find(".qcpd-single-list-pd, .qcpd-single-list-pd-1, .opd-list-pd-style-8, .opd-list-pd-style-9, .pd-container");
		}
		singleItemSelector.each(function(){
            //if( $(this).is(':visible') ){ // add this condition to display result from only visible list
				var visibleItems = $(this).find("li.showMe").length;
				
				//console.log(visibleItems);
				
				if(visibleItems==0){
					$(this).hide();
				}else{
					$(this).show();
				}
            //}
		});

		
		setTimeout(function(e){
			$grid = $('.qc-grid');
			$grid.packery('destroy').packery({
			  itemSelector: '.qc-grid-item',
			  gutter: 10
			});
		},1000);
		
		$grid = $('.qc-grid');
		$grid.packery('destroy').packery({
		  itemSelector: '.qc-grid-item',
		  gutter: 10
		});

		/*if( typeof google !== 'undefined' ){
			if( typeof map !== 'undefined' ){
				google.maps.event.addListener(map, "click", function(event) {
					
					jQuery('.gm-style-iw').each(function(){
						jQuery(this).next().click();
					})
					
				});
			}
		}*/
		

		if( combined_search == 1 ){
			setTimeout(function(){
				sbdradius_();
			}, 500)
		}
	});

	


	//jQuery(".pd_search_filter").keyup(function(){
	jQuery(document).on("keyup",".pd_search_filter", function(event){

		// Retrieve the input field text and reset the count to zero
		var filter = jQuery(this).val(), count = 0;

		if( !jQuery('.qcld_pd_tab_main').find('.qcld_pd_tabcontent') ){
			return false;
		}	

		var qcld_pd_event = jQuery('.qcld_pd_tablinks.qcld_pd_active').data('contentid');
		var mapid = jQuery('#'+qcld_pd_event+' .sbd_map').attr('id');
				
		if(typeof(mapid)!=="undefined" || mapid!==''){
			
			var container = L.DomUtil.get(mapid); 
			if(container != null){ container._leaflet_id = null; }

		  	mapCreate = L.map(mapid).setView([sld_variables.latitute, sld_variables.longitute], parseInt(sld_variables.zoom));
		  	markerClusters = L.markerClusterGroup(); 

			var minZoomVal = 2; 

			if(cluster.map_fullwidth == 'true'){
				var minZoomVal = 3; 
			}

			if(cluster.map_position == 'right'){
				var minZoomVal = 2; 
			}

		  	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				minZoom: minZoomVal,
				maxZoom: 50,
				attribution: '',
				key: 'BC9A493B41014CAABB98F0471D759707'
			}).addTo(mapCreate);


		}	
		
		var findmapid = jQuery('.qcld_pd_tabcontent:visible .sbd_map').length>0?jQuery('.qcld_pd_tabcontent:visible .sbd_map').attr('id'):'';
		
		if(findmapid!=''){
			SBDOpenStreetRemoveAllMarkers();
		}
		// Loop through the comment list
		jQuery(".qcld_pd_tabcontent:visible .qc-grid ul li").each(function(){

			var dataTitleTxt = jQuery(this).children('a').attr('data-title');
			var dataurl = jQuery(this).find('a').attr('href');
			//console.log(dataurl);


			if( typeof(dataurl) == 'undefined' ){
				dataurl = "-----";
			}


			if( typeof(dataTitleTxt) == 'undefined' ){
				dataTitleTxt = "-----";
			}

			var parentH3 = jQuery(this).parentsUntil('.qc-grid-item').children('h3').text();
 
			// If the list item does not contain the text phrase fade it out
			if (jQuery(this).text().search(new RegExp(filter, "i")) < 0 && dataurl.search(new RegExp(filter, "i")) < 0 && dataTitleTxt.search(new RegExp(filter, "i")) < 0 && parentH3.search(new RegExp(filter, "i")) < 0 ) {
				jQuery(this).fadeOut();
				jQuery(this).removeClass("showMe");		
 
			// Show the list item if the phrase matches and increase the count by 1
			}
			else {
				jQuery(this).show();
				jQuery(this).addClass("showMe");
				count++;
				//if( typeof google !== 'undefined' ){
					if(findmapid!=''){
						var obj = jQuery(this);
						
						var icons = obj.find('.pd-bottom-area').clone();
						icons.find('.pd-map').removeClass('open-mpf-sld-link');
			
						if(typeof(obj.attr('data-address'))!=='undefined' && obj.attr('data-address')!=''){
							icons.find('.pd-map').attr("href", "https://nominatim.openstreetmap.org/search?format=json&limit=1&q="+encodeURIComponent(obj.attr('data-address'))+"");
							icons.find('.pd-map').attr("target", "_blank");
						}
						icons.find('.open-mpf-sld-link').remove();
						icons.find('.pd-map').remove();
						icons = icons.html();
						
						var custom_content = obj.find('.sbd_custom_content').clone();
						custom_content = custom_content.html();
						//others information
						var others_info = '';
						if(obj.attr('data-local')!=''){
							others_info +="<p><b>Local: </b>"+obj.attr('data-local')+"</p>";
						}
						if(obj.attr('data-phone')!=''){
							others_info +="<p><b>Phone: </b>"+obj.attr('data-phone')+"</p>";
						}
						if(obj.attr('data-businesshour')!=''){
							others_info +="<p><b>Business Hours: </b>"+obj.attr('data-businesshour')+"</p>";
						}
						
						var imgurl = '';
						if(obj.find('img').length>0){
							imgurl = obj.find('img').attr('src');
						}
						var target = '';
			
						if(typeof(obj.find('a').attr('target'))!=='undefined'){
							target = 'target="_blank"';
						}
						if(obj.attr('data-latlon')!=='' && typeof(obj.attr('data-latlon'))!=='undefined'){
							
							var locations = obj.attr('data-latlon');
							var latlng = locations.split(',');
							var title = obj.attr('data-title');
							var address = obj.attr('data-address');
							var url = obj.attr('data-url');
							var subtitle = obj.attr('data-subtitle');
							var markericon = '';
							if(sld_variables.global_marker!=''){
								markericon = sld_variables.global_marker;
							}
							if(typeof(obj.attr('data-paid'))!='undefined' && obj.attr('data-paid')!=''){
								markericon = sld_variables.paid_marker_default; //default paid marker
								if(sld_variables.paid_marker!=''){
									markericon = sld_variables.paid_marker; // If paid marker is set then override the default
								}
							}

							if(typeof(obj.attr('data-marker'))!='undefined' && obj.attr('data-marker')!=''){
								markericon = obj.attr('data-marker'); // If icon is set in the item it self. Most priority.
							}
							var map_marker_id = jQuery(this).attr('id');
							var icon = markericon;

							maplatlang = infoWindow(marker, map, title, address, url, subtitle, imgurl, target, icons, custom_content, others_info);

							mapCreate.panTo(new L.LatLng(sld_variables.latitute, sld_variables.longitute), parseInt(sld_variables.zoom));

							if( icon != '' ){

								var myIcon = L.icon({
								  iconUrl: icon,
								  iconSize: [sbd_custom_marker_image_width, sbd_custom_marker_image_height],
								  iconAnchor: [25, 50],
								  popupAnchor: [-10, -50],
								});

								var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id, icon: myIcon } ); 

							}else{

								var marker = L.marker([ latlng[0], latlng[1] ],{title:map_marker_id} ); 
							}

							if( sbd_map_fitbounds == 'on' ){
			 
								marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
								
							}else{

								marker.addTo(mapCreate).bindPopup(maplatlang, {closeOnClick: true, autoClose: true, showOnMouseOver: true, autoPan:false} ).closePopup();
							}

							markerClusters.clearLayers();
							markers.push(marker);

							var isClicked = false;

							marker.on({
			                    mouseover: function(e) {

			                        for (var i in markers){
			                            var markerID = markers[i].options.title;
			                            if (markerID == e.target.options.title ){
			                                markers[i].openPopup();
			                                
			                            }else{
			                                markers[i].closePopup();
			                            };
			                        }


			                    },
			                    
			                });

						}

						var group = new L.featureGroup(markers);
						mapCreate.fitBounds(group.getBounds().pad(0.5));
					}
				//}
				
			}
		});
		
		jQuery(".qcpd-single-list-pd, .qcpd-single-list-pd-1, .opd-list-pd-style-8, .opd-list-pd-style-9, .pd-container").each(function(){
			
			var visibleItems = jQuery(this).find("li.showMe").length;
			
			if(visibleItems==0){
				jQuery(this).hide();
			}else{
				jQuery(this).show();
			}
		});

		
		jQuery('.qc-grid').packery({
		  itemSelector: '.qc-grid-item',
		  gutter: 10
		});



 
	});



	//jQuery('.sbd_radius_search').addClass('sbd_openstreet_map');

	function qcpd_openstreet_view_map_Functions(arr, currentDom){

		var out = "";
		var i;
		jQuery('.sbd_street_map_results').remove();
		currentDom.closest('.sbd_radius_search').append('<div class="sbd_street_map_results"></div>');

		if(arr.length > 0){
		 
			for(i = 0; i < arr.length; i++){
		  
				var display_name = '"' + arr[i].display_name + '"';
				out += "<div class='sbd_address' title='"+ arr[i].display_name +"' data-lat='"+ arr[i].lat +"' data-lon='"+ arr[i].lon +"' data-name='"+ arr[i].display_name +"' >" + arr[i].display_name + "</div>";
			}
			jQuery('.sbd_street_map_results').html(out);

		}else{
			jQuery('.sbd_street_map_results').html("Sorry, no results...");
			jQuery('.sbd_street_map_results').fadeOut();
			return;

		}
		
		jQuery('.sbd_street_map_results').show();

	}

	jQuery('.sbd_location_name').focus(function(){
		jQuery('.sbd_current_location').show();
	})
	
	jQuery('.sbd_location_name').blur(function(){
		setTimeout(function(){
			jQuery('.sbd_current_location').hide();
		},400)
		
	})


	jQuery("body").on('keyup', ".sbd_location_name", function(e){

			var currentDom = jQuery(this);
			var inp = currentDom.val();
			var xmlhttp = new XMLHttpRequest();
			var url = "https://nominatim.openstreetmap.org/search?format=json&limit=10&q=" + inp;
			 xmlhttp.onreadystatechange = function()
			 {
			   if (this.readyState == 4 && this.status == 200)
			   {
				var myArr = JSON.parse(this.responseText);
				qcpd_openstreet_view_map_Functions(myArr, currentDom);
			   }
			 };
			 xmlhttp.open("GET", url, true);
			 xmlhttp.send();


	});

	jQuery(document).on('click', 'body', function(e){
		jQuery('.sbd_street_map_results').hide();
	});



	jQuery(document).on('click', '.sbd_address', function(e){
		var currentDom = jQuery(this);
		var latVal = currentDom.attr('data-lat');
		var lonVal = currentDom.attr('data-lon');
		var nameVal = currentDom.attr('data-name');
		
		currentDom.closest('.sbd_radius_search').find('input.sbd_location_name').val(nameVal);
		currentDom.closest('.sbd_radius_search').find('input.sbd_location_name').attr('data-address',nameVal);
		currentDom.closest('.sbd_radius_search').find('input.sbd_location_name').attr('data-lat', latVal);
		currentDom.closest('.sbd_radius_search').find('input.sbd_location_name').attr('data-lon', lonVal);
		
		jQuery('.sbd_street_map_results').hide();
		
		
	});




	jQuery('.qc-grid').packery({
      itemSelector: '.qc-grid-item',
      gutter: 10
    });

	
});