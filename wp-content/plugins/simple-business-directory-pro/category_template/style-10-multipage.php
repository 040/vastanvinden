		<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-10-multipage/style.css"; ?>" />
		<style type="text/css">
		.qc-sbd-single-item-11 .qc-sbd-main {
			border-bottom:1px solid #eee
		}
		</style>
		<div class="qc-feature-container qc-sbd-single-item-11">
			<ul>
			<?php
				$ci = 0;
				foreach ($cterms as $cterm){
					?>
						<li class="opt-column-03">
							<a href="<?php echo $current_url; ?>/<?php echo $cterm->slug; ?>">
							<div class="qc-sbd-main">
							  <div class="qc-feature-media image">

							  <?php
							  
								$args = array(
									'numberposts' => -1,
									'post_type'   => 'pd',
								);
								$taxArray = array(
									array(
										'taxonomy' => 'pd_cat',
										'field'    => 'id',
										'terms'    => $cterm -> term_id,
									),
								);
								$args = array_merge($args, array( 'tax_query' => $taxArray ));								
								$listItems = get_posts( $args );
								$total_post = 0;
								if(!empty($listItems))
									$total_post = count($listItems);
								$total_items = 0;
								foreach ($listItems as $item){
									
									$total_items += count(qc_sbd_get_list_items( $item->ID ));
									
								}
								
							  ?>
							  
								<?php $image_id = get_term_meta ( $cterm -> term_id, 'category-image-id', true );
										if($image_id){
										?>
														
											<?php echo wp_get_attachment_image ( $image_id, 'full' ); ?>
										
										<?php } ?>
									<div class="sbd_total_lists">
										<?php
											if( (pd_ot_get_option('sbd_lan_lists') != '') && ($total_post>1) ){
												echo $total_post.pd_ot_get_option('sbd_lan_lists');
											}elseif( (pd_ot_get_option('sbd_lan_list') != '') && ($total_post<=1) ){
												echo $total_post.pd_ot_get_option('sbd_lan_list');
											}else{

												$list_text = sprintf( _n( '%s List', '%s Lists', $total_post, 'qc-pd' ), $total_post );
												echo $list_text;
											}
										?>
									</div>
									<div class="sbd_total_items">
										<?php
											if( (pd_ot_get_option('sbd_lan_items') != '') && ($total_items>1) ){
												echo $total_items.pd_ot_get_option('sbd_lan_items');
											}elseif( (pd_ot_get_option('sbd_lan_item') != '') && ($total_items<=1) ){
												echo $total_items.pd_ot_get_option('sbd_lan_item');
											}else{
 
												$item_text = sprintf( _n( '%s Item', '%s Items', $total_items, 'qc-pd' ), $total_items );
												echo $item_text;
											}
										?>
									</div>
							  </div>
							  <div class="qc-sbd-content">
								<h4 class="sbd-title"><?php echo $cterm->name; ?></h4>
								
								<p class="sub-title"><?php echo $cterm->description; ?></p>
							  </div>
							  
							  <div class="clear"></div>
							  </div>
						  </a>
						  
					  </li>
					<?php
					$ci++;					
				}
			?>
			</ul>
		</div>