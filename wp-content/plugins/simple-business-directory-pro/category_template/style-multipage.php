		<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-multipage/style.css"; ?>" />
		<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/style-multipage/responsive.css"; ?>" />
		<div class="qcld_pd_category_list">
			<ul>
			<?php
				$ci = 0;
				foreach ($cterms as $cterm){
					?>
						<li>
						
							<div class="column-grid3">
								<div class="pd-main-content-area bg-color-0<?php echo (($ci%5)+1); ?>">
									<div class="pd-main-panel">
										<div class="panel-title">
											<h3><?php echo $cterm->name; ?></h3>
										</div>
										<?php $image_id = get_term_meta ( $cterm -> term_id, 'category-image-id', true );
										if($image_id){
										?>
										<div class="feature-image">					
											<?php echo wp_get_attachment_image ( $image_id, 'thumbnail' ); ?>
										</div>
										<?php } ?>
									</div>
									<div class="pd-hover-content">
										<p><?php echo $cterm->description; ?></p>
										<?php 
											if(pd_ot_get_option('sbd_lan_visit_page')!=''){
												$visit_page = pd_ot_get_option('sbd_lan_visit_page');
											}else{
												$visit_page = __('Visit Page','qc-pd');
											}
										?>
										<a href="<?php echo $current_url; ?>/<?php echo $cterm->slug; ?>" ><?php echo $visit_page; ?></a>
									</div>
							<?php
							  
								$args = array(
									'numberposts' => -1,
									'post_type'   => 'pd',
								);
								$taxArray = array(
									array(
										'taxonomy' => 'pd_cat',
										'field'    => 'id',
										'terms'    => $cterm -> term_id,
									),
								);
								$args = array_merge($args, array( 'tax_query' => $taxArray ));								
								$listItems = get_posts( $args );
								$total_post = 0;
								if(!empty($listItems))
									$total_post = count($listItems);
								$total_items = 0;
								foreach ($listItems as $item){
									
									$total_items += count(qc_sbd_get_list_items( $item->ID ));
									
								}
								
							  ?>	
									<div class="sbd_total_lists">
										<?php
											if( (pd_ot_get_option('sbd_lan_lists') != '') && ($total_post>1) ){
												echo $total_post.pd_ot_get_option('sbd_lan_lists');
											}elseif( (pd_ot_get_option('sbd_lan_list') != '') && ($total_post<=1) ){
												echo $total_post.pd_ot_get_option('sbd_lan_list');
											}else{

												$list_text = sprintf( _n( '%s List', '%s Lists', $total_post, 'qc-pd' ), $total_post );
												echo $list_text;
											}
										?>
									</div>
									<div class="sbd_total_items">
										<?php
											if( (pd_ot_get_option('sbd_lan_items') != '') && ($total_items>1) ){
												echo $total_items.pd_ot_get_option('sbd_lan_items');
											}elseif( (pd_ot_get_option('sbd_lan_item') != '') && ($total_items<=1) ){
												echo $total_items.pd_ot_get_option('sbd_lan_item');
											}else{
 
												$item_text = sprintf( _n( '%s Item', '%s Items', $total_items, 'qc-pd' ), $total_items );
												echo $item_text;
											}
										?>
									</div>
								</div>
							</div>
						
						</li>
					<?php
					$ci++;					
				}
			?>
			</ul>
		</div>