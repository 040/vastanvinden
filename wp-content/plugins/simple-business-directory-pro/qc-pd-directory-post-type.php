<?php

/**
 * This function register new custom post type - pd
 *
 * @param void
 *
 * @return null
 */
function qcpd_register_cpt_pd() {
	//Register New Post Type
	$qc_list_labels = array(
		'name'               => _x( 'Manage List Items', 'qc-pd' ),
		'singular_name'      => _x( 'Manage List Item', 'qc-pd' ),
		'add_new'            => _x( 'New List', 'qc-pd' ),
		'add_new_item'       => __( 'Add New List Item','qc-pd' ),
		'edit_item'          => __( 'Edit List Item','qc-pd' ),
		'new_item'           => __( 'New List Item','qc-pd' ),
		'all_items'          => __( 'Manage List Items','qc-pd' ),
		'view_item'          => __( 'View List Item','qc-pd' ),
		'search_items'       => __( 'Search List ','qc-pd' ),
		'not_found'          => __( 'No List Item found','qc-pd' ),
		'not_found_in_trash' => __( 'No List Item found in the Trash','qc-pd' ), 
		'parent_item_colon'  => '',
		'menu_name'          => __('Simple Business Directory','qc-pd')
	);

	$qc_list_args = array(
		'labels'        => $qc_list_labels,
		'description'   => __('This post type holds all posts for your directory items.','qc-pd'),
		'public'        => true,
		'publicly_queryable' => false,
		'menu_position' => 25,
		'exclude_from_search' => true,
		'show_in_nav_menus' => false,
		'supports'      => array( 'title' ),
		'has_archive'   => true,
		'menu_icon' 	=> QCSBD_IMG_URL . '/menu_icon.png',
	);

	register_post_type( 'pd', $qc_list_args );	

	//Register New Taxonomy for Our New Post Type
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'List Categories', 'List Categories', 'qc-pd' ),
		'singular_name'     => _x( 'Category', 'taxonomy singular name', 'qc-pd' ),
		'search_items'      => __( 'Search List Categories', 'qc-pd' ),
		'all_items'         => __( 'All List Categories', 'qc-pd' ),
		'parent_item'       => __( 'Parent List Categories', 'qc-pd' ),
		'parent_item_colon' => __( 'Parent List Category:', 'qc-pd' ),
		'edit_item'         => __( 'Edit List Category', 'qc-pd' ),
		'update_item'       => __( 'Update List Category', 'qc-pd' ),
		'add_new_item'      => __( 'Add New List Category', 'qc-pd' ),
		'new_item_name'     => __( 'New List Category Name', 'qc-pd' ),
		'menu_name'         => __( 'List Categories', 'qc-pd' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'			=> false,
		'rewrite'           => array( 'slug' => 'pd_cat' ),
	);

	register_taxonomy( 'pd_cat', array( 'pd' ), $args );

}

/**
 * 
 * This hook register new custom post type and taxonomy for pd
 * Post Type: pd
 * Taxonomy: pd_cat
 *
 */
add_action( 'init', 'qcpd_register_cpt_pd' );


/**
 * 
 * Require CMB Metabox if it not exists already by some other
 * extensions.
 *
 */

if ( ! class_exists( 'CMB_Meta_Box' ) )
{
	require_once QCSBD_INC_DIR . '/cmb/custom-meta-boxes.php';
}

/**
 * Metabox for our custom post type - pd
 * This function enables all costm made metabox for the directory.
 *
 * @param array of meta fields
 *
 * @return array of meta fields
 */

function cmb_qcpd_dir_fields( array $meta_boxes ) {

	//Config Fields
	$le_fields = array(
		array( 'id' => 'list_border_color',  'name' => 'List Holder Color', 'type' => 'colorpicker', 'desc' => '(Normal State)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_bg_color',  'name' => 'Item Background Color', 'type' => 'colorpicker', 'desc' => '(Normal State)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_bg_color_hov',  'name' => 'Item Background Color', 'type' => 'colorpicker', 'desc' => '(On Mouseover)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_txt_color',  'name' => 'Item Text Color', 'type' => 'colorpicker', 'desc' => '(Normal State)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_txt_color_hov',  'name' => 'Item Text Color', 'type' => 'colorpicker', 'desc' => '(On Mouseover)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_subtxt_color',  'name' => 'Item Sub Text Color', 'type' => 'colorpicker', 'desc' => '(Normal State)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_subtxt_color_hov',  'name' => 'Item Sub Text Color', 'type' => 'colorpicker', 'desc' => '(On Mouseover)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'item_bdr_color',  'name' => 'Item Border Color', 'type' => 'colorpicker', 'desc' => '(Normal State)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'item_bdr_color_hov',  'name' => 'Item Border Color', 'type' => 'colorpicker', 'desc' => '(On Mouseover)', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'list_title_color',  'name' => 'List Title Color', 'type' => 'colorpicker', 'desc' => '', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'filter_background_color',  'name' => 'Filter Button Background Color', 'type' => 'colorpicker', 'desc' => '', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'filter_text_color',  'name' => 'Filter Button Text Color', 'type' => 'colorpicker', 'desc' => '', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'title_link',  'name' => 'Link List Title to a Page', 'type' => 'text', 'desc' => 'ex: http://example.com', 'cols' => 4, 'default' => '' ),
		array( 'id' => 'title_link_new_tab',  'name' => 'Open Link in New Tab', 'type' => 'checkbox', 'desc' => 'ex: http://example.com', 'cols' => 4, 'default' => 0 ),
	);

	$meta_boxes[] = array(
		'title' => 'List Configurations',
		'pages' => 'pd',
		'fields' => array(
			array(
				'id' => 'qcpd_list_conf',
				'name' => '',
				'type' => 'group',
				'repeatable' => false,
				'sortable' => false,
				'fields' => $le_fields,
				'desc' => ''
			)
		)
	);


	//Repeatable Fields
	$qcpd_item_fields = array(
		array( 'id' => 'qcpd_item_link',  'name' => __('Website Link (Ex: http://www.google.com)','qc-pd'), 'type' => 'text', 'cols' => 6 ),
		array( 'id' => 'qcpd_generate_title',  'name' => 'Generate info', 'type' => 'checkbox', 'cols' => 1 ),
		array( 'id' => 'qcpd_item_title', 'name' => __('Item Title','qc-pd'), 'type' => 'text', 'cols' => 5 ),
		
		array( 'id' => 'qcpd_item_subtitle',  'name' => __('Item Subtitle','qc-pd'), 'type' => 'text', 'cols' => 6 ),
		array( 'id' => 'qcpd_item_img_link',  'name' => 'Favicon / External Image / Direct Image Link', 'type' => 'text', 'cols' => 6 ),
		
		array( 'id' => 'qcpd_item_phone',  'name' => __('Main Phone Number (for tap to call) (Ex: 202-555-0178)','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		
		
		array( 'id' => 'qcpd_item_email', 'name' => __('Email Address (Ex: email@example.com)','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		array( 'id' => 'qcpd_item_full_address',  'name' => __('Full Address (for map)','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		
		
		array( 'id' => 'qcpd_item_latitude', 'desc'=>'', 'name' => __('Latitude','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		array( 'id' => 'qcpd_item_longitude', 'desc'=>'', 'name' => __('Longitude','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		array( 'id' => 'qcpd_item_location',  'name' => __('Locale/Designation','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		
		
		array( 'id' => 'qcpd_item_facebook', 'name' => (pd_ot_get_option('sbd_lan_facebook')!=''?pd_ot_get_option('sbd_lan_facebook'):__('Facebook Page','qc-pd')), 'type' => 'text_url', 'cols' => 4 ),
		array( 'id' => 'qcpd_item_twitter', 'name' => (pd_ot_get_option('sbd_lan_twitter')!=''?pd_ot_get_option('sbd_lan_twitter'):__('Twitter Page','qc-pd')), 'type' => 'text_url', 'cols' => 4 ),
		
		array( 'id' => 'qcpd_item_linkedin', 'name' => (pd_ot_get_option('sbd_lan_linkedin')!=''?pd_ot_get_option('sbd_lan_linkedin'):__('Linkedin/instagram Page','qc-pd')), 'type' => 'text_url', 'cols' => 4 ),
		
		array( 'id' => 'qcpd_item_yelp', 'name' => (pd_ot_get_option('sbd_lan_yelp')!=''?pd_ot_get_option('sbd_lan_yelp'):__('Yelp Page','qc-pd')), 'type' => 'text_url', 'cols' => 4 ),
		array( 'id' => 'qcpd_item_business_hour', 'name' => __('Business Hour (Ex: Mon - Fri 10:00am - 05:00pm)','qc-pd'), 'type' => 'text', 'cols' => 4 ),
		
		array( 'id' => 'qcpd_fa_icon', 'name' => __('Font Awesome Icon','qc-pd'), 'type' => 'text', 'cols' => 4, 'classes' => 'fa-popup-field' ),
		
		
		array( 'id' => 'qcpd_item_nofollow',  'name' => __('No Follow','qc-pd'), 'type' => 'checkbox', 'cols' => 4, 'default' => 0 ),
		array( 'id' => 'qcpd_item_newtab',  'name' => __('Open Link in a New Tab','qc-pd'), 'type' => 'checkbox', 'cols' => 4, 'default' => 0 ),
		array( 'id' => 'qcpd_new',  'name' => __('Mark Item as New','qc-pd'), 'type' => 'checkbox', 'cols' => 4, 'default' => 0, 'desc' => '' ),
		array( 'id' => 'qcpd_featured',  'name' => __('Mark Item as Featured','qc-pd'), 'type' => 'checkbox', 'cols' => 4, 'default' => 0, 'desc' => '' ),	
		
		
		
		array( 'id' => 'qcpd_item_img', 'name' => __('List Image','qc-pd'), 'type' => 'image', 'repeatable' => false, 'show_size' => false, 'cols' => 3, 'desc' => ''  ),
		array( 'id' => 'qcpd_item_marker', 'name' => __('Marker Image','qc-pd'), 'type' => 'image', 'repeatable' => false, 'show_size' => false, 'cols' => 3, 'desc' => ''  ),
		array( 'id' => 'qcpd_image_from_link',  'name' => __('Generate Image from Website Link','qc-pd'), 'type' => 'checkbox', 'cols' => 5, 'default' => 0, 'desc' => '' ),
		array( 'id' => 'qcpd_use_favicon',  'name' => 'Pick Image from the Direct Link', 'type' => 'checkbox', 'cols' => 4, 'default' => 0, 'desc' => '' ),	
		
		array( 'id' => 'qcpd_upvote_count',  'name' => 'Upvote Count', 'type' => 'text', 'cols' => 4, 'default' => '0' ),
		array( 'id' => 'qcpd_verified',  'name' => 'Verified Item', 'type' => 'text', 'cols' => 4, 'default' => '0' ),
		array( 'id' => 'qcpd_entry_time',  'name' => 'Entry Time', 'type' => 'text', 'cols' => 4, 'default' => ''.date("Y-m-d H:i:s").'' ),	
		array( 'id' => 'qcpd_timelaps',  'name' => 'Time Laps', 'type' => 'text', 'cols' => 4, 'default' => '' ),	
		array( 'id' => 'qcpd_paid',  'name' => 'Paid Item', 'type' => 'text', 'cols' => 4, 'default' => '' ),	
		array( 'id' => 'qcpd_is_bookmarked',  'name' => 'Is Bookmarked', 'type' => 'text', 'cols' => 4, 'default' => '0' ),
		
		array(
			'id'   => 'qcpd_description',
			'name' => __('Long Description (will show in lightbox and on multipage mode)','qc-pd'),
			'type' => 'wysiwyg',
			'options' => array(
				'textarea_rows' => 3
			)

		),
		array( 'id' => 'qcpd_other_list',  'name' => __('Copy this item to other Lists','qc-pd'), 'type' => 'text', 'cols' => 4, 'default' => '', 'desc' => '' ),
		array( 'id' => 'qcpd_unpublished',  'name' => __('Unpublish this Item','qc-pd'), 'type' => 'checkbox', 'cols' => 4, 'default' => 0, 'desc' => '' ),	
		array( 'id' => 'qcpd_tags',  'name' => __('Tags','qc-pd'), 'type' => 'text', 'cols' => 4, 'default' => '', 'desc' => '' ),
		array( 'id' => 'qcpd_ex_date',  'name' => __('Expire Date','qc-pd'), 'type' => 'text', 'cols' => 4, 'default' => '', 'desc' => '' ),
	);
	
	//code for custom field 1
	$qcpd_cs_1 = array();
	if(pd_ot_get_option('sbd_field_1_enable')=='on'){
		$qcpd_cs_1_label = pd_ot_get_option('sbd_field_1_label')!=''?pd_ot_get_option('sbd_field_1_label'):'Custom Field 1';
		$qcpd_cs_1 = array(
			array( 'id' => 'qcpd_field_1', 'name' => $qcpd_cs_1_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	
	//code for custom field 2
	$qcpd_cs_2 = array();
	if(pd_ot_get_option('sbd_field_2_enable')=='on'){
		$qcpd_cs_2_label = pd_ot_get_option('sbd_field_2_label')!=''?pd_ot_get_option('sbd_field_2_label'):'Custom Field 2';
		$qcpd_cs_2 = array(
			array( 'id' => 'qcpd_field_2', 'name' => $qcpd_cs_2_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for custom field 3
	$qcpd_cs_3 = array();
	if(pd_ot_get_option('sbd_field_3_enable')=='on'){
		$qcpd_cs_3_label = pd_ot_get_option('sbd_field_3_label')!=''?pd_ot_get_option('sbd_field_3_label'):'Custom Field 3';
		$qcpd_cs_3 = array(
			array( 'id' => 'qcpd_field_3', 'name' => $qcpd_cs_3_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for custom field 4
	$qcpd_cs_4 = array();
	if(pd_ot_get_option('sbd_field_4_enable')=='on'){
		$qcpd_cs_4_label = pd_ot_get_option('sbd_field_4_label')!=''?pd_ot_get_option('sbd_field_4_label'):'Custom Field 4';
		$qcpd_cs_4 = array(
			array( 'id' => 'qcpd_field_4', 'name' => $qcpd_cs_4_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for custom field 5
	$qcpd_cs_5 = array();
	if(pd_ot_get_option('sbd_field_5_enable')=='on'){
		$qcpd_cs_5_label = pd_ot_get_option('sbd_field_5_label')!=''?pd_ot_get_option('sbd_field_5_label'):'Custom Field 5';
		$qcpd_cs_5 = array(
			array( 'id' => 'qcpd_field_5', 'name' => $qcpd_cs_5_label, 'type' => 'text', 'cols' => 4 ),
		);
	}


	//code for multipage custom field 1
	$multipage_qcpd_cs_1 = array();
	if(pd_ot_get_option('multipage-sbd_field_1_enable')=='on'){
		$multipage_qcpd_cs_1_label = pd_ot_get_option('multipage-sbd_field_1_label')!=''?pd_ot_get_option('multipage-sbd_field_1_label'):'Multipage Custom Field 1';
		$multipage_qcpd_cs_1 = array(
			array( 'id' => 'multipage-qcpd_field_1', 'name' => $multipage_qcpd_cs_1_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	
	//code for multipage custom field 2
	$multipage_qcpd_cs_2 = array();
	if(pd_ot_get_option('multipage-sbd_field_2_enable')=='on'){
		$multipage_qcpd_cs_2_label = pd_ot_get_option('multipage-sbd_field_2_label')!=''?pd_ot_get_option('multipage-sbd_field_2_label'):'Multipage Custom Field 2';
		$multipage_qcpd_cs_2 = array(
			array( 'id' => 'multipage-qcpd_field_2', 'name' => $multipage_qcpd_cs_2_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for multipage custom field 3
	$multipage_qcpd_cs_3 = array();
	if(pd_ot_get_option('multipage-sbd_field_3_enable')=='on'){
		$multipage_qcpd_cs_3_label = pd_ot_get_option('multipage-sbd_field_3_label')!=''?pd_ot_get_option('multipage-sbd_field_3_label'):'Multipage Custom Field 3';
		$multipage_qcpd_cs_3 = array(
			array( 'id' => 'multipage-qcpd_field_3', 'name' => $multipage_qcpd_cs_3_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for multipage custom field 4
	$multipage_qcpd_cs_4 = array();
	if(pd_ot_get_option('multipage-sbd_field_4_enable')=='on'){
		$multipage_qcpd_cs_4_label = pd_ot_get_option('multipage-sbd_field_4_label')!=''?pd_ot_get_option('multipage-sbd_field_4_label'):'Multipage Custom Field 4';
		$multipage_qcpd_cs_4 = array(
			array( 'id' => 'multipage-qcpd_field_4', 'name' => $multipage_qcpd_cs_4_label, 'type' => 'text', 'cols' => 4 ),
		);
	}
	//code for multipage custom field 5
	$multipage_qcpd_cs_5 = array();
	if(pd_ot_get_option('multipage-sbd_field_5_enable')=='on'){
		$multipage_qcpd_cs_5_label = pd_ot_get_option('multipage-sbd_field_5_label')!=''?pd_ot_get_option('multipage-sbd_field_5_label'):'Multipage Custom Field 5';
		$multipage_qcpd_cs_5 = array(
			array( 'id' => 'multipage-qcpd_field_5', 'name' => $multipage_qcpd_cs_5_label, 'type' => 'text', 'cols' => 4 ),
		);
	}

	
	
	$qcpd_item_fields = array_merge($qcpd_item_fields,$qcpd_cs_5,$qcpd_cs_4,$qcpd_cs_3,$qcpd_cs_2,$qcpd_cs_1);
	$qcpd_item_fields = array_merge($qcpd_item_fields,$multipage_qcpd_cs_5,$multipage_qcpd_cs_4,$multipage_qcpd_cs_3,$multipage_qcpd_cs_2,$multipage_qcpd_cs_1);

	$meta_boxes[] = array(
		'title' => 'List Elements',
		'pages' => 'pd',
		'fields' => array(
			array(
				'id' => 'qcpd_list_item01',
				'name' => 'Create List Elements',
				'type' => 'group',
				'repeatable' => true,
				'sortable' => true,
				'fields' => $qcpd_item_fields,
				'desc' => ''
			)
		)
	);

	//Config Fields
	$le_fields2 = array(
		array( 'id' => 'add_block_text',  'name' => 'Raw Codes or Texts', 'type' => 'wysiwyg', 'desc' => 'This content will be displayed after list elements. Please paste your codes in TEXT mode only. You can use general text contents in both mode.', 'rows' => 4, 'default' => '' ),
	);

	$meta_boxes[] = array(
		'title' => 'Text/Ad Block',
		'pages' => 'pd',
		'fields' => array(
			array(
				'id' => 'pd_add_block',
				'name' => '',
				'type' => 'group',
				'repeatable' => false,
				'sortable' => false,
				'fields' => $le_fields2,
				'desc' => ''
			)
		)
	);

	return $meta_boxes;

}

add_filter( 'cmb_meta_boxes', 'cmb_qcpd_dir_fields' );

/**
 * Custom Columns for Directory Listing in the backend
 *
 * @param default column fields
 *
 * @return all column fields after modification
 */
function qcpd_list_columns_head($defaults) {

    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = __('Title');

    $new_columns['qcpd_item_count'] = 'Number of Elements';
    $new_columns['qcpd_item_category'] = 'Categories';
    $new_columns['shortcode_col'] = 'Shortcode';

    $new_columns['date'] = __('Date');

    return $new_columns;
}
//end of function qcpd_list_columns_head
 
/**
 * Custom Column values for Backend SLD post Listing
 *
 * @param column_name, post_ID
 *
 * @return null
 */
function qcpd_list_columns_content($column_name, $post_ID) {
    

    //Item Elements Count
    if ($column_name == 'qcpd_item_count') {
        echo count(get_post_meta( $post_ID, 'qcpd_list_item01' ));
    }

    //Item Categories
    if ($column_name == 'qcpd_item_category') {

        $terms = get_the_terms( $post_ID, 'pd_cat' );

        /* If terms were found. */
        if ( !empty( $terms ) ) {

            $out = array();

            /* Loop through each term, linking to the 'edit posts' page for the specific term. */
            foreach ( $terms as $term ) {
                $out[] = sprintf( '<a href="%s">%s</a>',
                    esc_url( add_query_arg( array( 'post_type' => 'pd', 'pd_cat' => $term->slug ), 'edit.php' ) ),
                    esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'pd_cat', 'display' ) )
                );
            }

            /* Join the terms, separating them with a comma. */
            echo join( ', ', $out );
        }

    }

    //Generated Shortcode
    if ($column_name == 'shortcode_col') {
        echo '[qcpd-directory mode="one" list_id="'.$post_ID.'"]';
    }

}
//end of function qcpd_list_columns_content

add_filter('manage_pd_posts_columns', 'qcpd_list_columns_head');
add_action('manage_pd_posts_custom_column', 'qcpd_list_columns_content', 10, 2);


//Filter by Custom Taxonomy
add_action( 'restrict_manage_posts', 'qcpd_sbd_restrict_manage_posts' );

/**
 * This function enable custom filtering by custom taxonomy type 
 * in the backend post listing
 *
 * @param void
 *
 * @return null
 */
function qcpd_sbd_restrict_manage_posts() {

    // only display these taxonomy filters on desired custom post_type listings
    global $typenow;

    if ($typenow == 'pd') {

        // create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
        $filters = array('pd_cat');

        foreach ($filters as $tax_slug) {
            // retrieve the taxonomy object
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            // retrieve array of term objects per taxonomy
            $terms = get_terms($tax_slug);

            // output html for taxonomy dropdown filter
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>Show All $tax_name</option>";
            foreach ($terms as $term) {
                // output each select option line, check against the last $_GET to show the current option selected
            	if( isset($_GET[$tax_slug]) && $_GET[$tax_slug] == $term->slug ){
	                echo '<option value="'. $term->slug. '" selected="selected">' . $term->name .' (' . $term->count .')</option>';
            	}else{
            		echo '<option value="'. $term->slug. '" >' . $term->name .' (' . $term->count .')</option>';
            	}
            }
            echo "</select>";
        }
    }
    
}


