<?php

/**
 * 1. Adds SBDQcOpdMostPopular_Widget widget.
 */
class SBDQcOpdMostPopular_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'SBDQcOpdMostPopular_Widget',
			esc_html__( 'SBD - Most Popular Links', 'qc-pd' ),
			array( 
				'description' => esc_html__( 'Widget to display most popular list items from - simple link directory.', 'qc-pd' ),
			)
		);
		if(is_active_widget(false, false, $this->id_base))
			{
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		$limit = 5;

		if ( ! empty( $instance['limit'] ) ) {
			$limit = $instance['limit'];
		}

		echo qcpd_get_most_popular_links_wi( $limit );

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Popular Links', 'qc-pd' );
		$limit = ! empty( $instance['limit'] ) ? $instance['limit'] : esc_html__( '5', 'qc-pd' );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_attr_e( 'Title:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>">
				<?php esc_attr_e( 'Limit:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '';

		return $instance;
	}

} // class SBDQcOpdMostPopular_Widget


/**
 * 2. Adds SBDQcOpdRandomLinks_Widget widget.
 */
class SBDQcOpdRandomLinks_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'SBDQcOpdRandomLinks_Widget',
			esc_html__( 'SBD - Random Links', 'qc-pd' ),
			array( 
				'description' => esc_html__( 'Widget to display randomly picked list items from - simple link directory.', 'qc-pd' ),
			)
		);
		if(is_active_widget(false, false, $this->id_base))
			{
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		$limit = 5;

		if ( ! empty( $instance['limit'] ) ) {
			$limit = $instance['limit'];
		}

		echo qcpd_get_random_links_wi( $limit );

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Random Links', 'qc-pd' );
		$limit = ! empty( $instance['limit'] ) ? $instance['limit'] : esc_html__( '5', 'qc-pd' );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_attr_e( 'Title:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>">
				<?php esc_attr_e( 'Limit:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '';

		return $instance;
	}

} // class SBDQcOpdRandomLinks_Widget


/**
 * 3. Adds QcOpdLatestLinks_Widget widget.
 */
class SBDQcOpdLatestLinks_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'SBDQcOpdLatestLinks_Widget',
			esc_html__( 'SBD - Latest Links', 'qc-pd' ),
			array( 
				'description' => esc_html__( 'Widget to display the most recent list items from - simple link directory.', 'qc-pd' ),
			)
		);
		if(is_active_widget(false, false, $this->id_base))
			{
				add_action('wp_enqueue_scripts', 'qcpd_load_global_scripts');
				add_action( 'wp_enqueue_scripts', 'sbd_my_scripts', 20, 1);
			}
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		$limit = 5;

		if ( ! empty( $instance['limit'] ) ) {
			$limit = $instance['limit'];
		}

		echo qcpd_get_latest_links_wi( $limit );

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Latest Links', 'qc-pd' );
		$limit = ! empty( $instance['limit'] ) ? $instance['limit'] : esc_html__( '5', 'qc-pd' );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
				<?php esc_attr_e( 'Title:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>">
				<?php esc_attr_e( 'Limit:', 'qc-pd' ); ?>
			</label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'limit' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'limit' ) ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? strip_tags( $new_instance['limit'] ) : '';

		return $instance;
	}

} // class QcOpdLatestLinks_Widget


// Register Widgets
function qcpd_register_custom_widgets() {
    register_widget( 'SBDQcOpdMostPopular_Widget' );
    register_widget( 'SBDQcOpdRandomLinks_Widget' );
    register_widget( 'SBDQcOpdLatestLinks_Widget' );
}

add_action( 'widgets_init', 'qcpd_register_custom_widgets' );