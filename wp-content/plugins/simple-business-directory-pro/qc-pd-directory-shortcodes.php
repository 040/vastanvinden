<?php
defined('ABSPATH') or die("No direct script access!");



/* URL Filtering Logic, to remove http:// or https:// */
function qcpd_remove_http($url) {
   $disallowed = array('http://', 'https://');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         return str_replace($d, '', $url);
      }
   }
   return trim($url);
}

/*Custom Item Sort Logic*/
function pd_custom_sort_by_tpl_upvotes($a, $b) {
    return $a['qcpd_upvote_count'] * 1 < $b['qcpd_upvote_count'] * 1;
}

function pd_custom_sort_by_tpl_title($a, $b) {
    return strnatcasecmp($a['qcpd_item_title'], $b['qcpd_item_title']);
}

function pd_custom_sort_by_tpl_timestamp($a, $b) {
	if( isset($a['qcpd_timelaps']) && isset($b['qcpd_timelaps']) )
	{
		$aTime = (int)$a['qcpd_timelaps'];
		$bTime = (int)$b['qcpd_timelaps'];
		return $aTime < $bTime;
	}
}

//For all list elements
add_shortcode('qcpd-directory', 'qcpd_directory_full_shortcode');
add_shortcode('qcpnd-directory', 'qcpd_directory_full_shortcode');

function qcpd_directory_full_shortcode( $atts = array() )
{
	ob_start();
    show_qcpd_full_list( $atts );
    $content = ob_get_clean();
    return $content;
}

function show_qcpd_full_list( $atts = array() )
{
	$template_code = "";

	//Defaults & Set Parameters
	extract( shortcode_atts(
		array(
			'orderby' => 'menu_order',
			'filterorderby' => 'menu_order',
			'order' => 'ASC',
			'filterorder' => 'ASC',
			'mode' => 'all',
			'list_id' => '',
			'column' => '1',
			'style' => 'simple',
			'list_img' => 'true',
			'search' => 'true',
			'radius' => '',
			'distance_search' => '',
			'category' => "",
			'upvote' => "on",
			'item_count' => "on",
			'top_area' => "on",
			'item_orderby' => "",
			'item_order' => "",
			'mask_url' => "off",
			'tooltip' => 'false',
			'paginate_items' => 'false',
			'per_page' => 5,
			'list_title_font_size' => '' ,
			'list_title_line_height' => '' ,
			'cattabid'		=> '',
            'title_font_size' => '',
            'subtitle_font_size' => '',
            'title_line_height' => '',
            'subtitle_line_height' => '',
            'filter_area' => 'normal',
            'topspacing' => 0,
            'infinityscroll' => 0,
            'itemperpage' => 5,
            'map' => 'show',
            'showmaponly' => 'no',
			'enable_embedding' => 'true',
			'main_click_action'=>1,
			'phone_number'=>1,
			'enable_rtl'=> 'false',
			'enable_left_filter'=>'false',
			'enable_tag_filter'	=> 'false',
			'enable_tag_filter_dropdown'	=> 'false',
			'enable_map_fullwidth'=>'false',
			'enable_map_fullscreen'=>'false',
			'map_position'=> 'top',
			'pdmapofflightbox'=>'false',
			'marker_cluster'=>'false',
			'image_infowindow'=>'false',
			'enable_map_toggle_filter'=>'false',
			'item_details_page'	=> 'off',
			'hide_list_title' => 'false',
			'review' => 'false',
			'actual_pagination'	=>	'false',
			'mailto'	=>	'false',
			'map_marker_scroll'	=>	'true',

		), $atts
	));



	//ShortCode Atts
	$shortcodeAtts = array(
		'orderby' => $orderby,
		'order' => $order,
		'mode' => $mode,
		'list_id' => $list_id,
		'column' => $column,
		'style' => $style,
		'list_img' => $list_img,
		'search' => $search,
		'category' => $category,
		'upvote' => $upvote,
		'item_count' => $item_count,
		'top_area' => $top_area,
		'item_orderby' => $item_orderby,
		'item_order' => $item_order,
		'mask_url' => $mask_url,
		'tooltip' => $tooltip,
        'list_title_font_size' => $list_title_font_size ,
        'list_title_line_height' => $list_title_line_height ,
        'title_font_size' => $title_font_size,
        'subtitle_font_size' => $subtitle_font_size,
        'title_line_height' => $title_line_height,
        'subtitle_line_height' => $subtitle_line_height,
        'filter_area' => $filter_area,
        'topspacing' => $topspacing,
		'map'=>$map,
        'showmaponly' => $showmaponly,
        'hide_list_title' => $hide_list_title,
		'distance_search' => $distance_search,
		'paginate_items' => $paginate_items,
		'per_page' => $per_page,
		'cattabid' => $cattabid,
		'infinityscroll' => $infinityscroll,
		'itemperpage' => $itemperpage,
		'enable_embedding' => $enable_embedding,
		'main_click_action' => $main_click_action,
		'phone_number' => $phone_number,
		'enable_rtl' => $enable_rtl,
		'enable_left_filter' => $enable_left_filter,
		'enable_tag_filter' => $enable_tag_filter,
		'enable_tag_filter_dropdown' => $enable_tag_filter_dropdown,
		'enable_map_fullwidth' => $enable_map_fullwidth,
		'enable_map_fullscreen' => $enable_map_fullscreen,
		'map_position' => $map_position,
		'pdmapofflightbox' => $pdmapofflightbox,
		'marker_cluster' => $marker_cluster,
		'image_infowindow' => $image_infowindow,
		'enable_map_toggle_filter' => $enable_map_toggle_filter,
		'item_details_page' => $item_details_page,
		'review' => $review,
		'filterorderby'	=>	$filterorderby,
		'actual_pagination' => $actual_pagination,
		'mailto'	=>	$mailto,
		'map_marker_scroll'	=>	$map_marker_scroll,
	);

	$limit = -1;

	if( $map != 'hide' ){
		wp_enqueue_script( 'qcopd-google-map-script');
		wp_enqueue_script( 'qcopd-google-map-scriptasd');
	}
	wp_enqueue_script( 'qcpd-custom-script');

	if( $mode == 'one' )
	{
		$limit = 1;
	}
	
	$radius = $distance_search;
	
	if($style=="simple" && $infinityscroll==1){
		$list_args_total = array(
			'post_type' => 'pd',
			'orderby' => $orderby,
			'order' => $order,
			'posts_per_page' => -1,
		);
		$total_list_query = new WP_Query( $list_args_total );
		$count = $total_list_query->post_count;
		$total_page_count = ceil($count/$itemperpage);
		
		//Query Parameters
		$list_args = array(
			'post_type' => 'pd',
			'posts_per_page' => $itemperpage,
			'paged'			=> 1
			
		);
	}else{
		//Query Parameters
		$list_args = array(
			'post_type' => 'pd',
			'orderby' => $orderby,
			'order' => $order,
			'posts_per_page' => $limit,
			
		);
	}

	if($radius=='' and pd_ot_get_option('sbd_enable_distant_search')=='on'){
		$radius = "true";
		$distance_search = "true";
	}

	
	if( $list_id != "" && $mode == 'one' )
	{
		$list_args = array_merge($list_args, array( 'p' => $list_id ));
	}

	if( $category != "" )
	{
		$category = explode(',',$category);
		$taxArray = array(
			array(
				'taxonomy' => 'pd_cat',
				'field'    => 'slug',
				'terms'    => $category,
			),
		);

		$list_args = array_merge($list_args, array( 'tax_query' => $taxArray ));

	}
	


	$sld_cluster = array(
		'cluster'			=>($marker_cluster=='true'?true:false),
		'image_infowindow'	=> ($image_infowindow=='true'?true:false),
		'pagination'		=> ($paginate_items === 'true'?'true':'false'),
		'per_page'			=> $per_page,
		'main_click' 		=> $main_click_action,
		'map' 				=> ($map=='show'?'true':'false'),
		'map_marker_scroll' => ($map_marker_scroll=='true'?'true':'false'),
		'distance_search' 	=> ($distance_search=='true'?'true':'false')
	);	

	wp_localize_script('qcpd-custom-script', 'cluster', $sld_cluster);

	// The Query
	$list_query = new WP_Query( $list_args );

    if ( isset($atts["style"]) && $atts["style"] )
        $template_code = $atts["style"];

    if (!$template_code)
        $template_code = "simple";

    if( $mode == 'one' and $template_code!='style-13' and $template_code!='style-11' and $template_code!='style-10' ){
    	//$column = '1';
    }
	$customjs = pd_ot_get_option( 'pd_custom_js' );

	if($topspacing==''){
		$topspacing = 0;
	}

?>
<script style="text/javascript">
var pduserMessage= '<?php echo pd_ot_get_option('pd_bookmark_popup_content');?>';
</script>

<?php
if($customjs!=''):
?>
<script type="text/javascript">
jQuery(document).ready(function($)
{
<?php echo $customjs; ?>

})
</script>
<?php
endif;
?>

<?php if($enable_map_fullwidth=='true' && @$cattabid=='' && $map_position!='right'): ?>
<script type="text/javascript">
function getOffset1( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}
jQuery(window).on('load',function(){
	
	var fullwidth = jQuery("body").prop("clientWidth"); 
	if(jQuery('.sbd_map').length > 0){
		var maindivcon = jQuery('.sbd_map').parent()[0];
		var getleft = getOffset1(maindivcon);
		jQuery('.sbd_map').css({
			'width':fullwidth+'px',		
			'left':'-'+getleft.left+'px',
		});
		
		
		jQuery(window).resize(function() {
			var fullwidth = jQuery("body").prop("clientWidth"); 
			var maindivcon = jQuery('.sbd_map').parent()[0];
			var getleft = getOffset1(maindivcon);
			jQuery('.sbd_map').css({
				'width':fullwidth+'px',		
				'left':'-'+getleft.left+'px',
			});
		});
	}
	
	if(jQuery('.sbd_maponly').length > 0){
		var maindivcon = jQuery('.sbd_maponly').parent()[0];
		var getleft = getOffset1(maindivcon);
		jQuery('.sbd_maponly').css({
			'width':fullwidth+'px',		
			'left':'-'+getleft.left+'px',
		});
		
		
		jQuery(window).resize(function() {
			var fullwidth = jQuery("body").prop("clientWidth"); 
			var maindivcon = jQuery('.sbd_maponly').parent()[0];
			var getleft = getOffset1(maindivcon);
			jQuery('.sbd_maponly').css({
				'width':fullwidth+'px',		
				'left':'-'+getleft.left+'px',
			});
		});
	}
	
	
})
</script>
<?php endif; ?>

<?php if($enable_map_fullscreen=='true' && @$cattabid=='' && $map_position!='right'): ?>
<script type="text/javascript">
function getOffset1( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}
jQuery(window).on('load',function(){
	
	var fullwidth = jQuery("body").prop("clientWidth"); 
	var fullheight = jQuery(window).height(); 
	if(jQuery('.sbd_map').length > 0){
		var maindivcon = jQuery('.sbd_map').parent()[0];
		var getleft = getOffset1(maindivcon);
		jQuery('.sbd_map').css({
			'width':fullwidth+'px',		
			// 'height':(fullheight - jQuery('.sbd_map').offset().top)+'px',		
			'height':(fullheight)+'px',		
			'left':'-'+getleft.left+'px',
		});
		
		
		jQuery(window).resize(function() {
			var fullwidth = jQuery("body").prop("clientWidth"); 
			var fullheight = jQuery(window).height(); 
			var maindivcon = jQuery('.sbd_map').parent()[0];
			var getleft = getOffset1(maindivcon);
			jQuery('.sbd_map').css({
				'width':fullwidth+'px',		
				// 'height':(fullheight - jQuery('.sbd_map').offset().top)+'px',		
				'height':(fullheight)+'px',	
				'left':'-'+getleft.left+'px',
			});
		});
	}
	
	if(jQuery('.sbd_maponly').length > 0){
		var maindivcon = jQuery('.sbd_maponly').parent()[0];
		var getleft = getOffset1(maindivcon);
		jQuery('.sbd_maponly').css({
			'width':fullwidth+'px',		
			'height':(fullheight - jQuery('.sbd_maponly').offset().top)+'px',		
			'left':'-'+getleft.left+'px',
		});
		
		
		jQuery(window).resize(function() {
			var fullwidth = jQuery("body").prop("clientWidth"); 
			var fullheight = jQuery(window).height(); 
			var maindivcon = jQuery('.sbd_maponly').parent()[0];
			var getleft = getOffset1(maindivcon);
			jQuery('.sbd_maponly').css({
				'width':fullwidth+'px',		
				'height':(fullheight - jQuery('.sbd_maponly').offset().top)+'px',		
				'left':'-'+getleft.left+'px',
			});
		});
	}
	
	
})
</script>
<?php endif; ?>


<style type="text/css">
.pd_scrollToTop{
	width: 30px;
    height: 30px;
    padding: 10px !important;
    text-align: center;
    font-weight: bold;
    color: #444;
    text-decoration: none;
    position: fixed;
    top: 88%;
    right: 29px;
    display: none;
    background: url('<?php echo QCSBD_IMG_URL;?>/up-arrow.ico') no-repeat 5px 5px;
    background-size: 20px 20px;
    text-indent: -99999999px;
    background-color: #ddd;
    border-radius: 3px;
	z-index:9999999999;
	box-sizing: border-box;
	
}
.pd_scrollToTop:hover{
text-decoration:none;
}
.sbd-filter-area{z-index: 99 !important;
    padding: 10px 0px;
    
}
.qc-grid-item h2{
    <?php if($list_title_font_size!=''){ ?> font-size:<?php echo $list_title_font_size; ?>;<?php } ?>
<?php if($list_title_line_height!=''){ ?> line-height:<?php echo $list_title_line_height; ?>;<?php } ?>
}

<?php if(pd_ot_get_option('sbd_enable_direction')!='on'): ?>
.pd-map{display:none !important;}
<?php endif; ?>

<?php if(pd_ot_get_option('sbd_map_height')!=''): 
	if($enable_map_fullscreen!='true'):
?>
.sbd_map, .sbd_maponly{height: <?php echo pd_ot_get_option('sbd_map_height'); ?> ;}
<?php 
endif; 
endif; 
?>


</style>
<?php if(pd_ot_get_option('pd_enable_scroll_to_top')=='on'): ?>
<a href="#"class="pd_scrollToTop">Scroll To Top</a>
<script type="text/javascript">
jQuery(document).ready(function($){
  $(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.pd_scrollToTop').fadeIn();
		} else {
			$('.pd_scrollToTop').fadeOut();
		}
	});

	//Click event to scroll to top
	$('.pd_scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

<?php if($filter_area=='fixed'): ?>
    if($("body").prop("clientWidth")>500){
        $(".sbd-filter-area").sticky({ topSpacing: <?php echo $topspacing; ?>, center:true });
    }
<?php endif; ?>
})
</script>
<?php endif; ?>
<?php
 if(pd_ot_get_option('pd_use_global_thumbs_up')!=''){
     $pd_thumbs_up = pd_ot_get_option('pd_use_global_thumbs_up');
 }else{
     $pd_thumbs_up = 'fa-thumbs-up';
 }
?>

<?php
	
	$sbd_fields_access = pd_ot_get_option('sbd_list_item_fields_show_only_loggedin_users');
	$search_text = '';
	if( isset($_POST['qcpd_searchtext']) ){ $search_text = $_POST['qcpd_searchtext']; }

	if ( file_exists( trailingslashit( get_stylesheet_directory() ) . 'sbd/templates/'.$template_code.'/template.php' ) ) {
		$current_template_path = get_stylesheet_directory_uri() . '/sbd/templates/';
		$qc_sbd_dir = trailingslashit( get_stylesheet_directory() ) . 'sbd/';
	// Check parent theme next
	} elseif ( file_exists( trailingslashit( get_template_directory() ) . 'sbd/templates/'.$template_code.'/template.php' ) ) {
		$current_template_path = get_template_directory_uri() . '/sbd/templates/';
		$qc_sbd_dir = trailingslashit( get_template_directory() ) . 'sbd/';

	// Check theme compatibility last
	} elseif ( file_exists( QCSBD_DIR ."/templates/".$template_code."/template.php" ) ) {
		$current_template_path = QCSBD_DIR ."/templates/";
    	$qc_sbd_dir = QCSBD_DIR;
	}

    $tempath = QCSBD_DIR ."/templates/".$template_code."/template.php";

	if($showmaponly=='yes'){
		$tempath = QCSBD_DIR ."/templates/maponly/template.php";
	}

	if( $search !== 'true' ){
	?>
		<form style="display: none;" id="live-search" action="" class="styled" method="post">
			<input type="text" class="text-input pd-search pd_search_filter" placeholder=""/>
		</form>
	<?php
	}

	if( file_exists( $tempath ) ){

    	require ( $tempath );
	}

	wp_reset_query();

	

	$customCss = pd_ot_get_option( 'pd_custom_style' );

	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>
<?php endif;
	if($search_text != ''){
?>
		<script type="text/javascript">
			jQuery(window).on('load',function(){
				setTimeout(function(){
					jQuery('#live-search input[type="text"]').val('<?php echo $search_text; ?>');
					jQuery('#live-search input[type="text"]').trigger('keyup');
				},100);
			});
		</script>
<?php
	}
	
}

/*TinyMCE button for Inserting Shortcode*/
/* Add Slider Shortcode Button on Post Visual Editor */
function qcpdpd_tinymce_button_function() {
	add_filter ("mce_external_plugins", "qcpd_pd_btn_js");
	add_filter ("mce_buttons", "qcpd_pd_btn");
}

function qcpd_pd_btn_js($plugin_array) {
	$plugin_array['qcpdpdbtn'] = plugins_url('assets/js/qcpd-tinymce-button.js', __FILE__);
	return $plugin_array;
}

function qcpd_pd_btn($buttons) {
	array_push ($buttons, 'qcpdpdbtn');
	return $buttons;
}

//add_action('init', 'qcpdpd_tinymce_button_function');


add_shortcode('qcpd-searchbar', 'qcpd_searchbar_function');
function qcpd_searchbar_function( $atts, $content = null ){
	$params = shortcode_atts( array(
					'post_id' => 0,
					'placeholder' => 'Search for your Items',
					'search_text' => '',
					'clear'	=>	'false'
				), $atts, 'qcpd-searchbar');
	$action_url = get_permalink($params['post_id']);
	ob_start();
	if( $params['clear'] == 'true' ){
?>
		<div class="pd-half">
	        <form action="<?php echo $action_url; ?>" class="styled sbd_clearable" method="post">
	            <input name="qcpd_searchtext" type="text" class="text-input pd-search pd_search_filter" placeholder="<?php echo $params['placeholder']; ?>"/>
	            <i class="sbd_clearable__clear">&times;</i>
	        </form>
	    </div>
<?php
	}else{
?>
		<div class="pd-half">
	        <form action="<?php echo $action_url; ?>" class="styled" method="post">
	            <input name="qcpd_searchtext" type="text" class="text-input pd-search pd_search_filter" placeholder="<?php echo $params['placeholder']; ?>"/>
	            <?php

            	$pd_global_search_icon = pd_ot_get_option('pd_global_search_icon') ? pd_ot_get_option('pd_global_search_icon') : 'fa-search';
            	?> 
	            <button type="submit" class="pb_submit_btn" value="submit"> <i class="fa <?php echo esc_html($pd_global_search_icon); ?>" ></i></button>

            	<style>
            		
                    .styled .pb_submit_btn {
                        padding: 0px 0px 0px 0px;
                        margin: 0px 0px 0px 0px;
                        line-height: 15px;
                        font-size: 15px;
                        vertical-align: middle;
                        color: #333;
                        background: #ddd;
                        width: 20px;
                        height: 20px;
                        background: transparent;
                        position: relative;
                        left: -22px;
                        cursor: pointer;
                        z-index: 99;
                        border:0px solid transparent;
                    }
                    .styled p {
					    display: inline-block;
					}
            	</style>
	        </form>
	    </div>
<?php
	}
	add_action('wp_footer', 'pd_global_search_script');
	return ob_get_clean();
}

function pd_global_search_script(){
	?>
		<script type="text/javascript">
			/**
			 * Clearable text inputs
			 */
			 jQuery(window).on('load', function(){
				jQuery(".sbd_clearable").each(function() {
				  var inp = jQuery(this).find("input:text"),
				      cle = jQuery(this).find(".sbd_clearable__clear");

				  inp.on("input", function(){
				    cle.toggle(!!this.value);
				  });
				  
				  cle.on("click", function(e) {
				    e.preventDefault();
				    inp.val("").trigger("input");
				  });
				  
				}); //End Clearable
			 });
		</script>
	<?php
}

add_action( 'dynamic_sidebar_params', 'sbd_dynamic_sidebar_params', 1, 1 );
function sbd_dynamic_sidebar_params( $content )
{
    $tag = 'sbd_single_item';
    if ( shortcode_exists( $tag ) ){
        do_action('sbd_single_item_shortcode_rendered_on_widget');
    }

    return $content;
}

add_action('sbd_single_item_shortcode_rendered_on_widget', 'sbd_single_item_shortcode_rendered_on_widget');
function sbd_single_item_shortcode_rendered_on_widget(){
	qcpd_load_global_scripts();
	qcsbd_dashboard_css();
	sbd_my_scripts();
}

add_shortcode('sbd_single_item', 'sbd_single_item_function');
function sbd_single_item_function($atts){
	$params = shortcode_atts( array(
					'item_id' => 0,
					'list_id' => 0,
					'height' => '600px',
					'width' => '100%'
				), $atts, 'qcpd-searchbar');
	ob_start();
	$list_items = get_post_meta( $params['list_id'], 'qcpd_list_item01', false );

	wp_enqueue_script( 'qcopd-google-map-script');
	wp_enqueue_script( 'qcopd-google-map-scriptasd');
	wp_enqueue_script( 'qcpd-custom-script');

	foreach ($list_items as $item_key => $list_item) {
		$timelaps =  $list_item['qcpd_timelaps'];
		
		if( $timelaps == $params['item_id'] ){
?>
		<div class="sbd-single-item-container sbd-single-item-container-<?php echo $params['item_id']; ?>">
			<div style="width: <?php echo $params['width']; ?>; height: <?php echo $params['height']; ?>" class="sbd-single-item-map" id="sbd-single-item-map-<?php echo $params['item_id']; ?>"></div>
			<div data-latitude="<?php echo $list_item['qcpd_item_latitude']; ?>" data-longitude="<?php echo $list_item['qcpd_item_longitude']; ?>" id="sbd-single-<?php echo $params['item_id'].'-'.time(); ?>" class="sbd-single-item sbd-single-item-<?php echo $params['item_id']; ?>">
				<div class="sbd-single-item-img sbd_pop_img">
					<a href="<?php echo $list_item['qcpd_item_link']; ?>" target="_blank" >
						<?php
							$iconClass = (isset($list_item['qcpd_fa_icon']) && trim($list_item['qcpd_fa_icon']) != "") ? $list_item['qcpd_fa_icon'] : "";

							$showFavicon = (isset($list_item['qcpd_use_favicon']) && trim($list_item['qcpd_use_favicon']) != "") ? $list_item['qcpd_use_favicon'] : "";

							$faviconImgUrl = "";
							$faviconFetchable = false;
							$filteredUrl = "";

							$directImgLink = (isset($list_item['qcpd_item_img_link']) && trim($list_item['qcpd_item_img_link']) != "") ? $list_item['qcpd_item_img_link'] : "";

							if( $showFavicon == 1 )
							{
								$filteredUrl = qcpd_remove_http( $item_url );

								if( $item_url != '' )
								{

									$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
								}

								if( $directImgLink != '' )
								{

									$faviconImgUrl = trim($directImgLink);
								}

								$faviconFetchable = true;

								if( $item_url == '' && $directImgLink == '' ){
									$faviconFetchable = false;
								}
							}
						?>

							<!-- Image, If Present -->
						<?php if( isset($list_item['qcpd_item_img'])  && $list_item['qcpd_item_img'] != "" ) : ?>
							<?php 
								if (strpos($list_item['qcpd_item_img'], 'http') === FALSE){
							?>
							<span class="ca-icon list-img-1">
								<?php
									$img = wp_get_attachment_image_src($list_item['qcpd_item_img']);
								?>
								<img src="<?php echo $img[0]; ?>" alt="<?php echo $list_item['qcpd_item_title']; ?>">
							</span>
							<?php
								}else{
							?>
							<span class="ca-icon list-img-1">
								<img src="<?php echo $list_item['qcpd_item_img']; ?>" alt="<?php echo $list_item['qcpd_item_title']; ?>">
							</span>
							<?php
								}
							?>
						<?php elseif( $iconClass != "" ) : ?>
							<span class="ca-icon list-img-1">
								<i class="fa <?php echo $iconClass; ?>"></i>
							</span>
						<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>
							<span class="ca-icon list-img-1 favicon-loaded">
								<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list_item['qcpd_item_title']; ?>">
							</span>
						<?php else : ?>
							<span class="ca-icon list-img-1">
								<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list_item['qcpd_item_title']; ?>">
							</span>
						<?php endif; ?>
					</a>
				</div>
				<div class="sbd_pop_text">
					<div class="sbd-single-item-title">
						<h3><?php echo $list_item['qcpd_item_title']; ?></h3>
					</div>
					<div class="sbd-single-item-subtitle">
						<p><?php echo $list_item['qcpd_item_subtitle']; ?></p>
					</div>
					<div class="sbd-single-item-address">
						<p><b><i class="fa fa-map-marker fa-map-marker-alt" aria-hidden="true"></i> </b>
						<?php echo $list_item['qcpd_item_full_address']; ?></p>
					</div>
				</div>

				<div class="pd-bottom-area">
			
					<?php if(isset($list_item['qcpd_item_phone']) and $list_item['qcpd_item_phone']!='' and $phone_number!=0): ?>
						<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list_item['qcpd_item_phone']); ?>" title="Call <?php echo $list_item['qcpd_item_phone']; ?>"><?php sbd_icons_content('phone'); ?></a></p>
					<?php endif; ?>
					
					<?php if($list_item['qcpd_item_link']!=''): ?>
						<p><a href="<?php echo $list_item['qcpd_item_link']; ?>" target="_blank" title="<?php echo pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website'; ?>"><?php sbd_icons_content('link'); ?></a></p>
					<?php endif; ?>
					
					<?php if(isset($list_item['qcpd_item_facebook']) and $list_item['qcpd_item_facebook']!=''): ?>
					<p><a target="_blank" href="<?php echo (isset($list_item['qcpd_item_facebook']) && $list_item['qcpd_item_facebook']!=''?trim($list_item['qcpd_item_facebook']):'#'); ?>"><?php sbd_icons_content('facebook'); ?></a></p>
					<?php endif; ?>
					
					<?php if(isset($list_item['qcpd_item_yelp']) and $list_item['qcpd_item_yelp']!=''): ?>
					<p><a target="_blank" href="<?php echo (isset($list_item['qcpd_item_yelp']) && $list_item['qcpd_item_yelp']!=''?trim($list_item['qcpd_item_yelp']):'#'); ?>"><?php sbd_icons_content('yelp'); ?></a></p>
					<?php endif; ?>
					
					<?php if(isset($list_item['qcpd_item_email']) and $list_item['qcpd_item_email']!=''): ?>
					<p><a data-email="<?php echo (isset($list_item['qcpd_item_email']) && $list_item['qcpd_item_email']!=''?trim($list_item['qcpd_item_email']):'#'); ?>" class="sbd_email_form" href="#"><?php sbd_icons_content('email'); ?></a></p>
					<?php endif; ?>
					
					<?php if(isset($list_item['qcpd_item_linkedin']) and $list_item['qcpd_item_linkedin']!=''): ?>
					<p><a target="_blank" href="<?php echo (isset($list_item['qcpd_item_linkedin']) && $list_item['qcpd_item_linkedin']!=''?trim($list_item['qcpd_item_linkedin']):'#'); ?>"><?php sbd_icons_content('linkedin', trim($list_item['qcpd_item_linkedin'])); ?></a></p>
					<?php endif; ?>
					
					<?php if(isset($list_item['qcpd_item_twitter']) and $list_item['qcpd_item_twitter']!=''): ?>
					<p><a target="_blank" href="<?php echo (isset($list_item['qcpd_item_twitter']) && $list_item['qcpd_item_twitter']!=''?trim($list_item['qcpd_item_twitter']):'#'); ?>"><?php sbd_icons_content('twitter'); ?></a></p>
					<?php endif; ?>
					
				
				</div>
			</div>
<?php
		}
	}
	return ob_get_clean();
}