<?php
/**
 * 
 */
if( !class_exists('QC_sbd_Help_License_Sub_Menu') ){

	class QC_sbd_Help_License_Sub_Menu
	{
		
		function __construct()
		{
			add_action('admin_menu', array($this, 'help_license_submenu') );
		}

		function help_license_submenu(){
			add_submenu_page( 
		        'edit.php?post_type=pd',
		        'Help & License',
		        'Help & License',
		        'manage_options',
		        'qcld_sbd_help_license',
		        array($this, 'qcld_sbd_help_license_callback')
		    );
		}

		function qcld_sbd_help_license_callback(){
?>
			<div id="wrap">
				<div id="licensing">
					<?php sbd_display_license_section(); ?>

					
					<div class="qcld-sbd-help-section">

						<h1>Welcome to the Simple Business Directory Pro! You are awesome, by the way <img draggable="false" class="emoji" alt="🙂" src="https://s.w.org/images/core/emoji/11/svg/1f642.svg"></h1>
						
						<div id="google-api-hint" class="qcld-sbd-section-block sbd-map-api-hint">
							<h3 class='shortcode-section-title sbd-color-red'>For Google Map API key to work:</h3>
							<div class=" left_section">
								<p class="list-element"><span style="font-weight:bold;">1.</span> Make sure to have both API and Application restriction set to None or restrict them to your domain name properly from Credentials->API Keys->Your API->Application restrictions</p>
								<p class="list-element"><span style="font-weight:bold;">2.</span>  Ensure that ALL the API libraries are enabled including Javascript, Geocoding and Places APIs.</p>
								<p class="list-element"><span style="font-weight:bold;">3.</span> Also, from the middle of 2018, Google requires you to add a Billing account. Although, for 99% cases you won't actually have to pay anything as the free usage limit is quite high for almost any use cases.</p>
							</div>
							
							<div class="image-container">
								<img src="<?php echo QCSBD_IMG_URL; ?>/api-restrictions-list.png" alt="api-restrictions-list">
								<img src="<?php echo QCSBD_IMG_URL; ?>/api-restrictions.jpg" alt="api-restrictions">
								<img src="<?php echo QCSBD_IMG_URL; ?>/api-search.png" alt="api-search">
								<!-- <img src="<?php echo QCSBD_IMG_URL; ?>/enable-apis.jpg" alt="enable-apis"> -->
								<img src="<?php echo QCSBD_IMG_URL; ?>/api-library-restrictions.png" alt="api-library-restrictions">
							</div>
						</div>

						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Getting Started</h3>
							<p>Over the years we have added a lot of advanced features to this plugin which may be overwhelming when you first get started. But do not worry – getting started is super easy and once you are familiar with how the system works, you can take advantage of all the advanced features.</p>
							<p>The plugin works a little different from others. The most important thing to remember is that the <strong>base pillars of this plugin are Lists</strong>, not individual businessed or categories. A list is simply a niche or subtopic to group your relevant businesses or any type of Listings together. The most common use of SBD is to create and display multiple businesses or listings on specific topics or subtopics on the same page. Everything revolves around the Lists. Once you create a few Lists, you can then display them in many different ways.</p>
							<p>With that in mind you should start with the following simple steps.</p>
							<p><br><span style="font-weight:bold;">1.</span> Go to New List and create one by giving it a name. Then simply start adding List items or links by filling up the fields you want. Use the <strong>Add New</strong> button to add more Listings in your list.</p>

							<p><br><span style="font-weight:bold;">2.</span> Though you can just create one list and use the Single List mode. This directory plugin works the best when you <strong>create a few Lists</strong> each conatining about <strong>15-20 List items</strong>. This is the most usual use case scenario. But you can do differently once you get the idea.</p>

							<p><br><span style="font-weight:bold;">3.</span> Now go to a page or post where you want to display the directory. On the right sidebar you will see a <strong>ShortCode Generator</strong> block. Click the button and a Popup LightBox will appear with all the options that you can select. Choose All Lists, and select a Style. Then Click Add Shortcode button. Shortcode will be generated. Simply <strong>copy paste</strong> that to a location on your page where you want the <strong>directory to show up</strong>.</p>
							<br>
							<p>That’s it! If you want to create a rather large directory or have more than one broad topics in mind then create some Categories from Category menu and assign your Lists to the relevant categories from the List edit page right panel. Then you can display Lists by a single category on different pages or use the multi page mode. But you can skip this step if you want just a single page directory.</p>
							<p>The above steps are for the basic usages. There are a lot of advanced options available. If you had any specific questions about how something works, do not hesitate to contact us from the <a href="<?php echo admin_url('edit.php?post_type=pd&page=qcpro-promo-page-qcld-pd-pro-1245support'); ?>">Support Page</a>. We are very friendly and would be glad to answer any question you might have! <img draggable="false" class="emoji" alt="🙂" src="https://s.w.org/images/core/emoji/11/svg/1f642.svg"></p>
						</div>
						
						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Note</h3>
							<p><strong>If you are having problem with adding more items or saving a list or your changes in the list are not getting saved then it is most likely because of a limitation set in your server. Your server has a limit for how many form fields it will process at a time. So, after you have added a certain number of links, the server refuses to save the List. The server’s configuration that dictates this is max_input_vars. You need to Set it to a high limit like max_input_vars = 15000. Since this is a server setting - you may need to contact your hosting company\'s support for this.</strong></p>
							
							<p><b>For Google Map API key to work, make sure to have both API and Application restriction set to None or restrict them to your domain name. Ensure that all the API libraries are enabled. Also, from the middle of 2018, Google requires you to add a Billing account. Although, for 99% cases you won\'t actually have to pay anything as the free usage limit is quite high for most cases.</b></p>
						</div>

						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Shortcode Generator</h3>
							<p>
							We encourage you to use the ShortCode generator found in the toolbar of your page/post editor in visual mode.</p> 
							
							<img src="<?php echo QCSBD_IMG_URL; ?>/classic.jpg" alt="shortcode generator" />
							
							<p>See sample below for where to find it for Gutenberg.</p>

							<img src="<?php echo QCSBD_IMG_URL; ?>/gutenburg.jpg" alt="shortcode generator" />						
							<img src="<?php echo QCSBD_IMG_URL; ?>/gutenburg2.jpg" alt="shortcode generator" />	<p>This is how the shortcode generator will look like.</p>				
							<img src="<?php echo QCSBD_IMG_URL; ?>/shortcode-generator1.jpg" alt="shortcode generator" />	
						</div>
						
						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Shortcode for frontend submission</h3>
							<p>
								This feature will allow your users to submit their links to your lists from website front end. To achieve this you have to create 4 different pages and paste the following short code in each page.
							</p>
							<p>
								Please make sure that you have installed and activated pd plugin before adding these shortcodes.
								<br>
								<strong><u>For Login Page:</u></strong>
								<br>
								[sbd_login]
								<br>
								Login From will appear when you add this shortcode on a page.
								<br>
								<br>
								<strong><u>For Registration Page:</u></strong>
								<br>
								[sbd_registration]
								<br>
								Registration From will appear when you add this shortcode on a page.
								<br>
								<br>
								<strong><u>For Dashboard:</u></strong>
								<br>
								[sbd_dashboard]
								<br>
								Dashboard (where people can manage there list items) will appear when you add this shortcode.
								<br>
								<br>
								<strong><u>For Restore SBD User Password:</u></strong>
								<br>
								[sbd_restore]
								<br>
								User will get password reset option when you add this shortcode on a page. 
								<br>
							</p>
						</div>
						
						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Shortcode for Global Search</h3>
							<p>You can add a search bar to any place of your website. Use the following shortcode to add the search bar.</p>
							
							<strong>[qcpd-searchbar post_id="your post id" placeholder="Search"]</strong>
							<br>
							<p>Or add <strong>do_shortcode('[qcpd-searchbar post_id="your post id" placeholder="Search"]')</strong> inside your theme file where you want to display the search bar.</p>
							<p>You will also need to create a page and place a regular SBD single page shortcode inside it and add the page's or post's ID in the shortcode to display the results.</p>
						</div>

						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Shortcode for Multipage/Main Directory page</h3>
							<p>Please make sure that you have installed and activated sbd plugin before adding these shortcodes.<br><br>
								<strong><u>For Multipage:</u></strong>
								<br>
								[qcpd-directory-multipage]
								<br>
							</p>
							<p>
								<strong><u>For Multipage Category only:</u></strong>
								<br>
								When a category is clicked, it will redirect to the main directory page where you added the main shortcode and show the Lists under a category.
								<br>
								[sbd-multipage-category]
								<br>
							</p>
						</div>
						
						<div class="qcld-sbd-section-block">
							<h3 class='shortcode-section-title'>Shortcode Example</h3>
							<p>
								<strong><u>Full Shortcode:</u></strong>
							[qcpd-directory mode="all" style="simple" column="3" upvote="on" search="true" distance_search="true" marker_cluster="false" image_infowindow="false" item_count="off" item_details_page="off" orderby="date" filterorderby="date" enable_embedding="true" main_click_action="1" phone_number="1" order="ASC" paginate_items="false" enable_rtl="false" enable_left_filter="false" enable_tag_filter="false" enable_map_fullwidth="true" enable_map_fullscreen="false" enable_map_toggle_filter="false" map="show" map_position="top" tooltip="false" list_title_font_size="" item_orderby="" list_title_line_height="" title_font_size="" subtitle_font_size="" title_line_height="" subtitle_line_height="" pdmapofflightbox="false" filter_area="normal" topspacing=""]
							</p>
							<p>
								<strong><u>For all the lists:</u></strong>
								<br>
								[qcpd-directory mode="all" style="simple" column="2" search="true" category="" upvote="on" item_count="on" orderby="date" order="DESC" item_orderby="title"]
								<br>
								<br>
								<strong><u>For only a single list:</u></strong>
								<br>
								[qcpd-directory mode="one" list_id="75"]
								<br>
								<br>
								<strong><u>For Category Tab</u></strong>
								<br>
								[pd-tab mode="categorytab" style="simple" column="2" search="true" category="" upvote="on" item_count="on" orderby="date" order="DESC" item_orderby="title"]
								<br>
								<br>
								<strong><u>Available Parameters:</u></strong>
								<br>
								
							</p>
							
							<p>
								<strong>1. mode</strong>
								<br>
								Value for this option can be set as "one" or "all".
								<br>
								<p>
								If you set mode="one", then filter option will not appear.
								</p>
								<p>
								If you set mode="favorite", show only Favorite List.
								</p>
							</p>
							<p>
								<strong>2. column</strong>
								<br>
								Avaialble values: "1", "2", "3" or "4".
							</p>
							<p>
								<strong>3. style</strong>
								<br>
								Avaialble values: "simple", "style-1", "style-2", "style-3", "style-4", "style-5", "style-6", "style-7", "style-8", "style-9", "style-10".
								<br>
								<br>
								To get details idea about how different style templates will look, please see the [Demo Images] tab from the left side.
							</p>
							<p>
								<strong>4. orderby</strong>
								<br>
								Compatible order by values: "ID", "author", "title", "name", "type", "date", "modified", "rand" and "menu_order".
							</p>
							<p>
								<strong>5. order</strong>
								<br>
								Value for this option can be set as "ASC" for Ascending or "DESC" for Descending order.
								<br>
								<br>
								<strong>For List Ordering to work, either specify orderby="menu_order" order="ASC in the short code or leave these empty.</strong>
								<br>
								<br>
							</p>
							<p>
								<strong>6. list_id</strong>
								<br>
								Only applicable if you want to display a single list [not all]. You can provide specific list id here as a value. You can also get ready shortcode for a single list under "Manage List Items" menu.
							</p>
							<p>
								<strong>7. category</strong>
								<br>
								Supply the category slug of your specific directory category.
								<br>
								Example: category="designs"
								<br>
								For multiple categories add slug with coma(,) seperated without having any space. Example: category="designs,planning"
							</p>
							<p>
								<strong>8. search</strong>
								<br>
								Values: true or false. If you want to display on-page search for items, then you can set this parameter to - true.
								<br>
								Example: search="true"
							</p>
							<p>
								<strong>9. upvote</strong>
								<br>
								Values: on or off. This options allows upvoting of your list items.
								<br>
								Example: upvote="on"
							</p>
							<p>
								<strong>10. item_count</strong>
								<br>
								Values: on or off. This options allows to display list items count just beside your list heading.
								<br>
								Example: item_count="on"
							</p>
							<p>
								<strong>11. top_area</strong>
								<br>
								Values: on or off. You can hide top area (search and link submit) from any individual templates if you require. This option is handy if you want to display multiple template in the same page.
								<br>
								Example: top_area="off"
							</p>
							<p>
								<strong>12. item_orderby</strong>
								<br>
								Values: "upvotes", "title". You can order/sort list items by upvote counts or by their titles.
								<br>
								Example: item_orderby="upvotes"
							</p>
							<p>
								<strong>13. mask_url</strong>
								<br>
								Values: "on", "off". This option will allow you to hide promotional/affliate links from the visitors. Visitors will not be able to see these type of links when they mouseover on the links, but upon clicking on these links - they will be redirected to the original/set affliate links.
								<br>
								Example: mask_url="on"
								<br>
								<strong><i>Please note that URL masking may hurt your SEO.</i></strong>
							</p>
							<p>
								<strong>14. paginate_items</strong>
								<br>
								Values: "true", "false". This option will allow you to paginate list items. It will break the list page wise.
								<br>
								Example: paginate_items="true"
								<br>
								[Only applicable for certain templates.]
							</p>
							<p>
								<strong>15. per_page</strong>
								<br>
								This option indicates the number of items per page. Default is "5". paginate_items="true" is required to find this parameter in action.
								<br>
								Example: per_page="5"
								<br>
								[Only applicable for certain templates.]
							</p>
							<p>
								<strong>16. tooltip</strong>
								<br>
								You can enable or disable tooltip by using this parameter. Accepted values are "true" and "false".
								<br>
								Example: tooltip="true"
								<br>
								[Only applicable for certain templates.]
							</p>
							<p>
								<strong>17. Filter Area</strong>
								<br>
								You can set the filter area fixed position using this below parameter.
								<br>
								Example: filter_area="fixed"
								<br>
								Available values: fixed, normal.
							</p>
							<p>
								<strong>18. Filter Area Top spacing</strong>
								<br>
								You can set Top Spacing for filter area using this below parameter.
								<br>
								Example: topspacing="50"
								<br>
								Available values: It could be any integer.
							</p>
							<p>
								<strong>19. Remove specific category from Category Tab</strong>
								<br>
								You can remove specific category from Category Tab using this below parameter.
								<br>
								Example: category_remove="50,51,52"
								<br>
								You can add multiple Category ID as coma(,) seperated value.
								
							</p>
							<p>
								<strong>20. Show/Hide All Tab Button from Category Tab</strong>
								<br>
								You can add "All" Tab category from Category Tab using this below parameter all_tab="show" and all_tab="hide". Default hide all tab category.
								<br>
								Example: all_tab="show"
								<br>
								
							</p>
							<p>
								<strong>21. Map marker with scroll top </strong>
								<br>
								You can stop map marker with scroll top 
								<br>
								Example: map_marker_scroll="false"
								<br>
								Default: map_marker_scroll="true"
							</p>
						</div>

						<div class="qcld-sbd-section-block">
							<h3 class="shortcode-section-title">Template Overriding</h3>
							<p>You can change SBD Templates from your theme. Follow the following steps to override SBD templates from your theme</p>
							<p>
								<strong>1.</strong> Unzip the plugin package in your computer and copy the "<strong>/simple-business-directory-pro/templates</strong>" folder.
							</p>
							<p>
								<strong>2.</strong> Create a folder named <strong>"sbd"</strong> inside your ACTIVE theme folder and paste the <strong>"templates"</strong> folder inside it. Now you can modify the template files inside as required.
							</p>
							<p style="color: red;">We strongly recommend you use a Child theme, so updating the main theme does not delete your SBD customization folders.</p>
						</div>

						<div class="qcld-sbd-section-block">
							<h3 class="shortcode-section-title">How to style map with Snazzy Map</h3>
							<p><strong>1)</strong> Sign up for an account at <a href="https://snazzymaps.com/account/developer">SnazzyMaps.com</a>.</p>
							<p><strong>2)</strong> Click your email address in the top left corner.</p>
							<p><strong>3)</strong> Click the developer menu item on the left side.</p>
							<p><strong>4)</strong> Click the <strong>"Generate API Key"</strong> button.</p>
							<p><strong>5)</strong> Copy the long generated API Key.</p>
							<p><strong>6)</strong> Paste the key into the <strong>"Settings"</strong> tab in the Snazzy Maps plugin.</p>
							<p><strong>7)</strong> You should now be able to access your favorites and private styles in the <strong>Explore</strong> tab by changing the first filter dropdown.</p>
						</div>
					</div>

				</div>
			</div>
<?php
		}
	}

	new QC_sbd_Help_License_Sub_Menu();
}

