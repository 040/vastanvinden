<?php $go_to_website_text = pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website'; ?>
<!-- Style-8 Template -->
<!--Adding Template Specific Style -->
<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/$template_code/template.css"; ?>" />
<script src="<?php echo QCSBD_ASSETS_URL . "/js/jquery-sticky.js"; ?>"></script>
<?php if( $paginate_items == true && $actual_pagination == "true" ) : ?>
	<?php
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-jpages', OCSBD_TPL_URL . "/simple/jPages.min.js", array('jquery'));
		wp_enqueue_style('pd-jpages-css', OCSBD_TPL_URL . "/simple/jPages.css" );
		wp_enqueue_style('pd-animate-css', OCSBD_TPL_URL . "/simple/animate.css" );
	?>
<?php endif; ?>

<?php
	$customCss = pd_ot_get_option( 'pd_custom_style' );

	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>

<?php endif; ?>

<?php

$bookmark_list = $mode;

// The Loop
if ( $list_query->have_posts() )
{

	//Getting Settings Values
	if($search=='true'){
		$searchSettings = 'on';
	}else{
		if($search=='false'){
			$searchSettings = 'off';
		}else{
			$searchSettings = pd_ot_get_option( 'pd_enable_search' );
		}
	}
	$itemAddSettings = pd_ot_get_option( 'pd_enable_add_new_item' );
	$itemAddLink = pd_ot_get_option( 'pd_add_item_link' );
	$enableTopArea = pd_ot_get_option( 'pd_enable_top_part' );
	$enableFiltering = pd_ot_get_option( 'pd_enable_filtering' );

	//Check if border should be set
	$borderClass = "";

	if( $searchSettings == 'on' || $itemAddSettings == 'on' )
	{
		$borderClass = "pd-border-bottom";
	}

	//Hook - Before Search Template
	do_action( 'qcpd_before_search_tpl', $shortcodeAtts);

	//If the top area is not disabled (both serch and add item)
	if( $enableTopArea == 'on' && $top_area != 'off' ) :

		//Load Search Template
		if($bookmark_list != "favorite" ){
			require ( dirname(__FILE__) . "/search-template.php" );
		}

	endif;

	if($radius=='true'){
		sbdRadiusSearch();
	}
	if($enable_tag_filter=='true'){
			if($enable_tag_filter_dropdown=='true'){
				show_tag_filter_dropdown($list_args);
			}else{
				show_tag_filter($category,$shortcodeAtts);
			}
		}
	//Hook - Before Filter Template
	do_action( 'qcpd_before_filter_tpl', $shortcodeAtts);

	//Enable Filtering
	if( $enableFiltering == 'on' && $mode == 'all' && $enable_left_filter!='true' ) :

		//Load Search Template
		if($bookmark_list != "favorite" ){
			require ( dirname(__FILE__) . "/filter-template.php" );
		}

	endif;
	if(pd_ot_get_option('pd_enable_filtering_left')=='on' || $enable_left_filter=='true') {
		$args = array(
			'numberposts' => - 1,
			'post_type'   => 'pd',
			'orderby'     => $filterorderby,
			'order'       => $filterorder,
		);

		if ( $category != "" ) {
			$taxArray = array(
				array(
					'taxonomy' => 'pd_cat',
					'field'    => 'slug',
					'terms'    => $category,
				),
			);

			$args = array_merge( $args, array( 'tax_query' => $taxArray ) );

		}

		$listItems = get_posts( $args );
		?>
        <style>
            .sbd-filter-area {

                position: relative;
            }

            .slick-prev::before, .slick-next::before {
                color: #489fdf;
            }

            .slick-prev, .slick-next {
                transform: translate(0px, -80%);
            }
        </style>
        <div class="sbd-filter-area-main pd_filter_mobile_view">
            <div class="sbd-filter-area" style="width: 100%;">

                <div class="filter-carousel">
                    <div class="item">
					<?php 
						$item_count_disp_all = 0;
						foreach ($listItems as $item){
							if( $item_count == "on" ){
								$item_count_disp_all += count(get_post_meta( $item->ID, 'qcpd_list_item01' ));
							}
						}
					?>
					<a href="#" class="filter-btn" data-filter="all">
						<?php _e('Show All', 'qc-pd'); ?>
						<?php
							if($item_count == 'on'){
								echo '<span class="opd-item-count-fil">('.$item_count_disp_all.')</span>';
							}
						?>
					</a>
                    </div>

					<?php foreach ( $listItems as $item ) :
						$config = get_post_meta( $item->ID, 'qcpd_list_conf' );
						$filter_background_color = '';
						$filter_text_color = '';
						if ( isset( $config[0]['filter_background_color'] ) and $config[0]['filter_background_color'] != '' ) {
							$filter_background_color = $config[0]['filter_background_color'];
						}
						if ( isset( $config[0]['filter_text_color'] ) and $config[0]['filter_text_color'] != '' ) {
							$filter_text_color = $config[0]['filter_text_color'];
						}
						?>

						<?php
						$item_count_disp = "";

						if ( $item_count == "on" ) {
							$item_count_disp = count( get_post_meta( $item->ID, 'qcpd_list_item01' ) );
						}
						?>

                        <div class="item">
                            <a href="#" class="filter-btn" data-filter="opd-list-id-<?php echo $item->ID; ?>"
                               style="background:<?php echo $filter_background_color ?>;color:<?php echo $filter_text_color ?>">
								<?php echo esc_html($item->post_title); ?>
								<?php
								if ( $item_count == 'on' ) {
									echo '<span class="opd-item-count-fil">(' . $item_count_disp . ')</span>';
								}
								?>
                            </a>
                        </div>

					<?php endforeach; ?>

                </div>

                <?php if($cattabid==''): ?>
                <script>
                    jQuery(document).ready(function ($) {

                        var fullwidth = window.innerWidth;
                        if (fullwidth < 479) {
                            $('.filter-carousel').not('.slick-initialized').slick({


                                infinite: false,
                                speed: 500,
                                slidesToShow: 1,


                            });
                        } else {
                            $('.filter-carousel').not('.slick-initialized').slick({

                                dots: false,
                                infinite: false,
                                speed: 500,
                                slidesToShow: 1,
                                centerMode: false,
                                variableWidth: true,
                                slidesToScroll: 3,

                            });
                        }

                    });
                </script>
				<?php endif; ?>

            </div>
        </div>
		<?php
	}
	//If RTL is Enabled
	$rtlSettings = pd_ot_get_option( 'pd_enable_rtl' );
	$rtlClass = "";
	if($enable_rtl=='true'){
		$rtlSettings = 'on';
	}
	if( $rtlSettings == 'on' )
	{
	   $rtlClass = "direction-rtl";
	}

	//Hook - Before Main List
	do_action( 'qcpd_before_main_list', $shortcodeAtts);

	//Directory Wrap or Container
	if($map=='show' && $map_position=='top'){
		echo '<div id="sbd_all_location'.($cattabid!=''?$cattabid:'').'" class="sbd_map"></div>';
	}else{
		if($map_position!='right'){
			if( $map != 'hide' ){
				echo '<div id="sbd_all_location'.($cattabid!=''?$cattabid:'').'" class="sbd_map" style="display:none;"></div>';
			}
		}
		
	}
	echo '<div class="sbd_main_wrapper '.( (isset($item_orderby) && $item_orderby == 'distance') ? 'sbd-distance-order' : '' ).'"><div class="qcpd-list-wrapper qc-full-wrapper" '.($map=='show' && $map_position=='right'?'style="width:60%;float:left;padding-right: 15px;"':'').'>';
	?>
	<?php
	if(pd_ot_get_option('pd_enable_filtering_left')=='on' || $enable_left_filter=='true') {
		$args = array(
			'numberposts' => - 1,
			'post_type'   => 'pd',
			'orderby'     => $filterorderby,
			'order'       => $filterorder,
		);

		if ( $category != "" ) {
			$taxArray = array(
				array(
					'taxonomy' => 'pd_cat',
					'field'    => 'slug',
					'terms'    => $category,
				),
			);

			$args = array_merge( $args, array( 'tax_query' => $taxArray ) );

		}

		$listItems = get_posts( $args );

		$filterType = pd_ot_get_option( 'pd_filter_ptype' ); //normal, carousel

//If FILTER TYPE is NORMAL



			?>

            <div class="sbd-filter-area left-side-filter">

                <a href="#" class="filter-btn" data-filter="all">
					<?php _e( 'Show All', 'qc-pd' ); ?>
                </a>

	            <?php foreach ( $listItems as $item ) :
		            $config = get_post_meta( $item->ID, 'qcpd_list_conf' );
		            $filter_background_color = '';
		            $filter_text_color = '';
		            if(isset($config[0]['filter_background_color']) and $config[0]['filter_background_color']!=''){
			            $filter_background_color = $config[0]['filter_background_color'];
		            }
		            if(isset($config[0]['filter_text_color']) and $config[0]['filter_text_color']!=''){
			            $filter_text_color = $config[0]['filter_text_color'];
		            }
		            ?>

		            <?php
		            $item_count_disp = "";

		            if ( $item_count == "on" ) {
			            $item_count_disp = count( get_post_meta( $item->ID, 'qcpd_list_item01' ) );
		            }
		            ?>

                    <a href="#" class="filter-btn" data-filter="opd-list-id-<?php echo $item->ID; ?>" style="background:<?php echo $filter_background_color ?>;color:<?php echo $filter_text_color ?>">
			            <?php echo esc_html($item->post_title); ?>
			            <?php
			            if ( $item_count == 'on' ) {
				            echo '<span class="opd-item-count-fil">(' . $item_count_disp . ')</span>';
			            }
			            ?>
                    </a>

	            <?php endforeach; ?>

            </div>

		<?php
	}
	?>
	<?php
	echo '<div id="sbdopd-list-holder" class="qc-grid qcpd-list-hoder '.$rtlClass.'">';
	global $wpdb;
	if(is_user_logged_in() and pd_ot_get_option('pd_enable_bookmark')=='on'){
		$b_title = pd_ot_get_option('pd_bookmark_title');
		
		$userid = get_current_user_id();
		$user_meta_data = get_user_meta($userid, 'pd_bookmark_user_meta');
		
		
		$conf['list_bg_color'] = pd_ot_get_option('pd_bookmark_item_background_color');
		
		$conf['list_bg_color_hov'] = pd_ot_get_option('pd_bookmark_item_background_color_hover');
		
		$conf['list_txt_color_hov'] = pd_ot_get_option('pd_bookmark_item_text_color_hover');
		$conf['list_txt_color'] = pd_ot_get_option('pd_bookmark_item_text_color');
		
		$conf['list_border_color'] = pd_ot_get_option('pd_bookmark_item_border_color');
		
		$conf['item_bdr_color'] = pd_ot_get_option('pd_bookmark_item_border_color');
		$conf['item_bdr_color_hov'] = pd_ot_get_option('pd_bookmark_item_border_color_hover');
		
		$conf['list_subtxt_color'] = pd_ot_get_option('pd_bookmark_item_sub_text_color');
		$conf['list_subtxt_color_hov'] = pd_ot_get_option('pd_bookmark_item_sub_text_color_hover');
	if(!empty($user_meta_data[0])){
?>
<style>
			#bookmark_list.style-8 h3:after{
				border-bottom: 2px solid <?php echo isset($conf['item_bdr_color'])?$conf['item_bdr_color']:''; ?>;
			}

			#bookmark_list.style-8 li a {
			    background-color: <?php echo isset($conf['list_bg_color'])?$conf['list_bg_color']:''; ?>;
			    border-color: -moz-use-text-color -moz-use-text-color <?php echo isset($conf['item_bdr_color'])?$conf['item_bdr_color']:''; ?> !important;
			}

			#bookmark_list.style-8 li a:hover {
			    background-color: <?php echo isset($conf['list_bg_color_hov'])?$conf['list_bg_color_hov']:''; ?>;
				color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
				border-color: -moz-use-text-color -moz-use-text-color <?php echo isset($conf['list_bg_color_hov'])?$conf['list_bg_color_hov']:''; ?> !important;
			}

			#bookmark_list.style-8 li a .li-txt {
			  color: <?php echo isset($conf['list_txt_color'])?$conf['list_txt_color']:''; ?>;
				<?php if($title_font_size!=''): ?>
				font-size:<?php echo $title_font_size; ?> !important;
				<?php endif; ?>

				<?php if($title_line_height!=''): ?>
				line-height:<?php echo $title_line_height; ?> !important;
				<?php endif; ?>
			}

			#bookmark_list.style-8 li a:hover .li-txt {
			  color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;

			}

			#bookmark_list.style-8 .tooltip_tpl8:before {
			  color: <?php echo isset($conf['list_subtxt_color'])?$conf['list_subtxt_color']:''; ?>;
				<?php if($subtitle_font_size!=''): ?>
				font-size:<?php echo $subtitle_font_size; ?> !important;
				<?php endif; ?>

				<?php if($subtitle_line_height!=''): ?>
				line-height:<?php echo $subtitle_line_height; ?>!important;
				<?php endif; ?>
			  background-color: <?php echo isset($conf['list_subtxt_color_hov'])?$conf['list_subtxt_color_hov']:''; ?>;
			  border: 1px solid;
			  border-color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
			}

			#bookmark_list.style-8 .tooltip_tpl8:after {
			  border-bottom: 6px solid <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
			}

			#bookmark_list.style-8 li .upvote-section .sbd-upvote-btn, #qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 .ca-menu li .upvote-section .upvote-count{
			  color: <?php echo isset($conf['list_subtxt_color'])?$conf['list_subtxt_color']:''; ?>;
			}

			#bookmark_list.style-8 li:hover .upvote-section .sbd-upvote-btn, #qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover .upvote-section .upvote-count{
			  color: <?php echo isset($conf['list_subtxt_color_hov'])?$conf['list_subtxt_color_hov']:''; ?>;
			}

			
		</style>

		<div id="bookmark_list" class="qc-grid-item qcpd-list-column opd-column-<?php echo $column; echo " style-8";?> <?php echo "opd-list-id-" . get_the_ID(); ?>">

			<div class="opd-list-pd-style-8">
				<h2><?php echo ($b_title!=''?$b_title:'Quick Links'); ?></h2>
				<ul class="tooltip_tpl8-tpl" id="pd_bookmark_ul">
					
					<?php
					$lists = array();
			if(!empty($user_meta_data)){
				foreach($user_meta_data[0] as $postid=>$metaids){
					if(!empty($metaids)){
						foreach($metaids as $metaid){
							
							$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = $postid AND meta_key = 'qcpd_list_item01'");
							if(!empty($results)){
								foreach ($results as $key => $value) {
									$unserialized = unserialize($value->meta_value);
									if (trim($unserialized['qcpd_timelaps']) == trim($metaid)) {
										$customdata = $unserialized;
										$customdata['postid'] = $postid;
										$lists[] = $customdata;
									}
								}
							}
						}
					}
				}
			}
					usort($lists, "pd_custom_sort_by_tpl_title");
					$b = 1;			
					foreach($lists as $list){
					?>
					<?php
						$canContentClass = "subtitle-present";

						if( !isset($list['qcpd_item_subtitle']) || $list['qcpd_item_subtitle'] == "" )
						{
							$canContentClass = "subtitle-absent";
						}
					?>
					<li id="pd_bookmark_li_<?php echo $b; ?>">

						<?php
							$item_url = $list['qcpd_item_link'];
							$masked_url = $list['qcpd_item_link'];

							$mask_url = isset($mask_url) ? $mask_url : 'off';

							if( $mask_url == 'on' ){
								$masked_url = 'http://' . qcpd_get_domain($list['qcpd_item_link']);
							}
							$hover_info = 'title="'.$go_to_website_text.'"';
							if($main_click_action=='0'){
								$masked_url = 'tel:'.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']);
								$hover_info = 'title="Call '.$list['qcpd_item_phone'].'"';
							}


                        if(isset($list['qcpd_item_subtitle']) and $list['qcpd_item_subtitle']!=''){
	                        $toolipsetup = 'class="tooltip-element tooltip_tpl8 fade" data-title="'.trim($list['qcpd_item_subtitle']).'<br>"';
                        }else{
	                        $toolipsetup = '';
                        }


						?>

						<a <?php if( $mask_url == 'on') { echo 'onclick="document.location.href = \''.$item_url.'\'; return false;"'; } ?> <?php echo $toolipsetup; ?> <?php echo (isset($list['qcpd_item_nofollow']) && $list['qcpd_item_nofollow'] == 1) ? 'rel="nofollow"' : ''; ?> <?php echo (isset($main_click_action)&&$main_click_action==3?'':'href="'.$masked_url.'"'); ?>" <?php echo (isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1) ? 'target="_blank"' : ''; ?> <?php echo $hover_info; ?> >

						    <?php
								$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

								$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

								$faviconImgUrl = "";
								$faviconFetchable = false;
								$filteredUrl = "";

								$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

								if( $showFavicon == 1 )
								{
									$filteredUrl = qcpd_remove_http( $item_url );

									if( $item_url != '' )
									{

										$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
									}

									if( $directImgLink != '' )
									{

										$faviconImgUrl = trim($directImgLink);
									}

									$faviconFetchable = true;

									if( $item_url == '' && $directImgLink == '' ){
										$faviconFetchable = false;
									}
								}
							?>

								<!-- Image, If Present -->
							<?php if( ($list_img == "true") && isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>
								<?php 
									if (strpos($list['qcpd_item_img'], 'http') === FALSE){
								?>
								<span class="logo-icon">
									<?php
									
										$pd_list_image_size = pd_ot_get_option('pd_list_image_size') ? pd_ot_get_option('pd_list_image_size') : 'thumbnail';

										$img = wp_get_attachment_image_src($list['qcpd_item_img'], $pd_list_image_size);
									?>
									<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
								<?php
									}else{
								?>
								<span class="logo-icon">
									<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
								<?php
									}
								?>
							<?php elseif( $iconClass != "" ) : ?>
								<span class="logo-icon">
									<i class="fa <?php echo $iconClass; ?>"></i>
								</span>
							<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>
								<span class="logo-icon favicon-loaded">
									<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
							<?php else : ?>
								<span class="logo-icon">
									<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
							<?php endif; ?>

							<div class="li-txt">
								<?php
									echo trim($list['qcpd_item_title']);
								?>
								<?php
							
								echo '<span style="display:block;font-size:11px">';
										
										if(isset($list['qcpd_item_location']) and $list['qcpd_item_location']!=''){
											echo ' <span style="font-weight:bold">'.$list['qcpd_item_location'].'</span> &nbsp;&nbsp;';
										}
										if(isset($list['qcpd_item_phone'])&&$list['qcpd_item_phone']!=''){
											echo ($phone_number=='1'?'<span class="sbd_phone"><i class="fa fa-phone"></i> '.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']).'</span>':'');
										}
										
										echo '</span>';
								
							 ?>
							</div>

						</a>

						<div class="bookmark-section">
							
								<?php 
								$bookmark = 1;
								if(isset($list['qcpd_is_bookmarked']) and $list['qcpd_is_bookmarked']!=''){
									$unv = explode(',',$list['qcpd_is_bookmarked']);
									if(in_array(get_current_user_id(),$unv)){
										$bookmark = 1;
									}
								}
								?>
							
							
								<span data-post-id="<?php echo $list['postid']; ?>" data-item-code="<?php echo trim($list['qcpd_timelaps']); ?>" data-is-bookmarked="<?php echo ($bookmark); ?>" data-li-id="pd_bookmark_li_<?php echo $b; ?>" class="bookmark-btn bookmark-on">
									
									<i class="fa fa-times-circle" aria-hidden="true"></i>
								</span>
								
							</div>
						<div class="pd-bottom-area">
						
								<?php if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!='' and $phone_number!=0): ?>
									<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>"><i class="fa fa-phone"></i></a></p>
								<?php endif; ?>
								
								<?php if($list['qcpd_item_link']!=''): ?>
									<p><a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank"><i class="fa fa-link"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><i class="fa fa-facebook"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><i class="fa fa-yelp"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''): ?>
								<p><a data-email="<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>" class="sbd_email_form" href="#"><?php sbd_icons_content('email'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><i class="fa <?php echo (sbd_is_linkedin($list['qcpd_item_linkedin'])?'fa-linkedin-square fa-linkedin':'fa-instagram'); ?>"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><i class="fa fa-twitter-square"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!='' and $pdmapofflightbox!='true'): 
								$marker = '';
								if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
									$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
								}
								?>
								<p><a href="#" class="pd-map open-mpf-sld-link" full-address="<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>" data-mfp-src="#map-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $marker; ?>><i class="fa fa-map"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_description']) and $list['qcpd_description']!=''): ?>
								<p><a href="#" class="open-mpf-sld-link" data-post-id="<?php echo $list['postid']; ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" data-mfp-src="#busi-<?php echo $list['postid'] ."-". $b; ?>"><i class="fa fa-info-circle"></i></a></p>
								<?php endif; ?>
							
						</div>
							
							
							<div id="map-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide"><a class="sbd_direction_btn" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo @urlencode(@$list['qcpd_item_full_address']); ?>&travelmode=driving" target="_blank"><?php echo (pd_ot_get_option('sbd_lan_get_direction')!=''?pd_ot_get_option('sbd_lan_get_direction'):'Get Direction'); ?></a>
								<div class="pd_map_container" id="mapcontainer-<?php echo get_the_ID() ."-". $count; ?>"></div>
							</div>
				
							<div id="busi-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
								<div class="sbd_business_container">
									<span class="qc_sbd_loading_details">Loading...</span>
								</div>
							</div>
					</li>

				<?php $b++; }; ?>
				</ul>

				

			</div>
		</div>
<?php
	}
	
}

if($bookmark_list == "favorite" ){
	return false;
}


	$outbound_conf = pd_ot_get_option( 'pd_enable_click_tracking' );

	$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
	$qcld_post_name = isset($optionPage->post_name) ? '/'. $optionPage->post_name : '';
	$directoryPage = home_url(). $qcld_post_name;
	
	$listId = 1;

	while ( $list_query->have_posts() )
	{
		$list_query->the_post();

		sbd_remove_duplicate_master(get_the_ID());
		
		//$lists = get_post_meta( get_the_ID(), 'qcpd_list_item01' );
		if(pd_ot_get_option('sbd_new_expire_after')!=''){
			sbd_new_expired(get_the_ID());
		}
		$lists = array();
		
		global $post;
		$terms = get_the_terms( get_the_ID(), 'pd_cat' );
		$slug = $post->post_name;
		
		if( $actual_pagination == "true" ){
			$sld_paged = isset($_GET['sld_paged']) && intval( $_GET['sld_paged'] > 0 ) ? $_GET['sld_paged'] : 1;
			$sld_list_id = isset($_GET['sld_list_id']) && intval( $_GET['sld_list_id'] > 0 ) ? $_GET['sld_list_id'] : 1;
			$all_result = $wpdb->get_var("SELECT COUNT(meta_id) FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page");
			$total_pagination_page = ceil($all_result / $per_page) ;
			
			if( $sld_paged > 1 && $sld_list_id == $post->ID ){
				$offset = ( ( $sld_paged - 1 ) * $per_page );
				$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page OFFSET $offset");	
			}else{
				$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page");	
			}
		}else{
			$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC");
		}
		if(!empty($results)){
			foreach($results as $result){
				$unserialize = unserialize($result->meta_value);
				if(!isset($unserialize['qcpd_unpublished']) or $unserialize['qcpd_unpublished']==0){					
					if(isset($unserialize['qcpd_ex_date']) && $unserialize['qcpd_ex_date']!='' && date('Y-m-d') > $unserialize['qcpd_ex_date']){
						//
					}else{
						$lists[] = $unserialize;
					}
				}
			}
		}

		$conf = get_post_meta( get_the_ID(), 'qcpd_list_conf', true );

		$addvertise = get_post_meta( get_the_ID(), 'pd_add_block', true );

		$addvertiseContent = isset($addvertise['add_block_text']) ? $addvertise['add_block_text'] : '';


		if( $item_orderby == 'upvotes' )
		{
			$lists = sbd_filter_by_upvote($lists);
		}

		if( $item_orderby == 'title' )
		{
			usort($lists, "pd_custom_sort_by_tpl_title");
		}

		if( $item_orderby == 'timestamp' )
		{
			usort($lists, "pd_custom_sort_by_tpl_timestamp");
		}

		if( $item_orderby == 'random' )
		{
			shuffle( $lists );
		}
		if(pd_ot_get_option('sbd_featured_item_top')=='on'){
							$lists = sbd_featured_at_top($lists);
						}
//adding extra variable in config
		//$conf['item_title_font_size'] = $title_font_size;
		//$conf['item_subtitle_font_size'] = $subtitle_font_size;
		//$conf['item_title_line_height'] = $title_line_height;
		//$conf['item_subtitle_line_height'] = $subtitle_line_height;
		?>

		<style>
			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 h3:after{
				border-bottom: 2px solid <?php echo isset($conf['item_bdr_color'])?$conf['item_bdr_color']:''; ?>;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li a {
			    background-color: <?php echo isset($conf['list_bg_color'])?$conf['list_bg_color']:''; ?>;
			    border-color: -moz-use-text-color -moz-use-text-color <?php echo isset($conf['item_bdr_color'])?$conf['item_bdr_color']:''; ?> !important;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover a {
			    background-color: <?php echo isset($conf['list_bg_color_hov'])?$conf['list_bg_color_hov']:''; ?>;
				color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
				border-color: -moz-use-text-color -moz-use-text-color <?php echo isset($conf['list_bg_color_hov'])?$conf['list_bg_color_hov']:''; ?> !important;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li a .li-txt {
			  color: <?php echo isset($conf['list_txt_color'])?$conf['list_txt_color']:''; ?>;
				<?php if($title_font_size!=''): ?>
				font-size:<?php echo $title_font_size; ?> !important;
				<?php endif; ?>

				<?php if($title_line_height!=''): ?>
				line-height:<?php echo $title_line_height; ?> !important;
				<?php endif; ?>
			}
			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li a .li-txt span{
				color: <?php echo isset($conf['list_subtxt_color'])?$conf['list_subtxt_color']:''; ?> !important;
				<?php if($subtitle_font_size!=''): ?>
				font-size:<?php echo $subtitle_font_size; ?> !important;
				<?php endif; ?>

				<?php if($subtitle_line_height!=''): ?>
				line-height:<?php echo $subtitle_line_height; ?>!important;
				<?php endif; ?>
			}
			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover a .li-txt span{
				color: <?php echo isset($conf['list_subtxt_color_hov'])?$conf['list_subtxt_color_hov']:''; ?> !important;
				
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover a .li-txt {
			  color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;

			}

			
			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 .tooltip_tpl8:before {
			  
			  border: 1px solid;
			  border-color: <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 .tooltip_tpl8:after {
			  border-bottom: 6px solid <?php echo isset($conf['list_txt_color_hov'])?$conf['list_txt_color_hov']:''; ?>;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li .upvote-section .sbd-upvote-btn, #qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 .ca-menu li .upvote-section .upvote-count{
			  color: <?php echo isset($conf['list_subtxt_color'])?$conf['list_subtxt_color']:''; ?>;
			}

			#qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover .upvote-section .sbd-upvote-btn, #qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>.style-8 li:hover .upvote-section .upvote-count{
			  color: <?php echo isset($conf['list_subtxt_color_hov'])?$conf['list_subtxt_color_hov']:''; ?>;
			}

			#item-<?php echo $listId .'-'. get_the_ID(); ?>-add-block .advertise-block.tpl-default{
				border: 1px solid #dedede;
				border-bottom: 2px solid <?php echo isset($conf['item_bdr_color'])?$conf['item_bdr_color']:''; ?>;
			}

			#item-<?php echo $listId .'-'. get_the_ID(); ?>-add-block .advertise-block.tpl-default ul{
				border: none;
				box-shadow: none !important;
				margin-bottom: 0 !important;
			}
		</style>

		<?php if( $paginate_items === 'true' && $actual_pagination == "true" ) : ?>

			<script>
				jQuery(document).ready(function($){
					$("#jp-holder-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>").jPages({
		    			containerID : "jp-list-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>",
		    			perPage : <?php echo $per_page; ?>,
		  			});
					
					<?php if(isset($cattabid)&&$cattabid!=''): ?>
					$(".pd_search_filter").keyup(function(){

						setTimeout(function(){
							$("#jp-holder-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>").jPages({
								containerID : "jp-list-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>",
								perPage : <?php echo $per_page; ?>,
							});
							$('.qc-grid').packery({
							  itemSelector: '.qc-grid-item',
							  gutter: 10
							});
						}, 900);

					})
					<?php endif; ?>
					

				});
			</script>

		<?php endif; ?>

		<div id="qcpd-list-<?php echo $listId .'-'. get_the_ID(); ?>" class="qc-grid-item qcpd-list-column opd-column-<?php echo $column; echo " style-8";?> <?php echo "opd-list-id-" . get_the_ID(); ?>">

			<div class="opd-list-pd-style-8 qcpd-single-list-pd">
				<?php
					$item_count_disp = "";

					if( $item_count == "on" ){
						$item_count_disp = count($lists);
					}
				?>
				<?php if( $hide_list_title != 'true' ){ ?>
					<h2 <?php echo (isset($conf['list_title_color'])&&$conf['list_title_color']!=''?'style="color:'.$conf['list_title_color'].';"':''); ?>>
						<?php
							if(isset($conf['title_link']) && $conf['title_link']!=''):
								echo '<a href="'.$conf['title_link'].'" '.(isset($conf['title_link_new_tab'])&&$conf['title_link_new_tab']==1?'target="_blank"':'').' >';
							endif;
							?>
							<?php echo esc_html(get_the_title()); ?>
							<?php
								if($item_count == 'on'){
									if( $actual_pagination == "true" ){
										echo '<span class="opd-item-count">('.$all_result.')</span>';
									}else{
										echo '<span class="opd-item-count">('.$item_count_disp.')</span>';
									}
								}
							?>
							<?php 
							if(isset($conf['title_link']) && $conf['title_link']!=''):
								echo '</a>';
							endif;
						?>
					</h2>
				<?php } ?>
				<ul class="tooltip_tpl8-tpl" id="jp-list-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>">
					<?php $count = 1; ?>
					<?php foreach( $lists as $list ) : ?>
					<?php
						$canContentClass = "subtitle-present";

						if( !isset($list['qcpd_item_subtitle']) || $list['qcpd_item_subtitle'] == "" )
						{
							$canContentClass = "subtitle-absent";
						}
						$latlon = '';
						if(isset($list['qcpd_item_latitude']) && $list['qcpd_item_latitude']!='' && isset($list['qcpd_item_longitude']) && $list['qcpd_item_longitude']!=''){
							$latlon = $list['qcpd_item_latitude'].','.$list['qcpd_item_longitude'];
						}
						$marker = '';
						if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
							$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
						}
					?>
					<li id="item-<?php echo get_the_ID() ."-". $count; ?>" data-latlon="<?php echo $latlon; ?>" data-title="<?php echo $list['qcpd_item_title']; ?>" data-subtitle="<?php echo $list['qcpd_item_subtitle']; ?>" data-phone="<?php echo $list['qcpd_item_phone']; ?>" data-address="<?php echo $list['qcpd_item_full_address']; ?>" data-url="<?php echo $list['qcpd_item_link']; ?>" <?php echo $marker; ?> data-local="<?php echo $list['qcpd_item_location']; ?>" data-businesshour="<?php echo isset($list['qcpd_item_business_hour'])?$list['qcpd_item_business_hour']:''; ?>" data-paid="<?php echo (isset($list['qcpd_paid'])?$list['qcpd_paid']:''); ?>">

						<?php
							$popContent='';
							$toolipsetup = '';
							$item_url = $list['qcpd_item_link'];
							$masked_url = $list['qcpd_item_link'];

							$mask_url = isset($mask_url) ? $mask_url : 'off';

							if( $mask_url == 'on' ){
								$masked_url = 'http://' . qcpd_get_domain($list['qcpd_item_link']);
							}
							$hover_info = 'title="'.$go_to_website_text.'"';
							if($main_click_action=='0'){
								$masked_url = 'tel:'.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']);
								$hover_info = 'title="Call '.$list['qcpd_item_phone'].'"';
							}


                        // if(isset($list['qcpd_item_subtitle']) and $list['qcpd_item_subtitle']!=''){
	                       //  $toolipsetup = 'class="tooltip-element tooltip_tpl8 fade" data-title="'.trim($list['qcpd_item_subtitle']).'"';
                        // }else{
	                       //  $toolipsetup = '';
                        // }
						if($main_click_action=='2'){
							$masked_url = '#';
							if(isset($list['qcpd_item_subtitle']) and $list['qcpd_item_subtitle']!=''){
		                        $popContent = 'class="tooltip-element tooltip_tpl8 fade open-mpf-sld-link" data-title="'.trim($list['qcpd_item_subtitle']).'" data-post-id="'.get_the_ID().'" data-item-title="'.trim($list['qcpd_item_title']).'" data-item-link="'.$list['qcpd_item_link'].'" data-mfp-src="#busi-'.get_the_ID() ."-". $count.'"';
	                        }else{
								$popContent = 'class="tooltip-element fade open-mpf-sld-link" data-post-id="'.get_the_ID().'" data-item-title="'.trim($list['qcpd_item_title']).'" data-item-link="'.$list['qcpd_item_link'].'" data-mfp-src="#busi-'.get_the_ID() ."-". $count.'"';
	                        }
							$hover_info = pd_ot_get_option('sbd_lan_open_popup') ? 'title="'.pd_ot_get_option('sbd_lan_open_popup').'"' : 'title="Open Popup"';
						}else{
							if(isset($list['qcpd_item_subtitle']) and $list['qcpd_item_subtitle']!=''){
		                        $popContent = 'class="tooltip-element tooltip_tpl8 fade" data-title="'.trim($list['qcpd_item_subtitle']).'"';
	                        }else{
		                        $popContent = '';
	                        }
						}
						if($main_click_action=='4' && !empty($optionPage) && pd_ot_get_option('sbd_enable_multipage')=='on'){
							$category = 'default';
							if(!empty($terms)){
								$category = $terms[0]->slug;
							}
							$popContent = '';
							$masked_url = $directoryPage.'/'.$category.'/'.$slug.'/'.urlencode(str_replace(' ','-',strtolower($list['qcpd_item_title']))).'/'.$list['qcpd_timelaps'];
						}

						?>

						<a <?php if( $mask_url == 'on') { echo 'onclick="document.location.href = \''.$item_url.'\'; return false;"'; } ?> <?php //echo $toolipsetup; ?> <?php echo (isset($list['qcpd_item_nofollow']) && $list['qcpd_item_nofollow'] == 1) ? 'rel="nofollow"' : ''; ?> <?php echo (isset($main_click_action)&&$main_click_action==3?'':'href="'.$masked_url.'"'); ?> <?php echo (isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1) ? 'target="_blank"' : ''; ?> data-tag="<?php echo (isset($list['qcpd_tags'])?$list['qcpd_tags']:'' ); ?>" <?php echo $popContent; ?> <?php echo $hover_info; ?> >

						    <?php
								$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

								$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

								$faviconImgUrl = "";
								$faviconFetchable = false;
								$filteredUrl = "";

								$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

								if( $showFavicon == 1 )
								{
									$filteredUrl = qcpd_remove_http( $item_url );

									if( $item_url != '' )
									{

										$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
									}

									if( $directImgLink != '' )
									{

										$faviconImgUrl = trim($directImgLink);
									}

									$faviconFetchable = true;

									if( $item_url == '' && $directImgLink == '' ){
										$faviconFetchable = false;
									}
								}
							?>

							
								
								<!-- Image, If Present -->
							<?php if( ($list_img == "true") && isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>
								<?php 
									if (strpos($list['qcpd_item_img'], 'http') === FALSE){
								?>
								<span class="logo-icon">
									<?php
									
										$pd_list_image_size = pd_ot_get_option('pd_list_image_size') ? pd_ot_get_option('pd_list_image_size') : 'thumbnail';

										$img = wp_get_attachment_image_src($list['qcpd_item_img'], $pd_list_image_size);
									?>
									<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
								<?php
									}else{
								?>
								<span class="logo-icon">
									<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
								<?php
									}
								?>
								
							<?php elseif( $iconClass != "" ) : ?>
								<span class="logo-icon">
									<i class="fa <?php echo $iconClass; ?>"></i>
								</span>
							<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>
								<span class="logo-icon favicon-loaded">
									<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
							<?php else : ?>
								<span class="logo-icon">
									<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">
								</span>
							<?php endif; ?>

							<div class="li-txt">
								<?php
									if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('title', $sbd_fields_access) ){
										if( is_user_logged_in() ){
											echo trim($list['qcpd_item_title']);
										}
									}else{
										echo trim($list['qcpd_item_title']);
									}
								?>
								<?php 
							
								echo '<span style="display:block;font-size:11px">';
										
										if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('locale_designation', $sbd_fields_access) ){
											if( is_user_logged_in() ){
												echo ' <span style="font-weight:bold">'.$list['qcpd_item_location'].'</span> &nbsp;&nbsp;';
											}
										}else{
											echo ' <span style="font-weight:bold">'.$list['qcpd_item_location'].'</span> &nbsp;&nbsp;';
										}

										if(isset($list['qcpd_item_phone'])&&$list['qcpd_item_phone']!=''){
										
											if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('main_phone', $sbd_fields_access) ){
												if( is_user_logged_in() ){
													echo ($phone_number=='1'?'<span class="sbd_phone"><i class="fa fa-phone"></i> '.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']).'</span>':'');
												}
											}else{
												echo ($phone_number=='1'?'<span class="sbd_phone"><i class="fa fa-phone"></i> '.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']).'</span>':'');
											}
										}
										
								echo '</span>';
								
							?>

							<?php
								$average_rating_point = 0;
								$negative_rating_point = 5;
								if( function_exists('qcpdr_get_item_average_rating') && function_exists('qcpdr_ot_get_option')  && qcpdr_ot_get_option('qcpdr_enable_reviews') != 'off' && ( isset($review) && $review=='true' ) ){	
									global $post;
									$saved_post = $post;
									$average_rating = qcpdr_get_item_average_rating( get_the_ID(), $list['qcpd_timelaps'] );

									if( !empty($average_rating) && isset($average_rating['average_rating']) ){
										$average_rating_point = $average_rating['average_rating'];
										$negative_rating_point = (5 - $average_rating_point);

										$half_rating = ($average_rating_point + $negative_rating_point);

									?>
										<div class="sbd-average-review-field sbd-review-fields">
											<?php
												for ($i=1; $i <= $average_rating_point; $i++) { 
													echo '<i data-value="'.$i.'" class="fa fa-star qcpdr-fa-active"></i>';
												}
												if( is_float($average_rating_point) ){
													echo '<i data-value="'.$i.'" class="fa fa-star-half qcpdr-fa-active"></i>';	
													echo '<i data-value="'.$i.'" class="fa fa-star fa-star-blank-half"></i>';	
												}
												for ($i=1; $i <= $negative_rating_point; $i++) { 
													echo '<i data-value="'.$i.'" class="fa fa-star"></i>';
												}
											?>
											<span class="sbd-review-count">
												(<?php echo $average_rating['total_review_number']; ?>)
											</span>
										</div>
									<?php
									}
									if($saved_post) {
									    $post = $saved_post;
									}
								}
							?>

							<div class="sbd_custom_content">
							<?php
										include(QCSBD_DIR_MOD.'/template-common/qc_custom_body_content.php');
									?>
							</div>
							</div>
							

						</a>

						<?php if( $upvote == 'on' ) : ?>

							<!-- upvote section -->
							<div class="upvote-section">
							
								<?php if(pd_ot_get_option('pd_enable_bookmark')=='on'): ?>
							
							<?php
								if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('bookmark', $sbd_fields_access) ){
									if( is_user_logged_in() ){
										?>
											<!-- upvote section -->
											<div class="bookmark-section">
											
												<?php 
												$bookmark = 0;
												if(isset($list['qcpd_is_bookmarked']) and $list['qcpd_is_bookmarked']!=''){
													$unv = explode(',',$list['qcpd_is_bookmarked']);
													if(in_array(get_current_user_id(),$unv) && get_current_user_id()!=0){
														$bookmark = 1;
													}
												}
												?>
											
											
												<span data-post-id="<?php echo get_the_ID(); ?>" data-item-code="<?php echo trim($list['qcpd_timelaps']); ?>" data-is-bookmarked="<?php echo ($bookmark); ?>" class="bookmark-btn bookmark-on">
													
													<i class="fa <?php echo ($bookmark==1?'fa-star':'fa-star-o'); ?>" aria-hidden="true"></i>
												</span>
												
											</div>
										<?php
									}
								}else{
									?>
										<!-- upvote section -->
										<div class="bookmark-section">
										
											<?php 
											$bookmark = 0;
											if(isset($list['qcpd_is_bookmarked']) and $list['qcpd_is_bookmarked']!=''){
												$unv = explode(',',$list['qcpd_is_bookmarked']);
												if(in_array(get_current_user_id(),$unv) && get_current_user_id()!=0){
													$bookmark = 1;
												}
											}
											?>
										
										
											<span data-post-id="<?php echo get_the_ID(); ?>" data-item-code="<?php echo trim($list['qcpd_timelaps']); ?>" data-is-bookmarked="<?php echo ($bookmark); ?>" class="bookmark-btn bookmark-on">
												
												<i class="fa <?php echo ($bookmark==1?'fa-star':'fa-star-o'); ?>" aria-hidden="true"></i>
											</span>
											
										</div>
									<?php
								}
							?>
							<?php endif; ?>
								
								<?php
								if( isset($sbd_fields_access) && !empty($sbd_fields_access) && in_array('upvote', $sbd_fields_access) ){
									if( is_user_logged_in() ){ ?>
										<span data-post-id="<?php echo get_the_ID(); ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" class="sbd-upvote-btn upvote-on">
											<i class="fa <?php echo $pd_thumbs_up; ?>"></i>
										</span>
										<span class="upvote-count">
											<?php
											  if( isset($list['qcpd_upvote_count']) && (int)$list['qcpd_upvote_count'] > 0 ){
											  	echo (int)$list['qcpd_upvote_count'];
											  }
											?>
										</span>
								<?php	}
								}else{ ?>
									<span data-post-id="<?php echo get_the_ID(); ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" class="sbd-upvote-btn upvote-on">
										<i class="fa <?php echo $pd_thumbs_up; ?>"></i>
									</span>
									<span class="upvote-count">
										<?php
										  if( isset($list['qcpd_upvote_count']) && (int)$list['qcpd_upvote_count'] > 0 ){
										  	echo (int)$list['qcpd_upvote_count'];
										  }
										?>
									</span>
								<?php	}
								?>
							</div>
							<!-- /upvote section -->

						<?php endif; ?>
						
							
							<?php if(isset($list['qcpd_new']) and $list['qcpd_new']==1):?>
							<!-- new icon section -->
							<div class="new-icon-section">
								<span>new</span>
							</div>
							<!-- /new icon section -->
							<?php endif; ?>
							
							<?php if(sbd_display_claim_badge($list)): ?>
							<!-- new icon section -->
							<div class="new-icon-section sbd-claim-badge">
								<span>claimed</span>
							</div>
							<!-- /new icon section -->
							<?php endif; ?>
							
							<?php if(isset($list['qcpd_featured']) and $list['qcpd_featured']==1):?>
							<!-- featured section -->
							<div class="featured-section">
								<i class="fa fa-bolt"></i>
							</div>
							<!-- /featured section -->
							<?php endif; ?>
							
							<?php if(isset($list['qcpd_verified']) and $list['qcpd_verified']==1):?>
							<!-- featured section -->
							<div class="sbd-verified-style5" title="Verified Listing">
								<i class="fa fa-check-circle" aria-hidden="true"></i>
							</div>
							<!-- /featured section -->
							<?php endif; ?>

							<div class="pd-bottom-area">
						
								<?php 
								$category = 'default';
								if(!empty($terms)){
									$category = $terms[0]->slug;
								}
								
								$newurl = $directoryPage.'/'.$category.'/'.$slug.'/'.urlencode(str_replace(' ','-',strtolower($list['qcpd_item_title']))).'/'.$list['qcpd_timelaps'];
								if($item_details_page=='on' && !empty($optionPage) && pd_ot_get_option('sbd_enable_multipage')=='on'):
								?>
								<p><a class="sld_internal_link" href="<?php echo $newurl; ?>" title="Go to link details page"><i class="fa fa-external-link-square fa-external-link-square-alt" aria-hidden="true"></i></a></p>
								<?php endif; ?>
						
								<?php if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!='' and $phone_number!=0): ?>
									<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>" title="Call <?php echo $list['qcpd_item_phone']; ?>"><?php sbd_icons_content('phone'); ?></a></p>
								<?php endif; ?>
								
								<?php if($list['qcpd_item_link']!=''): ?>
									<p><a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank" title="<?php echo pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website'; ?>"><?php sbd_icons_content('link'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><?php sbd_icons_content('facebook'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><?php sbd_icons_content('yelp'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''): ?>
								<p><a data-email="<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>" class="sbd_email_form" href="#"><?php sbd_icons_content('email'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><?php sbd_icons_content('linkedin',trim($list['qcpd_item_linkedin'])); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><?php sbd_icons_content('twitter'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!='' and $pdmapofflightbox!='true'): 
								$marker = '';
								if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
									$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
								}
								?>
								<p><a href="#" class="pd-map open-mpf-sld-link" full-address="<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>" data-mfp-src="#map-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $marker; ?>><?php sbd_icons_content('map'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_description']) and $list['qcpd_description']!=''): ?>
								<p><a href="#" class="open-mpf-sld-link" data-post-id="<?php echo get_the_ID(); ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" data-mfp-src="#busi-<?php echo get_the_ID() ."-". $count; ?>"><?php sbd_icons_content('info'); ?></a></p>
								<?php endif; ?>
								
								<?php
										include(QCSBD_DIR_MOD.'/template-common/qc_custom_icon_content.php');
									?>


								<?php if(  class_exists('QCPD\QCPD_Review_Rating') && function_exists('qcpdr_ot_get_option')  && qcpdr_ot_get_option('qcpdr_enable_reviews') != 'off' && ( isset($review) && $review=='true' ) ){ ?>
									<div data-list-id="<?php echo get_the_ID(); ?>" data-item-id="<?php echo $list['qcpd_timelaps']; ?>" data-mfp-src="#sbd-item-average-rating-form-<?php echo $count.'-'.get_the_ID().'-'.$list['qcpd_timelaps']; ?>" class="sbd-rating-inverse sbd-item-review-opener sbd-item-review-opener-active">
										<div class="sbd-item-review-opener-inner <?php if( $average_rating_point == 0 ){ echo 'sbd-no-rating'; } ?>">
											<span class="fa-stack fa-x">
											    <i class="fa fa-star qcpdr-fa-active fa-stack-2x"></i>
											      <span class="fa fa-stack-1x">
											          <span class="rating-number">
											              <?php if( $average_rating_point > 0 ){ echo round($average_rating_point * 2) / 2; }else{ echo 0; } ?>
											          </span>
											    </span>
											</span>	
										</div>
										<div class="sbd-item-star-all-icons">
											<?php
												for ($i=1; $i <= $average_rating_point; $i++) { 
													echo '<i data-value="'.$i.'" class="fa fa-star qcpdr-fa-active"></i>';
												}
												if( is_float($average_rating_point) ){
													echo '<i data-value="'.$i.'" class="fa fa-star-half qcpdr-fa-active"></i>';	
													echo '<i data-value="'.$i.'" class="fa fa-star fa-star-blank-half"></i>';	
												}
												for ($i=1; $i <= $negative_rating_point; $i++) { 
													echo '<i data-value="'.$i.'" class="fa fa-star"></i>';
												}
											?>
										</div>
									</div>
									<div data-list-id="<?php echo get_the_ID(); ?>" data-item-id="<?php echo $list['qcpd_timelaps']; ?>" id="sbd-item-average-rating-form-<?php echo $count.'-'.get_the_ID().'-'.$list['qcpd_timelaps']; ?>" class="white-popup mfp-hide">
										<div class="sbd-review-ajax-form">
											<?php
												if( class_exists('QCPD\FrontEnd\Review\Reviews') ){
													// $sbd_review = new \QCPD\FrontEnd\Review\Reviews();
													// $sbd_review->display_ajax_review_fields( $list, get_the_ID() );
												}
											?>
										</div>
									</div>
								<?php } ?>
							
						</div>
							
							
							<div id="map-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide"><a class="sbd_direction_btn" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo @urlencode(@$list['qcpd_item_full_address']); ?>&travelmode=driving" target="_blank"><?php echo (pd_ot_get_option('sbd_lan_get_direction')!=''?pd_ot_get_option('sbd_lan_get_direction'):'Get Direction'); ?></a>
								<div class="pd_map_container" id="mapcontainer-<?php echo get_the_ID() ."-". $count; ?>"></div>
							</div>
				
							<div id="busi-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
								<div class="sbd_business_container">
									<span class="qc_sbd_loading_details">Loading...</span>
								</div>
							</div>
							
					</li>

					<?php $count++; endforeach; ?>
				</ul>

				<?php if( $paginate_items === 'true' && $actual_pagination == "true" ) : ?>

				<!-- navigation panel -->
				<div id="jp-holder-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>" class="pdp-holder"></div>

				<?php endif; ?>

				<?php
					if( $actual_pagination == 'true' && $total_pagination_page > 1 ){
						echo sld_pagination_links( $sld_paged, $total_pagination_page, $post->ID);
					}
				?>

			</div>
		</div>

		<?php if( $addvertiseContent != '' ) : ?>
		<!-- Add Block -->
		<div class="qc-grid-item qcpd-list-column opd-column-<?php echo $column; ?> <?php echo "opd-list-id-" . get_the_ID(); ?>" id="item-<?php echo $listId .'-'. get_the_ID(); ?>-add-block">
			<div class="advertise-block tpl-default">
				<?php echo apply_filters('the_content',$addvertiseContent); ?>
			</div>
		</div>
		<!-- /Add Block -->
		<?php endif; ?>

		<?php

		$listId++;
	}

	echo '<div class="pd-clearfix"></div>
			</div>
		<div class="pd-clearfix"></div>';

	//Hook - After Main List
	do_action( 'qcpd_after_main_list', $shortcodeAtts);

}

?>
	</div>
	<?php if($map=='show' && $map_position=='right'){ ?>
			<div class="map-right-side" id="sbd_map_sidebar">
				<?php echo '<div id="sbd_all_location'.($cattabid!=''?$cattabid:'').'" class="sbd_map" style="height:100%" ></div>'; ?>
			</div>
		<?php } ?>
</div>
<script>
var login_url_pd = '<?php echo pd_ot_get_option('pd_bookmark_user_login_url'); ?>';
var template = '<?php echo $style; ?>';
var bookmark = {
	<?php 
	if ( is_user_logged_in() ) {
	?>
	is_user_logged_in:true,
	<?php
	} else {
	?>
	is_user_logged_in:false,
	<?php
	}
	?>
	userid: <?php echo get_current_user_id(); ?>

};
	jQuery(document).ready(function($){

		$( '.filter-btn[data-filter="all"]' ).on( "click", function() {
	  		//Masonary Grid
		    $('.qc-grid').packery({
		      itemSelector: '.qc-grid-item',
		      gutter: 10
		    });
		});

		<?php if( isset($cattabid) && ($cattabid!='') ){ ?>
			<?php if($cattabid == 0){ ?>
				$( '.filter-btn[data-filter="all"]' ).trigger( "click" );
			<?php } ?>
		<?php }else{ ?>
			$( '.filter-btn[data-filter="all"]' ).trigger( "click" );
		<?php } ?>

	});
<?php if($map=='show' && $map_position=='right'){ ?>
jQuery(document).ready(function($) {
	
	var StickyS = new StickySidebar('#sbd_map_sidebar', {
			topSpacing: 20,
			bottomSpacing: 20,
			containerSelector: '.sbd_main_wrapper',
			innerWrapperSelector: '#sbd_all_location<?php echo ($cattabid!=''?$cattabid:''); ?>'
		});

	var windowWidth = jQuery(window).width();
	if (windowWidth < 991) {
		StickyS.destroy();
	}
		
	jQuery(window).resize(function() {
		var windowWidth = jQuery(window).width();
		if (windowWidth < 991) {
			StickyS.destroy();
		}
	});

})
<?php } ?>
</script>
