<?php
//wp_enqueue_script( 'qcpd-custom-maponly-script', QCSBD_ASSETS_URL . '/js/directory-maponly-script.js', array('jquery'),'',true);
$pd_map_open_street_map = pd_ot_get_option('pd_map_open_street_map') ? pd_ot_get_option('pd_map_open_street_map') : '';

if($pd_map_open_street_map == 'on'){
	wp_enqueue_script( 'qcpd-custom-maponly-script', QCSBD_ASSETS_URL . '/js/directory-maponly-street-map-script.js', array('jquery'),'',true);
}else{
	wp_enqueue_script( 'qcpd-custom-maponly-script', QCSBD_ASSETS_URL . '/js/directory-maponly-script.js', array('jquery'),'',true);
}
$sld_variables_maponly = array(
	'distance_location_text' =>	(pd_ot_get_option('sbd_lan_distance_location')!=''?pd_ot_get_option('sbd_lan_distance_location'):'Please provide location'),
	'distance_value_text'=>	(pd_ot_get_option('sbd_lan_distance_value')!=''?pd_ot_get_option('sbd_lan_distance_value'):'Please provide Distance value'),
	'distance_no_result_text' => (pd_ot_get_option('sbd_lan_no_result')!=''?pd_ot_get_option('sbd_lan_no_result'):'No result found!'),
	'radius_circle_color'=> (pd_ot_get_option('sbd_radius_circle_color')!=''?pd_ot_get_option('sbd_radius_circle_color'):'#FF0000'),
	'zoom'=> (pd_ot_get_option('sbd_map_zoom')!=''?pd_ot_get_option('sbd_map_zoom'):'13'),
	'popup_zoom'=> (pd_ot_get_option('sbd_popup_map_zoom')!=''?pd_ot_get_option('sbd_popup_map_zoom'):'13'),
	'cluster'=> ($marker_cluster=='true'?true:false),
	'image_infowindow'=> ($image_infowindow=='true'?true:false),
	'latitute'=> (pd_ot_get_option('sbd_map_lat')!=''?pd_ot_get_option('sbd_map_lat'):'37.090240'),
	'longitute'=> (pd_ot_get_option('sbd_map_long')!=''?pd_ot_get_option('sbd_map_long'):'-95.712891'),
	'global_marker'=>(pd_ot_get_option('sbd_global_marker_image')!=''?pd_ot_get_option('sbd_global_marker_image'):''),
	'paid_marker'=>(pd_ot_get_option('sbd_paid_marker_image')!=''?pd_ot_get_option('sbd_paid_marker_image'):''),
	'paid_marker_default'=> QCSBD_IMG_URL.'/Pink-icon.png',
);

wp_localize_script('qcpd-custom-maponly-script', 'sld_variables_maponly', $sld_variables_maponly);

// The Loop
if ( $list_query->have_posts() )
{

	//Getting Settings Values
	if($search=='true'){
		$searchSettings = 'on';
	}else{
		if($search=='false'){
			$searchSettings = 'off';
		}else{
			$searchSettings = pd_ot_get_option( 'pd_enable_search' );
		}
	}


	$itemAddSettings = pd_ot_get_option( 'pd_enable_add_new_item' );
	$itemAddLink = pd_ot_get_option( 'pd_add_item_link' );
	$enableTopArea = pd_ot_get_option( 'pd_enable_top_part' );
	$enableFiltering = pd_ot_get_option( 'pd_enable_filtering' );

	//Check if border should be set
	$borderClass = "";

	if( $searchSettings == 'on' || $itemAddSettings == 'on' )
	{
		$borderClass = "pd-border-bottom";
	}
	if($enable_map_toggle_filter=='false'){
	//Hook - Before Search Template
		do_action( 'qcpd_before_search_tpl', $shortcodeAtts);
		
		//If the top area is not disabled (both serch and add item)
		if( $enableTopArea == 'on' && $top_area != 'off' ) :
			//Load Search Template
			require ( dirname(__FILE__) . "/search-template.php" );

		endif;
		
		if($radius=='true'){
			sbdRadiusSearch();
		}
		
		if($enable_tag_filter=='true'){
			if($enable_tag_filter_dropdown=='true'){
				show_tag_filter_dropdown($list_args);
			}else{
				show_tag_filter($category,$shortcodeAtts);
			}
		}
			

		//Hook - Before Filter Template
		do_action( 'qcpd_before_filter_tpl', $shortcodeAtts);

		//Enable Filtering
		if( $enableFiltering == 'on') :

			//Load Search Template
			require ( dirname(__FILE__) . "/filter-template.php" );
		endif;
	}

	echo '<div class="sbd_maponly_parent"><div id="sbd_maponly_container" class="sbd_maponly"></div>';
	
		if($enable_map_toggle_filter=='true'){
			echo '<div class="sbd_maponly_top_part">';
			echo '<div class="sbd_filter_toggle" title="Toggle - click to expand & collapse Filtering"><i class="fa fa-bars" aria-hidden="true"></i></div>';
			//Hook - Before Search Template
			echo '<div class="sbd_filter_parent">';
			do_action( 'qcpd_before_search_tpl', $shortcodeAtts);
			
			//If the top area is not disabled (both serch and add item)
			if( $enableTopArea == 'on' && $top_area != 'off' ) :
				//Load Search Template
				require ( dirname(__FILE__) . "/search-template.php" );

			endif;
			
			if($radius=='true'){
				sbdRadiusSearch();
			}
			
			if($enable_tag_filter=='true')
				show_tag_filter($category,$shortcodeAtts);

			//Hook - Before Filter Template
			do_action( 'qcpd_before_filter_tpl', $shortcodeAtts);

			//Enable Filtering
			if( $enableFiltering == 'on') :

				//Load Search Template
				require ( dirname(__FILE__) . "/filter-template.php" );
			endif;
			echo '</div>';
			echo '</div>';
		}
		
	echo '</div>';
	do_action( 'qcpd_after_main_list', $shortcodeAtts);
	global $wpdb;

?>
<script type="text/javascript">
var all_items = [];
<?php
	$optionPage = get_page(sbd_get_option_page('sbd_directory_page'));
	$qcld_post_name = isset($optionPage->post_name) ? '/'. $optionPage->post_name : '';
	$directoryPage = home_url(). $qcld_post_name;
	
	while ( $list_query->have_posts() )
	{
		$list_query->the_post();
		
		$lists = array();
		$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC");
		
		$terms = get_the_terms( get_the_ID(), 'pd_cat' );
		$slug = get_post_field( 'post_name', get_the_ID() );
		
		if(!empty($results)){
			$count = 1;
			foreach($results as $result){
				$unserialize = unserialize($result->meta_value);	
				$img = '';
				if (strpos($unserialize['qcpd_item_img'], 'http') === FALSE){
					$img1 = wp_get_attachment_image_src($unserialize['qcpd_item_img']);
					$img = $img1 ? $img1 : '';
					//$img = '';
				}else{
					$img = $unserialize['qcpd_item_img'];
				}
				if(!isset($unserialize['qcpd_unpublished']) or $unserialize['qcpd_unpublished']==0){
					
					

					
					
				?>
					all_items.push({
					map_marker_id: "item-<?php echo get_the_ID(); ?>-<?php echo $count++; ?>",
					title: "<?php echo htmlspecialchars($unserialize['qcpd_item_title']); ?>",
					link: "<?php echo $unserialize['qcpd_item_link']; ?>",
					subtitle: "<?php echo htmlspecialchars($unserialize['qcpd_item_subtitle']); ?>",

				<?php
					$newurl = '';
					if($item_details_page=='on' && !empty($optionPage) && pd_ot_get_option('sbd_enable_multipage')=='on'):
						$cat_slug = 'default';
						if( isset($category[0]) && $category[0] != '' ){
							$cat_slug = $category[0];
						}
						$newurl = $directoryPage.'/'.$cat_slug.'/'.$slug.'/'.urlencode(str_replace(' ','-',strtolower($unserialize['qcpd_item_title']))).'/'.$unserialize['qcpd_timelaps'];
					endif;
				?>
					item_details_page: "<?php echo $newurl; ?>",
					phone: "<?php echo $unserialize['qcpd_item_phone']; ?>",
					email: "<?php echo $unserialize['qcpd_item_email']; ?>",
					fulladdress: "<?php echo $unserialize['qcpd_item_full_address']; ?>",
					latitude: "<?php echo $unserialize['qcpd_item_latitude']; ?>",
					longitude: "<?php echo $unserialize['qcpd_item_longitude']; ?>",
					location: "<?php echo $unserialize['qcpd_item_location']; ?>",
					image: "<?php echo $img; ?>",
					markericon: "<?php echo (isset($unserialize['qcpd_item_marker'])&&$unserialize['qcpd_item_marker']!=''?wp_get_attachment_image_src($unserialize['qcpd_item_marker'])[0]:''); ?>",
					filter: "opd-list-id-<?php echo get_the_ID(); ?>",
					searchcontent: "<?php echo htmlspecialchars($unserialize['qcpd_item_title']); ?> <?php echo $unserialize['qcpd_item_link']; ?> <?php echo htmlspecialchars($unserialize['qcpd_item_subtitle']); ?> <?php echo $unserialize['qcpd_item_phone']; ?> <?php echo $unserialize['qcpd_item_email']; ?> <?php echo $unserialize['qcpd_item_full_address']; ?> <?php echo $unserialize['qcpd_item_location']; ?>",
					tags: "<?php echo htmlspecialchars($unserialize['qcpd_tags']); ?>",
					facebook: "<?php echo (isset($unserialize['qcpd_item_facebook']) && $unserialize['qcpd_item_facebook']!=''?trim($unserialize['qcpd_item_facebook']):''); ?>",
					yelp: "<?php echo (isset($unserialize['qcpd_item_yelp']) && $unserialize['qcpd_item_yelp']!=''?trim($unserialize['qcpd_item_yelp']):''); ?>",
					linkedin: "<?php echo (isset($unserialize['qcpd_item_linkedin']) && $unserialize['qcpd_item_linkedin']!=''?trim($unserialize['qcpd_item_linkedin']):''); ?>",
					twitter: "<?php echo (isset($unserialize['qcpd_item_twitter']) && $unserialize['qcpd_item_twitter']!=''?trim($unserialize['qcpd_item_twitter']):''); ?>",
					business: "<?php echo $unserialize['qcpd_item_business_hour']; ?>",
					paid: "<?php echo (isset($unserialize['qcpd_paid'])?$unserialize['qcpd_paid']:''); ?>",
					custom_body_content: "<?php echo qc_sbd_custom_body($unserialize); ?>",
					custom_body_icon: "<?php echo esc_js(qc_sbd_custom_icon($unserialize)); ?>",
					});
				<?php	
				}
			}
		}

	}
?>

</script>
<?php
}

?>

