<?php $go_to_website_text = pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website'; ?>
<link rel="stylesheet" type="text/css" href="<?php echo OCSBD_TPL_URL . "/$template_code/style.css"; ?>" />
<script src="<?php echo QCSBD_ASSETS_URL . "/js/jquery-sticky.js"; ?>"></script>

<?php
	$customCss = pd_ot_get_option( 'pd_custom_style' );

	if( trim($customCss) != "" ) :
?>
	<style>
		<?php echo trim($customCss); ?>
	</style>

<?php endif; ?>

<?php

// The Loop
if ( $list_query->have_posts() )
{

	//Getting Settings Values
	if($search=='true'){
		$searchSettings = 'on';
	}else{
		if($search=='false'){
			$searchSettings = 'off';
		}else{
			$searchSettings = pd_ot_get_option( 'pd_enable_search' );
		}
	}
	$itemAddSettings = pd_ot_get_option( 'pd_enable_add_new_item' );
	$itemAddLink = pd_ot_get_option( 'pd_add_item_link' );
	$enableTopArea = pd_ot_get_option( 'pd_enable_top_part' );
	$enableFiltering = pd_ot_get_option( 'pd_enable_filtering' );

	//Check if border should be set
	$borderClass = "";

	if( $searchSettings == 'on' || $itemAddSettings == 'on' )
	{
		$borderClass = "pd-border-bottom";
	}

	//Hook - Before Search Template
	do_action( 'qcpd_before_search_tpl', $shortcodeAtts);

	//If the top area is not disabled (both serch and add item)
	if( $enableTopArea == 'on' && $top_area != 'off' ) :

		//Load Search Template
		require ( dirname(__FILE__) . "/search-template.php" );

	endif;
	
	if($radius=='true'){
		sbdRadiusSearch();
	}
	if($enable_tag_filter=='true'){
		if($enable_tag_filter_dropdown=='true'){
			show_tag_filter_dropdown($list_args);
		}else{
			show_tag_filter($category,$shortcodeAtts);
		}
	}
	//Hook - Before Filter Template
	do_action( 'qcpd_before_filter_tpl', $shortcodeAtts);

	//Enable Filtering
	if( $enableFiltering == 'on' && $mode == 'all' && $enable_left_filter!='true' ) :

		//Load Search Template
		require ( dirname(__FILE__) . "/filter-template.php" );

	endif;

	//If RTL is Enabled
	$rtlSettings = pd_ot_get_option( 'pd_enable_rtl' );
	$rtlClass = "";
	
	if($enable_rtl=='true'){
		$rtlSettings = 'on';
	}
	
	if( $rtlSettings == 'on' )
	{
	   $rtlClass = "direction-rtl";
	}

	//Hook - Before Main List
	do_action( 'qcpd_before_main_list', $shortcodeAtts);
	
	if($map=='show' && $map_position=='top'){
		echo '<div id="sbd_all_location'.($cattabid!=''?$cattabid:'').'" class="sbd_map"></div>';
	}

	echo '<div class="qcpd-list-wrapper qc-full-wrapper">';
	?>

	<?php
	echo '<div id="sbdopd-list-holder" class="qcpd-list-hoder '.$rtlClass.'">';
	
	echo '<section class="qc-page-section qc-main-section"><div class="qc-sbd-inner-row-11 qc-sbd-wrapper" id="sbd_slide_container"><div class="qc-sbd-grid-11" '.($map!='show'||$map_position=='top'?'style="width:100%"':'').' >';
	
	global $wpdb;
	

	

	$outbound_conf = pd_ot_get_option( 'pd_enable_click_tracking' );

	$listId = 1;
		while ( $list_query->have_posts() )
	{
		$list_query->the_post();

		sbd_remove_duplicate_master(get_the_ID());
		
		//$lists = get_post_meta( get_the_ID(), 'qcpd_list_item01' );
		if(pd_ot_get_option('sbd_new_expire_after')!=''){
			sbd_new_expired(get_the_ID());
		}
		$lists = array();
		if( $actual_pagination == "true" ){
			$sld_paged = isset($_GET['sld_paged']) && intval( $_GET['sld_paged'] > 0 ) ? $_GET['sld_paged'] : 1;
			$sld_list_id = isset($_GET['sld_list_id']) && intval( $_GET['sld_list_id'] > 0 ) ? $_GET['sld_list_id'] : 1;
			$all_result = $wpdb->get_var("SELECT COUNT(meta_id) FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page");
			$total_pagination_page = ceil($all_result / $per_page) ;
			
			if( $sld_paged > 1 && $sld_list_id == get_the_ID() ){
				$offset = ( ( $sld_paged - 1 ) * $per_page );
				$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page OFFSET $offset");	
			}else{
				$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC LIMIT $per_page");	
			}
		}else{
			$results = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE post_id = ".get_the_ID()." AND meta_key = 'qcpd_list_item01' order by `meta_id` ASC");
		}
		if(!empty($results)){
			foreach($results as $result){
				$unserialize = unserialize($result->meta_value);
				if(!isset($unserialize['qcpd_unpublished']) or $unserialize['qcpd_unpublished']==0){					
					if(isset($unserialize['qcpd_ex_date']) && $unserialize['qcpd_ex_date']!='' && date('Y-m-d') > $unserialize['qcpd_ex_date']){
						//
					}else{
						$lists[] = $unserialize;
					}
				}
			}
		}
		
		$conf = get_post_meta( get_the_ID(), 'qcpd_list_conf', true );

		$addvertise = get_post_meta( get_the_ID(), 'pd_add_block', true );

		$addvertiseContent = isset($addvertise['add_block_text']) ? $addvertise['add_block_text'] : '';

		//adding extra variable in config
		$conf['item_title_font_size'] = $title_font_size;
		$conf['item_subtitle_font_size'] = $subtitle_font_size;
		$conf['item_title_line_height'] = $title_line_height;
		$conf['item_subtitle_line_height'] = $subtitle_line_height;
		
		
		
		?>

		<style>


			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> .qc-sbd-main{
				background-color: <?php echo isset($conf['list_bg_color'])?$conf['list_bg_color']:''; ?> !important;
				
			}
			
			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> .sbd-title{
				
				color: <?php echo isset($conf['list_txt_color'])?$conf['list_txt_color']:''; ?> !important;

				<?php if(isset( $conf['item_title_font_size'] ) && $conf['item_title_font_size']!=''): ?>
				font-size:<?php echo isset($conf['item_title_font_size'])?$conf['item_title_font_size']:''; ?>;
				<?php endif; ?>

				<?php if(isset( $conf['item_title_line_height'] ) && $conf['item_title_line_height']!=''): ?>
				line-height:<?php echo isset($conf['item_title_line_height'])?$conf['item_title_line_height']:''; ?>;
				<?php endif; ?>

				
			}
			
			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> .sub-title{
				
				color: <?php echo isset($conf['list_subtxt_color'])?$conf['list_subtxt_color']:''; ?> !important;
				<?php if(isset( $conf['item_subtitle_font_size'] ) && $conf['item_subtitle_font_size']!=''): ?>
				font-size:<?php echo isset($conf['item_subtitle_font_size'])?$conf['item_subtitle_font_size']:''; ?> !important;
				<?php endif; ?>

				<?php if(isset( $conf['item_subtitle_line_height'] ) && $conf['item_subtitle_line_height']!=''): ?>
				line-height:<?php echo isset($conf['item_subtitle_line_height']) ? $conf['item_subtitle_line_height'] : ''; ?>!important;
				<?php endif; ?>

				
			}
			
			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> ul li:hover a .sub-title{
				
				color: <?php echo isset($conf['list_subtxt_color_hov']) ? $conf['list_subtxt_color_hov'] : ''; ?> !important;

				
			}
			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> ul li:hover .upvote-icon-section{
				
				color: <?php echo isset($conf['list_subtxt_color_hov']) ? $conf['list_subtxt_color_hov'] : ''; ?> !important;

				
			}
			
			#list-item-<?php echo $listId .'-'. get_the_ID(); ?> ul li:hover a .sbd-title{
				
				color: <?php echo isset($conf['list_txt_color_hov']) ? $conf['list_txt_color_hov'] : ''; ?> !important;

			}



		</style>
	
		<div id="list-item-<?php echo $listId .'-'. get_the_ID(); ?>" class="qc-feature-container qc-grid-item qc-sbd-single-item-11 qcpd-list-column <?php echo $style;?> <?php echo "opd-list-id-" . get_the_ID(); ?>">
              	
				<?php
					$item_count_disp = "";

					if( $item_count == "on" ){
						$item_count_disp = count($lists);
					}
				?>
				<?php if( $hide_list_title != 'true' ){ ?>
					<h2 <?php echo (isset($conf['list_title_color'])&&$conf['list_title_color']!=''?'style="color:'.$conf['list_title_color'].';"':''); ?>>
						<?php
							if(isset($conf['title_link']) && $conf['title_link']!=''):
								echo '<a href="'.$conf['title_link'].'" '.(isset($conf['title_link_new_tab'])&&$conf['title_link_new_tab']==1?'target="_blank"':'').' >';
							endif;
							?>
							<?php echo esc_html(get_the_title()); ?>
							<?php
								if($item_count == 'on'){
									echo '<span class="opd-item-count">('.$item_count_disp.')</span>';
								}
							?>
							<?php 
							if(isset($conf['title_link']) && $conf['title_link']!=''):
								echo '</a>';
							endif;
						?>
					</h2>
				<?php } ?>
				<ul id="jp-list-<?php echo get_the_ID(); ?><?php echo (isset($cattabid)&&$cattabid!=''?'-'.$cattabid:''); ?>">
                    <?php

						if( $item_orderby == 'upvotes' )
						{
    						$lists = sbd_filter_by_upvote($lists);
						}

						if( $item_orderby == 'title' )
						{
    						usort($lists, "pd_custom_sort_by_tpl_title");
						}

						if( $item_orderby == 'timestamp' )
						{
							usort($lists, "pd_custom_sort_by_tpl_timestamp");
						}

						if( $item_orderby == 'random' )
						{
							shuffle( $lists );
						}
						if(pd_ot_get_option('sbd_featured_item_top')=='on'){
							$lists = sbd_featured_at_top($lists);
						}
						$count = 1;

						foreach( $lists as $list ) :
						$all_location[] = $list;
						
						$tooltip_content = '';

						if( $tooltip === 'true' ){
							$tooltip_content = ' data-tooltip="'.$list['qcpd_item_subtitle'].'" data-tooltip-stickto="top" data-tooltip-color="#000" data-tooltip-animate-function="scalein"';
						}
						//print_r($list);exit;
						$latlon = '';
						if(isset($list['qcpd_item_latitude']) && $list['qcpd_item_latitude']!='' && isset($list['qcpd_item_longitude']) && $list['qcpd_item_longitude']!=''){
							$latlon = $list['qcpd_item_latitude'].','.$list['qcpd_item_longitude'];
						}
						$marker = '';
						if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
							$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
						}
					?>
                  
                  <li class="opt-column-0<?php echo $column; ?>" id="item-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $tooltip_content; ?> data-latlon="<?php echo $latlon; ?>" data-title="<?php echo $list['qcpd_item_title']; ?>" data-subtitle="<?php echo $list['qcpd_item_subtitle']; ?>" data-phone="<?php echo $list['qcpd_item_phone']; ?>" data-address="<?php echo $list['qcpd_item_full_address']; ?>" data-url="<?php echo $list['qcpd_item_link']; ?>" <?php echo $marker; ?> data-local="<?php echo $list['qcpd_item_location']; ?>" data-businesshour="<?php echo isset($list['qcpd_item_business_hour'])?$list['qcpd_item_business_hour']:''; ?>" data-paid="<?php echo (isset($list['qcpd_paid'])?$list['qcpd_paid']:''); ?>">
						<?php
							$item_url = $list['qcpd_item_link'];
							$masked_url = $list['qcpd_item_link'];
							$popContent = '';

							$mask_url = isset($mask_url) ? $mask_url : 'off';
							if( $mask_url == 'on' ){
								$masked_url = 'http://' . qcpd_get_domain($list['qcpd_item_link']);
							}
							$hover_info = 'title="'.$go_to_website_text.'"';
							if($main_click_action=='0'){
								$masked_url = 'tel:'.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']);
								$hover_info = 'title="Call '.$list['qcpd_item_phone'].'"';
							}
							if($main_click_action=='2'){
								$masked_url = '#';
								$popContent = 'class="open-mpf-sld-link" data-post-id="'.get_the_ID().'" data-item-title="'.trim($list['qcpd_item_title']).'" data-item-link="'.$list['qcpd_item_link'].'" data-mfp-src="#busi-'.get_the_ID() ."-". $count.'"';
								$hover_info = pd_ot_get_option('sbd_lan_open_popup') ? 'title="'.pd_ot_get_option('sbd_lan_open_popup').'"' : 'title="Open Popup"';
							}
						?>
						
						<?php 
											
							global $wp_query;
							//constructing new url for multipage//
							$newurl = home_url();
							if(isset($wp_query->query_vars['pagename']) and $wp_query->query_vars['pagename']!=''){
								$newurl = $newurl.'/'.$wp_query->query_vars['pagename'];
							}
							if(isset($wp_query->query_vars['sbdcat']) and $wp_query->query_vars['sbdcat']!=''){
								$newurl = $newurl.'/'.$wp_query->query_vars['sbdcat'];
							}
							if(isset($wp_query->query_vars['sbdlist']) and $wp_query->query_vars['sbdlist']!=''){
								$newurl = $newurl.'/'.$wp_query->query_vars['sbdlist'];
							}else{
								$newurl = $newurl.'/'.get_post(get_the_ID())->post_name;
							}
							
							
							if(isset($list['qcpd_item_title']) && $list['qcpd_item_title']!=''){
								$newurl = $newurl.'/'.urlencode(str_replace(' ','-',strtolower($list['qcpd_item_title'])));
							}
							
							if(isset($list['qcpd_timelaps']) && $list['qcpd_timelaps']!=''){
								$newurl = $newurl.'/'.trim($list['qcpd_timelaps']);
							}
							$item_url = $masked_url = $newurl;
							$othersetting = '';
							if(pd_ot_get_option('sbd_multi_same_window')=='off'){
								if(isset($list['qcpd_item_nofollow']) && $list['qcpd_item_nofollow'] == 1){
									$othersetting .= ' rel="nofollow"';
								}
								if(isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1){
									$othersetting .=' target="_blank"';
								}
							}
							
							if(pd_ot_get_option('sbd_lan_visit_page')!=''){
								$visit_page = pd_ot_get_option('sbd_lan_visit_page');
							}else{
								$visit_page = __('Visit Page','qc-pd');
							}
						?>
						
						
                        <a <?php if( $mask_url == 'on') { echo 'onclick="document.location.href = \''.$item_url.'\'; return false;"'; } ?> <?php echo (isset($main_click_action)&&$main_click_action==3?'':'href="'.$masked_url.'"'); ?>" <?php echo $othersetting; ?>
							 data-tag="<?php echo (isset($list['qcpd_tags'])?$list['qcpd_tags']:'' ); ?>" <?php echo $popContent; ?> <?php echo $hover_info; ?> <?php echo (isset($list['qcpd_item_newtab']) && $list['qcpd_item_newtab'] == 1?'data-newtab="1"':''); ?> >
                        <div class="qc-sbd-main">
                          <div class="qc-feature-media image">
                          	<?php
								$iconClass = (isset($list['qcpd_fa_icon']) && trim($list['qcpd_fa_icon']) != "") ? $list['qcpd_fa_icon'] : "";

								$showFavicon = (isset($list['qcpd_use_favicon']) && trim($list['qcpd_use_favicon']) != "") ? $list['qcpd_use_favicon'] : "";

								$faviconImgUrl = "";
								$faviconFetchable = false;
								$filteredUrl = "";

								$directImgLink = (isset($list['qcpd_item_img_link']) && trim($list['qcpd_item_img_link']) != "") ? $list['qcpd_item_img_link'] : "";

								if( $showFavicon == 1 )
								{
									$filteredUrl = qcpd_remove_http( $item_url );

									if( $item_url != '' )
									{

										$faviconImgUrl = 'https://www.google.com/s2/favicons?domain=' . $filteredUrl;
									}

									if( $directImgLink != '' )
									{

										$faviconImgUrl = trim($directImgLink);
									}

									$faviconFetchable = true;

									if( $item_url == '' && $directImgLink == '' ){
										$faviconFetchable = false;
									}
								}

							?>

							<!-- Image, If Present -->
							<?php if( ($list_img == "true") && isset($list['qcpd_item_img'])  && $list['qcpd_item_img'] != "" ) : ?>
								<?php 
									if (strpos($list['qcpd_item_img'], 'http') === FALSE){
								?>
								
									<?php
										$img = wp_get_attachment_image_src($list['qcpd_item_img'], 'medium_large');
										
										
									?>
									<img src="<?php echo $img[0]; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								
								<?php
									}else{
								?>
								
									<img src="<?php echo $list['qcpd_item_img']; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								
								<?php
									}
								?>

							<?php elseif( $iconClass != "" ) : ?>
							
								
									<i class="fa <?php echo $iconClass; ?> sbd_f_icon"></i>
								
							<?php elseif( $showFavicon == 1 && $faviconFetchable == true ) : ?>
								
									<img src="<?php echo $faviconImgUrl; ?>" alt="<?php echo $list['qcpd_item_title']; ?>">
								
							<?php else : ?>
								
									<img src="<?php echo QCSBD_IMG_URL; ?>/list-image-placeholder.png" alt="<?php echo $list['qcpd_item_title']; ?>">
								
							<?php endif; ?>
							
                            <div class="upvote-section">
								<?php 
								$bookmark = 0;
								if(isset($list['qcpd_is_bookmarked']) and $list['qcpd_is_bookmarked']!=''){
									$unv = explode(',',$list['qcpd_is_bookmarked']);
									if(in_array(get_current_user_id(),$unv) && get_current_user_id()!=0){
										$bookmark = 1;
									}
								}
								
								?>
								<?php if(pd_ot_get_option('pd_enable_bookmark')=='on'): ?>
                                <span data-post-id="<?php echo get_the_ID(); ?>" data-item-code="<?php echo trim($list['qcpd_timelaps']); ?>" data-is-bookmarked="<?php echo ($bookmark); ?>" class="bookmark-btn bookmark-on">
									
									<i class="fa <?php echo ($bookmark==1?'fa-star':'fa-star-o'); ?>" aria-hidden="true"></i>
								</span>
								<?php endif; ?>
								
								<?php if( $upvote == 'on' ) : ?>
                                <div class="favourite">
                                    <span data-post-id="<?php echo get_the_ID(); ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" class="sbd-upvote-btn upvote-on">
									<i class="fa <?php echo $pd_thumbs_up; ?>"></i>
									</span>
									<span class="upvote-count">
										<?php
										  if( isset($list['qcpd_upvote_count']) && (int)$list['qcpd_upvote_count'] > 0 ){
											echo (int)$list['qcpd_upvote_count'];
										  }
										?>
									</span>
                                </div>
								<?php endif; ?>
                            </div> 
                          </div>
                          <div class="qc-sbd-content">
                            <h4 class="sbd-title"><?php echo $list['qcpd_item_title']; ?></h4>
							<p class="sub-title"><?php echo $list['qcpd_item_subtitle']; ?>
                            <span class="designation"><?php echo $list['qcpd_item_location']; ?> &nbsp;&nbsp;
							<?php echo ($phone_number=='1'?'<span class="sbd_phone"><i class="fa fa-phone"></i> '.str_replace(array('(',')'),array('',''),$list['qcpd_item_phone']).'</span>':''); ?>
							</span>
                            <div class="sbd_custom_content">
							<?php
								include(QCSBD_DIR_MOD.'/template-common/qc_custom_body_content.php');
							?>
							</div>
							</p>
							
                          </div>
                          
                          <div class="clear"></div>
                          </div>
						<?php if(isset($list['qcpd_new']) and $list['qcpd_new']==1):?>
						<!-- new icon section -->
						<div class="new-icon-section">
							<span>new</span>
						</div>
						<!-- /new icon section -->
						<?php endif; ?>

						<?php if(sbd_display_claim_badge($list)): ?>
							<!-- new icon section -->
							<div class="new-icon-section sbd-claim-badge">
								<span>claimed</span>
							</div>
							<!-- /new icon section -->
							<?php endif; ?>
						
						
						<?php if(isset($list['qcpd_featured']) and $list['qcpd_featured']==1):?>
						<!-- featured section -->
						<div class="featured-section">
							<i class="fa fa-bolt"></i>
						</div>
						<!-- /featured section -->
						<?php endif; ?>
                      </a>
					  
                      <div class="bottom-area-icon pd-bottom-area">
					  
								<?php if($newurl!=''): ?>
								<p><a class="sld_internal_link" href="<?php echo $newurl; ?>" title="Go to link details page"><i class="fa fa-external-link-square fa-external-link-square-alt" aria-hidden="true"></i></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_phone']) and $list['qcpd_item_phone']!='' and $phone_number!=0): ?>
									<p><a href="tel:<?php echo preg_replace("/[^0-9]/", "",$list['qcpd_item_phone']); ?>" title="Call <?php echo $list['qcpd_item_phone']; ?>"><?php sbd_icons_content('phone'); ?></a></p>
								<?php endif; ?>
								
								<?php if($list['qcpd_item_link']!=''): ?>
									<p><a href="<?php echo $list['qcpd_item_link']; ?>" target="_blank" title="<?php echo pd_ot_get_option('sbd_lan_go_to_website') ? pd_ot_get_option('sbd_lan_go_to_website') : 'Go to website'; ?>"><?php sbd_icons_content('link'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_facebook']) and $list['qcpd_item_facebook']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_facebook']) && $list['qcpd_item_facebook']!=''?trim($list['qcpd_item_facebook']):'#'); ?>"><?php sbd_icons_content('facebook'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_yelp']) and $list['qcpd_item_yelp']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_yelp']) && $list['qcpd_item_yelp']!=''?trim($list['qcpd_item_yelp']):'#'); ?>"><?php sbd_icons_content('yelp'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_email']) and $list['qcpd_item_email']!=''): ?>
								<p><a data-email="<?php echo (isset($list['qcpd_item_email']) && $list['qcpd_item_email']!=''?trim($list['qcpd_item_email']):'#'); ?>" class="sbd_email_form" href="#"><?php sbd_icons_content('email'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_linkedin']) and $list['qcpd_item_linkedin']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_linkedin']) && $list['qcpd_item_linkedin']!=''?trim($list['qcpd_item_linkedin']):'#'); ?>"><?php sbd_icons_content('linkedin',trim($list['qcpd_item_linkedin'])); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_twitter']) and $list['qcpd_item_twitter']!=''): ?>
								<p><a target="_blank" href="<?php echo (isset($list['qcpd_item_twitter']) && $list['qcpd_item_twitter']!=''?trim($list['qcpd_item_twitter']):'#'); ?>"><?php sbd_icons_content('twitter'); ?></a></p>
								<?php endif; ?>
								
								<?php if(isset($list['qcpd_item_full_address']) and $list['qcpd_item_full_address']!='' and $pdmapofflightbox!='true'): 
								$marker = '';
								if(isset($list['qcpd_item_marker']) && !empty($list['qcpd_item_marker']) ){
									$marker = 'data-marker="'.wp_get_attachment_image_src($list['qcpd_item_marker'])[0].'"';
								}
								?>
								<p><a href="#" class="pd-map open-mpf-sld-link" full-address="<?php echo (isset($list['qcpd_item_full_address']) && $list['qcpd_item_full_address']!=''?trim($list['qcpd_item_full_address']):''); ?>" data-mfp-src="#map-<?php echo get_the_ID() ."-". $count; ?>" <?php echo $marker; ?>><?php sbd_icons_content('map'); ?></a></p>
								<?php endif; ?>

								
								<?php if(isset($list['qcpd_description']) and $list['qcpd_description']!=''  && pd_ot_get_option('sbd_i_icon')=='on'): ?>
								<p><a href="#" class="open-mpf-sld-link" data-post-id="<?php echo get_the_ID(); ?>" data-item-title="<?php echo trim($list['qcpd_item_title']); ?>" data-item-link="<?php echo $list['qcpd_item_link']; ?>" data-mfp-src="#busi-<?php echo get_the_ID() ."-". $count; ?>"><?php sbd_icons_content('info'); ?></a></p>
								<?php endif; ?>
								<?php
								include(QCSBD_DIR_MOD.'/template-common/qc_custom_icon_content.php');
							?>
													
                      </div>
							<div id="map-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide"><a class="sbd_direction_btn" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo @urlencode(@$list['qcpd_item_full_address']); ?>&travelmode=driving" target="_blank"><?php echo (pd_ot_get_option('sbd_lan_get_direction')!=''?pd_ot_get_option('sbd_lan_get_direction'):'Get Direction'); ?></a>
								<div class="pd_map_container" id="mapcontainer-<?php echo get_the_ID() ."-". $count; ?>"></div>
							</div>
				
							<div id="busi-<?php echo get_the_ID() ."-". $count; ?>" class="white-popup mfp-hide">
								<div class="sbd_business_container">
									<span class="qc_sbd_loading_details">Loading...</span>
								</div>
							</div>
                  </li>
				  
				  <?php $count++; endforeach; ?>

				  <?php if( $addvertiseContent != '' ) : ?>
					<li class=" opt-column-0<?php echo $column; ?>" >
						<div class="qc-sbd-main">
							<div class="qc-sbd-content">
								<?php echo apply_filters('the_content',$addvertiseContent); ?>
							</div>
						</div>
					</li>
					<?php endif; ?>
				  
                  </ul>

                   	<div class="clear"></div>
				<?php
					if( $actual_pagination == 'true' && $total_pagination_page > 1 ){
						echo sld_pagination_links( $sld_paged, $total_pagination_page, get_the_ID());
					}
				?>
        </div>
            
			
	

<?php 
}
?>
		</div>
		<?php if($map=='show' && $map_position=='right'){ ?>
			<div class="map-right-side" id="sbd_map_sidebar">
				<?php echo '<div id="sbd_all_location'.($cattabid!=''?$cattabid:'').'" class="sbd_map" style="height:100%" ></div>'; ?>
			</div>
		<?php } ?>
          
          <div class="clear"></div>
		
		</div>
		</section>
<?php
	echo '<div class="pd-clearfix"></div>
			</div>
		<div class="pd-clearfix"></div>
	</div>';

	//Hook - After Main List
	do_action( 'qcpd_after_main_list', $shortcodeAtts);

}

?>
<script type="text/javascript">

	jQuery(document).ready(function($){

	$('.qc-sld-masonary-15').packery({
		itemSelector: '.qc-sld-masonary-child-selector'
			 
	});
	
	
});

</script>
<script>
var login_url_pd = '<?php echo pd_ot_get_option('pd_bookmark_user_login_url'); ?>';
var template = '<?php echo $style; ?>';
var bookmark = {
	<?php 
	if ( is_user_logged_in() ) {
	?>
	is_user_logged_in:true,
	<?php
	} else {
	?>
	is_user_logged_in:false,
	<?php
	}
	?>
	userid: <?php echo get_current_user_id(); ?>

};
	jQuery(document).ready(function($){

		$( '.filter-btn[data-filter="all"]' ).on( "click", function() {
	  		//Masonary Grid
		    $('.qc-grid').packery({
		      itemSelector: '.qc-grid-item',
		      gutter: 10
		    });
		});

		<?php if( isset($cattabid) && ($cattabid!='') ){ ?>
			<?php if($cattabid == 0){ ?>
				$( '.filter-btn[data-filter="all"]' ).trigger( "click" );
			<?php } ?>
		<?php }else{ ?>
			$( '.filter-btn[data-filter="all"]' ).trigger( "click" );
		<?php } ?>
		
		$('.qc-sbd-content').each(function(){
			var obj = $(this);
			if(obj[0].scrollHeight > 180){
				obj.parent().addClass('pd_content_overflow_active');
			}
		})
		
		

	});
	

<?php if($map=='show' && $map_position=='right'){ ?>	
jQuery(document).ready(function($) {
	
	var StickyS = new StickySidebar('#sbd_map_sidebar', {
			topSpacing: 20,
			bottomSpacing: 20,
			containerSelector: '.qc-sbd-wrapper',
			innerWrapperSelector: '#sbd_all_location<?php echo ($cattabid!=''?$cattabid:''); ?>'
		});

	var windowWidth = jQuery(window).width();
	if (windowWidth < 991) {
		StickyS.destroy();
	}
		
	jQuery(window).resize(function() {
		var windowWidth = jQuery(window).width();
		if (windowWidth < 991) {
			StickyS.destroy();
		}
	});

})
<?php } ?>
</script>

