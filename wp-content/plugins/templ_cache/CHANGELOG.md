
# Change Log
All notable changes to Templ Cache will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [1.3.0] - 2023-01-17

### Added

- CDN Enabler extension: possibility to update hostname using WP-CLI.

### Fixed

- Made automatic purging of cache when new content is published the default setting.

### Removed

- Experimental WP Rocket integration.


## [1.2.3] - 2022-11-30
 
### Added

- Added support for WP's "Site Health" page cache detection, to enable detection of Templ Cache.


## [1.2.2] - 2022-05-30
 
### Fixed

- Removed a purge cache hook that caused the cache to be purged every time a page built with Elementor page was visited.
