��          �      �       0  :   1     l     �  �   �          .     :     F     O     U  �   a     3  ~  E  ;   �             �   (     �     �     �     �     �     �  �        �           
                   	                            Automatically remove all cached pages when content changes Cache could not be purged.  Cache purged. Helper plugin for the Templ server cache. Purges the cache automatically when content changes. Also includes a manual purge button. Purge Automatically Purge Cache Purge cache Settings Templ Templ Cache The Templ Cache plugin is activated automatically when needed by the server. If enabled, you may see the the need to manually purge the cache at times. If there are any questions, please contact Templ support. https://templ.io/ Project-Id-Version: Templ Cache 1.0.4
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/templ_cache
PO-Revision-Date: 2023-01-17 16:17+0100
Last-Translator: 
Language-Team: 
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
X-Domain: templio-cache
 Rensa automatiskt alla cachade sidor när innehåll ändras Cache kunde inte rensas.  Cache rensad. Hjälpplugin för Templs server-cache. Rensar cache automatiskt när innehållet på sidan ändras. Inkluderar också en knapp för manuell rensning. Automatisk rensning Rensa cache Rensa cache Inställningar Templ Templ Cache Detta plugin aktiveras automatiskt av servern vid behov. Om det är aktiverat kan du ibland behöva rensa cache manuellt. Om du har några frågor, tveka inte på att höra av dig till Templs support. https://templ.io/ 